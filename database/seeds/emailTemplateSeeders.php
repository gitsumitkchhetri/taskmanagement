<?php
use Illuminate\Database\Seeder;
class emailTemplateSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('email_templates')->insert([
        'title'=>'login-credential',
        'email_subject'=>'Credential For Login',
        'email_body'=> '<table border="0" cellpadding="0" cellspacing="0" class="body">
          <tr>
            <td>&nbsp;</td>
            <td class="container">
              <div class="content">

                <!-- START CENTERED WHITE CONTAINER -->
                <span class="preheader">An account has been created in Comlink App.</span>
                <table class="main">

                  <!-- START MAIN CONTENT AREA -->
                  <tr>
                    <td class="wrapper">
                      <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td>
                            <p>Dear %name%,</p>
                            <p>An account has been created in comlink app, your login details are:<br>Role : %role% <br>Username : %email% <br>Password : %password% <br><br>Regards,<br>Comlink App</p>
                            <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                              <tbody>
                                <tr>
                                  <td align="left">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td> %url% </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <p>
                            if you are having problem with logging then copy and paste this login url:
                              %rawurl%
                            </p>
                            <p>If you think this link was send without your consent please contact admin of Comlink System!</p>
                            <p>Thank you!</p>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>

                  <!-- END MAIN CONTENT AREA -->
                  </table>

                <!-- START FOOTER -->
                <div class="footer">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="content-block">
                        <span class="apple-link">Comlink Inc, 3 Abbey Road, Florida 94102</span>

                      </td>
                    </tr>
                    <tr>
                      <td class="content-block powered-by">
                        Powered by <a href="#">Comlink</a>.
                      </td>
                    </tr>
                  </table>
                </div>

                <!-- END FOOTER -->

    <!-- END CENTERED WHITE CONTAINER --></div>
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>'
      ]);



    }
}
