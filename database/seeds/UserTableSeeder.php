<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {

      if(DB::table('users')->where('id',1)->where('username','admin')->first()==null){
        DB::table('users')->insert([

            [
              'id' => 1,
              'first_name'=>'Ekbana',
                'last_name'=>'Ekbana',
                'status'=>true,
              'username'=>'admin',
              'email'=>'info@ekbana.com',
              'password'=> bcrypt('123admin@'),
            ]
        ]);
      }
    }
}
