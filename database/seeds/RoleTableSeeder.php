<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    public function run()
    {

        //Superuser
        if(DB::table('roles')->where('role_id',config('rolesconfig.roles.superuser.role_id'))->orWhere('name','superuser')->first()==null){
          DB::table('roles')->insert([
            [
                'id'   => 1,
                'name' => config('rolesconfig.roles.superuser.name'),
                'role_id' => config('rolesconfig.roles.superuser.role_id')
            ]
          ]);
        }else{
            DB::table('roles')->where('role_id',config('rolesconfig.roles.superuser.role_id'))->orWhere('name','superuser')->update(
              [
                'role_id'=>config('rolesconfig.roles.superuser.role_id')
              ]
            );
        }

        //Role User
        if(DB::table('role_user')->where('user_id',1)->where('role_id',1)->first()==null){
          DB::table('role_user')->insert([
              [
                  'user_id' => 1,
                  'role_id' => 1,
              ]
          ]);
        }
    }
}
