<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefCostOncsProjectsTechnicians extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_project_technicians', 'cost'))
        {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->integer('cost')->default(0)->change();

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_project_technicians', 'cost')) {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->integer('cost')->change();

            });
        }
    }
}
