<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefValOfClientIdCrmDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('crm_deals', 'client_id'))
        {
            Schema::table('crm_deals', function (Blueprint $table) {
                $table->unsignedInteger('client_id')->default(0)->change();


            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('crm_deals', 'client_id')) {
            Schema::table('crm_deals', function (Blueprint $table) {
                $table->unsignedInteger('client_id')->default(0)->change();
            });
        }
    }
}
