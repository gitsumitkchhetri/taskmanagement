<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_deals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->string('title');
            $table->string('description');
            $table->unsignedInteger('sales_person');
            $table->timestamps();

            $table->foreign('client_id')
                ->references('id')
                ->on('crm_clients')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('sales_person')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crm_deals');
    }
}
