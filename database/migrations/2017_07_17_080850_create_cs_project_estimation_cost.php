<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsProjectEstimationCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_projects_estimation_cost', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('task_id');
            $table->integer('total_cost');
            $table->integer('cost_changes');
            $table->unsignedInteger('project_id');

            $table->timestamps();

            $table->foreign('task_id')
                ->references('id')
                ->on('cs_tasks')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('cs_projects')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_projects_estimation_cost');
    }
}
