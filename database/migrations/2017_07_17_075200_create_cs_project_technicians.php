<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsProjectTechnicians extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_project_technicians', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('task_id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('technician_id');
            $table->integer('total_hour');
            $table->integer('cost');
            $table->date('date');
            $table->text('description');

//            $table->foreign('task_id')
//                ->references('id')
//                ->on('cs_tasks')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');
//
//            $table->foreign('project_id')
//                ->references('id')
//                ->on('cs_projects')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');
//
//            $table->foreign('technician_id')
//                ->references('id')
//                ->on('cs_project_technicians')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_project_technicians');
    }
}
