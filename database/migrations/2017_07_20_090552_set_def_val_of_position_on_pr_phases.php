<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefValOfPositionOnPrPhases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_project_phases', 'position'))
        {
            Schema::table('cs_project_phases', function (Blueprint $table) {
                $table->integer('position')->default(0)->change();

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_project_phases', 'position')) {
            Schema::table('cs_project_phases', function (Blueprint $table) {
                $table->integer('position')->change();

            });
        }
    }
}
