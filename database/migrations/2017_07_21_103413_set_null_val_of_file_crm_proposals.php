<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullValOfFileCrmProposals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('crm_proposals', 'file'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->unsignedInteger('file')->nullable()->change();


            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('crm_proposals', 'file')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->unsignedInteger('file')->notNullable()->change();
            });
        }
    }
}
