<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeFieldsOfCsProjectEquipmentNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cs_project_equipments', function (Blueprint $table) {

            $table->string('title')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->text('cost')->default(0)->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cs_project_equipments', function (Blueprint $table) {

            $table->string('title')->change();
            $table->text('description')->change();
            $table->text('cost')->change();


        });
    }
}
