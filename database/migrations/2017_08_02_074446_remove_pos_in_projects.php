<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePosInProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_projects', 'position'))
        {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->dropColumn('position');

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('cs_projects', 'position')) {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->integer('position');

            });
        }
    }
}
