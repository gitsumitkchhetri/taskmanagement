<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefValOfPositionOnPrTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_project_tasks', 'position'))
        {
            Schema::table('cs_project_tasks', function (Blueprint $table) {
                $table->integer('position')->default(0)->change();

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_project_tasks', 'position')) {
            Schema::table('cs_project_tasks', function (Blueprint $table) {
                $table->integer('position')->change();

            });
        }
    }
}
