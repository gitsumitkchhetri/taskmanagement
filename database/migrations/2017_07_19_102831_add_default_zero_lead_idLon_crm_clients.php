<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultZeroLeadIdLonCrmClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('crm_clients', 'lead_id'))
        {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->unsignedInteger('lead_id')->default(0)->change();

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('crm_clients', 'lead_id')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->unsignedInteger('lead_id')->change();

            });
        }
    }
}
