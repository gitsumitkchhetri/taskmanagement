<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTechnicianIdOnCsTimesheet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cs_timesheet', 'technician_id'))
        {
            Schema::table('cs_timesheet', function (Blueprint $table) {
                $table->unsignedInteger('technician_id');

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_timesheet', 'technician_id')) {
            Schema::table('cs_timesheet', function (Blueprint $table) {
                $table->dropColumn('technician_id');

            });
        }
    }
}
