<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsOnCrmProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cs_projects', 'job_types'))
        {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->string('job_types')->nullable();

            });
        }

        if (!Schema::hasColumn('cs_projects', 'sub_systems'))
        {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->longText('sub_systems')->nullable();
            });
        }

        if (!Schema::hasColumn('cs_projects', 'project_type'))
        {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->string('project_type')->nullable();

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_projects', 'job_types')) {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->dropColumn('job_types');

            });
        }

        if (Schema::hasColumn('cs_projects', 'sub_systems')) {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->dropColumn('sub_systems');

            });
        }

        if (Schema::hasColumn('cs_projects', 'project_type')) {
            Schema::table('cs_projects', function (Blueprint $table) {
                $table->dropColumn('project_type');

            });
        }
    }
}
