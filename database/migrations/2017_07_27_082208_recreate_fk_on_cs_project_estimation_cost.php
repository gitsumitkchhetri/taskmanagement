<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateFkOnCsProjectEstimationCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_projects_estimation_cost', 'task_id')) {
            Schema::table('cs_projects_estimation_cost', function (Blueprint $table) {
                $table->dropForeign(['task_id']);
            });

            Schema::table('cs_projects_estimation_cost', function (Blueprint $table) {
                $table->foreign('task_id')
                    ->references('id')
                    ->on('cs_tasks')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_projects_estimation_cost', 'task_id')) {
            Schema::table('cs_projects_estimation_cost', function (Blueprint $table) {
                $table->foreign('task_id')
                    ->references('id')
                    ->on('cs_tasks')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        }

    }
}
