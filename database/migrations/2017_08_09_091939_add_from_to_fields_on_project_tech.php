<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFromToFieldsOnProjectTech extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cs_project_technicians', 'from_date'))
        {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->dateTime('from_date')->nullable();

            });

        }

        if (!Schema::hasColumn('cs_project_technicians', 'to_date'))
        {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->dateTime('to_date')->nullable();

            });

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_project_technicians', 'from_date')) {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->dropColumn('from_date');

            });
        }

        if (Schema::hasColumn('cs_project_technicians', 'to_date')) {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->dropColumn('to_date');

            });
        }
    }
}
