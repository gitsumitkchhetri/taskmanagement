<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableFieldOncsProjectsTechnicians extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_project_technicians', 'total_hour'))
        {


            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->integer('total_hour')->nullable()->change();

            });
        }

        if (Schema::hasColumn('cs_project_technicians', 'date')) {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->date('date')->nullable()->change();

            });
        }
        if (Schema::hasColumn('cs_project_technicians', 'description')) {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->text('description')->nullable()->change();

            });
        }

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_project_technicians', 'total_hour'))
        {


            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->integer('total_hour')->change();

            });
        }

        if (Schema::hasColumn('cs_project_technicians', 'date')) {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->date('date')->change();

            });
        }
        if (Schema::hasColumn('cs_project_technicians', 'description')) {
            Schema::table('cs_project_technicians', function (Blueprint $table) {
                $table->text('description')->change();

            });
        }
    }
}
