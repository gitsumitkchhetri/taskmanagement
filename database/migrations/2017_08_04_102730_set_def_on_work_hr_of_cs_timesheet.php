<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefOnWorkHrOfCsTimesheet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_timesheet', 'work_hr'))
        {
            Schema::table('cs_timesheet', function (Blueprint $table) {
                $table->unsignedInteger('work_hr')->default(0)->change();


            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_timesheet', 'work_hr')) {
            Schema::table('cs_timesheet', function (Blueprint $table) {
                $table->unsignedInteger('work_hr')->default(0)->change();
            });
        }
    }
}
