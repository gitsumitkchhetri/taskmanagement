<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmProposals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('deal_id');
            $table->string('proposal_url');
            $table->string('portal_number');
            $table->string('file');
            $table->string('status');
            $table->string('loss_remarks');
            $table->integer('cost');
            $table->date('expected_close_date');
            $table->timestamps();

            $table->foreign('deal_id')
                ->references('id')
                ->on('crm_deals')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crm_proposals');
    }
}
