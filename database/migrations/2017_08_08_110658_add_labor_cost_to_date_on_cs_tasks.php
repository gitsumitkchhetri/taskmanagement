<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLaborCostToDateOnCsTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('cs_tasks', 'labor_cost_to_date'))
        {
            Schema::table('cs_tasks', function (Blueprint $table) {
                $table->bigInteger('labor_cost_to_date')->default(0);

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_tasks', 'labor_cost_to_date')) {
            Schema::table('cs_tasks', function (Blueprint $table) {
                $table->dropColumn('labor_cost_to_date');

            });
        }
    }
}
