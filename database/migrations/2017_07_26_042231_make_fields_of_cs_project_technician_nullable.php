<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeFieldsOfCsProjectTechnicianNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cs_project_technicians', function (Blueprint $table) {

            $table->string('total_hour')->nullable()->change();
            $table->date('date')->nullable()->change();
            $table->text('cost')->default(0)->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cs_project_technicians', function (Blueprint $table) {

            $table->string('total_hour')->change();
            $table->date('date')->change();
            $table->text('cost')->change();


        });
    }
}
