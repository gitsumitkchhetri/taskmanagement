<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsPhasesPerProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_project_per_phases', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('project_id');

            $table->string('title');
            $table->integer('position');
            $table->text('description');

            $table->timestamps();

            $table->foreign('project_id')
                ->references('id')
                ->on('cs_projects')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_project_per_phases');
    }
}
