<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropForeignKeyContraintOnCrmClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('crm_clients', 'lead_id')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->dropForeign(['lead_id']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('crm_clients', 'lead_id')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->foreign('lead_id')
                    ->references('id')
                    ->on('crm_leads')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
        }

    }
}
