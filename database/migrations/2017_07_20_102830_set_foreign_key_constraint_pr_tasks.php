<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetForeignKeyConstraintPrTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cs_project_tasks', 'phase_id'))
        {
            Schema::table('cs_project_tasks', function (Blueprint $table) {
                $table->unsignedInteger('phase_id')->change();


            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('cs_project_tasks', 'phase_id')) {
            Schema::table('cs_project_tasks', function (Blueprint $table) {
                $table->integer('phase_id')->change();
            });
        }
    }
}
