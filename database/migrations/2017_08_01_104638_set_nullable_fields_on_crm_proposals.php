<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableFieldsOnCrmProposals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('crm_proposals', 'proposal_url'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('proposal_url')->nullable()->change();


            });
        }

        if (Schema::hasColumn('crm_proposals', 'portal_number'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('portal_number')->nullable()->change();


            });
        }

        if (Schema::hasColumn('crm_proposals', 'file'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('file')->nullable()->change();


            });
        }

        if (Schema::hasColumn('crm_proposals', 'status'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->string('status')->nullable()->change();


            });
        }

        if (Schema::hasColumn('crm_proposals', 'loss_remarks'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('loss_remarks')->nullable()->change();


            });
        }

        if (Schema::hasColumn('crm_proposals', 'cost'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->integer('cost')->nullable()->change();


            });
        }

        if (Schema::hasColumn('crm_proposals', 'expected_close_date'))
        {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('expected_close_date')->nullable()->change();


            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('crm_proposals', 'proposal_url')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('proposal_url')->notNullable()->change();
            });
        }

        if (Schema::hasColumn('crm_proposals', 'portal_number')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('portal_number')->notNullable()->change();
            });
        }

        if (Schema::hasColumn('crm_proposals', 'file')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('file')->notNullable()->change();
            });
        }

        if (Schema::hasColumn('crm_proposals', 'status')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->string('status')->notNullable()->change();
            });
        }

        if (Schema::hasColumn('crm_proposals', 'loss_remarks')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('loss_remarks')->notNullable()->change();
            });
        }

        if (Schema::hasColumn('crm_proposals', 'cost')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->integer('cost')->notNullable()->change();
            });
        }

        if (Schema::hasColumn('crm_proposals', 'expected_close_date')) {
            Schema::table('crm_proposals', function (Blueprint $table) {
                $table->text('expected_close_date')->notNullable()->change();
            });
        }
    }
}
