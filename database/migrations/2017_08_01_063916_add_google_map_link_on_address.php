<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoogleMapLinkOnAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('crm_addresses', 'google_map_link'))
        {
            Schema::table('crm_addresses', function (Blueprint $table) {
                $table->text('google_map_link')->nullable();

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('crm_addresses', 'google_map_link')) {
            Schema::table('crm_addresses', function (Blueprint $table) {
                $table->dropColumn('google_map_link');

            });
        }
    }
}
