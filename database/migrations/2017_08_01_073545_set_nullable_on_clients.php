<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableOnClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('crm_clients', 'first_name'))
        {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('first_name')->nullable()->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'last_name'))
        {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('last_name')->nullable()->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'last_name'))
        {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('last_name')->nullable()->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'company_name'))
        {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('company_name')->nullable()->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'phone'))
        {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('phone')->nullable()->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'email'))
        {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('email')->nullable()->change();

            });
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('crm_clients', 'first_name')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('first_name')->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'last_name')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('last_name')->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'company_name')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('company_name')->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'phone')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('phone')->change();

            });
        }

        if (Schema::hasColumn('crm_clients', 'email')) {
            Schema::table('crm_clients', function (Blueprint $table) {
                $table->text('email')->change();

            });
        }
    }
}
