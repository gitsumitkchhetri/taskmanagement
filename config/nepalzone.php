<?php
return [
    'developmentregionfields' => [
        'region'
    ],
    'fields' => [
        'gid',
        'name'
    ],
    'districtfields' => [
        'gid',
        'name',
        'zone_id'
    ],
    'townfields' => [
        'id',
        'district_id',
        'created_by',
        'updated_by',
        'name',
        'created_on',
        'updated_on',
        'zone_id'
    ],
    'channelfields' => [
        'id',
        'name'
    ],
    'categoryfields' => [
        'id',
        'channel_id',
        'created_by',
        'updated_by',
        'name',
        'created_on',
        'updated_on',
        'map_color'
    ],
    'retailoutletfields' => [
        'longitude',
        'latitude',
        'id',

    ],
    'retailoutlet' => [
        'category_id',
        'name',
        'address',
        'owner_name',
        'owner_phone',
        'pan_no',
        'phone',
        'longitude',
        'latitude',
        'v_longitude',
        'v_latitude',
        'sku_id',
        'store_size',
        'channel_id',
        'street_id',
        'zone_id',
        'district_id',
        'development_region_id',
        'catalog_id',
        'monthly_avg_business',
        'id',
        'town_id'

    ],

];