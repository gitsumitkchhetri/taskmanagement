<?php
  //Roles defined here like contributors , journalist , admin
  return [
    'job_types'     => [
        'New Construction Residential'         => 'New Construction Residential',
        'New Construction Commercial'         => 'New Construction Commercial',
        'Retrofit Residential'         => 'Retrofit Residential',
        'Retrofit Commercial'         => 'Retrofit Commercial',
        'Remodel Residential'         => 'Remodel Residential'
    ],
      'sub_systems'     => [
          'Security'         => 'Security',
          'Surveillance'         => 'Surveillance',
          'Structured Wiring'         => 'Structured Wiring',
          'Wifi Everywhere'         => 'Wifi Everywhere',
          'Multiroom Audio'         => 'Multiroom Audio',
          'Multiroom Video'         => 'Multiroom Video',
          'Access Control'         => 'Access Control',
          'Central Vacuum'         => 'Central Vacuum',
          'Home Theater'         => 'Home Theater',
          'Home Automation'         => 'Home Automation',
      ],
      'project_type'     => [
          'Multi Phase'         => 'Multi Phase',
          'Single Phase'         => 'Single Phase'
      ]
  ];
?>
