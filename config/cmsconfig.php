<?php
return [
    //List of modules are defined here
    'modules'     => [
        'home'    => "Admin Dashboard",
//        'config'  => 'Admin Config',
        'user'    => "User Management",
        'leads'    => "Leads Management",
        'clients'    => "Clients Management",
        'configs'    => "Config",
//        'tech_levels'    => "Tech Levels",
        'technicians'    => "Technicians",
        'timesheet'    => "Timesheet",
        //'dispatch'    => "Dispatch",
//        'project_phases'    => "Project Phases",
        'projects'    => "Projects",
        'deals'    => "Deals Management",
        'log'     => 'Log Management',
//        'templates'     => 'Page Templates'
    ],
    //Module sub modules contains here
    'modulepages' =>  [
        'home'    =>  ['home' => 'Admin Dashboard'],
//        'config'  =>  ['settings' => 'Settings','language'=>'Language'],
//        'user'    =>  ['permission' => 'Permissions','roles'=>'Users Group','users'=>'Users'],
        'user'    =>  ['role'=>'Roles','user'=>'Users'],
        'leads'    =>  ['leads'    => "Leads Management"],
        'clients'    =>  ['clients'    => "Clients Management"],
        'configs'    =>  ['tech_levels'    => "Tech Levels",'project_phases'    => "Project Phases"],
        //'tech_levels'    =>  ['tech_levels'    => "Tech Levels"],
        'technicians'    =>  ['technicians'    => "Technicians"],
        'timesheet'    =>  ['timesheet'    => "Timesheet"],
        'dispatch'    =>  ['dispatch'    => "Dispatch"],
        //'project_phases'    =>  ['project_phases'    => "Project Phases"],
        'projects'    =>  ['projects'    => "Projects"],
        'deals'    =>  ['deals'    => "Deals Management"],
        'log'     =>  ['log' => 'Log Management','LogViewer' => 'Log Tracker'],
//        'templates'     =>  ['list' => 'List View','form'=>'Form View']
    ],
    //Permissions for each module is defined here
    'modulepermissions' => [
//        'config'   => [
//                        //Settings Sub Module
//                        'config.settings.view'     =>  'View Settings',
//                        'config.settings.create'   =>  'Create Settings',
//                        'config.settings.edit'     =>  'Edit Settings',
//                        'config.settings.delete'   =>  'Delete Settings',
//                        //Language Sub Module
//                        'config.language.view'     =>  'View Language',
//                        'config.language.create'   =>  'Create Language',
//                        'config.language.edit'     =>  'Edit Language',
//                        'config.language.delete'   =>  'Delete Language'
//                      ],
        'user'   => [
            //Roles Sub Module
            'user.role.index'        =>  'View Roles',
            'user.role.create'      =>  'Create Roles',
            'user.role.edit'        =>  'Edit Roles',
            'user.role.delete'      =>  'Delete Roles',
            //Users Sub Module
            'user.user.index'        =>  'View Users',
            'user.user.create'      =>  'Create Users',
            'user.user.edit'        =>  'Edit Users',
            'user.user.destroy'      =>  'Delete Users',
            'password.password.change_password'        =>  'Change Personal Password',
        ],
        'log'      => [
            'log.log.index'          =>  'View Log',
            'log.log.destroy'        =>  'Destroy Log',
            'log.LogViewer.index' => 'Cms log tracker view'
        ],
        'configs'      => [
            //tech_level Sub Module
            'configs.tech_levels.index'          =>  'View Tech Level',
            'configs.tech_levels.destroy'        =>  'Destroy Tech Level',
            'configs.tech_levels.create'         => 'Create Tech Level',
            'configs.tech_levels.edit'           => 'Edit Tech Level',

            //project_phases Sub Module
            'configs.project_phases.index'          =>  'View project Phase',
            'configs.project_phases.destroy'        =>  'Destroy project Phase',
            'configs.project_phases.create'         => 'Create project Phase',
            'configs.project_phases.edit'           => 'Edit Project Phase'

        ],
        'leads'      => [
            'leads.leads.index'          =>  'Lead View',
            'leads.leads.destroy'        =>  'Lead destroy',
            'leads.leads.create'      =>  'Create Lead',
            'leads.leads.edit'        =>  'Edit Lead',
        ],
//        'tech_levels'      => [
//            'tech_levels.tech_levels.index'          =>  'Tech Level View',
//            'tech_levels.tech_levels.destroy'        =>  'Tech Level destroy',
//            'tech_levels.tech_levels.create'      =>  'Tech Level Create Roles',
//            'tech_levels.tech_levels.edit'        =>  'Tech Level Edit Roles',
//        ],
        'clients'      => [
            'clients.clients.index'          =>  'Clients View',
            'clients.clients.destroy'        =>  'Clients destroy',
            'clients.clients.create'      =>  'Clients Create Roles',
            'clients.clients.edit'        =>  'Clients Edit Roles',
        ],
//        'project_phases'      => [
//            'project_phases.project_phases.index'          =>  'Project Phases View',
//            'project_phases.project_phases.destroy'        =>  'Project Phases destroy',
//            'project_phases.project_phases.create'      =>  'Project Phases Create Roles',
//            'project_phases.project_phases.edit'        =>  'Project Phases Edit Roles',
//        ],
        'project_tasks'      => [
            'project_tasks.project_tasks.index'          =>  'Project Tasks View',
            'project_tasks.project_tasks.destroy'        =>  'Project Tasks destroy',
            'project_tasks.project_tasks.create'      =>  'Project Tasks Create Roles',
            'project_tasks.project_tasks.edit'        =>  'Project Tasks Edit Roles',
        ],
        'addresses'      => [
            'addresses.addresses.index'          =>  'Addresses View',
            'addresses.addresses.destroy'        =>  'Destroy Addresses',
            'addresses.addresses.create'      =>  'Create Addresses',
            'addresses.addresses.edit'        =>  'Edit Addresses',
        ],
        'deals'      => [
            'deals.deals.index'          =>  'Deals View',
            'deals.deals.destroy'        =>  'Deals destroy',
            'deals.deals.create'      =>  'Deals Create Roles',
            'deals.deals.edit'        =>  'Deals Edit Roles',
        ],
        'projects'      => [
            'projects.projects.index'          =>  'Projects View',
            'projects.projects.destroy'        =>  'Projects destroy',
            'projects.projects.create'      =>  'Projects Create Roles',
            'projects.projects.edit'        =>  'Projects Edit Roles',
        ],
        'phases'      => [
            'phases.phases.index'          =>  'Phases View',
            'phases.phases.destroy'        =>  'Phases destroy',
            'phases.phases.create'      =>  'Phases Create Roles',
            'phases.phases.edit'        =>  'Phases Edit Roles',
        ],
        'timesheet'      => [
            'timesheet.timesheet.index'          =>  'Timesheet View',
            'timesheet.timesheet.destroy'        =>  'Timesheet destroy',
            'timesheet.timesheet.create'      =>  'Timesheet Create Roles',
            'timesheet.timesheet.edit'        =>  'Timesheet Edit Roles',
        ],
        'dispatch'      => [
            'dispatch.dispatch.index'          =>  'Dispatch View',
            'dispatch.dispatch.destroy'        =>  'Dispatch destroy',
            'dispatch.dispatch.create'      =>  'Dispatch Create Roles',
            'dispatch.dispatch.edit'        =>  'Dispatch Edit Roles',
        ],

    ],
    //Permissions for each module is defined here
    //Icons for eash modules is defined here
    'moduleicons' =>  [
        'home'    => 'fa-home',
        'user'    => 'fa-users',
        'config'  => 'fa-gear',
        'configs'  => 'fa-cogs',
        'log'     => 'fa-file-o',
        'tech_levels'     => 'fa-microchip',
        'project_phases'     => 'fa-bars',
        'technicians'     => 'fa-wrench',
        'projects'     => 'fa-bars',
        'deals'     => 'fa-certificate',
        'timesheet'     => 'fa-calendar',
        'dispatch'     => 'fa-truck',
//        'templates'     => 'icon-file-o'
    ],
    //Icons for eash modules is defined here
    'cmsTitle'    => 'Ekcms', //Cms Title
    'logotitle'   => 'Ekbana' // Logo title
];


?>
