@extends('backend.master')
@section('title',trans($pageTitle))
@section('content')



    <div id="page-title">
        <h2 style="display:inline-block">{{$pageTitle}}</h2>
        {{--<div class="right" style="float:right">--}}
        {{--@if(Auth::user()->canDo('clients.clients.create'))--}}
        {{--<a class="btn btn-primary" href="{{URL::to(PREFIX.'/projects/create')}}"><i class="glyph-icon icon-plus" style="margin-right:10px;"></i>Add New</a>--}}
        {{--@endif--}}
        {{--</div>--}}
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li class="active"><a href="Javascript::void();" >{{$pageTitle}}</a></li>
        </ol>
    </div>

    @include('errors.errors')

    <div class="panel">
        <div class="panel-body">
            @if(isset($prevWeekFrom))
            <div class="form-group pull-left">
                <div class="col-sm-12">
                    <a href="{{URL::route('timesheet.index',["prevWeekFrom" => $prevWeekFrom,"project_id" => Input::get('project_id')])}}" class="btn btn-primary"><i class="fa fa-backward"></i> Prev</a>

                </div>
            </div>
            @endif
            @if(isset($nextWeekFrom))
            <div class="form-group pull-right">
                <div class="col-sm-12">
                    <a href="{{URL::route('timesheet.index',["nextWeekFrom" => $nextWeekFrom,"project_id" => Input::get('project_id')])}}" class="btn btn-primary">Next <i class="fa fa-forward"></i></a>

                </div>
            </div>
            @endif

            <div class="clearfix"></div>
            {!!Form::open(['method'=>'GET','url'=>'', 'class'=>'form-horizontal'])!!}
            <div class="form-group">

            <div class="col-sm-3"></div>
            <div class="col-md-4"></div>
            <label class="col-sm-2 control-label">Project:</label>
            <div class="col-md-3">
            <div class="input-group">
            {!! Form::select("project_id",$projectList,Input::get("project_id"),["class" => "form-control"]) !!}
            {{--<input type="text" class="form-control" name="keywords" value="{{Input::get('keywords')}}" autocomplete="off">--}}
            <span class="input-group-btn">
            {{--<button class="btn btn-primary" type="submit">Go !</button>--}}
            </span>

            </div>
            </div>
            </div>
            {!!Form::close() !!}




@if(!empty($phasePerProjectData))
        {!!Form::open(['method'=>'POST','url'=>PREFIX.'/timesheet/save', 'class'=>'form-horizontal'])!!}
            <div class="example-box-wrapper">
                <div class="scroll-columns">
                    {{--{{dd($phasePerProjectData[$key])}}--}}
                    <div class="panel panel-default">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <table class="table table-bordered cf" data-toggle="dataTable" data-form="deleteForm">
                                <thead class="cf">
                                <tr>
                                    <th rowspan="2" style="vertical-align: middle;">Code</th>
                                    <th rowspan="2" style="vertical-align: middle;">Title</th>
                                    @foreach($range as $r)
                                        <th>{{$r}}</th>
                                    @endforeach
                                </tr>
                                <tr>

                                    @foreach($range as $r)
                                        <th>{{date("l",strtotime($r))}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($phasePerProjectData as $key=>$phase)
                                    @php $i = 0; @endphp
                                    @if(isset($phasePerProjectData[$key]["task"]))
                                        @php ++$i;$j = 0; @endphp
                                        @foreach($phasePerProjectData[$key]["task"] as $tskKey => $tsk)
                                            @if(isset($tsk["code"]))
                                                @php ++$j @endphp
                                                @if($j == 1)
                                                    <tr><td colspan="1000" style="color:#000;"><b>{{$phase["attributes"]["title"]}}</b></td></tr>
                                                @endif
                                                <tr>
                                                    <td>{{$tsk["code"]}}</td>
                                                    <td colspan="1000" style="color:#000;"><b>{{$tsk["title"]}}</b></td>
                                                </tr>
                                                @php
                                                    $work_hour = 0;
                                                @endphp
                                                @foreach($technicians as $technician)
                                                    @if(isset($tsk["labors"][$technician->id]))
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            {{$technician->name}}
                                                        </td>
                                                        @foreach($range as $rge)
                                                            {{--<input type="hidden" name="timesheet[{{Input::get('project_id')}}][phase]" value="{{$key}}">--}}
                                                            {{--<input type="hidden" name="timesheet[{{Input::get('project_id')}}][phase][{{$key}}][task]" value="{{$tskKey}}">--}}
                                                            {{--<input type="hidden" name="timesheet[{{Input::get('project_id')}}][phase][{{$key}}][task][{{$tskKey}}][technician]" value="{{$technician->id}}">--}}
                                                            {{--<input type="hidden" name="timesheet[{{Input::get('project_id')}}][phase][{{$key}}][task][{{$tskKey}}][technician][{{$technician->id}}][date]" value="{{$rge}}">--}}
                                                            {{--<input type="hidden" name="timesheet[{{Input::get('project_id')}}][phase][{{$key}}][task][{{$tskKey}}][technician][{{$technician->id}}][date][{{$rge}}]" value="{{$rge}}">--}}
                                                            <td>
                                                                {!! Form::number("timesheet[".Input::get('project_id')."][".$key."][".$tskKey."][".$technician->id."][".$rge."]",$tsk["labors"][$technician->id][$rge],["class" => "form-control"]) !!}
                                                                {{--<input type="text" class="form-control" name="timesheet[{{Input::get('project_id')}}][phase][{{$key}}][task][{{$tskKey}}][technician][{{$technician->id}}][date][{{$rge}}]" value="{{$tsk["labors"][$technician->id][$rge]}}">--}}
                                                            </td>
                                                        @endforeach




                                                    </tr>
                                                    @endif
                                                @endforeach

                                                {{--<tr>--}}

                                                    {{--{!! Form::hidden("type",$tsk["type"]) !!}--}}
                                                    {{--{!! Form::hidden("project_id",Input::get("project_id")) !!}--}}
                                                    {{--{!! Form::hidden("task_id",$tskKey) !!}--}}
                                                    {{--<td>{{$tsk["code"]}}</td>--}}
                                                    {{--<td>{{$tsk["title"]}}</td>--}}

                                                    {{--<td>{!! Form::text("") !!}</td>--}}

                                                    {{--<td></td>--}}


                                                    {{--<td></td>--}}


                                                    {{--<td></td>--}}

                                                    {{--<td></td>--}}


                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}

                                                    {{--<td>--}}
                                                        {{--<a class="btn btn-primary btn-sm" href="{{URL::route('phases.create',["project_id" => Input::get("project_id"),"task_id" =>$tskKey,"phase_id" => $key])}}" > Update </a>--}}
                                                        {{--<a class="btn btn-danger btn-sm" href="{{URL::route('phases.destroy',["id" =>$tskKey])}}" > Delete </a>--}}
                                                        {{--@if($tsk["type"] == "Equipment")--}}
                                                            {{--<a class="btn btn-info btn-sm" href="{{URL::route('project_equipments.index',["project_id" => Input::get("project_id"),"task_id" =>$tskKey])}}" > {{$tsk["type"]}} </a>--}}
                                                        {{--@endif--}}

                                                        {{--@if($tsk["type"] == "Labor")--}}
                                                            {{--<a class="btn btn-info btn-sm" href="{{URL::route('project_technicians.index',["project_id" => Input::get("project_id"),"task_id" =>$tskKey])}}" > {{$tsk["type"]}} </a>--}}
                                                        {{--@endif--}}

                                                    {{--</td>--}}


                                                {{--</tr>--}}
                                                @endif
                                        @endforeach

                                    @else
                                        <tr>

                                            <td class="no-data" colspan="1000">
                                                <b>No data to display!</b>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr><td colspan="1000"></td></tr>

                                </tbody>
                            </table>


                            <div class="form-group col-lg-6 ">
                                    <button type="submit" class="btn btn-primary" value="save" name="submit">Save</button>

                                    <button type="submit" class="btn btn-primary" value="authenticate" name="submit">Authenticate</button>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
            </div>
                {!!Form::close() !!}
@elseif(Input::has('project_id'))
                    No Labors are assigned in this Project.
@endif
            {{--@if(!$data->isEmpty())--}}
            {{--<div class="pagination-tile"> <label class="pagination-sub">Showing {{($data->currentpage()-1)*$data->perpage()+1}} to {{(($data->currentpage()-1)*$data->perpage())+$data->count()}} of {{$data->total()}} entries</label>--}}
            {{--<ul class="pagination" style="float:right ; margin:0px;">--}}
            {{--{!! str_replace('/?', '?',$data->appends(['keywords'=>Input::get('keywords')])->render()) !!}--}}

            {{--</ul>--}}
            {{--</div>--}}
            {{--@endif--}}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">

    <script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch.js')}}"></script>
    <script type="text/javascript">
        /* Input switch */

        $(function() { "use strict";
            $('.input-switch').bootstrapSwitch();
        });




    </script>

    <script>
        $("select[name= project_id]").change(function () {
            window.location = "{{URL::to(PREFIX.'/timesheet?project_id=')}}"+$(this).val();

        });
    </script>

@stop
