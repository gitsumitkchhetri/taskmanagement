@extends('backend.master')

@section('title', trans('Edit New Technician'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Technician</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/technicians')}}" >Technician</a></li>
            <li class="active"><a href="Javascript::void();" >Edit Technician</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($technician,['method'=>'PUT','route'=>[$routeName,$technician->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                @include('backend.technicians.includes.form',['btnTxt'=>'Update'])

            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>


@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/datepicker/datepicker.css')}}">
    <script type="text/javascript" src="{{URL::asset('backend/widgets/datepicker/datepicker.js')}}"></script>

    <script type="text/javascript">
        /* Datepicker bootstrap */

        $(function() { "use strict";
            $('.bootstrap-datepicker').bsdatepicker({
                format: 'yyyy-mm-dd'
            });
        });

    </script>

@stop
