@extends('backend.master')
@section('title',trans($pageTitle))
@section('content')



    <div id="page-title">
        <h2 style="display:inline-block">{{$pageTitle}}</h2>
        {{--<div class="right" style="float:right">--}}
        {{--@if(Auth::user()->canDo('clients.clients.create'))--}}
        {{--<a class="btn btn-primary" href="{{URL::to(PREFIX.'/projects/create')}}"><i class="glyph-icon icon-plus" style="margin-right:10px;"></i>Add New</a>--}}
        {{--@endif--}}
        {{--</div>--}}
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li class="active"><a href="Javascript::void();" >{{$pageTitle}}</a></li>
        </ol>
    </div>

    @include('errors.errors')

    <div class="panel">
        <div class="panel-body">
            {!!Form::open(['method'=>'GET','url'=>$formURL, 'class'=>'form-horizontal'])!!}
            {!!Form::open(['method'=>'GET','url'=>'', 'class'=>'form-horizontal'])!!}
            <div class="form-group">

            <div class="col-sm-3"></div>
            <div class="col-md-4"></div>
            <label class="col-sm-2 control-label">Project:</label>
            <div class="col-md-3">
            <div class="input-group">
            {!! Form::select("project_id",$projectList,Input::get("project_id"),["class" => "form-control"]) !!}
            {{--<input type="text" class="form-control" name="keywords" value="{{Input::get('keywords')}}" autocomplete="off">--}}
            <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">Go !</button>
            </span>

            </div>
            </div>
            </div>
            {!!Form::close() !!}





            <div class="example-box-wrapper">
                <div class="scroll-columns">
                    @foreach($phasePerProjectData as $key=>$phase)
                        {{--{{dd($phaseData[$key])}}--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">{{$phase["attributes"]["title"]}}</div>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped table-condensed cf" data-toggle="dataTable" data-form="deleteForm">
                                    <thead class="cf">
                                    <tr>
                                        <th>S.N.</th>
                                        <th>Title</th>
                                        <th>Position</th>
                                        <th>Budget</th>
                                        <th>Changes</th>
                                        <th>Revised Budget</th>
                                        <th>Cost To Date</th>
                                        <th>% of Budget</th>
                                        <th>Balance</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($phaseData[$key]["task"]))
                                        @foreach($phaseData[$key]["task"] as $tskKey => $tsk)
                                            {{--@php--}}
                                            {{--$total_cost = "";--}}
                                            {{--$cost_changes = "";--}}
                                            {{--$cost_to_date = "";--}}
                                            {{--if(isset($tsk["total_cost"])){--}}
                                            {{--$total_cost--}}
                                            {{--}--}}

                                            {{--if(isset($tsk["cost_changes"])){--}}

                                            {{--}--}}

                                            {{--if(isset($tsk["data"])){--}}

                                            {{--}--}}

                                            {{--@endphp--}}
                                            {!! Form::open(['route' => 'phases.store']) !!}
                                            <tr>
                                                {!! Form::hidden("type",$tsk["type"]) !!}
                                                {!! Form::hidden("project_id",$project_id) !!}
                                                {!! Form::hidden("task_id",$tskKey) !!}
                                                <td>{{$tsk["code"]}}</td>
                                                <td>{{$tsk["title"]}}</td>

                                                @if(isset($tsk["total_cost"]))
                                                    <td>{!! Form::text("budget",$tsk["total_cost"],["class"=>"form-control"]) !!}</td>

                                                @else
                                                    <td>{!! Form::text("budget","",["class"=>"form-control"]) !!}</td>

                                                @endif

                                                @if(isset($tsk["cost_changes"]))
                                                    <td>{!! Form::text("changes",$tsk["cost_changes"],["class"=>"form-control"]) !!}</td>
                                                @else
                                                    <td>{!! Form::text("changes","",["class"=>"form-control"]) !!}</td>

                                                @endif

                                                <td>$956</td>

                                                @if(isset($tsk["data"]))
                                                    <td>{!! Form::text("cost_to_date",$tsk["data"]["cost"],["class"=>"form-control"]) !!}</td>
                                                @else
                                                    <td>{!! Form::text("cost_to_date","",["class"=>"form-control"]) !!}</td>
                                                @endif

                                                <td>100%</td>
                                                <td>$0.00</td>
                                                <td>{{$tsk["type"]}}</td>
                                                <td>
                                                    <button class="btn btn-success" type="submit" > Save </button>

                                                </td>


                                            </tr>
                                            {!! Form::open() !!}
                                        @endforeach

                                    @else
                                        <tr>

                                            <td class="no-data" colspan="1000">
                                                <b>No data to display!</b>
                                            </td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            {{--@if(!$data->isEmpty())--}}
            {{--<div class="pagination-tile"> <label class="pagination-sub">Showing {{($data->currentpage()-1)*$data->perpage()+1}} to {{(($data->currentpage()-1)*$data->perpage())+$data->count()}} of {{$data->total()}} entries</label>--}}
            {{--<ul class="pagination" style="float:right ; margin:0px;">--}}
            {{--{!! str_replace('/?', '?',$data->appends(['keywords'=>Input::get('keywords')])->render()) !!}--}}

            {{--</ul>--}}
            {{--</div>--}}
            {{--@endif--}}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">

    <script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch.js')}}"></script>
    <script type="text/javascript">
        /* Input switch */

        $(function() { "use strict";
            $('.input-switch').bootstrapSwitch();
        });




    </script>

@stop
