<ul id="sidebar-menu">
  @foreach($modulesPermission as $modules)
    <?php if($modules['pages']>1) $url = "#";else $url = PREFIX."/".$modules['id'];?>

  <li>
      <a href="{{URL::to($url)}}" title="{{$modules['title']}}" >
          <i class="fa {{$modules['icon']}} fa-lg" style="margin-right: 5px;"></i>
          {{--<span>{{$modules['title']}}</span>--}}
          <span>{{trans($modules['title'] )}}</span>
      </a>
      @if($modules['pages']>1)
        <div class="sidebar-submenu">

            <ul>
                @foreach($modules['subPages'] as $pageId=>$pageTitle)
                  <?php $url = PREFIX."/".$pageId;?>

                  <li><a href="{{URL::to($url)}}" title="{{$pageTitle}}"><span>{{trans($pageTitle )}}</span></a></li>
                @endforeach
            </ul>

        </div><!-- .sidebar-submenu -->
      @endif
  </li>
  <li class="divider"></li>
  @endforeach

</ul><!-- #sidebar-menu -->

