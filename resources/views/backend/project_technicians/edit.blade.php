@extends('backend.master')

@section('title', trans('Edit Project Technician'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Project Technician</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li class="active"><a href="{{URL::to(PREFIX.'/projects')}}" >{{"Project Management"}}</a></li>
            <li><a href="{{ URL::route('projects.show',Input::get("project_id")) }}" >Project Details</a></li>
            <li><a href="{{URL::route('project_technicians.index',["project_id" => Input::get('project_id'),"task_id" => Input::get('task_id')])}}" >Project Technicians</a></li>
            <li class="active"><a href="Javascript::void();" >Edit Project Technician</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($project_technician,['method'=>'PUT','route'=>[$routeName,$project_technician->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
            {!! Form::hidden("project_id",Input::get('project_id')) !!}
            {!! Form::hidden("task_id",Input::get('task_id')) !!}
                @include('backend.project_technicians.includes.form',['btnTxt'=>'Update'])
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

@stop
