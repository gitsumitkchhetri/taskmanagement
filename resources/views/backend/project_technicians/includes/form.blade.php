<h4>Technicians Details</h4>
<div class="form-group">
    <label class="col-sm-3 control-label">Technician</label>
    <div class="col-sm-6">
        {!! Form::select("technician_id",$technician,null,["class" => "form-control","placeholder" => "Technician"]) !!}
    </div>
</div>


<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{ $btnTxt }}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?project_id='.Input::get("project_id").'&task_id='.Input::get("task_id"))}}">Cancel</a>

    </div></div>