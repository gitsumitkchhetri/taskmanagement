@extends('backend.master')

@section('title', trans('Add New Project Phase'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Add Project Phase</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
      <li><a href="javascript:void(0);">Config</a></li>
    <li><a href="{{URL::to(PREFIX.'/user')}}" >Project Phase</a></li>
    <li class="active"><a href="Javascript::void();" >Add Project Phase</a></li>
  </ol>
</div>

@include('errors/errors')
{!!Form::open(['method'=>'POST','url'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}

<div class="panel">
  <div class="panel-body">
      @include('backend.project_phases.includes.form')

      <div class="clearfix"></div>

    </div>
</div>

<div class="panel">
    <div class="panel-body">
        @include('backend.project_tasks.includes.form',['btnTxt'=>'Save'])
    </div>
</div>
{!!Form::close() !!}


@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
