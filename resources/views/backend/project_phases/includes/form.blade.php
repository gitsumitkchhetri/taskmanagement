<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Title</label>
    <div class="col-sm-6">
        {!! Form::text("title",null,["class" => "form-control","placeholder" => "Title"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>
    <div class="col-sm-6">
        {!! Form::textarea("description",null,["class" => "form-control","placeholder" => "Description"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Position</label>
    <div class="col-sm-6">
        {!! Form::number("position",null,["class" => "form-control","placeholder" => "Position"]) !!}
    </div>
</div>

