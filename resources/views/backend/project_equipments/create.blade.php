@extends('backend.master')

@section('title', trans('Add New Equipment'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Add Equipment</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
      <li class="active"><a href="{{URL::to(PREFIX.'/projects')}}" >{{"Project Management"}}</a></li>
      <li><a href="{{ URL::route('projects.show',Input::get("project_id")) }}" >Project Details</a></li>
      <li><a href="{{URL::route('project_equipments.index',["project_id" => Input::get('project_id'),"task_id" => Input::get('task_id')])}}" >Project Equipments</a></li>

      <li class="active"><a href="Javascript::void();" >Add Equipments</a></li>
  </ol>
</div>

@include('errors/errors')

{!!Form::open(['method'=>'POST','url'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
<div class="panel">
  <div class="panel-body">
      {!! Form::hidden("project_id",Input::get('project_id')) !!}
      {!! Form::hidden("task_id",Input::get('task_id')) !!}
      @include('backend.project_equipments.includes.form',['btnTxt'=>'Save'])


      <div class="clearfix"></div>

    </div>
</div>


{!!Form::close() !!}
@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
