<h4>Equipment Details</h4>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Cost</label>
    <div class="col-sm-6">
        {!! Form::text("cost",null,["class" => "form-control","placeholder" => "Cost"]) !!}
    </div>
</div>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Title</label>
    <div class="col-sm-6">
        {!! Form::text("title",null,["class" => "form-control","placeholder" => "Title"]) !!}
    </div>
</div>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Description</label>
    <div class="col-sm-6">
        {!! Form::textarea("description",null,["class" => "form-control","placeholder" => "Description"]) !!}
    </div>
</div>


<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{ $btnTxt }}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?project_id='.Input::get("project_id").'&task_id='.Input::get("task_id"))}}">Cancel</a>

    </div></div>