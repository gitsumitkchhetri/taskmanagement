@extends('backend.master')

@section('title', 'Edit Lead'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Lead</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/leads')}}" >{{$pageTitle}}</a></li>
            <li class="active"><a href="Javascript::void();" >Edit Lead</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($lead,['method'=>'PUT','route'=>[$routeName,$lead->id], 'class'=>'form-horizontal bordered-row'])!!}
            {!! Form::hidden("id",null) !!}
                @include('backend.leads.includes.form',['btnTxt'=>'Update'])
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

@stop
