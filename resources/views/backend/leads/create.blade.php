@extends('backend.master')

@section('title', trans('words.Add New Lead'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Add Lead</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
    <li><a href="{{URL::to(PREFIX.'/leads')}}" >{{$pageTitle}}</a></li>
    <li class="active"><a href="Javascript::void();" >Add Lead</a></li>
  </ol>
</div>

@include('errors/errors')

<div class="panel">
  <div class="panel-body">
    {!!Form::open(['method'=>'POST','url'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
      @include('backend.leads.includes.form',['btnTxt'=>'Save'])

      {!!Form::close() !!}
      <div class="clearfix"></div>

    </div>
</div>

@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
