<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Name</label>
    <div class="col-sm-6">
        {!! Form::text("name",null,["class" => "form-control","placeholder" => "Name"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Email</label>
    <div class="col-sm-6">
        {!! Form::text("email",null,["class" => "form-control","placeholder" => "Email"]) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Phone</label>
    <div class="col-sm-6">
        {!! Form::text("phone",null,["class" => "form-control","placeholder" => "Phone"]) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label require">Source</label>
    <div class="col-sm-6">
        {!! Form::select("source",$sources,null,["class" => "form-control","placeholder" => "Source"]) !!}
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label require">Status</label>
    <div class="col-sm-6">
        <label class="radio-inline">
            {{ Form::radio('status',"New",true) }}
            New
        </label>
        <label class="radio-inline">
            {{ Form::radio('status','Verified') }}
            Verified
        </label>
        <label class="radio-inline">
            {{ Form::radio('status',"Lost") }}
            Lost
        </label>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{ $btnTxt }}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL)}}">Cancel</a>

    </div></div>