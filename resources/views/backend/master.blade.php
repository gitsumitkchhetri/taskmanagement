<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> CounterStrike - @yield('title') </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/bootstrap/css/bootstrap.css')}}">


    <!-- HELPERS -->

   <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/helpers/animate.css')}}">
   <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/helpers/backgrounds.css')}}">
   <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/helpers/boilerplate.css')}}">
   <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/helpers/utils.css')}}">

    <link href="{{ URL::to('backend/css/select2.css') }}" rel="stylesheet" type="text/css" />
   <!-- for graph -')}}->

   <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/helpers/colors.css')}}">

    <!-- ELEMENTS -->

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/elements/badges.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/elements/content-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/elements/dashboard-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/elements/panel-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/elements/tile-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/elements/form.css')}}">


    <!-- ICONS -->

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/icons/fontawesome/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/icons/linecons/linecons.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">



    <!-- WIDGETS -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/charts/piegage/piegage.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/dropdown/dropdown.css')}}">

    <!-- SNIPPETS -->

    <!-- Admin theme -->

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/themes/admin/layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/themes/admin/color-schemes/default.css')}}">

    <!-- Components theme -->

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/themes/components/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/themes/components/border-radius.css')}}">

    <!-- Admin responsive -->

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/helpers/responsive-elements.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/helpers/admin-responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/themes/admin/bemita.css')}}">

    <!-- JS Core -->

    <script type="text/javascript" src="{{URL::asset('backend/js-core/jquery-core.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('backend/js-core/modernizr.js')}}"></script>



    <!-- Bootstrap DateTimePicker dependencies -->
    <script src="{{URL::asset('backend/datetimepicker/js/moment-with-locales.js')}}"></script>
    <script src="{{URL::asset('backend/datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

    <link rel="stylesheet" href="{{URL::asset('backend/datetimepicker/css/font-awesome.min.css')}}">

    <link href="{{URL::asset('backend/datetimepicker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
    <script>
        $(function () {
            $('.datepicker').datetimepicker({
                format : 'YYYY-MM-DD',
                minDate:new Date()
            });
            $('.timepicker').datetimepicker({
                format : 'hh:mm'
            });
//            $('.timepicker').datetimepicker({
//                format:'LT'
//            });
            $('.datetimepicker').datetimepicker({
                format : 'YYYY-MM-DD hh:mm',
                minDate:new Date()

            });
            $(".datetimepicker-today").datetimepicker({
                format : 'YYYY-MM-DD hh:mm',
                date: new Date()
            });
        });
    </script>


</head>
<body>
<div id="sb-site">
    <div id="page-wrapper">
        <div id="page-header" class="bg-gradient-9">
            <div id="mobile-navigation">
                <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar">
                    <span></span></button>
                <a href="{{URL::to(PREFIX.'/home')}}" class="logo-content-small" title="MonarchUI"></a>
            </div>
            <div id="header-logo" class="logo-bg">
                <a href="{{URL::to(PREFIX.'/home')}}" class="logo-content-big" title="MonarchUI">
                    Monarch <i>UI</i>
                    <span>The perfect solution for user interfaces</span>
                </a>
                <a href="{{URL::to(PREFIX.'/home')}}" class="logo-content-small" title="MonarchUI">
                    Monarch <i>UI</i>
                    <span>The perfect solution for user interfaces</span>
                </a>
                <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
                {{--<a href="{{URL::to(PREFIX.'/home')}}"  class="logo-content-small" title="CounterStrike">--}}
                    {{--Counter <i>Strike</i>--}}
                    {{--<span>The perfect solution for user interfaces</span>--}}
                {{--</a>--}}
            </div>

            <div id="header-nav-left" style="float: left;">
                <div class="user-account-btn dropdown">
                    {{--<a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">--}}
                        {{--<img width="28" src="http://localhost:8888/tsukasa/public/backend/image-resources/gravatar.jpg" alt="Profile image'">--}}
                        {{--<span>Ekbana Ekbana</span>--}}
                        {{--<i class="glyph-icon icon-angle-down"></i>--}}
                    {{--</a>--}}

                    <a href="#" class="user-profile selected-language clearfix" data-toggle="dropdown">
                        <img src="{{ asset(array_values(Config::get('languages')[App::getLocale()])[0]) }}">
                       <span> {{ (array_keys(Config::get('languages')[App::getLocale()])[0]) }}</span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>

                    <div class="dropdown-menu custom-dropdown float-left">
                        <div class="box-sm language-box">

                            <ul>
                                @foreach (Config::get('languages') as $lang => $language)

                                    @if ($lang != App::getLocale())
                                        <li>
                                            <img class="language-img" src="{{ asset(array_values($language)[0]) }}">  <a href="{{ route('lang.switch', $lang) }}">{{array_keys($language)[0]}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
            </div>


            <div id="header-nav-left">
                <div class="user-account-btn dropdown">
                    <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                        <img width="28" src="{{URL::asset('backend/image-resources/gravatar.jpg')}}" alt="Profile image'">
                        <span>{{ Auth::user()->first_name .' '. Auth::user()->last_name}}</span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>

                    <div class="dropdown-menu float-left">
                        <div class="box-sm">
                            <div class="login-box clearfix">
                                <div class="user-info">
                                    <span>

                                        {{ Auth::user()->first_name .' '. Auth::user()->last_name}}

                                    </span>
                                    @if(Auth::user()->canDo('password.password.change_password'))
                                    <a href="{{URL::to(PREFIX.'/user/change_password/'.Auth::user()->id)}}" title="Edit profile">@lang('words.Change Password')</a>
                                        @endif
                                </div>
                            </div>

                            <div class="pad5A button-pane button-pane-alt text-center">
                                <a href="{{URL::to('/system/logout')}}" class="btn display-block font-normal btn-danger">
                                    <i class="glyph-icon icon-power-off"></i>
                                    @lang('words.Logout')
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- #header-nav-right -->

        </div>
        <div id="page-sidebar">
            <div class="scroll-sidebar">

              @include('backend.sidebar')

            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content">
                <div style="
    background: rgba(255, 255, 255, 0.50)" class="modal fade in loaderImage">
                    {{--<img src="{{  asset('frontend/img/icons/preloader.GIF') }}" class="">--}}
                    <img style="position: absolute;
    left: 0;
    right: 0;
    margin: 15% auto; " src="{{  asset('backend/ajax-loader.gif') }}" class="">
                </div>




                <!-- confirm modal -->

                <div class="modal" id="confirm">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Delete Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure, you want to delete?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>



                  @yield('content')

                <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Confirm Submit
                            </div>
                            <div class="modal-body">
                                Are you sure, you want to delete?

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="#" id="submitDeleteForm" class="btn btn-success success">Submit</a>


                            </div>
        </div>
    </div>




    <!-- WIDGETS -->

    <script type="text/javascript" src="{{URL::asset('backend/bootstrap/js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('backend/widgets/superclick/superclick.js')}}"></script>

    <!-- Input switch alternate -->

    <script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch-alt.js')}}"></script>

    <!-- Slim scroll -->

    <script type="text/javascript" src="{{URL::asset('backend/widgets/slimscroll/slimscroll.js')}}"></script>

    <!-- Slidebars -->

    <script type="text/javascript" src="{{URL::asset('backend/widgets/slidebars/slidebars.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('backend/widgets/slidebars/slidebars-demo.js')}}"></script>

    <!-- PieGage -->

    {{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/piegage/piegage.js')}}"></script>--}}
    {{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/piegage/piegage-demo.js')}}"></script>--}}

    <!-- Screenfull -->

    <script type="text/javascript" src="{{URL::asset('backend/widgets/screenfull/screenfull.js')}}"></script>



    <!-- Widgets init for demo -->

    <script type="text/javascript" src="{{URL::asset('backend/js-init/widgets-init.js')}}"></script>

    <!-- Theme layout -->



    <script type="text/javascript" src="{{URL::asset('backend/themes/admin/layout.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('backend/js/custom.js')}}"></script>
    <script src="{{ URL::to('backend/js/select2.min.js') }}" type="text/javascript"></script>

    <script>

        @if(!empty(Input::get('errorlevel')))
        $('#merge-dialog').modal('show');
        localStorage.setItem('isModalOpen', true);
        @endif


    $(document).ready(function() {
            $('.span11').select2({
                delimiter: ',',
                persist: false,
                create: function(input) {
                    return {
                        value: input,
                        text: input
                    }
                }
            });

            $('.span12').select2({
//                maxItems: 3,
                delimiter: ',',
                persist: false,
                create: function(input) {
                    return {
                        value: input,
                        text: input
                    }
                }
            });

            if (!!window.performance && window.performance.navigation.type === 2) {
                // value 2 means "The page was accessed by navigating into the history"
                console.log('Reloading');
                window.location.reload(); // reload whole page

            }

      $('a[data-confirm]').click(function(ev) {
        var href = $(this).attr('href');
        if (!$('#dataConfirmModal').length) {
          $('body').append('<div id="dataConfirmModal"  class="modal modal-attr "  role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
        }
        $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
        $('#dataConfirmOK').attr('href', href);
        $('#dataConfirmModal').modal({show:true});
        return false;
      });


    });
    </script>

</div>

  @yield('scripts')

</body>
</html>
