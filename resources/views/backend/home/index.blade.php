@extends('backend.master')
@section('title', trans('words.DASHBOARD'))

@section('content')
    {!! Charts::assets() !!}

<div id="page-title">
    <h2>@lang('words.DASHBOARD')</h2>
    <p>The most complete user interface framework that can be used to create stunning admin
        dashboards and presentation websites.</p>
</div>
@include('errors.errors')



@stop

@section('scripts')

<!-- Sparklines charts -->

{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/sparklines/sparklines.js')}}"></script>--}}
{{--<script type="text/javascript"--}}
        {{--src="{{URL::asset('backend/widgets/charts/sparklines/sparklines-demo.js')}}"></script>--}}

{{--<!-- Flot charts -->--}}

{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/flot/flot.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/flot/flot-resize.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/flot/flot-stack.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/flot/flot-pie.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/flot/flot-tooltip.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/charts/flot/flot-demo-1.js')}}"></script>--}}

@stop
