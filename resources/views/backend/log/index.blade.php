@extends('backend.master')
@section('title',trans('words.'.$pageTitle))
@section('content')

<div id="page-title">
    <h2 style="display:inline-block">@lang('words.'.$pageTitle)</h2>
    <div class="right" style="float:right">
    </div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">@lang('words.Home')</a></li>
    <li class="active"><a href="Javascript::void();" >@lang('words.'.$pageTitle)</a></li>
  </ol>
</div>

@include('errors.errors')

<div class="panel">
  <div class="panel-body">
    {!!Form::open(['method'=>'GET','url'=>PREFIX.'/log', 'class'=>'form-horizontal'])!!}
      <div class="form-group">

        <div class="col-sm-3"></div>
        <div class="col-md-4"></div>
        <label class="col-sm-2 control-label">@lang('words.Search')</label>
          <div class="col-md-3">
            <div class="input-group">

                <input type="text" class="form-control" name="keywords" value="{{Input::get('keywords')}}" autocomplete="off">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">@lang('words.Go')!</button>
                </span>

            </div>
          </div>
      </div>
    {!!Form::close() !!}





    <div class="example-box-wrapper">
      <div class="scroll-columns">
        <table class="table table-bordered table-striped table-condensed cf" data-toggle="dataTable" data-form="deleteForm">
          <thead class="cf">
            <tr>
                <th>S.N.</th>
                <th>@lang('words.User')</th>
                <th>@lang('words.Module')</th>
                <th>@lang('words.Log Message')</th>
                @if(Auth::user()->canDo('log.log.destroy'))
                <th>@lang('words.Action')</th>
                    @endif
            </tr>
          </thead>
          <tbody>
          @if($data->isEmpty())
              <tr>

                  <td class="no-data" colspan="5">
                      <b>@lang('words.No data to display!')</b>
                  </td>
              </tr>
          @else
          @php  $a=$data->perPage() * ($data->currentPage()-1); @endphp
            @foreach($data as $d)
                @php $a++ @endphp
            <tr>
                <td>{{ $a }}</td>
                <td>{{$d->getUserById($d->user_id)}}</td>
                <td>{{$d->module}}</td>
                <td>{{$d->data}}</td>
                @if(Auth::user()->canDo('log.log.destroy'))
                <td>
                    {!! Form::model($d, ['method' => 'delete', 'route' => ['log.destroy', $d->id], 'class' =>'form-inline form-delete']) !!}
                    {!! Form::hidden('id', $d->id) !!}
                    <button type="submit" name="delete_modal" class="btn btn-round btn-sm btn-danger" ><i class="glyphicon glyphicon-trash"></i> @lang('words.Delete') </button>
                    {!! Form::close() !!}


                </td>
                    @endif
            </tr>
            @endforeach
              @endif
          </tbody>
        </table>
      </div>
    </div>
      @if(!$data->isEmpty())
          <div class="pagination-tile"> <label class="pagination-sub">@lang('words.Showing') {{($data->currentpage()-1)*$data->perpage()+1}} @lang('words.to') {{(($data->currentpage()-1)*$data->perpage())+$data->count()}} @lang('words.of') {{$data->total()}} @lang('words.entries')</label>
              <ul class="pagination" style="float:right ; margin:0px;">
                  {!! str_replace('/?', '?',$data->appends(['keywords'=>Input::get('keywords')])->render()) !!}

              </ul>
          </div>
      @endif
      <div class="clearfix"></div>

</div>
</div>

@stop

@section('scripts')

<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">

<script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch.js')}}"></script>
<script type="text/javascript">
/* Input switch */

$(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
});




</script>

@stop
