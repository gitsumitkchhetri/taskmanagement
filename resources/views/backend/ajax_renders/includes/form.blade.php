<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">First Name</label>
    <div class="col-sm-6">
        {!! Form::text("first_name",null,["class" => "form-control","placeholder" => "First Name"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label require">Last Name</label>
    <div class="col-sm-6">
        {!! Form::text("last_name",null,["class" => "form-control","placeholder" => "Last Name"]) !!}
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Company Name</label>
    <div class="col-sm-6">
        {!! Form::text("company_name",null,["class" => "form-control","placeholder" => "Company Name"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label require">Email</label>
    <div class="col-sm-6">
        {!! Form::text("email",null,["class" => "form-control","placeholder" => "Email"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label require">Phone</label>
    <div class="col-sm-6">
        {!! Form::number("phone",null,["class" => "form-control","placeholder" => "Phone"]) !!}
    </div>
</div>





