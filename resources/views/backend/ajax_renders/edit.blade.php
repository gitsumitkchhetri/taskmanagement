@extends('backend.master')

@section('title', trans('Edit New Client'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Client</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/user')}}" >Client</a></li>
            <li class="active"><a href="Javascript::void();" >Add Client</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($clients,['method'=>'PUT','route'=>[$routeName,$clients->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                @include('backend.clients.includes.form',['btnTxt'=>'Update'])
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?client_id='.Input::get("client_id"))}}">Cancel</a>

                </div>
            </div>
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>


@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

@stop
