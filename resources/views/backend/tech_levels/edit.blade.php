@extends('backend.master')

@section('title', trans('Add New Tech Level'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Tech Level</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="javascript:void(0);">Config</a></li>
            <li><a href="{{URL::to(PREFIX.'/tech_levels')}}" >Tech Levels</a></li>
            <li class="active"><a href="Javascript::void();" >Edit Tech Level</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($techLvl,['method'=>'PUT','route'=>[$routeName,$techLvl->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                @include('backend.tech_levels.includes.form',['btnTxt'=>'Update'])
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

@stop
