@extends('backend.master')

@section('title', trans('Add New Proposal'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Proposal</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/deals')}}">Deals Management</a></li>
            <li><a href="{{URL::to(PREFIX.'/proposals?deal_id='.Input::get('deal_id'))}}" >Proposal</a></li>
            <li class="active"><a href="Javascript::void();" >Add Proposal</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($proposal,['method'=>'PUT','route'=>[$routeName,$proposal->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                {!! Form::hidden("deal_id",Input::get("deal_id")) !!}
                @include('backend.proposals.includes.form',['btnTxt'=>'Update'])
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/datepicker/datepicker.css')}}">
    <script type="text/javascript" src="{{URL::asset('backend/widgets/datepicker/datepicker.js')}}"></script>

    <script type="text/javascript">
        /* Datepicker bootstrap */

        $(function() { "use strict";
            $('.bootstrap-datepicker').bsdatepicker({
                format: 'yyyy-mm-dd'
            });
        });

    </script>

@stop
