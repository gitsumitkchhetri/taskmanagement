@extends('backend.master')

@section('title', trans('Add New Proposal'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Add Proposal</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
      <li><a href="{{URL::to(PREFIX.'/deals')}}">Deals Management</a></li>
    <li><a href="{{URL::to(PREFIX.'/proposals?deal_id='.Input::get('deal_id'))}}" >Proposals</a></li>
    <li class="active"><a href="Javascript::void();" >Add Proposal</a></li>
  </ol>
</div>

@include('errors/errors')

{!!Form::open(['method'=>'POST','url'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
<div class="panel">
  <div class="panel-body">
      {!! Form::hidden("deal_id",Input::get('deal_id')) !!}
      @include('backend.proposals.includes.form',['btnTxt'=>'Save'])


      <div class="clearfix"></div>

    </div>
</div>


{!!Form::close() !!}
@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/datepicker/datepicker.js')}}"></script>--}}
{{--<script type="text/javascript">--}}
    {{--/* Datepicker bootstrap */--}}

    {{--$(function() { "use strict";--}}
        {{--$('.bootstrap-datepicker').bsdatepicker({--}}
            {{--format: 'yyyy-mm-dd'--}}
        {{--});--}}
    {{--});--}}

{{--</script>--}}

{{--<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/datepicker/datepicker.css')}}">--}}
{{--<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">--}}
{{--<link rel="stylesheet" type="text/css" href="http://localhost:8888/ekcms-ver4/public/backend/widgets/input-switch/inputswitch.css">--}}
{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/datepicker/datepicker.js')}}"></script>--}}
{{--<script type="text/javascript">--}}
    {{--/* Input switch */--}}

    {{--$(function() { "use strict";--}}
        {{--$('.bootstrap-datepicker').bootstrapSwitch();--}}
    {{--});--}}
{{--</script>--}}
{{--<script type="text/javascript">--}}
    {{--/* Datepicker bootstrap */--}}

    {{--$(function() { "use strict";--}}
        {{--$('.bootstrap-datepicker').bsdatepicker({--}}
            {{--format: 'yyyy-mm-dd'--}}
        {{--});--}}
    {{--});--}}

{{--</script>--}}

@stop
