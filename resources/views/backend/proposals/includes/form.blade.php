<h4>Proposals Details</h4>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Proposal URL</label>
    <div class="col-sm-6">
        {!! Form::text("proposal_url",null,["class" => "form-control","placeholder" => "Proposal URL"]) !!}
    </div>
</div>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Portal Number</label>
    <div class="col-sm-6">
        {!! Form::text("portal_number",null,["class" => "form-control","placeholder" => "Portal Number"]) !!}
    </div>
</div>

@if(isset($proposal) || Request::get("status") == "Cancelled" || Input::old("status") ==  "Cancelled")
    @if($proposal->status == "Cancelled" || Request::get("status") == "Cancelled" || Input::old("status") ==  "Cancelled")
        <div class="form-group" style="border-top: 0px;" id="loss_remarks">
    @else
        <div class="form-group" style="border-top: 0px;display: none;" id="loss_remarks">
    @endif
@else
        <div class="form-group" style="border-top: 0px;display: none;" id="loss_remarks">
@endif
            <label class="col-sm-3 control-label">Cancellation Remarks</label>
            <div class="col-sm-6">
                {!! Form::textarea("loss_remarks",null,["class" => "form-control","placeholder" => "Cancellation Remarks"]) !!}
            </div>
        </div>

<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Cost</label>
    <div class="col-sm-6">
        {!! Form::text("cost",null,["class" => "form-control","placeholder" => "Cost"]) !!}
    </div>
</div>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Expected Close Date</label>
    <div class="col-sm-6">
        {!! Form::text("expected_close_date",null,["class" => "form-control datepicker","placeholder" => "Expected Close Date"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label require">Status</label>
    <div class="col-sm-6">
        <label class="radio-inline">
            {{ Form::radio('status',"Pending",true) }}
            Pending
        </label>
        <label class="radio-inline">
            {{ Form::radio('status','Success') }}
            Success
        </label>
        <label class="radio-inline">
            {{ Form::radio('status',"Cancelled") }}
            Cancelled
        </label>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{ $btnTxt }}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?deal_id='.Input::get("deal_id"))}}">Cancel</a>

    </div></div>

<script>
    $('input[type="radio"]').click(function(){
        if ($(this).is(':checked'))
        {
            if($(this).val() == "Cancelled"){
                $("#loss_remarks").show();
            }else{
                $("#loss_remarks").hide();
            }
        }
    });

</script>