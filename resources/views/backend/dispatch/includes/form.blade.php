<div class="form-group" id="client_id">
    <label class="col-sm-3 control-label" id = "technicians">Technicians</label>
    <div class="col-sm-6">

        @if(isset($tech))
            {!! Form::select("technician_id[]",$technicians,$tech,["class" => "form-control span12","placeholder" => "Technicians","multiple"=>""]) !!}
        @else
            {!! Form::select("technician_id[]",$technicians,[],["class" => "form-control span12","placeholder" => "Technicians","multiple"=>""]) !!}

        @endif
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label require">Project</label>
    <div class="col-sm-6">
        {!! Form::select("project_id",$projectsLists,null,["class" => "form-control","placeholder" => "Project"]) !!}
    </div>
</div>

@if(isset($labor_tasks))
    <div class="form-group">
        <label class="col-sm-3 control-label require">Tasks</label>
        <div class="col-sm-6">
            {!! Form::select("task_id",$labor_tasks,null,["class" => "form-control","placeholder" => "No Task Selected"]) !!}
        </div>
    </div>
@endif


<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">From</label>
    <div class="col-sm-6">
        {!! Form::text("from_date",null,["class" => "form-control datetimepicker","placeholder" => "From"]) !!}
    </div>
</div>

<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">To</label>
    <div class="col-sm-6">
        {!! Form::text("to_date",null,["class" => "form-control datetimepicker","placeholder" => "To"]) !!}
    </div>
</div>









<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{$btnTxt}}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL)}}">Cancel</a>

    </div>
</div>

<script>
    $("select[name= project_id]").change(function () {
        window.location = "{{URL::to(PREFIX.'/dispatch/create?project_id=')}}"+$(this).val();

    });
</script>




