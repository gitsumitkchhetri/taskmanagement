@extends('backend.master')

@section('title', trans('Add New Task'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Add Task</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
    <li><a href="{{URL::to(PREFIX.'/user')}}" >Tasks</a></li>
    <li class="active"><a href="Javascript::void();" >Add Tasks</a></li>
  </ol>
</div>

@include('errors/errors')

{!!Form::open(['method'=>'POST','url'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
<div class="panel">
  <div class="panel-body">
      {!! Form::hidden("phase_id",Input::get('phase_id')) !!}
      @include('backend.project_tasks.includes.form',['btnTxt'=>'Save'])


      <div class="clearfix"></div>

    </div>
</div>


{!!Form::close() !!}
@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
