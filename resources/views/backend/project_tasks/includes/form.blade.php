<h4>Project Tasks Details</h4>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Title</label>
    <div class="col-sm-6">
        @if(Input::has("phase_id"))
            {!! Form::text("title",null,["class" => "form-control","placeholder" => "Title"]) !!}
        @else
            {!! Form::text("task_title",null,["class" => "form-control","placeholder" => "Title"]) !!}
        @endif
    </div>
</div>


<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Type</label>
    <div class="col-sm-6">
        {!! Form::select("type",$tasksType,null,["class" => "form-control","placeholder" => "Type"]) !!}
    </div>
</div>

<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Position</label>
    <div class="col-sm-6">
        @if(Input::has("phase_id"))
            {!! Form::text("position",null,["class" => "form-control","placeholder" => "Position"]) !!}
        @else
            {!! Form::text("task_position",null,["class" => "form-control","placeholder" => "Position"]) !!}
        @endif
    </div>
</div>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Code</label>
    <div class="col-sm-6">
        {!! Form::text("code",null,["class" => "form-control","placeholder" => "Code"]) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{ $btnTxt }}</button>

        @if(Input::has("phase_id"))
            <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?phase_id='.Input::get("phase_id"))}}">Cancel</a>
        @else
            <a class="btn btn-warning" href="{{URL::to($redirectBackURL)}}">Cancel</a>
        @endif

    </div></div>