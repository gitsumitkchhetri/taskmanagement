@extends('backend.master')

@section('title', trans('Add New Task'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Task</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/user')}}" >Task</a></li>
            <li class="active"><a href="Javascript::void();" >Add Task</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($project_tasks,['method'=>'PUT','route'=>[$routeName,$project_tasks->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                {!! Form::hidden("phase_id",Input::get("phase_id")) !!}
                @include('backend.project_tasks.includes.form',['btnTxt'=>'Update'])
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

@stop
