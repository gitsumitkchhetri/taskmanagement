<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Name</label>
    <div class="col-sm-6">
        {!! Form::text("name",null,["class" => "form-control","placeholder" => "Name"]) !!}
    </div>
</div>

<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Phone</label>
    <div class="col-sm-6">
        {!! Form::text("phone",null,["class" => "form-control","placeholder" => "Phone"]) !!}
    </div>
</div>



<div class="form-group">
    <label class="col-sm-3 control-label require">Tech Level</label>
    <div class="col-sm-6">
        {!! Form::select("level_id",$tech_level,null,["class" => "form-control","placeholder" => "Level"]) !!}
    </div>
</div>



<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{$btnTxt}}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL)}}">Cancel</a>

    </div>
</div>




