@extends('backend.master')

@section('title', trans('words.Change Password'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">@lang('words.Change Password')</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">@lang('words.Home')</a></li>
    <li class="active"><a href="Javascript::void();" >@lang('words.Change Password')</a></li>
  </ol>
</div>

@include('errors.errors')

<div class="panel">
  <div class="panel-body" style="padding:60px 60px;">
      {!!Form::open(['method'=>'POST','url'=>PREFIX.'/user/change_password/'.Auth::user()->id, 'class'=>'form-horizontal bordered-row'])!!}
      <input type="hidden" name="id" value="{{$data->id}}">
      <div class="form-group">
        <label class="col-sm-3 control-label">New Password</label>
        <div class="col-sm-6">
            {!! Form::password('password',['class'=>"form-control"]) !!}
            {{--<input type="password" name="password" placeholder="New Password" class="form-control">--}}
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Retype Password</label>
        <div class="col-sm-6">
            {!! Form::password('password_confirmation',['class'=>"form-control"]) !!}
            {{--<input type="password" name="password_confirmation" placeholder="Retype Password" class="form-control">--}}
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
          <div class="pull-right">
            <a class="btn btn-primary" href="{{URL::to(PREFIX.'/user')}}"><i class="glyphicon glyphicon-chevron-left" style="margin-right:10px;"></i>@lang('words.Back')</a>
            <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-ok" style="margin-right:10px;"></i>@lang('words.Save')</button>
          </div>
        </div>
      </div>
      {!!Form::close() !!}
      <div class="clearfix"></div>

    </div>
</div>


@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
