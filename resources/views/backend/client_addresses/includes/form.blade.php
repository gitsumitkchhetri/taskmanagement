<h4>Address Details</h4>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Address</label>
    <div class="col-sm-6">
        {!! Form::text("address",null,["class" => "form-control","placeholder" => "Address"]) !!}
    </div>
</div>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Latitude</label>
    <div class="col-sm-6">
        {!! Form::text("lat",null,["class" => "form-control","placeholder" => "Latitude"]) !!}
    </div>
</div>
<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Longitude</label>
    <div class="col-sm-6">
        {!! Form::text("lng",null,["class" => "form-control","placeholder" => "Longitude"]) !!}
    </div>
</div>

<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label">Google Map Link</label>
    <div class="col-sm-6">
        {!! Form::text("google_map_link",null,["class" => "form-control","placeholder" => "Google Map Link"]) !!}
    </div>
</div>


<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Contact Number</label>
    <div class="col-sm-6">
        {!! Form::text("contact_no",null,["class" => "form-control","placeholder" => "Contact Number"]) !!}
    </div>
</div>



<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{ $btnTxt }}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?client_id='.Input::get("client_id"))}}">Cancel</a>

    </div></div>