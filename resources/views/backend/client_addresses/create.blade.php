@extends('backend.master')

@section('title', trans('Add New Address'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Add Address</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
      <li><a href="{{URL::to(PREFIX.'/clients')}}" >Clients Management</a></li>
      <li><a href="{{URL::to(PREFIX.'/addresses?client_id='.Input::get('client_id'))}}" >{{$pageTitle}}</a></li>
    <li class="active"><a href="Javascript::void();" >Add Address</a></li>
  </ol>
</div>

@include('errors/errors')

{!!Form::open(['method'=>'POST','url'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
<div class="panel">
  <div class="panel-body">
      {!! Form::hidden("client_id",Input::get('client_id')) !!}
      @include('backend.client_addresses.includes.form',['btnTxt'=>'Save'])


      <div class="clearfix"></div>

    </div>
</div>


{!!Form::close() !!}
@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
