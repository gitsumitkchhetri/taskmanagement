@extends('backend.master')

@section('title', trans('Edit Address'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit {{$pageTitle}}</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/clients')}}" >Clients Management</a></li>
            <li><a href="{{URL::to(PREFIX.'/addresses?client_id='.Input::get('client_id'))}}" >{{$pageTitle}}</a></li>
            <li class="active"><a href="Javascript::void();" >Edit Address</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($address,['method'=>'PUT','route'=>[$routeName,$address->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                {!! Form::hidden("client_id",Input::get("client_id")) !!}
                @include('backend.client_addresses.includes.form',['btnTxt'=>'Update'])
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>

@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

@stop
