@extends('backend.master')

@section('title', trans('Edit New Deal'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Deal</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/deals')}}" >{{$pageTitle}}</a></li>
            <li class="active"><a href="Javascript::void();" >Edit Deal</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($deal,['method'=>'PUT','route'=>[$routeName,$deal->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                @include('backend.deals.includes.form',['btnTxt'=>'Update'])
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?deal_id='.Input::get("deal_id"))}}">Cancel</a>

                </div>
            </div>
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>


@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/datepicker/datepicker.css')}}">
    <script type="text/javascript" src="{{URL::asset('backend/widgets/datepicker/datepicker.js')}}"></script>

    <script type="text/javascript">
        /* Datepicker bootstrap */

        $(function() { "use strict";
            $('.bootstrap-datepicker').bsdatepicker({
                format: 'yyyy-mm-dd'
            });
        });

    </script>

@stop
