@extends('backend.master')

@section('title', trans('Add New Deal'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Add Deal</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
    <li><a href="{{URL::to(PREFIX.'/deals')}}" >{{$pageTitle}}</a></li>
    <li class="active"><a href="Javascript::void();" >Add Deal</a></li>
  </ol>
</div>

@include('errors/errors')

{!!Form::open(['method'=>'POST','url'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
<div class="panel">
  <div class="panel-body">

      @include('backend.deals.includes.form')


      <div class="clearfix"></div>

    </div>
</div>

<div class="panel">
    <div class="panel-body">
        @include('backend.proposals.includes.form',['btnTxt'=>'Save'])
    </div>
</div>


{!!Form::close() !!}
@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>


{{--<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">--}}


{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch.js')}}"></script>--}}

{{--<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/datepicker/datepicker.css')}}">--}}
{{--<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/timepicker/timepicker.css')}}">--}}
{{--<script type="text/javascript" src="{{URL::asset('backend/widgets/datepicker/datepicker.js')}}"></script>--}}

{{--<script type="text/javascript">--}}
    {{--/* Input switch */--}}

    {{--$(function() { "use strict";--}}
        {{--$('.input-switch').bootstrapSwitch();--}}
    {{--});--}}
{{--</script>--}}

{{--<script type="text/javascript">--}}
    {{--/* Datepicker bootstrap */--}}

    {{--$(function() { "use strict";--}}
        {{--$('.bootstrap-datepicker').bsdatepicker({--}}
            {{--format: 'yyyy-mm-dd'--}}
        {{--});--}}
    {{--});--}}

{{--</script>--}}

{{--<script type="text/javascript">--}}
    {{--$(function () {--}}
        {{--$('.datetimepicker1').datetimepicker();--}}
    {{--});--}}
{{--</script>--}}


@stop
