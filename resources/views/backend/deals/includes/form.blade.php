<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Title</label>
    <div class="col-sm-6">
        {!! Form::text("title",null,["class" => "form-control","placeholder" => "First Name"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label require">Description</label>
    <div class="col-sm-6">
        {!! Form::textarea("description",null,["class" => "form-control","placeholder" => "description"]) !!}
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Client</label>
    <div class="col-sm-6">
        {!! Form::select("client_id",$clients,null,["class" => "form-control","placeholder" => "Client"]) !!}
    </div>
</div>


@if(Auth::user()->id == 1)
<div class="form-group">
    <label class="col-sm-3 control-label require">Sales Person</label>
    <div class="col-sm-6">
        {!! Form::select("sales_person",$sales_persons,null,["class" => "form-control","placeholder" => "Sales Person"]) !!}
    </div>
</div>
@else
<input type="hidden" name="sales_person" value="{{Auth::user()->id}}">
@endif





