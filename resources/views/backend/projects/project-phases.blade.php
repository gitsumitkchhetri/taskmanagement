@extends('backend.master')
@section('title',trans("Project Details"))
@section('content')



<div id="page-title">
    <h2 style="display:inline-block">Project Details of '{{$projectObj->title}}'</h2>
    {{--<div class="right" style="float:right">--}}
        {{--@if(Auth::user()->canDo('clients.clients.create'))--}}
            {{--<a class="btn btn-primary" href="{{URL::to(PREFIX.'/projects/create')}}"><i class="glyph-icon icon-plus" style="margin-right:10px;"></i>Add New</a>--}}
        {{--@endif--}}
    {{--</div>--}}
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
    <li class="active"><a href="{{URL::to(PREFIX.'/projects')}}" >{{"Project Management"}}</a></li>
    <li class="active"><a href="Javascript::void();" >{{"Project Details"}}</a></li>
  </ol>
</div>

@include('errors.errors')

<div class="panel">
  <div class="panel-body">
    {{--{!!Form::open(['method'=>'GET','url'=>$formURL, 'class'=>'form-horizontal'])!!}--}}
    {{--{!!Form::open(['method'=>'GET','url'=>'', 'class'=>'form-horizontal'])!!}--}}
      {{--<div class="form-group">--}}

        {{--<div class="col-sm-3"></div>--}}
        {{--<div class="col-md-4"></div>--}}
        {{--<label class="col-sm-2 control-label">Search</label>--}}
          {{--<div class="col-md-3">--}}
            {{--<div class="input-group">--}}

                {{--<input type="text" class="form-control" name="keywords" value="{{Input::get('keywords')}}" autocomplete="off">--}}
                {{--<span class="input-group-btn">--}}
                    {{--<button class="btn btn-primary" type="submit">Go !</button>--}}
                {{--</span>--}}

            {{--</div>--}}
          {{--</div>--}}
      {{--</div>--}}
    {{--{!!Form::close() !!}--}}





    <div class="example-box-wrapper">
      <div class="scroll-columns">
                  {{--{{dd($phasePerProjectData[$key])}}--}}
          <div class="panel panel-default">
              <div class="panel-heading"></div>
              <div class="panel-body">
                    <table class="table table-bordered table-condensed cf" data-toggle="dataTable" data-form="deleteForm">
                      <thead class="cf">
                        <tr>
                            <th>Code</th>
                            <th>Title</th>
                            <th>Budget</th>
                            <th>Changes</th>
                            <th>Revised Budget</th>
                            <th>Cost To Date</th>
                            <th>% of Budget</th>
                            <th>Balance</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($phasePerProjectData as $key=>$phase)
                          @php $i = 0; @endphp
                          @if(isset($phasePerProjectData[$key]["task"]))
                              @php ++$i;$j = 0; @endphp
                            @foreach($phasePerProjectData[$key]["task"] as $tskKey => $tsk)
                                @php ++$j @endphp
                                @if($j == 1)
                                <tr><td colspan="1000" style="color:#000;"><b>{{$phase["attributes"]["title"]}}</b></td></tr>
                                @endif
                                <tr>

                                    {!! Form::hidden("type",$tsk["type"]) !!}
                                    {!! Form::hidden("project_id",$project_id) !!}
                                    {!! Form::hidden("task_id",$tskKey) !!}
                                    <td>{{$tsk["code"]}}</td>
                                    <td>{{$tsk["title"]}}</td>

                                    <td>${{$tsk["total_cost"]}}</td>

                                    <td>${{$tsk["cost_changes"]}}</td>


                                    <td>${{$tsk["revisedBudget"]}}</td>


                                    <td>
                                        @if( $tsk["type"] == "Labor")
                                            ${{$tsk["labor_cost_to_date"]}}

                                        @else
                                            ${{$tsk["cost"]}}

                                        @endif
                                    </td>

                                    <td>{{round($tsk["percentageOfBudget"],2)}}%</td>

                                    @if($tsk["balance"] < 0)
                                        <td>-${{abs($tsk["balance"])}}</td>
                                    @else
                                        <td>${{abs($tsk["balance"])}}</td>
                                    @endif

                                    <td>{{$tsk["type"]}}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" href="{{URL::route('phases.create',["project_id" => $project_id,"task_id" =>$tskKey,"phase_id" => $key])}}" > Update </a>
                                        {{--<a class="btn btn-danger btn-sm" href="{{URL::route('phases.destroy',["id" =>$tskKey])}}" > Delete </a>--}}
                                        @if($tsk["type"] == "Equipment")
                                        <a class="btn btn-info btn-sm" href="{{URL::route('project_equipments.index',["project_id" => $project_id,"task_id" =>$tskKey])}}" > {{$tsk["type"]}} </a>
                                        @endif

                                        @if($tsk["type"] == "Labor")
                                            <a class="btn btn-info btn-sm" href="{{URL::route('project_technicians.index',["project_id" => $project_id,"task_id" =>$tskKey])}}" > {{$tsk["type"]}} </a>
                                        @endif

                                    </td>


                                </tr>
                            @endforeach

                          @else
                              <tr>

                                  <td class="no-data" colspan="1000">
                                      <b>No data to display!</b>
                                  </td>
                              </tr>
                          @endif
                      @endforeach
                      <tr><td colspan="1000"></td></tr>
                      <tr>


                          <th></th>
                          <th>Total</th>

                          <th>${{$sumData["total_cost_sum"]}}</th>

                          <th>${{$sumData["cost_changes_sum"]}}</th>


                          <th>${{$sumData["revisedBudget_sum"]}}</th>


                          <th>${{$sumData["cost_sum"]}}</th>

                          <th>${{round($sumData["percentageOfBudget_sum"],2)}}%</th>


                          <th>${{abs($sumData["balance_sum"])}}</th>
                          <th></th>
                          <th></th>




                      </tr>
                      </tbody>
                    </table>
              </div>
          </div>

      </div>
    </div>
      {{--@if(!$data->isEmpty())--}}
          {{--<div class="pagination-tile"> <label class="pagination-sub">Showing {{($data->currentpage()-1)*$data->perpage()+1}} to {{(($data->currentpage()-1)*$data->perpage())+$data->count()}} of {{$data->total()}} entries</label>--}}
              {{--<ul class="pagination" style="float:right ; margin:0px;">--}}
                  {{--{!! str_replace('/?', '?',$data->appends(['keywords'=>Input::get('keywords')])->render()) !!}--}}

              {{--</ul>--}}
          {{--</div>--}}
      {{--@endif--}}
      <div class="clearfix"></div>

</div>
</div>

@stop

@section('scripts')

<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">

<script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch.js')}}"></script>
<script type="text/javascript">
/* Input switch */

$(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
});




</script>

@stop
