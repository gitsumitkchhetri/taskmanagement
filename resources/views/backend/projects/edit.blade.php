@extends('backend.master')

@section('title', trans('Edit New Project'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Project</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li><a href="{{URL::to(PREFIX.'/projects')}}" >Project</a></li>
            <li class="active"><a href="Javascript::void();" >Add Project</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::model($project,['method'=>'PUT','route'=>[$routeName,$project->id], 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                @include('backend.projects.includes.form',['btnTxt'=>'Update'])


            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>


@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>


    <script>
        function getDeals(ele) {
            console.log($(ele).val());
            $.ajax({
                url: "{{URL::to(PREFIX.'/ajax_handler')}}?client_id="+$(ele).val(),
                data: {
                    format: 'json'
                },
                error: function() {
                    alert("Error has occured.");
                },

                success: function(data) {
                    if ($("#deal_id").length <= 0) {
                        $("#client_id").after(data);
                    }else{
                        $("#deal_id").replaceWith(data);
                    }

                },
                type: 'GET'
            });
        }

    </script>


@stop
