<div class="form-group" style="border-top: 0px;">
    <label class="col-sm-3 control-label require">Title</label>
    <div class="col-sm-6">
        {!! Form::text("title",null,["class" => "form-control","placeholder" => "Title"]) !!}
    </div>
</div>




<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>
    <div class="col-sm-6">
        {!! Form::textarea("description",null,["class" => "form-control","placeholder" => "Description"]) !!}
    </div>
</div>

<div class="form-group" id="client_id">
    <label class="col-sm-3 control-label">Client</label>
    <div class="col-sm-6">
        @if(isset($client))
            {!! Form::select("client_id",$clients,$client->id,["class" => "form-control","placeholder" => "Client","onchange" => "javascript:getDeals(this)"]) !!}
        @else
            {!! Form::select("client_id",$clients,null,["class" => "form-control","placeholder" => "Client","onchange" => "javascript:getDeals(this)"]) !!}
        @endif
    </div>
</div>

@if(isset($deal))
    <div class="form-group" id="deal_id">
        <label class="col-sm-3 control-label require">Deal</label>
        <div class="col-sm-6">
            {!! Form::select("deal_id",$deal,$project->deal_id,["class" => "form-control","placeholder" => "Deal"]) !!}
        </div>
    </div>
@endif

<div class="form-group" id="client_id">
    <label class="col-sm-3 control-label">Project Type</label>
    <div class="col-sm-6">
        {!! Form::select("project_type",$project_type,null,["class" => "form-control","placeholder" => "Project Type"]) !!}
    </div>
</div>




<div class="form-group" id="client_id">
    <label class="col-sm-3 control-label" id = "sub_systems">Sub Systems</label>
    <div class="col-sm-6">
        @if(isset($sub_sys_selected))
            {!! Form::select("sub_systems[]",$sub_systems,$sub_sys_selected,["class" => "form-control span12","placeholder" => "Sub Systems","multiple"=>""]) !!}

        @else
            {!! Form::select("sub_systems[]",$sub_systems,[],["class" => "form-control span12","placeholder" => "Sub Systems","multiple"=>""]) !!}

        @endif
    </div>
</div>

<div class="form-group" id="client_id">
    <label class="col-sm-3 control-label">job Types</label>
    <div class="col-sm-6">
        {!! Form::select("job_types",$job_types,null,["class" => "form-control","placeholder" => "job Types"]) !!}
    </div>
</div>







<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">{{ $btnTxt }}</button>
        <a class="btn btn-warning" href="{{URL::to($redirectBackURL.'?client_id='.Input::get("client_id"))}}">Cancel</a>

    </div></div>

<script>
    $(document).ready(function(){
                @if(null !== Input::old('sub_systems'))
        var selectedValuesStreet = new Array();
        @php $n=0; @endphp
                @foreach(Input::old('sub_systems') as $ds)
            selectedValuesStreet["{{$n}}"] = "{{$ds}}";
        @php $n++; @endphp
        @endforeach
    $("#sub_systems").val(selectedValuesStreet);
        @endif
    });
</script>
