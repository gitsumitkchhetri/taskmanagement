@extends('backend.master')

@section('title', trans('Edit Details'))

@section('content')

    <div id="page-title">
        <h2 style="display:inline-block">Edit Details</h2>
        <div class="clearfix"></div>
    </div>

    <div class="breadcrumb-section clearfix">
        <ol class="breadcrumb">
            <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
            <li class="active"><a href="{{URL::to(PREFIX.'/projects')}}" >{{"Project Management"}}</a></li>
            <li><a href="{{ URL::route('projects.show',Input::get("project_id")) }}" >Project Details</a></li>
            <li class="active"><a href="Javascript::void();" >Edit Details</a></li>
        </ol>
    </div>

    @include('errors/errors')

    <div class="panel">
        <div class="panel-body">

            {!!Form::open(['method'=>'POST','route'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
                {!! Form::hidden("id",null) !!}
                @include('backend.phases.includes.update-form')
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">Update</button>

                </div>
            </div>
            {!!Form::close() !!}
            <div class="clearfix"></div>

        </div>
    </div>


@stop

@section('scripts')

    <style>
        .form-horizontal .control-label{
            text-align:left;
        }
    </style>

@stop
