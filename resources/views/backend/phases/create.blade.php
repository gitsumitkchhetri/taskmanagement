@extends('backend.master')

@section('title', trans('Update Detail'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Update Detail</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
    <li class="active"><a href="{{URL::to(PREFIX.'/projects')}}" >{{"Project Management"}}</a></li>
    <li><a href="{{ URL::route('projects.show',Input::get("project_id")) }}" >Project Details</a></li>
    <li class="active"><a href="Javascript::void();" >Update Details</a></li>
  </ol>
</div>

@include('errors/errors')

{!!Form::open(['method'=>'POST','route'=>$routeName, 'class'=>'form-horizontal bordered-row'])!!}
<div class="panel">
  <div class="panel-body">

      @include('backend.phases.includes.update-form')

      <div class="form-group">
          <div class="col-sm-12">
              <button type="submit" class="btn btn-primary">Save</button>

          </div>
      </div>
      <div class="clearfix"></div>

    </div>
</div>




{!!Form::close() !!}
@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
