





<div class="form-group">
    <label class="col-sm-3 control-label require">Total Budget</label>
    <div class="col-sm-6">
        {!! Form::number("total_cost",$total_cost,["class" => "form-control","placeholder" => "Total Budget"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label require">Cost Changes</label>
    <div class="col-sm-6">
        {!! Form::number("cost_changes",$cost_changes,["class" => "form-control","placeholder" => "Cost"]) !!}
    </div>
</div>

@if( !($taskType == "Labor" || $taskType == "Equipment"))
    <div class="form-group">
        <label class="col-sm-3 control-label require">Cost To Date</label>
        <div class="col-sm-6">
            @if(isset($taskDetails))
                {!! Form::number("cost",$taskDetails->cost,["class" => "form-control","placeholder" => "Cost To Date"]) !!}

            @else
                {!! Form::number("cost",null,["class" => "form-control","placeholder" => "Cost To Date"]) !!}

            @endif
        </div>
    </div>
@endif

<input type="hidden" name="project_id" value="{{$project_id}}">
<input type="hidden" name="task_id" value="{{$task_id}}">
<input type="hidden" name="task_type" value="{{$taskType}}">




