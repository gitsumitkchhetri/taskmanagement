@extends('backend.master')
@section('title',trans($pageTitle))
@section('content')



<div id="page-title">
    <h2 style="display:inline-block">{{$pageTitle}}</h2>
    <div class="right" style="float:right">
        @if(Auth::user()->canDo('clients.clients.create'))
            <a class="btn btn-primary" href="{{URL::to(PREFIX.'/clients/create')}}"><i class="glyph-icon icon-plus" style="margin-right:10px;"></i>Add New</a>
        @endif
    </div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">Home</a></li>
    <li class="active"><a href="Javascript::void();" >{{$pageTitle}}</a></li>
  </ol>
</div>

@include('errors.errors')

<div class="panel">
  <div class="panel-body">
    {!!Form::open(['method'=>'GET','url'=>$formURL, 'class'=>'form-horizontal'])!!}
      <div class="form-group">

        <div class="col-sm-3"></div>
        <div class="col-md-4"></div>
        <label class="col-sm-2 control-label">Search</label>
          <div class="col-md-3">
            <div class="input-group">

                <input type="text" class="form-control" name="keywords" value="{{Input::get('keywords')}}" autocomplete="off">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Go !</button>
                </span>

            </div>
          </div>
      </div>
    {!!Form::close() !!}





    <div class="example-box-wrapper">
      <div class="scroll-columns">
        <table class="table table-bordered table-striped table-condensed cf" data-toggle="dataTable" data-form="deleteForm">
          <thead class="cf">
            <tr>
                <th>S.N.</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Company Name</th>
                <th>Phone</th>
                <th>Email</th>
                @if(Auth::user()->canDo('clients.clients.destroy'))
                <th>Action</th>
                    @endif
            </tr>
          </thead>
          <tbody>
          @if($data->isEmpty())
              <tr>

                  <td class="no-data" colspan="7">
                      <b>No data to display!</b>
                  </td>
              </tr>
          @else
          @php  $a=$data->perPage() * ($data->currentPage()-1); @endphp
            @foreach($data as $d)
                @php $a++ @endphp
            <tr>
                <td>{{ $a }}</td>
                <td>{{$d->first_name}} </td>
                <td>{{$d->last_name}}</td>
                <td>{{$d->company_name}}</td>
                <td>{{$d->phone}}</td>
                <td>{{$d->email}}</td>

                <td>

                    @if(Auth::user()->canDo('clients.clients.destroy'))
                        {!! Form::model($d, ['method' => 'delete', 'route' => ['clients.destroy', $d->id], 'class' =>'form-inline form-delete']) !!}
                        {!! Form::hidden('id', $d->id) !!}
                        <button type="submit" name="delete_modal" class="btn btn-round btn-sm btn-danger" ><i class="glyphicon glyphicon-trash"></i> Delete </button>
                        {!! Form::close() !!}
                    @endif

                    @if(Auth::user()->canDo('clients.clients.edit'))

                        <a href="{{ URL::route('clients.edit',$d->id) }}"  class="btn btn-round btn-sm btn-info" ><i class="glyphicon glyphicon-edit"></i> Edit </a>
                    @endif
                        <a href="{{ URL::route('addresses.index',['client_id'=>$d->id]) }}"  class="btn btn-round btn-sm btn-info" ><i class="glyphicon glyphicon-bars"></i> View Addresses ({{$d->addresses->count()}})</a>
                        @if($d->lead != null) <span class="label label-warning " style="font-size: small;">Converted From Lead</span> @endif

                </td>

            </tr>
            @endforeach
              @endif
          </tbody>
        </table>
      </div>
    </div>
      @if(!$data->isEmpty())
          <div class="pagination-tile"> <label class="pagination-sub">Showing {{($data->currentpage()-1)*$data->perpage()+1}} to {{(($data->currentpage()-1)*$data->perpage())+$data->count()}} of {{$data->total()}} entries</label>
              <ul class="pagination" style="float:right ; margin:0px;">
                  {!! str_replace('/?', '?',$data->appends(['keywords'=>Input::get('keywords')])->render()) !!}

              </ul>
          </div>
      @endif
      <div class="clearfix"></div>

</div>
</div>

@stop

@section('scripts')

<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">

<script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch.js')}}"></script>
<script type="text/javascript">
/* Input switch */

$(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
});




</script>

@stop
