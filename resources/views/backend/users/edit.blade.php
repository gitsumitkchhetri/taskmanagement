@extends('backend.master')

@section('title', 'Edit Users')

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">Edit Users</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">@lang('words.Home')</a></li>
    <li><a href="{{URL::to(PREFIX.'/user')}}" >@lang('words.Users')</a></li>
    <li class="active"><a href="Javascript::void();" >Edit User</a></li>
  </ol>
</div>

@include('errors/errors')
<div class="panel">
  <div class="panel-body">
    {!!Form::model($data,['method'=>'PUT','url'=>PREFIX.'/user/'.$data->id, 'class'=>'form-horizontal bordered-row'])!!}
    <input type="hidden" name="id" value="{{$data->id}}">
      <div class="form-group" style="border-top: 0px;">
          <label class="col-sm-3 control-label require">First Name</label>
          <div class="col-sm-6">
              <input type="text" name="first_name" placeholder="First Name" class="form-control" value="{{$data->first_name}}">
          </div>
      </div>
      <div class="form-group" >
          <label class="col-sm-3 control-label require">Last Name</label>
          <div class="col-sm-6">
              <input type="text" name="last_name" placeholder="Last Name" class="form-control" value="{{$data->last_name}}">
          </div>
          </div>
      <div class="form-group">
        <label class="col-sm-3 control-label require">Email</label>
        <div class="col-sm-6">
            <input type="text" name="email" placeholder="Email" class="form-control" value="{{$data->email}}">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label require">Username</label>
        <div class="col-sm-6">
            <input type="text" name="username" placeholder="Username" class="form-control" value="{{$data->username}}">
        </div>
      </div>


      @if($data->rolesUser->role_id == 1)
          @if(($superUserCount) <= 1)
              <div class="form-group">
              </div>
              <input type="hidden" name="roles" value="{{$data->rolesUser->role_id}}">
              <fieldset disabled>
                  @endif
                  <div class="form-group">
                      <label class="col-sm-3 control-label require">Roles</label>
                      <div class="col-sm-6">
                          {!! Form::select('roles',$roles,$data->rolesUser->role_id,['class'=>'form-control']) !!}
                      </div>
                  </div>
                  @if(($superUserCount) <= 1)
              </fieldset>
          @endif
          @else
          <div class="form-group">
              <label class="col-sm-3 control-label require">Group</label>
              <div class="col-sm-6">
                  {!! Form::select('roles',$roles,$data->rolesUser->role_id,['class'=>'form-control']) !!}
              </div>
          </div>
      @endif

      <div class="form-group">
          <label class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
          <div class="col-sm-6">
              <label class="radio-inline">
                  {{ Form::radio('status',1,true) }}

                  Active
              </label>
              <label class="radio-inline">
                  {{ Form::radio('status',0) }}
                  Inactive
              </label>

          </div>
      </div>



      <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <a class="btn btn-warning" href="{{URL::to(PREFIX.'/user')}}">@lang('words.Cancel')</a>

                            </div></div>
      {!!Form::close() !!}
      <div class="clearfix"></div>

    </div>
</div>


@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
