@extends('backend.master')

@section('title', trans('words.'.$pageTitle))

@section('content')

<div id="page-title">
    <h2 style="display:inline-block">@lang('words.'.$pageTitle)</h2>
    <div class="right" style="float:right">
        @if(Auth::user()->canDo('user.users.create'))
      <a class="btn btn-primary" href="{{URL::to(PREFIX.'/user/create')}}"><i class="glyph-icon icon-plus" style="margin-right:10px;"></i>@lang('words.Add New')</a>
            @endif
    </div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">@lang('words.Home')</a></li>
    <li class="active"><a href="Javascript::void();" >@lang('words.'.$pageTitle)</a></li>
  </ol>
</div>

@include('errors.errors')

<div class="panel">
  <div class="panel-body">
    {!!Form::open(['method'=>'GET','url'=>PREFIX.'/user', 'class'=>'form-horizontal'])!!}
    <div class="form-group">

      <div class="col-sm-3"></div>
      <div class="col-md-4"></div>
      <label class="col-sm-2 control-label">@lang('words.Search')</label>
        <div class="col-md-3">
          <div class="input-group">

              <input type="text" class="form-control" name="keywords" value="{{Input::get('keywords')}}" autocomplete="off">
              <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit">@lang('words.Go')!</button>
              </span>

          </div>
        </div>
    </div>
    {!!Form::close() !!}



    <div class="example-box-wrapper">
      <div class="scroll-columns">
        <table class="table table-bordered table-striped table-condensed cf" data-toggle="dataTable" data-form="deleteForm">
          <thead class="cf">
            <tr>
                <th>S.N.</th>
                <th>@lang('words.Name') </th>
                <th>@lang('words.username') </th>
                <th>@lang('words.email') </th>
                <th>@lang('Status') </th>
                @if(Auth::user()->canDo('user.users.delete') || Auth::user()->canDo('user.users.edit'))
                    <th>@lang('words.action')</th>
                    @endif
            </tr>
          </thead>
          <tbody>
          @if($data->isEmpty())
          <tr>

              <td class="no-data" colspan="5">
                  <b>@lang('words.No data to display!')</b>
              </td>
          </tr>
          @else
              @php  $a=$data->perPage() * ($data->currentPage()-1); @endphp
              @foreach($data as $d)
                  @php $a++;@endphp
            <tr>
                <td>{{ $a }}</td>
                <td>{{$d->full_name}}</td>
                <td>{{$d->username}}</td>
                <td>{{$d->email}}</td>
                <td>@if($d->status == 1) <span class="btn btn-xs btn-success">Active</span>  @else <span class="btn btn-xs btn-danger">Inactive</span> @endif</td>
                @if(Auth::user()->canDo('user.users.delete') || Auth::user()->canDo('user.users.edit'))
                <td>
                    @if(Auth::user()->canDo('user.users.edit'))
                  <a class="btn btn-round btn-sm btn-info btn_glyph" href="{{URL::to(PREFIX.'/user/'.$d->id.'/edit')}}"><i class="glyphicon glyphicon-edit"></i> @lang('words.Edit')</a> @endif
                        @if(Auth::user()->canDo('user.users.destroy'))
                            {!! Form::model($d, ['method' => 'delete', 'route' => ['user.destroy', $d->id], 'class' =>'form-inline form-delete']) !!}
                            {!! Form::hidden('id', $d->id) !!}
                            <button type="submit" name="delete_modal" class="btn btn-round btn-sm btn-danger" ><i class="glyphicon glyphicon-trash"></i> @lang('words.Delete') </button>
                            {!! Form::close() !!}

                        @endif
                        @if(Auth::user()->canDo('user.users.edit'))
                  <a class="btn btn-round btn-sm btn-primary btn_glyph" href="{{URL::to(PREFIX.'/user/password/'.$d->id)}}"><i class="glyphicon glyphicon-wrench"></i> @lang('words.Reset Password')</a>
                            @endif
                </td>
                    @endif
            </tr>
            @endforeach
              @endif
          </tbody>
        </table>
      </div>
    </div>

      @if(!$data->isEmpty())
          <div class="pagination-tile"> <label class="pagination-sub">@lang('words.Showing') {{($data->currentpage()-1)*$data->perpage()+1}} @lang('words.to') {{(($data->currentpage()-1)*$data->perpage())+$data->count()}} @lang('words.of') {{$data->total()}} @lang('words.entries')</label>
              <ul class="pagination" style="float:right ; margin:0px;">
                  {!! str_replace('/?', '?',$data->appends(['keywords'=>Input::get('keywords')])->render()) !!}

              </ul>
          </div>
      @endif


    <div class="clearfix"></div>

</div>
</div>

@stop

@section('scripts')

<link rel="stylesheet" type="text/css" href="{{URL::asset('backend/widgets/input-switch/inputswitch.css')}}">

<script type="text/javascript" src="{{URL::asset('backend/widgets/input-switch/inputswitch.js')}}"></script>
<script type="text/javascript">
/* Input switch */

$(function() { "use strict";
    $('.input-switch').bootstrapSwitch();
});
</script>

@stop
