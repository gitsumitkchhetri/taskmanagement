@extends('backend.master')

@section('title', trans('words.Add New User'))

@section('content')

<div id="page-title">
        <h2 style="display:inline-block">@lang('words.Add User')</h2>
        <div class="clearfix"></div>
</div>

<div class="breadcrumb-section clearfix">
  <ol class="breadcrumb">
    <li><a href="{{URL::to(PREFIX.'/home')}}">@lang('words.Home')</a></li>
    <li><a href="{{URL::to(PREFIX.'/user')}}" >@lang('words.Users')</a></li>
    <li class="active"><a href="Javascript::void();" >@lang('words.Add User')</a></li>
  </ol>
</div>

@include('errors/errors')

<div class="panel">
  <div class="panel-body">
    {!!Form::open(['method'=>'POST','url'=>PREFIX.'/user', 'class'=>'form-horizontal bordered-row'])!!}
      <div class="form-group" style="border-top: 0px;">
        <label class="col-sm-3 control-label require">First Name</label>
        <div class="col-sm-6">
            <input type="text" name="first_name" placeholder="First Name" class="form-control" value="{{Input::old('first_name')}}">
        </div>
      </div>
      <div class="form-group" >
          <label class="col-sm-3 control-label require">Last Name</label>
          <div class="col-sm-6">
              <input type="text" name="last_name" placeholder="Last Name" class="form-control" value="{{Input::old('last_name')}}">
          </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label require">Email</label>
        <div class="col-sm-6">
            <input type="text" name="email" placeholder="Email" class="form-control" value="{{Input::old('email')}}">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label require">Username</label>
        <div class="col-sm-6">
            <input type="text" name="username" placeholder="Username" class="form-control" value="{{Input::old('username')}}">
        </div>
      </div>
      {{--<div class="form-group">--}}
          {{--<label class="col-sm-3 control-label">Phone No.</label>--}}
          {{--<div class="col-sm-6">--}}
              {{--<input type="text" name="phone_no" placeholder="Phone" class="form-control" value="{{Input::old('phone_no') }}">--}}
          {{--</div>--}}
      {{--</div>--}}
      {{--<div class="form-group">--}}
          {{--<label class="col-sm-3 control-label">Pin No.</label>--}}
          {{--<div class="col-sm-6">--}}
              {{--<input type="number" name="pin_no" placeholder="Pin" class="form-control" value="{{Input::old('pin_no') }}">--}}
          {{--</div>--}}
      {{--</div>--}}
      {{--<div class="form-group">--}}
          {{--<label class="col-sm-3 control-label">IMEI No.</label>--}}
          {{--<div class="col-sm-6">--}}
              {{--<input type="number" name="imei_no" placeholder="IMEI" class="form-control" value="{{Input::old('imei_no') }}">--}}
          {{--</div>--}}
      {{--</div>--}}
      <div class="form-group">
        <label class="col-sm-3 control-label require">Password</label>
        <div class="col-sm-6">
            <input type="password" name="password" placeholder="Password" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label require">Confirm Password</label>
        <div class="col-sm-6">
            <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-3 control-label require">Group</label>
        <div class="col-sm-6">
            {!! Form::select('roles',$roles,Input::old('roles'),['class'=>'form-control']) !!}
        </div>
      </div>

      <div class="form-group">
          <label class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
          <div class="col-sm-6">
              <label class="radio-inline">
                  {{ Form::radio('status',1,true) }}

                  Active
              </label>
              <label class="radio-inline">
                  {{ Form::radio('status',0) }}
                  Inactive
              </label>

          </div>
      </div>

      <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">@lang('words.Save')</button>
                                <a class="btn btn-warning" href="{{URL::to(PREFIX.'/user')}}">@lang('words.Cancel')</a>

                            </div></div>

      {!!Form::close() !!}
      <div class="clearfix"></div>

    </div>
</div>

@stop

@section('scripts')

<style>
.form-horizontal .control-label{
text-align:left;
}
</style>

@stop
