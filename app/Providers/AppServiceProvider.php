<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\ServiceProvider;
use App\Http\Validator\CustomValidationRule;
use Illuminate\Support\Facades\Validator;
use App\Http\Validators\HashValidator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::resolver(function ($translator, $data, $rules, $messages) {

            return new CustomValidationRule($translator, $data, $rules, $messages);

        });
        Validator::resolver(function($translator, $data, $rules, $messages) {
            return new HashValidator($translator, $data, $rules, $messages);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
