<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Deal extends Model
{
    protected $table = "crm_deals";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['client_id','title','description','sales_person'];
    protected $perPage = 30;

    public function getAllData($keyword = null,$user){


        $data = $this->query();


//        $userObj = User::find($user);
//
//        if( !($user == 1 || $userObj->roles->first()->name == "superuser" || $userObj->roles->first()->name == "superadmin") ){
//            $data->where("sales_person","=",$user);
//        }

        if($keyword != null){
            $keyword = trim($keyword);
            $data->where("title","LIKE","%$keyword%");
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }



    public  function proposals(){
        return $this->hasMany("App\Proposal","deal_id","id");
    }

    public  function client(){
        return $this->belongsTo("App\Clients","client_id","id");
    }

    public  function salesPerson(){

        return $this->belongsTo("App\User","sales_person","id");
    }
}
