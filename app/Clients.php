<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = "crm_clients";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['first_name','last_name','company_name','phone','email','lead_id'];
    protected $perPage = 30;

    public function getAllData($keyword = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("first_name","LIKE","%$keyword%")
                    ->orWhere("last_name","LIKE","%$keyword%")
                    ->orWhere("company_name","LIKE","%$keyword%")
                    ->orWhere("email","LIKE","%$keyword%")
                    ->orWhere("phone","LIKE","%$keyword%");
            });
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }

    public  function lead(){
        return $this->belongsTo("App\Lead","lead_id","id");
    }

    public  function addresses(){
        return $this->hasMany("App\Address","client_id","id");
    }
}
