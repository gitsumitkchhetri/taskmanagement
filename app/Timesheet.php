<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timesheet extends Model
{
    protected $table = "cs_timesheet";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['project_id','task_id','phase_id','date','work_hr','technician_id'];
    protected $perPage = 30;

    public function getAllData($keyword = null,$filter = null,$user){

        $data = $this->query();

//        $userObj = User::find($user);
//
//        if( !($user == 1 || $userObj->roles->first()->name == "superuser" || $userObj->roles->first()->name == "superadmin") ){
//            $dealsArr =Deal::where("sales_person","=",$user)->pluck("id")->toArray();
//
//            $data->whereIn("deal_id",$dealsArr);
//        }

        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("title","LIKE","%$keyword%")
                    ->orWhere("total_budget","LIKE","%$keyword%");
            });
        }

        if($filter != null){
            $filter = trim($filter);
            $data->where("deal_id","=",$filter);
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }
}
