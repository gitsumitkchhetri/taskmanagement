<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTechnicians extends Model
{
    protected $table = "cs_project_technicians";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['task_id','total_hour','project_id','cost','date','technician_id'];
    protected $perPage = 30;

    public function getAllData($project_id,$task_id,$keyword = null){

        $data = $this->where("project_id","=",$project_id)->where("task_id","=",$task_id);
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("title","LIKE","%$keyword%")
                    ->orWhere("cost","LIKE","%$keyword%");
            });
        }

//        if($taskFilter != null){
//            $taskFilter = trim($taskFilter);
//            $data->where("task_id","=",$taskFilter);
//        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }

    public  function technician(){
        return $this->belongsTo("App\Technician","technician_id","id");
    }
}
