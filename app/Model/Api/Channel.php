<?php

namespace App\Model\Api;


class Channel extends \App\Channel
{

    public function getAllChannel()
    {
        return Channel::all();
    }

    public function countChannelById($channelId)
    {
        return Channel::whereIn('id',$channelId)->count();
    }

  

}