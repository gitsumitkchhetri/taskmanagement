<?php
namespace App\Model\Api;

use App\Zone;

class Zones extends Zone
{

    public function getZoneDetails()
    {
        return Zones::all();
    }

    public function getZoneIdByName($name)
    {
        return Zone::where('zone_name',$name)->get();
    }

    public function getZoneIdById($id)
    {
        return Zone::whereIn('gid',$id)->get();
    }

    public function town()
    {
        return $this->hasMany('App\Model\Api\Town','zone_id','id');
    }
    public function shops()
    {
        return $this->hasMany('App\Model\Api\RetailOutlet','zone_id','id');
    }

}
