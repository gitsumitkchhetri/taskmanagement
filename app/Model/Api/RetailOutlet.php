<?php

namespace App\Model\Api;


class RetailOutlet extends \App\RetailOutlet
{

    public function getShopsById($id)
    {
        return RetailOutlet::where('id',$id)
        ->select('category_id',
        'name',
        'address',
        'owner_name',
        'owner_phone',
        'pan_no',
        'phone',
        'longitude',
        'latitude',
        'monthly_avg_business',
        'id',
        'town_id')->first();
    }

    public function user()
    {
        return $this->belongsTo('App\Model\Api\User','verfied_by','id');
    }

    public function registeredBy()
    {
        return $this->belongsTo('App\Model\Api\User','created_by','id');
 }
    public function zone()
    {
        return $this->belongsTo('App\Model\Api\Zones','zone_id','id');
    }
}