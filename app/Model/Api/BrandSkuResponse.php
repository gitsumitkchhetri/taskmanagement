<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class BrandSkuResponse extends Model
{
    protected $connection = 'pgsql';
    protected $table='brandskuresponse';
    protected $fillable =['id','count','parser','executiontime','statuscode'];

    public function addData($inputData){
        return BrandSkuResponse::create($inputData);
    }
}
