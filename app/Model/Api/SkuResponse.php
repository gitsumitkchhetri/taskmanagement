<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class SkuResponse extends Model
{
    protected $connection = 'pgsql';
    protected $table='skuresponse';
    protected $fillable =['id','count','parser','executiontime','statuscode'];

    public function addData($inputData){
        return SkuResponse::create($inputData);
    }
}
