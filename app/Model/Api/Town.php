<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class Town extends \App\Town
{
    public function getTownByZoneId($zoneId)
    {
        return Town::where('zone_id',$zoneId)->get();
    }

    public function getTown()
    {
        return Town::all();
    }
    

    public function zone()
    {
        return $this->belongsTo('App\Model\Api\Zone','zone_id','id');
    }
    public function getTownIdByZoneId($zoneId)
    {
        return Town::where('zone_id',$zoneId)->pluck('id');
    }

    public function countTownById($townIds)
    {
        return Town::whereIn('id',$townIds)->count();
    }


}
