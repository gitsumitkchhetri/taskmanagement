<?php
namespace App\Model\Api;


class District extends \App\District
{

    public function getDistricts()
    {
        return District::all();
    }

    public function getDistrictsById($id)
    {
        return District::whereIn('gid',$id)->get();
    }

    public function getZoneIdByName($name)
    {
        return Zone::where('name',$name)->pluck('id');
    }

    public function town()
    {
        return $this->hasMany('App\Model\Api\Town','zone_id','id');
    }

}
