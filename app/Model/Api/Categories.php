<?php

namespace App\Model\Api;


class Categories extends \App\Category
{

    public function getAllCategory()
    {
        return Categories::all();
    }

    public function getChannelByCatgoryId($channelId)
    {
        return Categories::where('channel_id',$channelId)->get();
    }

    public function countCategoryById($categoryId)
    {
        return Categories::whereIn('id',$categoryId)->count();
    }

    public function existCategoryByName($categoryName)
    {
        return Categories::whereExists('name',$categoryName)->exists();
    }
}
