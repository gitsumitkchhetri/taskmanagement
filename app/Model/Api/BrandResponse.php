<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;

class BrandResponse extends Model
{
    protected $connection = 'pgsql';
    protected $table='retailoutletresponse';
    protected $fillable =['id','count','parser','executiontime','statuscode'];

    public function addData($inputData){
        return BrandResponse::create($inputData);
    }
}
