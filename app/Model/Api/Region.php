<?php

namespace App\Model\Api;
use DB;


class Region extends \App\Region
{

    public function getRegion($commaSeparatedFields)
    {
       return Region::select(DB::raw('st_asgeojson(st_transform(geom,4326)) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),4326),4326)) AS centroid,'. $commaSeparatedFields))
            ->get();
    }

}