<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;
    protected $table = "role_user";

    protected $fillable = ['user_id','role_id'];

    public $timestamps = false;

}
