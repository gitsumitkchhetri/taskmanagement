<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model
{
    protected $table = "cs_project_tasks";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['phase_id','title','position','type','code'];
    protected $perPage = 30;



    public function getAllData($keyword = null,$phase_id = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("title","LIKE","%$keyword%")
                    ->orWhere("type","LIKE","%$keyword%")
                    ->orWhere("code","LIKE","%$keyword%");
            });
        }

        if($phase_id != null){
            $data->where("phase_id","=",$phase_id);
        }

        return $data->orderBy('position')->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }



    public  function phase(){
        return $this->belongsTo("App/ProjectPhases","phase_id","id");
    }

}
