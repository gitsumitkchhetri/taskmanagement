<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
     public function render($request, Exception $e)
     {
//         if ($e instanceof \ReflectionException && \Request::segment(1) == 'system') {
//             return response()->view('errors.404', [], 404);
//         }
//         if ($e instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException && \Request::segment(1) == 'system') {
//             return response()->view("errors.404", ['exception' => $e], 404);
//             //return response()->view('errors.404')->withErrors(['errors'=>session()->get('errors', new \Illuminate\Support\ViewErrorBag)]);
//             // return response()->view('errors.404', ['errors'=>session()->get('errors', new \Illuminate\Support\ViewErrorBag)], 404);
//         }
//         if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException && \Request::segment(1) != 'system') {
//             //return redirect('/404');
//             //dd($e);
//             return response()->view("errors.504", ['errors' => session()->get('errors', new \Illuminate\Support\ViewErrorBag)], 404);
//             // return response()->view('errors.404', ['errors'=>session()->get('errors', new \Illuminate\Support\ViewErrorBag)], 504);
//         }
         if ($e instanceof \Illuminate\Session\TokenMismatchException) {
             if (in_array(PREFIX, $request->segments())) {
                 return redirect()->guest('system/login');
             } else {
                 return redirect()->guest('login');
             }
         }
//         if ($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
//             return redirect(\URL::previous())->withErrors(['alert-danger'=>'Data not found!']);
//         }
         return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('system/login');
    }
}
