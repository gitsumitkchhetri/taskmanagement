<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    protected $table = "cs_technicians";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['level_id','name','phone'];
    protected $perPage = 30;

    public function getAllData($keyword = null,$filter = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("name","LIKE","%$keyword%")
                    ->orWhere("phone","LIKE","%$keyword%");
            });
        }

        if($filter != null){
            $filter = trim($filter);
            $data->where("level_id","=",$filter);
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }



    public  function proposals(){
        return $this->hasMany("App\Proposal","deal_id","id");
    }

    public  function TechLevel(){
        return $this->belongsTo("App\TechLevel","level_id","id");
    }


}
