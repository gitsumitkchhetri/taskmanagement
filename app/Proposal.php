<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $table = "crm_proposals";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['proposal_url','portal_number','loss_remarks','cost','expected_close_date','status','file','deal_id'];
    protected $perPage = 30;

    public function getAllData($keyword = null,$deal_id = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("proposal_url","LIKE","%$keyword%")
                    ->orWhere("portal_number","LIKE","%$keyword%")
                    ->orWhere("cost","LIKE","%$keyword%")
                    ->orWhere("expected_close_date","LIKE","%$keyword%")
                    ->orWhere("file","LIKE","%$keyword%")
                    ->orWhere("loss_remarks","LIKE","%$keyword%");
            });
        }

        if($deal_id != null){
            $data->where("deal_id","=",$deal_id);
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }



    public  function deal(){
        return $this->belongsTo("App/Deal","deal_id","id");
    }

}
