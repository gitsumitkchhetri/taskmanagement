<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\CheckPermission;
use Illuminate\Database\Eloquent\SoftDeletes;

use Hash;

class User extends Authenticatable
{
    use CheckPermission,Notifiable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_at';
    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updated_at';


    //public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'username','password','permissions','created_by','created_at','updated_at','role_id','updated_by','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAllData($data=array()){
      $users = User::query();
      if(isset($data['keywords'])){
//        $users->where('first_name','LIKE','%'.pg_escape_string(preg_replace('/[\s]+.*/','',$data['keywords'])).'%')->orWhere('last_name','LIKE','%'.preg_replace('/[\s]+.*/','',$data['keywords']).'%')->orWhere('username','LIKE','%'.preg_replace('/[\s]+.*/','',$data['keywords']).'%')->orWhere('email','LIKE','%'.preg_replace('/[\s]+.*/','',$data['keywords']).'%');
      $users->whereRaw(" LOWER(CONCAT_WS('', first_name,last_name,username,email)) LIKE LOWER('%".pg_escape_string(trim($data['keywords'])) ."%')");
      }
      return $users->orderBy('created_at','desc')->paginate(20);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function rolesUser()
    {
        return $this->hasOne('App\RoleUser','user_id','id');
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

}
