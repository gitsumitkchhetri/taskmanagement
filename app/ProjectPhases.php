<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhases extends Model
{
    protected $table = "cs_project_phases";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['title','position','description'];
    protected $perPage = 30;

    public function getAllData($keyword = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where("title","LIKE","%$keyword%");

        }

        return $data->orderBy('position')->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }

    public  function tasks(){
        return $this->hasMany("App/ProjectTask","phase_id","id");
    }
}
