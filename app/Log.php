<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

  protected $guarded = ['id', '_token'];

  protected $fillable = ['user_id','module','data'];

  public function getAllData($data=array()){
    $language = Log::query();
    if(isset($data['keywords'])){
      //$language->where('module','LIKE','%'.$data['keywords'].'%')->orWhere('data','LIKE','%'.pg_escape_string(preg_replace('/[\s]+.*/','',$data['keywords'])).'%');
        $user = User::whereRaw(" LOWER(CONCAT_WS('', first_name,last_name)) LIKE LOWER('%". pg_escape_string(trim($data['keywords'])) ."%')")->pluck('id')->toArray();

        $language->orWhereIn('user_id',$user)->orWhereRaw(" LOWER(CONCAT_WS('', module,data)) LIKE LOWER('%". pg_escape_string(trim($data['keywords'])) ."%')");
    }
    return $language->paginate(20);
  }

  public function getUserById($userId){
    $userName = "N/A";
    $userData = User::where('id',$userId)->first();
    if(!empty($userData)){
      $userName = $userData->first_name. ' '.$userData->last_name;
    }
    return $userName;
  }
}
