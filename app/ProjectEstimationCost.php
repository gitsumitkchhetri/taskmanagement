<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectEstimationCost extends Model
{
    protected $table = "cs_projects_estimation_cost";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['task_id','total_cost','project_id','cost_changes'];
    protected $perPage = 30;

    public function getAllData($keyword = null,$filter = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("cost_changes","LIKE","%$keyword%")
                    ->orWhere("total_cost","LIKE","%$keyword%");
            });
        }

        if($filter != null){
            $filter = trim($filter);
            $data->where("deal_id","=",$filter);
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }

    public  function deal(){
        return $this->belongsTo("App\Deal","deal_id","id");
    }
}
