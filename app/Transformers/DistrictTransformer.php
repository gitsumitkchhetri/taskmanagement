<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 4/17/17
 * Time: 12:37 PM
 */

namespace App\Transformers;


use App\Model\Api\District;
use App\Model\Api\Town;
use League\Fractal\Resource\NullResource;
use League\Fractal\TransformerAbstract;

class DistrictTransformer extends TransformerAbstract
{
    private $params = [];

    protected $defaultIncludes = [
        'towns'
    ];

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function transform($district)
    {
        return [
            'id' => $district->gid,
            'name' => $district->name,
            'geojson' => $district->geojson,
            'centroid' => $district->centroid,
        ];


    }

    public function includeTowns($district)
    {
        $districtId = $district->gid;
        if (empty($districtId)) {
            return $this->nullResource();
        }
        $district = Town::where('district_id', $districtId)->select('id','name')->get();

        if (empty($district)) {
            return $this->nullResource();
        }

        return $this->Collection($district, new TownTransformer(null), 'town');
    }

    public function nullResource()
    {
        return new NullResource();
    }


}