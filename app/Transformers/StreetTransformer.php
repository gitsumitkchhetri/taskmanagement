<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 4/7/17
 * Time: 1:43 PM
 */

namespace App\Transformers;


use League\Fractal\TransformerAbstract;

class StreetTransformer extends TransformerAbstract
{
    public function transform($cordinates)
    {
        return [
            'id' => $cordinates->id,
            'town-id' => $cordinates->town_id,
            'name' => $cordinates->name,
        ];

    }

}