<?php
/**
 * Created by PhpStorm.
 * User: samina
 * Date: 3/7/17
 * Time: 5:49 PM
 */

namespace App\Transformers;


use App\Model\Api\Categories;
use App\Model\Api\Channel;
use League\Fractal\TransformerAbstract;
use DB;

class ChannelTransformer extends TransformerAbstract
{
    private $params = [];

    protected $defaultIncludes = [
        'categories'
    ];

    public function __construct($params = [])
    {
        $this->params = $params;
        $this->category = new Categories();
    }

    public function transform($cordinates)
    {
        if (isset($cordinates->gid) == false) {
            $id = $cordinates->id;
        } else {
            $id = $cordinates->gid;
        }
        $value = $this->params;
        $array1 = [];
        if ($value != null) {
            $fields = array_values($value);
            foreach ($fields as $field) {
                $key = str_replace("_", "-", $field);
                $array1[$key] = $cordinates->$field;

            }
            if (isset($cordinates->geojson) == true && isset($cordinates->centroid) == true) {
                $array2 = [
                    'id' => $id,
                    'geojson' => $cordinates->geojson,
                    'centroid' => $cordinates->centroid,

                ];
            } else {
                $array2 = [
                    'id' => $id,
                ];
            }

            $data = array_merge($array1, $array2);
            return $data;
        } else {
            foreach ($cordinates as $key => $cordinate) {
                $keyName = str_replace("_", "-", $key);
                $array1[$keyName] = $cordinate;
            }

            $array2 = [
                'id' => $id,

            ];
            $data = array_merge($array1, $array2);
            return $data;
        }


    }

    public function includeCategories($cordinates)
    {
        $channelId [] =$cordinates->id;
        $channelFieldId = "'".implode ("','", array_unique($channelId))."'";
        $query = "SELECT * FROM retail_outlet_categories where channel_id IN ($channelFieldId)";
        $categories = DB::select($query);
        return $this->collection($categories, new NepalZoneSimplifiedTransformer(null), 'category');


    }


}