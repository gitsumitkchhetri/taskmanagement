<?php
/**
 * Created by PhpStorm.
 * User: samina
 * Date: 3/15/17
 * Time: 12:52 PM
 */

namespace App\Transformers;


use App\Model\Api\Categories;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{

    public function transform(Categories $category)
    {
        return [
            'id' => $category->id,
            'channel-id' => $category->channel_id,
            'created-by' => $category->created_by,
            'updated-by' => $category->updated_by,
            'name' => $category->name,
            'created-on' => $category->created_on,
            'updated-on' => $category->updated_on,
            'map-color' => $category->map_color,
        ];


    }


}