<?php
/**
 * Created by PhpStorm.
 * User: samina
 * Date: 3/11/17
 * Time: 2:07 PM
 */

namespace App\Transformers;

use App\Http\Controllers\Api\v1\ZoneDistrictTransformer;
use App\Model\Api\Categories;
use App\Model\Api\District;
use App\Model\Api\Town;
use App\Model\Api\Zones;
use App\Street;
use League\Fractal\Resource\NullResource;
use League\Fractal\TransformerAbstract;
use DB;

class ShopTransformer extends TransformerAbstract
{
    private $params = [];

    protected $defaultIncludes = [
        'category',
        'town',
        'street',
        'district',
        'zone',
    ];

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function transform($cordinates)
    {
        if (isset($cordinates[0]->gid) == false) {
            $id = $cordinates[0]->id;
        } else {
            $id = $cordinates[0]->gid;
        }
        $value = $this->params;
        $array1 = [];
        if ($value != null) {
            $fields = array_values($value);
            foreach ($fields as $field) {
                $key = str_replace("_", "-", $field);
                $array1[$key] = $cordinates->$field;

            }
            if (isset($cordinates->geojson) == true && isset($cordinates->centroid) == true) {
                $array2 = [
                    'id' => $id,
                    'geojson' => $cordinates->geojson,
                    'centroid' => $cordinates->centroid,

                ];
            } else {
                $array2 = [
                    'id' => $id,
                ];
            }

            $data = array_merge($array1, $array2);
            return $data;
        } else {
            foreach ($cordinates[0] as $key => $cordinate) {
                $keyName = str_replace("_", "-", $key);
                $array1[$keyName] = $cordinate;
            }

            $array2 = [
                'id' => $id,

            ];
            $data = array_merge($array1, $array2);
            return $data;
        }


    }


    public function includeCategory($cordinates)
    {
        $categoryId =$cordinates[0]->category_id;
        $id = explode (',',$categoryId);
        $category=Categories::where('id',$id)->first();
        return $this->Item($category, new CategoryTransformer(null), 'category');


    }

    public function includeTown($cordinates)
    {
        $townId =$cordinates[0]->town_id;
        if (empty($townId)) {
          return  $this->nullResource();
        }
        $id = explode (',',$townId);
        $town=Town::where('id',$id)->first();

        if (empty($town)) {
            return $this->nullResource();
        }

        return $this->Item($town, new TownTransformer(null), 'town');
    }
    public function includeStreet($cordinates)
    {
        $streetId =$cordinates[0]->street_id;

        if (empty($streetId)) {
           return $this->nullResource();
        }
        $id = explode (',',$streetId);
        $street=Street::where('id',$id)->first();

        if (empty($street)) {
            return $this->nullResource();
        }

        return $this->Item($street, new StreetTransformer(null), 'street');
    }
    public function includeDistrict($cordinates)
    {
        $districtId =$cordinates[0]->district_id;
        if (empty($districtId)) {
            return  $this->nullResource();
        }
        $id = explode (',',$districtId);
        $district=District::where('gid',$id)->first();

        if (empty($district)) {
            return $this->nullResource();
        }

        return $this->Item($district, new ZoneDistrictTransformer(null), 'district');
    }

    public function includeZone($cordinates)
    {
        $zoneId =$cordinates[0]->zone_id;

        if (empty($zoneId)) {
            return $this->nullResource();
        }
        $id = explode (',',$zoneId);
        $zones=Zones::where('gid',$id)->first();

        if (empty($zones)) {
            return $this->nullResource();
        }

        return $this->Item($zones, new ZoneTransformer(null), 'zone');
    }

    public function nullResource()
    {
        return new NullResource();
    }

}