<?php
/**
 * Created by PhpStorm.
 * User: samina
 * Date: 3/7/17
 * Time: 5:49 PM
 */

namespace App\Transformers;


use League\Fractal\TransformerAbstract;

class NepalZoneSimplifiedTransformer extends TransformerAbstract
{
    private $params = [];

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function transform($cordinates)
    {
        if (isset($cordinates->gid) == false){
            $id = $cordinates->id;
        } else {
            $id = $cordinates->gid;
    }
        $value = $this->params;
        $array1 = [];
        if ($value != null) {
            $fields = array_values($value);
            foreach ($fields as $field) {
                $key = str_replace("_", "-", $field);
                $array1[$key] = $cordinates->$field;

            }
            if (isset($cordinates->geojson) == true && isset($cordinates->centroid) == true) {
                $array2 = [
                    'id' => $id,
                    'geojson' => $cordinates->geojson,
                    'centroid' => $cordinates->centroid,

                ];
            } else {
                $array2 = [
                    'id' => $id,
                ];
            }

            $data = array_merge($array1, $array2);
            return $data;
        } else {
            foreach ($cordinates as $key => $cordinate) {
                $keyName = str_replace("_", "-", $key);
                $array1[$keyName] = $cordinate;
            }

            $array2 = [
                'id' => $id,

            ];
            $data = array_merge($array1, $array2);
            return $data;
        }


    }


}