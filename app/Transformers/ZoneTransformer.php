<?php
/**
 * Created by PhpStorm.
 * User: samina
 * Date: 3/7/17
 * Time: 5:49 PM
 */

namespace App\Transformers;


use App\Http\Controllers\Api\v1\ZoneDistrictTransformer;
use App\Model\Api\District;
use League\Fractal\Resource\NullResource;
use League\Fractal\TransformerAbstract;

class ZoneTransformer extends TransformerAbstract
{
    private $params = [];

    protected $defaultIncludes = [
        'districts'
    ];

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function transform($zones)
    {
        return [
            'id' => $zones->gid,
            'name' => $zones->name,
            'geojson' => $zones->geojson,
            'centroid' => $zones->centroid,
        ];


    }

    public function includeDistricts($cordinates)
    {
        $zoneId =$cordinates->gid;

        if (empty($zoneId)) {
            return  $this->nullResource();
        }
        $distict = District::where('zone_id',$zoneId)->get();

        if (empty($distict)) {
            return $this->nullResource();
        }

        return $this->Collection($distict, new DistrictTransformer(null), 'district');
    }

    public function nullResource()
    {
        return new NullResource();
    }


}