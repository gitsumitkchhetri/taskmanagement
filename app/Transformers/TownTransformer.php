<?php
/**
 * Created by PhpStorm.
 * User: samina
 * Date: 3/7/17
 * Time: 5:49 PM
 */

namespace App\Transformers;


use App\Model\Api\District;
use App\Model\Api\Town;
use App\Model\Api\Zones;
use League\Fractal\TransformerAbstract;

class TownTransformer extends TransformerAbstract
{


    public function transform($cordinates)
    {
        return [
            'id' => $cordinates->id,
            'name' => $cordinates->name,
        ];

    }


}