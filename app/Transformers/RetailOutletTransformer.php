<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 4/13/17
 * Time: 10:31 AM
 */

namespace App\Transformers;


use App\Model\Api\Categories;
use App\Model\Api\RetailOutlet;
use App\Model\Api\Town;
use App\Model\Api\User;
use App\RetailOutletUser;
use App\Street;
use League\Fractal\Resource\NullResource;
use League\Fractal\TransformerAbstract;

class RetailOutletTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'category','town','street'
    ];

    public function transform(RetailOutlet $retailOutlet)
    {
      $userData = User::where('id',$retailOutlet->verfied_by)->first();
        $registeredby = User::where('id',$retailOutlet->created_by)->first();

        return [
            'id' => $retailOutlet->id,
            'category-id' => $retailOutlet->category_id,
            'name' => $retailOutlet->name,
            'address' => $retailOutlet->address,
            'owner-name' => $retailOutlet->owner_name,
            'owner-phone' => $retailOutlet->owner_phone,
            'pan-no' => $retailOutlet->pan_no,
            'phone' => $retailOutlet->phone,
            'longitude' => $retailOutlet->longitude,
            'latitude' => $retailOutlet->latitude,
            'store-size' => $retailOutlet->store_size,
            'monthly-avg-business' => $retailOutlet->monthly_avg_business,
            'verification-by'=> $userData != null ? $userData->first_name.' '. $userData->last_name : null,
            'registered-on' => $retailOutlet->created_on,
            'registered-by' => $registeredby != null ? $registeredby->first_name.' '. $registeredby->last_name : null,
            'v-longitude' => $retailOutlet->v_longitude,
            'v-latitude' => $retailOutlet->v_latitude,
            'hotspot' => $retailOutlet->hotspot,
            'is-verified' => $retailOutlet->is_verified,
            'verified-on' => $retailOutlet->verified_on,
        ];

    }

    public function includeCategory(RetailOutlet $retailOutlet)
    {
        $category=Categories::where('id',$retailOutlet->category_id)->first();
        return $this->Item($category, new CategoryTransformer(null), 'category');


    }

    public function includeTown(RetailOutlet $retailOutlet)
    {
        $townId =$retailOutlet->town_id;
        if (empty($townId)) {
            return  $this->nullResource();
        }
        $town=Town::where('id',$townId)->first();

        if (empty($town)) {
            return $this->nullResource();
        }

        return $this->Item($town, new TownTransformer(null), 'town');
    }
    public function includeStreet(RetailOutlet $retailOutlet)
    {
        $streetId =$retailOutlet->street_id;

        if (empty($streetId)) {
            return $this->nullResource();
        }
        $street=Street::where('id',$streetId)->first();

        if (empty($street)) {
            return $this->nullResource();
        }

        return $this->Item($street, new StreetTransformer(null), 'street');
    }

    public function nullResource()
    {
        return new NullResource();
    }
}