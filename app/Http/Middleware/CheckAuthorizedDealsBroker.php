<?php

namespace App\Http\Middleware;

use Closure;
use App\Deal;
use App\Project;
use Auth;

class CheckAuthorizedDealsBroker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if( !(Auth::user()->roles->first()->name == "superuser" || Auth::user()->roles->first()->name == "superadmin") ){
            if($request->has("deal_id")){
                $checkUserAssocitaion = Deal::where("sales_person","=",Auth::user()->id)->where("id","=",$request->get("deal_id"))->get();

                if($checkUserAssocitaion->isEmpty()){
                    return redirect(PREFIX."/home")->withErrors(["alert-danger" => "You have no access to manipulate previous data."]);
                }

            }

            if($request->has("project_id")){
                $checkProject = Project::find($request->get("project_id"));


                if($checkProject == null){
                    return redirect(PREFIX."/home")->withErrors(["alert-danger" => "The Project you are trying to access doesn't exists on our record."]);
                }

                $checkDeal = Deal::where("id","=",$checkProject->deal_id)->where("sales_person","=",Auth::user()->id)->get();

                if($checkDeal->isEmpty()){
                    return redirect(PREFIX."/home")->withErrors(["alert-danger" => "You have no access to manipulate previous data."]);
                }

            }
        }


        return $next($request);
    }
}
