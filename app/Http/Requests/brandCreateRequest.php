<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class brandCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('catalog.catalog.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('catalog.brand.create');
        }

        if ($request->isMethod('GET') && $request->is('*/edit')) {
            return $request->user()->canDo('catalog.brand.edit');
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ($request->isMethod('POST') && $request->is('*/store')) {
            return [
//                'catalog_level_id'=>'required',
                'parent'=>'required',
                'name' => 'required'
            ];
        }

        if ($request->isMethod('POST') && $request->is('*/update')) {
            return [
//                'catalog_level_id'=>'required',
                'parent'=>'required',
                'name' => 'required'
            ];
        }

        return [
            //
        ];
    }

    public function messages()
    {
        return [
            'catalog_level_id.required' => 'At least one catalog level must be selected!',
            'parent.required' => 'At least one category should be selected!',
            'name.required' => 'Category name field is required!'
        ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
