<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('leads.leads.index');
        }
        if ($request->isMethod('GET') && $request->is('*/leads')) {
            return $request->user()->canDo('leads.leads.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('leads.leads.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('leads.leads.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('leads.leads.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('leads.leads.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ($request->isMethod('POST') && $request->is('*/leads')) {
            return [
                'name' => 'required|max:15',
                'email' => 'email|unique:crm_leads,email',
                'source' => 'required',

            ];
        }
        if ( $request->isMethod('PUT') || $request->isMethod('PATCH') ) {

            return [
                'name' => 'required|max:15',
                'email' => 'email|unique:crm_leads,email,' . $request->get('id'),
                'source' => 'required',

            ];
        }
    }
}
