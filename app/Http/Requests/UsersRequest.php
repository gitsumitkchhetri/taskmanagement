<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if ($request->isMethod('POST') && $request->is('*/user')) {
            return [
                'first_name' => 'required|max:15',
                'last_name' => 'required|max:15',
                'email' => 'required|email|unique:users',
                'username' => 'required|unique:users|max:10|regex:/^(?=.{1,15}$)[a-zA-Z][a-zA-Z0-9]*(?: [a-zA-Z0-9]+)*$/',
                'password' => 'required|password|min:6|confirmed',
                'password_confirmation' => 'required|min:6',
                'roles' => 'required',
'status'    =>  'required',
//                'phone_no'             =>  'sometimes|digits_between:10,16',
//                'pin_no'             =>  'sometimes|digits_between:2,20',
//                'imei_no'             =>  'sometimes|digits_between:2,30',
            ];
        }
        if ($request->isMethod('POST') && $request->is('*/update/*')) {
            return [
                'first_name' => 'required|max:15',
                'last_name' => 'required|max:15',
                'email' => 'required|email|unique:users,email,' . $request->get('id'),
                'username' => 'required|min:3|max:10|regex:/^(?=.{1,15}$)[a-zA-Z][a-zA-Z0-9]*(?: [a-zA-Z0-9]+)*$/|unique:users,username,' . $request->get('id'),
                'roles' => 'required',
'status'=>'required',
//                'phone_no'             =>  'sometimes|digits_between:10,16',
//                'pin_no'             =>  'sometimes|digits_between:2,20',
//                'imei_no'             =>  'sometimes|digits_between:2,30',
            ];
        }

        if ($request->isMethod('POST') && $request->is('*/change_password/*')) {
            return [
                'password' => 'required|password|min:6|confirmed',
                'password_confirmation' => 'required|min:6'
            ];
        }
        if ($request->isMethod('POST') && $request->is('*/update_password/*')) {
            return [
                'password' => 'required|password|min:6|confirmed',
                'password_confirmation' => 'required|min:6'
            ];
        }
        return [
            //
        ];
    }


    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
