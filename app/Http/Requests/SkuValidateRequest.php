<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SkuValidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('sku.sku.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('sku.sku.create');
        }

        if ($request->isMethod('GET') && $request->is('*/edit')) {
            return $request->user()->canDo('sku.sku.edit');
        }

        if ($request->is('*/destroy')) {
            return $request->user()->canDo('sku.sku.edit');
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ($request->isMethod('POST') && $request->is('*/store')) {
            return [
//                'catalog_level_id'=>'required',
                'catalog_id'=>'required',
                'name' => 'required'
            ];
        }

        if ($request->isMethod('POST') && $request->is('*/update')) {
            return [
//                'catalog_level_id'=>'required',
                'catalog_id'=>'required',
                'name' => 'required'
            ];
        }

        return [
            //
        ];
    }

    public function messages()
    {
        return [
            'catalog_id.required' => 'At least one brand must be selected!',
            'name.required' => 'SKU name is required!'
        ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
