<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ProjectEquipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('project_equipments.project_equipments.index');
        }
        if ($request->isMethod('GET') && $request->is('*/addresses')) {
            return $request->user()->canDo('project_equipments.project_equipments.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('project_equipments.project_equipments.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('project_equipments.project_equipments.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('project_equipments.project_equipments.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('project_equipments.project_equipments.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {





        if ($request->isMethod('POST') && $request->is('*/project_equipments')) {
            return [
                'project_id' => 'required',
                'task_id' => 'required',
                'title' => 'required',
                'cost' => 'required|numeric',

            ];
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
            return [
                'title' => 'required|unique:cs_project_equipments,title,' . $request->get("id"),
                'project_id' => 'required',
                'task_id' => 'required',
                'cost' => 'required|numeric',

            ];
        }
    }
}
