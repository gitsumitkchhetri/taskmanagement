<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('projects.projects.index');
        }
        if ($request->isMethod('GET') && $request->is('*/projects')) {
            return $request->user()->canDo('projects.projects.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('projects.projects.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('projects.projects.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('projects.projects.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('projects.projects.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if ($request->isMethod('POST') && $request->is('*/projects')) {
            return [
                'title' => 'required|max:100',
                'position' => 'numeric',
                'job_types' => 'required',
                'sub_systems' => 'required',
                'project_type' => 'required',
                //'total_budget' => 'required|numeric',
                'deal_id' => 'required',

            ];
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
            return [
                'title' => 'required|unique:cs_projects,deal_id,' . $request->get("id"),
                'position' => 'numeric',
               // 'total_budget' => 'required|numeric',
                'deal_id' => 'required',
                'job_types' => 'required',
                'sub_systems' => 'required',
                'project_type' => 'required',

            ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'deal_id.required'  => 'You must associate a Deal with the Project Phase.',
        ];
    }
}
