<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class ClientsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('clients.clients.index');
        }
        if ($request->isMethod('GET') && $request->is('*/clients')) {
            return $request->user()->canDo('clients.clients.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('clients.clients.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('clients.clients.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('clients.clients.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('clients.clients.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {



        if ($request->isMethod('POST') && $request->is('*/clients')) {
            return [
                'first_name' => 'required|max:20',
                'last_name' => 'required|max:20',
                'phone' => 'required',
                'email' => 'required|email|unique:crm_clients,email',
                'address' => 'required',
                'contact_no' => 'required',


            ];
        }
        if ( $request->isMethod('PUT') || $request->isMethod('PATCH') ) {

            return [
                'first_name' => 'required|max:20',
                'last_name' => 'required|max:20',
                'phone' => 'required',
                'email' => 'required|email|unique:crm_clients,email,' . $request->get('id'),

            ];
        }
    }
}
