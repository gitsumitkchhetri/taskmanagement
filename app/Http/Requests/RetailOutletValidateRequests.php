<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RetailOutletValidateRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('retail_outlet.retail_outlet.create');
        }
        if ($request->isMethod('GET') && $request->is('*/retail_outlet')) {
            return $request->user()->canDo('retail_outlet.retail_outlet.index');
        }

        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('retail_outlet.retail_outlet.index');
        }

        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('retail_outlet.retail_outlet.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy')) {
            return $request->user()->canDo('retail_outlet.retail_outlet.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return [
//                'catalog_level_id'=>'required',
//                'name' => 'required'
//            ];
//        }
//
        if ($request->isMethod('POST') && $request->is('*/update')) {
            return [
                'name' => 'required',
                'category_id' => 'required',
                'sku_id' => 'required',
                'street_id' => 'required',
                'owner_name' => 'required',
                'phone' => 'required',
                'latitude' => 'required|latitude',
                'longitude' => 'required|longitude',
                'catalog_id' => 'required',
            ];
        }

        return [
            //
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => 'At least one category must be selected!',
            'street_id.required' => 'At least one street must be selected!',
            'phone.required' => 'Phone no. must be provided!',
            'catalog_id.required' => 'At least one brand must be selected!',
            'sku_id.required' => 'At least one SKU must be selected!',
        ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}