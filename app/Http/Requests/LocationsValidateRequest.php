<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Input;

class LocationsValidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('locations.locations.create');
        }
        if ($request->isMethod('GET') && $request->is('*/locations')) {
            return $request->user()->canDo('locations.locations.index');
        }

        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('locations.locations.index');
        }

        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('locations.locations.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy')) {
            return $request->user()->canDo('locations.locations.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if(!Input::has('type') || Input::get('type') === 'town'){
            if ($request->isMethod('POST') && $request->is('*/store')) {
                return [
                    'district_id'=>'required',
                    'name' => 'required|unique:town'
                ];
            }

            if ($request->isMethod('POST') && $request->is('*/update')) {
                return [
                    'district_id' => 'required',
                    'name' => 'required|unique:town,name,' . $request->get('id'),
                ];
            }


        }elseif(Input::has('type') && Input::get('type') === 'street')
        {
            if ($request->isMethod('POST') && $request->is('*/store')) {
                return [
                    'town_id'=>'required',
                    'name' => 'required'
                ];
            }

            if ($request->isMethod('POST') && $request->is('*/update')) {
                return [
                    'town_id'=>'required',
                    'name' => 'required'
                ];
            }

        }else{

        }


        return [
            //
        ];
    }

    public function messages()
    {
        return [
            'town_id.required' => 'At least one town must be selected',
            'district_id.required' => 'At least one district must be selected',
        ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
