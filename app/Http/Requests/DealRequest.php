<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('deals.deals.index');
        }
        if ($request->isMethod('GET') && $request->is('*/deals')) {
            return $request->user()->canDo('deals.deals.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('deals.deals.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('deals.deals.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('deals.deals.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('deals.deals.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {


        if ($request->isMethod('POST') && $request->is('*/deals')) {
            if($request->has('status') && $request->get('status') == "Failed"){
                return [
                    'client_id' => 'required',
                    'title' => 'required|max:50',
                    'description' => 'required',
                    'sales_person' => 'required',

                    'proposal_url' => 'required',
                    'portal_number' => 'required|max:50',
                    'status' => 'required',
                    'loss_remarks' => 'required',
                    'cost' => 'required|numeric',
                    'expected_close_date' => 'required|date_format:Y-m-d|after_or_equal:today',


                ];
            }else{
                return [
                    'client_id' => 'required',
                    'title' => 'required|max:50',
                    'description' => 'required',
                    'sales_person' => 'required',

                    'proposal_url' => 'required',
                    'portal_number' => 'required|max:50',
                    'status' => 'required',
                    'cost' => 'required|numeric',
                    'expected_close_date' => 'required|date_format:Y-m-d|after_or_equal:today',


                ];
            }

        }
        if ( $request->isMethod('PUT') || $request->isMethod('PATCH') ) {

            return [
                'client_id' => 'required',
                'title' => 'required|max:50',
                'description' => 'required',
                'sales_person' => 'required',

            ];
        }


    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'expected_close_date.after_or_equal' => 'The Expected Close Date cannot be past date',
            'title.required' => 'A title is required',
            'client_id.required'  => 'You must associate a Client with the Deal.',
            'sales_person.required'  => 'You must associate a Sales Person with the Deal.',
        ];
    }
}
