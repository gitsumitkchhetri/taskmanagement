<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class TechnicianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('technicians.technicians.index');
        }
        if ($request->isMethod('GET') && $request->is('*/technicians')) {
            return $request->user()->canDo('technicians.technicians.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('technicians.technicians.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('technicians.technicians.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('technicians.technicians.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('technicians.technicians.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {


        return [
            'name' => 'required|max:100',
            'level_id' => 'required|numeric',

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'level_id.required'  => 'You must associate a Technician Level with the Technician.',
        ];
    }
}
