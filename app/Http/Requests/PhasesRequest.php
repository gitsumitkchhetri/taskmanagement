<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PhasesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('phases.phases.index');
        }
        if ($request->isMethod('GET') && $request->is('*/phases')) {
            return $request->user()->canDo('phases.phases.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('phases.phases.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('phases.phases.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('phases.phases.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('phases.phases.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {



            if($request->has('task_type')){
                if($request->get('task_type') == "Equipment"){
                    return [
                        'total_cost' => 'required|max:20',
                        'cost_changes' => 'required|max:20',
                    ];
                }elseif($request->get('task_type') == "Labor"){
                    return [
                        'total_cost' => 'required|max:20',
                        'cost_changes' => 'required|max:20',
                    ];
                }else{
                    return [
                        'total_cost' => 'required|max:20',
                        'cost_changes' => 'required|max:20',
                        'cost' => 'required',
                    ];
                }

            }else{
                return [
                    'phase_type' => 'require'
                    ];
            }




    }
}
