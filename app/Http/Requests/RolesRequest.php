<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {

      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
      if ($request->isMethod('POST')) {
        return [
          'name' => 'required|unique:roles',
          'permissions' => 'required'
        ];
      }
      if ($request->isMethod('PATCH') || $request->isMethod('PUT')) {
        return [
            'name' => 'required|unique:roles,name,'. $request->get('id'),
            'permissions' => 'required'
        ];
      }
      return [
          //
      ];

    }
}
