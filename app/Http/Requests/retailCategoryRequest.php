<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class retailCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('retail_channel_category.retail_channel_category.create');
        }
        if ($request->isMethod('GET') && $request->is('*/retail_channel_category')) {
            return $request->user()->canDo('retail_channel_category.retail_channel_category.index');
        }

        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('retail_channel_category.retail_channel_category.index');
        }

        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('retail_channel_category.retail_channel_category.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy')) {
            return $request->user()->canDo('retail_channel_category.retail_channel_category.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return [
//                'catalog_level_id'=>'required',
//                'name' => 'required'
//            ];
//        }
//
//        if ($request->isMethod('POST') && $request->is('*/update')) {
//            return [
//                'catalog_level_id'=>'required',
//                'name' => 'required'
//            ];
//        }

        return [
            //
        ];
    }

//    public function messages()
//    {
////        return [
////            'catalog_level_id.required' => 'At least one catalog level must be selected',
////            'name.required' => 'Category name field is required'
////        ];
//    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
