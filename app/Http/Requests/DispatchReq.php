<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DispatchReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('dispatch.dispatch.index');
        }
        if ($request->isMethod('GET') && $request->is('*/technicians')) {
            return $request->user()->canDo('dispatch.dispatch.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('dispatch.dispatch.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('dispatch.dispatch.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('dispatch.dispatch.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('dispatch.dispatch.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {


        return [
            'project_id' => 'required',
            'technician_id' => 'required',
            //'from_date' => 'required|required',
            'from_date' => 'required|required|date_format:Y-m-d H:i|after_or_equal:today',
            'to_date' => 'required|required|date_format:Y-m-d H:i|after_or_equal:today',

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'project_id.required'  => 'You must associate a Project while dispatching.',
            'technician_id.required'  => 'You must associate a Technician while dispatching.',
        ];
    }
}
