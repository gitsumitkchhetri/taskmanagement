<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ProjectPhasesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('project_phases.project_phases.index');
        }
        if ($request->isMethod('GET') && $request->is('*/project_phases')) {
            return $request->user()->canDo('project_phases.project_phases.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('project_phases.project_phases.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('project_phases.project_phases.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('project_phases.project_phases.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('project_phases.project_phases.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if ($request->isMethod('POST') && $request->is('*/project_phases')) {
            return [
                'title' => 'required|max:50|unique:cs_project_phases,title',
                'type' => 'required|max:20',
                'task_title' => 'required|max:20',
                'position' => 'required|numeric',
                'task_position' => 'required|numeric',
                'description' => 'required',
                'code' => 'required|unique:cs_project_tasks,code',


            ];
        }
        if ( $request->isMethod('PUT') || $request->isMethod('PATCH') ) {

            return [
                'title' => 'required|max:50||unique:cs_project_phases,title'.$request->get('id'),
                'position' => 'required|numeric',
                'description' => 'required',

            ];
        }
    }
}
