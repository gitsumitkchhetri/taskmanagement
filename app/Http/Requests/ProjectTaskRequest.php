<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ProjectTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        // Determine if the user is authorized to create an entry,
        if ($request->isMethod('GET') && $request->is('*/index')) {
            return $request->user()->canDo('project_tasks.project_tasks.index');
        }
        if ($request->isMethod('GET') && $request->is('*/project_tasks')) {
            return $request->user()->canDo('project_tasks.project_tasks.index');
        }

        if ($request->isMethod('GET') && $request->is('*/create')) {
            return $request->user()->canDo('project_tasks.project_tasks.create');
        }


//        if ($request->isMethod('POST') && $request->is('*/store')) {
//            return $request->user()->canDo('project_tasks.project_tasks.store');
//        }


        // Determine if the user is authorized to update an entry.
        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            return $request->user()->canDo('project_tasks.project_tasks.edit');
        }
        // Determine if the user is authorized to delete an entry.
        if ($request->isMethod('DELETE') || $request->is('*/destroy/*')) {
            return $request->user()->canDo('project_tasks.project_tasks.destroy');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if ($request->isMethod('POST') && $request->is('*/project_tasks')) {
            return [
                'title' => 'required|max:20',
                'type' => 'required',
                'position' => 'numeric',
                'phase_id' => 'required',
                'code' => 'required|unique:cs_project_tasks,code',

            ];
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
            return [
                'title' => 'required|max:20',
                'type' => 'required',
                'position' => 'numeric',
                'phase_id' => 'required',
                'code' => 'required|unique:cs_project_tasks,code,' . $request->get("id"),

            ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'phase_id.required'  => 'You must associate a Project Task with the Project Phase.',
        ];
    }
}
