<?php

namespace App\Http\Controllers\Admin\phases;

use App\ProjectEquipment;
use App\ProjectTask;
use App\TaskPerProject;
use App\ProjectTaskCost;
use App\ProjectEstimationCost;
use App\ProjectTechnicians;
use App\Http\Requests\PhasesRequest;
use App\ProjectPhases;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class PhasesController extends Controller
{

    public function __construct(ProjectEquipment $projectEquipment,ProjectEstimationCost $projectEstimationCost,User $users,Permission $permisionModel,ProjectPhases $clients,TaskPerProject $tasks,ProjectTask $projectTask,ProjectTechnicians $projectTechnicians,ProjectTaskCost $projectTaskCost)
    {
        $this->pageTitle = "Project Phases Management";
        $this->model = $clients;
        $this->users = $users;
        $this->projectEstimationCost = $projectEstimationCost;
        $this->projectTechnicians = $projectTechnicians;
        $this->projectEquipment = $projectEquipment;
        $this->taskPerProject = $tasks;
        $this->projectTask = $projectTask;
        $this->projectTaskCost = $projectTaskCost;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/phases";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["project_id"] = Input::get("project_id");
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = "ProjectPhases";
        return view('backend.projects.project-phases',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(Input::all());

        $data["project_id"] = Input::get("project_id");
        $data["task_id"] = Input::get("task_id");

        $taskPerProject = $this->taskPerProject->find(Input::get("task_id"));
//        $projectTask = $this->projectTask->find(Input::get("task_id"));

        $isSetTaskPerProject = $taskPerProject == null ? false : true;

        $taskDetails = null;
        if($taskPerProject != null){
            if($taskPerProject->type == "Others"){

                $taskDetails = $this->projectTaskCost->where("project_id","=",Input::get("project_id"))->where("task_id","=",Input::get("task_id"))->first();

            }
        }


        $projectEstimation = $this->projectEstimationCost->where("project_id","=",Input::get("project_id"))->where("task_id","=",Input::get("task_id"))->first();

        $total_cost = 0;
        $cost_changes = 0;

        if($projectEstimation != null){
            $total_cost = $projectEstimation->total_cost;
            $cost_changes = $projectEstimation->cost_changes;
        }

        $data["isSetTaskPerProject"] = $isSetTaskPerProject;
        $data["taskDetails"] = $taskDetails;
        $data["total_cost"] = $total_cost;
        $data["cost_changes"] = $cost_changes;
        $data["taskType"] = $taskPerProject->type;
        $data["routeName"] = "phases.store";

        //dd($taskDetails);

        if($taskDetails == null){
            return view('backend.phases.create',$data);
        }else{
            return view('backend.phases.edit',$data);
        }






        //$inputs = Input::all();

        return view('backend.clients.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhasesRequest $phasesRequest)
    {

        //dump(Input::all());
        $task = Input::get("task_type");
        //$projectTask = $this->projectTask->find(Input::get("task_id"));
        $taskPerProject = $this->taskPerProject->find(Input::get("task_id"));
        //dd($projectTask);
        //$taskPerProjectCode = null;
//        if($projectTask != null){
//                $taskPerProject = $this->taskPerProject->where("code","=",$projectTask->code)->first();
//                $taskPerProjectCode = $taskPerProject == null ? null : $taskPerProject->code;
//        }


        $taskPerProjectId = 0;
//        if($taskPerProjectCode != null){
//            $taskPerProjectId = $this->taskPerProject->where("code","=",$projectTask->code)->first()->id;
//        }else{
//            $taskPerProjectData = $taskPerProjectCode->toArray();
//            unset($taskPerProjectData["id"]);
//            $taskPerProject = $this->taskPerProject->create($taskPerProjectData);
            $taskPerProjectId = $taskPerProject->id;
//        }

        if($task == "Others"){
            $other = $this->projectTaskCost->where("project_id","=",Input::get("project_id"))->where("task_id","=",$taskPerProjectId)->first();
            if($other != null){
                $other->cost = Input::get("cost");
                $other->save();
            }else{
                $insertData = [];
                $insertData["project_id"] = Input::get("project_id");
                $insertData["task_id"] = $taskPerProjectId;
                $insertData["cost"] = Input::get("cost");
                $this->projectTaskCost->create($insertData);
            }
        }
//        elseif($task == "Equipment"){
//            $equipment = $this->projectEquipment->where("project_id","=",Input::get("project_id"))->where("task_id","=",$taskPerProjectId)->first();
//            if($equipment != null){
//                $equipment->cost = Input::get("cost");
//                $equipment->save();
//            }else{
//                $insertData = [];
//                $insertData["project_id"] = Input::get("project_id");
//                $insertData["task_id"] = $taskPerProjectId;
//                $insertData["cost"] = Input::get("cost");
//                $insertData["title"] = Input::get("title");
//                $insertData["description"] = Input::get("description");
//                $this->projectEquipment->create($insertData);
//            }
//        }else{
//            $technical = $this->projectTechnicians->where("project_id","=",Input::get("project_id"))->where("task_id","=",$taskPerProjectId)->first();
//            if($technical != null){
//                $technical->cost = Input::get("cost");
//                $technical->date = date("Y-m-d");
//                $technical->save();
//            }else{
//                $insertData = [];
//                $insertData["project_id"] = Input::get("project_id");
//                $insertData["task_id"] = $taskPerProjectId;
//                $insertData["cost"] = Input::get("cost");
//                $this->projectTechnicians->create($insertData);
//            }
//        }

        $projectEstimate = $this->projectEstimationCost->where("project_id","=",Input::get("project_id"))->where("task_id","=",$taskPerProjectId)->first();
        if($projectEstimate != null){
            $projectEstimate->total_cost = Input::get("total_cost");
            $projectEstimate->cost_changes = Input::get("cost_changes");
            $projectEstimate->save();
        }else{
            $estimateData = [];
            $estimateData["project_id"] = Input::get("project_id");
            $estimateData["task_id"] = $taskPerProjectId;
            $estimateData["cost_changes"] = Input::get("cost_changes");
            $estimateData["total_cost"] = Input::get("total_cost");
            $this->projectEstimationCost->create($estimateData);
        }


        return redirect(PREFIX.'/projects/'.Input::get("project_id"))->withErrors(["alert-success" => "Successfully Updated Task Details"]);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dump($techLvl);
        $clients = ProjectPhases::findOrFail($id);
        //dd($data);
        //dd($clients);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "clients.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.clients.edit',$data,compact('clients'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PhasesRequest $clientRequest)
    {
        try{
            $this->model->updateData($clientRequest->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Phase."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->taskPerProject->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Phase."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
