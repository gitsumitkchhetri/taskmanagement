<?php

namespace App\Http\Controllers\Admin\configs;

use App\Http\Requests\ProjectPhasesRequest;
use App\ProjectPhases;
use App\Permission;
use App\ProjectTask;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Config;


class ProjectPhasesController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,ProjectPhases $projectPhases,ProjectTask $projectTask)
    {
        $this->pageTitle = "Project Phases";
        $this->model = $projectPhases;
        $this->projectTask = $projectTask;
        $this->users = $users;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/project_phases";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('keywords'));
        return view('backend.project_phases.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["tasksType"] = ["" => "None"]+Config::get('taskTypeConfig.tasksType');
        return view('backend.project_phases.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectPhasesRequest $projectPhasesRequest)
    {
        //try{
            $phaseObj = $this->model->addData($projectPhasesRequest->except('task_title','task_position','type','code'));
            $taskData = $projectPhasesRequest->only('task_title','task_position','type','code');
            $taskData['phase_id'] = $phaseObj->id;
            $taskData['title'] = $taskData['task_title'];
            $taskData['position'] = $taskData['task_position'];
            unset($taskData['task_title']);
            unset($taskData['task_position']);
            $this->projectTask->addData($taskData);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Project Phase."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectPhases $projectPhases,$id )
    {
        //dump($techLvl);
        $projectPhase = ProjectPhases::findOrFail($id);
        //dd($data);
        //dd($projectPhase);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "project_phases.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.project_phases.edit',$data,compact('projectPhase'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectPhasesRequest $projectPhasesRequest, $id)
    {
        try{
            $this->model->updateData($projectPhasesRequest->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Phase."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Phase."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
