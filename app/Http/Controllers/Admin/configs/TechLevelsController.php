<?php

namespace App\Http\Controllers\Admin\configs;

use App\Http\Requests\TechLevelRequest;
use App\TechLevel;
use App\Permission;
use App\Technician;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class TechLevelsController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,TechLevel $techLevel,Technician $technician)
    {
        $this->pageTitle = "Tech Levels";
        $this->model = $techLevel;
        $this->users = $users;
        $this->technician = $technician;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/tech_levels";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('keywords'));
        return view('backend.tech_levels.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        return view('backend.tech_levels.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TechLevelRequest $leadRequest)
    {
        //try{
            $this->model->addData($leadRequest->all());
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Lead."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TechLevel $techLvl,$id )
    {
        //dump($techLvl);
        $techLvl = TechLevel::findOrFail($id);
        //dd($data);
        //dd($techLvl);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "tech_levels.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.tech_levels.edit',$data,compact('techLvl'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TechLevelRequest $leadRequest, $id)
    {
        try{
            $this->model->updateData($leadRequest->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Lead."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $checkAssociation = $this->technician->where("level_id","=",$id)->get();
            if(!$checkAssociation->isEmpty()){
                return redirect()->back()->withErrors(["alert-danger" => "The tech Level is associated with some Technicians.Please,remove those technicians before removing this level."]);
            }

            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Lead."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
