<?php

namespace App\Http\Controllers\Admin\config;

use App\Http\Controllers\Controller;
use App\Http\Requests\LanguageRequest;
use App\Language;
use File;
use Image;
use Input;
use Redirect;
use Validator;

class languageController extends Controller
{

    public function __construct(Language $language)
    {

        $this->pageTitle = "Language Configuration";
        $this->model = $language;
        $this->redirectUrl = PREFIX . "/config/pages/language";

    }

    public function index()
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['redirectUrl'] = $this->redirectUrl;
        $data['allDatas'] = $this->model->getAllData(Input::all());
        return view('backend.language.index')->with('datas', $data);
    }

    public function create()
    {
        return view('backend.language.create');
    }

    public function store(LanguageRequest $request)
    {

        $rules = [
            'lang_for' => 'required',
            'default' => 'required',
            'flag' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
            'title' => 'required|max:15',
            'file' => 'required|max:2048|json',
            'slug' => 'required|unique:languages|max:2|regex:/^.*(?=.*[a-z]).*$/|',

        ];

        $messages =
            ['lang_for.required' => 'Language For must be selected!',
            'default.required' => 'Please Select Default Language',
            'slug.max' => 'Slug must contain only two letters !',
        ];

        $validate = Validator::make(Input::all(), $rules, $messages);

        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput($request->all());
        }

        try {
            $attributes = $request->all();

            if ($request->default == "yes" && $request->lang_for == "backend") {
                if ($this->checkDefForBack()) {
                    return redirect()->back()->withInput()->withErrors(['alert-danger' => 'Only one language can be choosen as default']);

                } else {
                    $this->saveImage($request, $attributes);
                    if ($this->model->create($attributes)) {
                        $this->defaultLang();
                    }

                    return redirect($this->redirectUrl)->withErrors(['alert-success' => 'Successfully Added']);
                }

            } else {
                $this->saveImage($request, $attributes);
                $language = $this->model->create($attributes);
                $this->defaultLang();
                return redirect($this->redirectUrl)->withErrors(['alert-success' => 'Successfully Added']);
            }

        } catch (Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger' => 'Data was not saved!']);
        }

    }

    public function edit(LanguageRequest $request)
    {
        $pageTitle = $this->pageTitle;
        $data = $this->model->find(Input::get('id'));
        if (empty($data)) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger' => 'Data was not found!']);
        }
        return view('backend.language.edit', compact('data', 'pageTitle'));
    }

    public function update(LanguageRequest $request)
    {

        $rules = [
            'lang_for' => 'required',
            'default' => 'required',
            'flag' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
            'title' => 'required|max:15',
            'file' => 'max:2048|json',
            'slug' => 'required|max:2|regex:/^.*(?=.*[a-z]).*$/|unique:languages,slug,' . $request->get('id'),

        ];

        $messages =
            ['lang_for.required' => 'Language For must be selected!',
            'default.required' => 'Please Select Default Language',
            'slug.max' => 'Slug must contain only two letters !',
        ];

        $validate = Validator::make(Input::all(), $rules, $messages);

        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput($request->all());
        }

        $languageData = $this->model->where('id', $request->id)->first();

        try
        {
            $attributes = $request->all();

            if ($request->default == "yes" && $request->lang_for == "backend") {
                if ($this->checkDefForBack($request->id)) {
                    return redirect()->back()->withInput()->withErrors(['alert-danger' => 'Only one language can be choosen as default']);

                } else {
                    $this->saveImage($request, $attributes);
                    if ($languageData->update($attributes)) {
                        $this->defaultLang();
                        return redirect($this->redirectUrl)->withErrors(['alert-success' => 'Successfully Updated']);
                    }
                }
            } else {
                $this->saveImage($request, $attributes);
                $languageData->update($attributes);
                $this->defaultLang();
                return redirect($this->redirectUrl)->withErrors(['alert-success' => 'Successfully Updated']);
            }
        } catch (Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger' => 'Data was not saved!']);
        }

    }

    public function destroy(LanguageRequest $request)
    {
        try
        {
            $language = $this->model->find(Input::get('id'));
            if (empty($language)) {
                return redirect($this->redirectUrl)->withErrors(['alert-danger' => 'Data was not found!']);
            }
            if (null !== $language->flag && file_exists(public_path() . '/uploads/language/flag/' . $language->flag)) {
                \File::delete(public_path() . '/uploads/language/flag/' . $language->flag);
            }
            if (null !== $language->file && file_exists(public_path() . '/uploads/language/file/' . $language->file)) {
                \File::delete(public_path() . '/uploads/language/file/' . $language->file);
            }
            $t = $language->delete();
            return redirect()->back()->withErrors(['alert-success' => 'Successfully Deleted']);
        } catch (Exception $e) {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

    }

    public function toggleStatus()
    {
        $data = $this->model->find(Input::get('id'));
        if ($data != null) {
            if ($data->status == 'active') {
                $data->status = 'inactive';
            } else {
                $data->status = 'active';
            }
            $data->save();
            return redirect()->back();
        } else {
            return redirect($this->redirectUrl)->withErrors(['alert-danger' => 'status couldnot be changed']);
        }
    }

    public function SetDefault()
    {
        $data = $this->model->find(Input::get('id'));
        if ($data != null) {
            if ($data->default == 'yes') {
                $data->default == 'no';
            } else {
                $data->default == 'yes';
            }
            $data->save();
            return redirect()->back();
        } else {
            return redirect($this->redirectUrl)->withErrors(['alert-danger' => 'The language could not be set as default']);
        }

    }

    public function changeLang()
    {
        $id = Input::get('id');
        $langs = Language::where('id', $id)->first()->slug;
        $old = \Session()->forget('default_language');
        $newLang = \Session()->put('default_language', $langs);
        $lang = \Session()->get('default_language');
        localize();
        return redirect()->back();
    }

    public function defaultLang()
    {
        $last_lang = Language::all()->last();

        $default = $last_lang->default;
        $lang_for = $last_lang->lang_for;
        if ($default == 'yes' && $lang_for == 'backend') {
            \Session::put('default_language', $last_lang->slug);
            localize();
        } else {
            \Session::forget('default_language');
        }

    }

    public function checkDefForBack($id = null)
    {
        $def = Language::where('lang_for', 'backend')->where('default', 'yes')->where('id', '!=', $id)->first();
        if ($def != null) {
            return true;
        } else {
            return false;
        }

    }
    public function saveImage($request, &$attributes)
    {

        if (!empty($request->file('flag'))) {
            $image = $request->file('flag');
            $path = public_path() . '/uploads/language/flag';
            if (is_dir($path) != true) {
                \File::makeDirectory($path, 0777, true);
            }
            $filename = uniqid() . '.' . $request->file('flag')->getClientOriginalExtension();
            $img = \Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path . '/' . $filename);
            $attributes['flag'] = $filename;
        }

        if (!empty($request->file('file'))) {
            $filename = uniqid() . '.' . $request->file('file')->getClientOriginalExtension();
            $path = public_path() . '/uploads/language/file';
            if (is_dir($path) != true) {
                \File::makeDirectory($path, 0777, true);
            }
            $request->file('file')->move(public_path('uploads/language/file/'), $filename);
            $attributes['file'] = $filename;
        }
        return 0;
    }

}
