<?php
namespace App\Http\Controllers\Admin\config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use Input;
use App\Log;
use App\EmailTemplate;
use Flash;
use App\Http\Requests\EmailTemplateValidateRequest;

class emailController extends Controller
{
    /**
       * Determine if the user is authorized to make this request.
       *
       * @return bool
       */
      public function __construct(EmailTemplate $emailTemplate
      ){
        $this->model = $emailTemplate;
        $this->redirectUrl = PREFIX."/config/pages/email";
      }
      /**
       * store a new log
       *
       * @return index page
       */
  	public function store(Request $request)
  	{
    }
    /**
       * show all log
       *
       * @return list of all log
       */
      public function index()
      {
        $pageTitle = 'Email Template List';
        $email = $this->model->getAllData(Input::all());
        return view('backend.email.index',compact('email','pageTitle'));
      }
      /**
       * Create a new log
       *
       * @return desigantion creation page
       */
      public function create()
      {
      }
      /**
       * update log
       *
       * @return back
       */
      public function update(EmailTemplateValidateRequest $request)
      {
        $id = Input::get('id');
         $email = EmailTemplate::find($id);
         $u_status = $email->update($request->all());
        if($u_status)
        {
          Flash::success('Successfully Updated!');
          return redirect(PREFIX.'/config/pages/email');
        }else{
          Flash::error('Email Template update Failed!');
          return redirect()->back();
        }
      }
      /**
       * show log
       *
       * @return void
       */
      public function show($id)
      {
      }
      /**
       * delete log
       *
       * @return void
       */
      public function destroy()
      {
      }
      /**
       * edit page for log
       *
       * @return edit page
       */
      public function edit(EmailTemplateValidateRequest $request)
      {
        $id = Input::get('id');
        $pageTitle = 'Edit Email Template';
        $email = EmailTemplate::find($id);
        if($email == null){
          return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        return view('backend.email.edit',compact('email','pageTitle'));
      }
}
