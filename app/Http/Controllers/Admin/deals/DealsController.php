<?php

namespace App\Http\Controllers\Admin\deals;

use App\Address;
use App\Clients;
use App\Deal;
use App\Http\Requests\DealRequest;
use App\Permission;
use App\Proposal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Auth;


class DealsController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,Deal $deal,Proposal $proposal,Clients $clients)
    {
        $this->pageTitle = "Deals Management";
        $this->model = $deal;
        $this->users = $users;
        $this->clients = $clients;
        $this->proposal = $proposal;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/deals";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('keywords'),Auth::user()->id);
        return view('backend.deals.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["clients"] = $this->clients->selectRaw("CONCAT(first_name,' ',last_name) AS name,id")->pluck("name","id")->toArray();
        $data["sales_persons"] = $this->users->pluck("username","id")->toArray();
        return view('backend.deals.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealRequest $dealReq)
    {
        //dd($dealReq->except('proposal_url','portal_number','loss_remarks','cost','expected_close_date','status','file'));
        //try{
            $dealObj = $this->model->addData($dealReq->except('proposal_url','portal_number','loss_remarks','cost','expected_close_date','status','file'));
            $proposalData = $dealReq->only('proposal_url','portal_number','loss_remarks','cost','expected_close_date','status','file');
            $proposalData["deal_id"] = $dealObj->id;
            $this->proposal->addData($proposalData);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Project Deal."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dump($techLvl);
        $deal = Deal::findOrFail($id);
        //dd($data);
        //dd($deal);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "deals.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["clients"] = $this->clients->selectRaw("CONCAT(first_name,' ',last_name) AS name,id")->pluck("name","id")->toArray();
        $data["sales_persons"] = $this->users->pluck("username","id")->toArray();
        return view('backend.deals.edit',$data,compact('deal'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DealRequest $dealReq, $id)
    {
        try{
            $this->model->updateData($dealReq->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Deal."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Deal."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
