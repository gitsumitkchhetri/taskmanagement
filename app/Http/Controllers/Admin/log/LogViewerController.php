<?php

namespace App\Http\Controllers\Admin\log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use Input;
use App\Log;
use App\Http\Requests\LogRequest;
class LogViewerController extends Controller
{

  public function __construct(Log $log)
  {
      $this->pageTitle = "Log Management";
      $this->model = $log;
      $this->redirectUrl = PREFIX."/log";
  }

  public function index()
  {
      $pageTitle = $this->pageTitle;
      //$data = $this->model->getAllData(Input::all());
      return redirect('/system/log/pages/Exception-logs');
  } 


}
