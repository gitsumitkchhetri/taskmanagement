<?php

namespace App\Http\Controllers\Admin\projects;

use App\Clients;
use App\Deal;
use App\ProjectEquipment;
use App\ProjectEstimationCost;
use App\ProjectPhases;
use App\ProjectPerPhase;
use App\Http\Requests\ProjectRequest;
use App\Project;
use App\Permission;
use App\ProjectTask;
use App\ProjectTaskCost;
use App\ProjectTechnicians;
use App\TaskPerProject;
use App\TechLevel;
use App\Technician;
use App\Timesheet;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use DB;
use Config;
use Auth;


class ProjectsController extends Controller
{

    public function __construct(
        User $users,
        Permission $permisionModel,
        Project $projects,
        ProjectPhases $projectPhases,
        Deal $deal,
        Clients $clients,
        ProjectPerPhase $projectPerPhase,
        ProjectTask $projectTask,
        ProjectEquipment $projectEquipment,
        ProjectEstimationCost $projectEstimationCost,
        ProjectTaskCost $projectTaskCost,
        TaskPerProject $taskPerProject,
        Timesheet $timesheet,
        Technician $technician,
        TechLevel $techLevel,
        ProjectTechnicians $projectTechnicians
    )
    {
        $this->pageTitle = "Project Management";
        $this->model = $projects;
        $this->users = $users;
        $this->timesheet = $timesheet;
        $this->deal = $deal;
        $this->taskPerProject = $taskPerProject;
        $this->projectTask = $projectTask;
        $this->projectPerPhase = $projectPerPhase;
        $this->projectEquipment = $projectEquipment;
        $this->projectEstimationCost = $projectEstimationCost;
        $this->projectTaskCost = $projectTaskCost;
        $this->projectTechnicians = $projectTechnicians;
        $this->clients = $clients;
        $this->technician = $technician;
        $this->techLevel = $techLevel;
        $this->projectPhases = $projectPhases;
        $this->permisionModel = $permisionModel;
        $this->projectEquipmentTable = "cs_project_equipments";
        $this->projectTechniciansTable = "cs_project_technicians";
        $this->redirectUrl = PREFIX."/projects";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('keywords'),Input::get('filter'),Auth::user()->id);
        return view('backend.projects.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        //$data["deal"] =["" => "none"]+$this->deal->pluck("title","id")->toArray();
        $data["job_types"] =["" => "none"]+Config::get('ProjectConfig.job_types');
        $data["sub_systems"] =["" => "none"]+Config::get('ProjectConfig.sub_systems');
        $data["project_type"] =["" => "none"]+Config::get('ProjectConfig.project_type');
        $data["clients"] =["" => "none"]+$this->clients->selectRaw("CONCAT(first_name,' ',last_name) as name,id")->pluck("name","id")->toArray();
        return view('backend.projects.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $projectReq)
    {

        //dd($projectReq->get("sub_systems"));


        //try{
            $projectData = $projectReq->all();
            $projectData["sub_systems"] =  implode(",",$projectData["sub_systems"]);
            $projectObj = $this->model->addData($projectData);
            $phasesConfigs = $this->projectPhases->all();

            if($projectReq->get("project_type") != "Single Phase" ){
                foreach ($phasesConfigs as $phasesConfig){

                    $projectPerPhaseData = $phasesConfig->toArray();
                    $projectPerPhaseData["project_id"] = $projectObj->id;
                    $projectPerPhaseObj = $this->projectPerPhase->addData($projectPerPhaseData);

                    $tasksOfPhase = $this->projectTask->where("phase_id","=",$phasesConfig->id)->get();
                    //dd($tasksOfPhase);
                    foreach ($tasksOfPhase as $task){
                        $taskData = $task->toArray();
                        $taskData["phase_id"] = $projectPerPhaseObj->id;
                        unset($taskData["id"]);
                        $this->taskPerProject->create($taskData);

                    }
                }
            }


            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Project Phase."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $projectObj = $this->model->find($id);

        if($projectObj == null){
            return redirect()->back()->withErrors(["alert-danger" => "The Project you're trying to access, no longer exists on our record."]);

        }
        $data["projectObj"] =  $projectObj;


        $phases = $this->projectPhases->all();
        $phasesPerProject = $this->projectPerPhase->where("project_id","=",$id)->get();
        $phasePerProjectData = [];
//        $phaseData = [];
//          foreach ($phases as $tempPhase){
//            $phaseData[$tempPhase->title]["attributes"] = $tempPhase->toArray();
//
//            $tasks = $this->projectTask->where("phase_id","=",$tempPhase->id)->get();
//
//            if(!$tasks->isEmpty()){
//                $phaseData[$tempPhase->title]["task"] = [];
//                foreach ($tasks as $task){
//                    $phaseData[$tempPhase->title]["task"][$task->code]["title"] = $task->title;
//                    $phaseData[$tempPhase->title]["task"][$task->code]["type"] = $task->type;
//                    $phaseData[$tempPhase->title]["task"][$task->code]["code"] = $task->code;
//
//
//                    if($task->type == "Others"){
//                        $other = $this->projectTaskCost->where("project_id","=",$id)->where("task_id","=",$task->id)->first();
//                        //dd($id.":".$task->id);
//                        if($other != null){
//                            $phases[$phase->id]["task"][$task->id]["data"] = $other->toArray();
//                        }
//                    }elseif($task->type == "Equipment"){
//                        $equipment = $this->projectEquipment->where("project_id","=",$id)->where("task_id","=",$task->id)->first();
//                        if($equipment != null){
//                            $phases[$phase->id]["task"][$task->id]["data"] = $equipment->toArray();
//                        }
//                    }else{
//                        $technical = $this->projectTechnicians->where("project_id","=",$id)->where("task_id","=",$task->id)->first();
//                        if($technical != null){
//                            $phases[$phase->id]["task"][$task->id]["data"] = $technical->toArray();
//                        }
//                    }
//
//
//                    $projectEstimation = $this->projectEstimationCost->where("project_id","=",$id)->where("task_id","=",$task->id)->first();
//
//                    if($projectEstimation != null){
//                        $phases[$phase->id]["task"][$task->id]["total_cost"] = $projectEstimation->total_cost;
//                        $phases[$phase->id]["task"][$task->id]["cost_changes"] = $projectEstimation->cost_changes;
//                    }
//
//                }
//            }
//
//
//        }
        //dd($tempPhases);
        $total_cost_sum = 0;
        $cost_changes_sum = 0;
        $cost_sum = 0;
        $revisedBudget_sum = 0;
        $balance_sum = 0;
        $percentageOfBudget_sum = 0;
        $sumData = [];



        foreach ($phasesPerProject as $phase){
            $phasePerProjectData[$phase->id]["attributes"] = $phase->toArray();
            //dump($phase->id);
            $tasksPerProject = $this->taskPerProject->where("phase_id","=",$phase->id)->get();
//            if(!$tasksPerProject->isEmpty()){
                $tasks = $tasksPerProject;

//            }else{
//                $tasks = $this->projectTask->where("phase_id","=",$phase->id)->get();
//            }


            if(!$tasks->isEmpty()){
                $phasePerProjectData[$phase->id]["task"] = [];



                foreach ($tasks as $task){

                    $phasePerProjectData[$phase->id]["task"][$task->id]["title"] = $task->title;
                    $phasePerProjectData[$phase->id]["task"][$task->id]["type"] = $task->type;
                    $phasePerProjectData[$phase->id]["task"][$task->id]["code"] = $task->code;
                    $phasePerProjectData[$phase->id]["task"][$task->id]["labor_cost_to_date"] = $task->labor_cost_to_date;



                    $phasePerProjectData[$phase->id]["task"][$task->id]["cost"] = 0;
                    if($task->type == "Others"){
                        $other = $this->projectTaskCost->where("project_id","=",$id)->where("task_id","=",$task->id)->first();
                        //dd($id.":".$task->id);
                        if($other != null){
                            $phasePerProjectData[$phase->id]["task"][$task->id]["cost"] = $other->cost;
                        }
                    }elseif($task->type == "Equipment"){
                        $equipment = $this->projectEquipment->where("project_id","=",$id)->where("task_id","=",$task->id)->get();
                        if(!$equipment->isEmpty()){
                            $phasePerProjectData[$phase->id]["task"][$task->id]["cost"] = $this->projectEquipment->selectRaw('sum(cost) as total_cost')
                                ->where("project_id","=",$id)
                                ->where("task_id","=",$task->id)
                                ->first()->total_cost;


                        }
                    }


                    $phasePerProjectData[$phase->id]["task"][$task->id]["total_cost"] = 0;
                    $phasePerProjectData[$phase->id]["task"][$task->id]["cost_changes"] = 0;
                    $projectEstimation = $this->projectEstimationCost->where("project_id","=",$id)->where("task_id","=",$task->id)->first();

                    if($projectEstimation != null){
                        $phasePerProjectData[$phase->id]["task"][$task->id]["total_cost"] = $projectEstimation->total_cost;
                        $phasePerProjectData[$phase->id]["task"][$task->id]["cost_changes"] = $projectEstimation->cost_changes;
                    }

                    $total_cost = $phasePerProjectData[$phase->id]["task"][$task->id]["total_cost"];
                    $total_cost_sum+=$total_cost;

                    $cost_changes = $phasePerProjectData[$phase->id]["task"][$task->id]["cost_changes"];
                    $cost_changes_sum+=$cost_changes;



                    $cost = $phasePerProjectData[$phase->id]["task"][$task->id]["cost"];
                    if($task->type == "Labor"){
                        $cost = $task->labor_cost_to_date;
                    }

                    $cost_sum+=$cost;

                    $revisedBudget = $phasePerProjectData[$phase->id]["task"][$task->id]["revisedBudget"] = $total_cost + $cost_changes;
                    $revisedBudget_sum+=$revisedBudget;

                    $balance = $phasePerProjectData[$phase->id]["task"][$task->id]["balance"] = $revisedBudget - $cost;

//                    dump('$revisedBudget:'.$revisedBudget);
//                    dump('$cost:'.$cost);
//                    dump('$balance:'.$balance);
//                    dump('#############');

                    $balance_sum+=$balance;

                    if($revisedBudget != 0){
                        $percentageOfBudget = ($cost/$revisedBudget) * 100;
                    }else{
                        $percentageOfBudget = 0;
                    }
//                    dump($cost);
//                    dump($task->labor_cost_to_date);
//                    dump($revisedBudget);
//                    dump($percentageOfBudget);
//                    if($revisedBudget != 0)dump('%:'.($cost/$revisedBudget) * 100);
//                    dump("################");
                    $phasePerProjectData[$phase->id]["task"][$task->id]["percentageOfBudget"] = $percentageOfBudget;
                    $percentageOfBudget_sum+=$percentageOfBudget;


                }
            }


        }




        //dd("asd234");
        $sumData["total_cost_sum"] = $total_cost_sum;
        $sumData["cost_changes_sum"] = $cost_changes_sum;
        $sumData["cost_sum"] = $cost_sum;
        $sumData["revisedBudget_sum"] = $revisedBudget_sum;
        $sumData["balance_sum"] = $balance_sum;

        if($revisedBudget_sum != 0){
            $sumData["percentageOfBudget_sum"] = ($cost_sum/$revisedBudget_sum) * 100;
        }else{
            $sumData["percentageOfBudget_sum"] = 0;
        }

        $data["phasePerProjectData"] =$phasePerProjectData ;
//        $data["phaseData"] =$phaseData ;
        $data["project_id"] =$id ;
        $data["sumData"] =$sumData ;
        //dd($phasePerProjectData);
        return view ('backend.projects.project-phases',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dump($techLvl);
        $data["clients"] =["" => "none"]+$this->clients->selectRaw("CONCAT(first_name,' ',last_name) as name,id")->pluck("name","id")->toArray();


        $project = Project::findOrFail($id);

        if($project == null){
            return redirect()->back()->withErrors(["alert-danger" => "The Project you're trying to edit, no longer exists on our record."]);
        }

        $dealObj = Deal::find($project->deal_id);

        if($project == null){
            return redirect()->back()->withErrors(["alert-danger" => "The Deal of the Project you're trying to edit, no longer exists on our record."]);
        }

        $client = Clients::find($dealObj->client_id);

        if($project == null){
            return redirect()->back()->withErrors(["alert-danger" => "The Client of the Project you're trying to edit, no longer exists on our record."]);
        }

        $data["client"] = $client;





        $deals = Deal::where("client_id","=",$client->id)->get();
        //dd($deal);
        $dealArr = [""=>"None"];
        foreach ($deals as $d){
            //dd($d->proposals);
            $isAnyPropsaSuccess = false;
            if($d->proposals != null){
                foreach ($d->proposals as $proposal){
                    //dump($proposal->status);
                    if($proposal->status == "Success"){
                        $isAnyPropsaSuccess = true;
                        break;
                    }
                }
            }


            if($isAnyPropsaSuccess == true){
                $dealArr[$d->id] = $d->title;

            }

        }

        $data["deal"] = $dealArr;
        //dd($data);
//        dd($project);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "projects.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["deal"] =["" => "none"]+$this->deal->pluck("title","id")->toArray();
        $data["job_types"] =["" => "none"]+Config::get('ProjectConfig.job_types');
        $data["sub_systems"] =["" => "none"]+Config::get('ProjectConfig.sub_systems');
        $data["project_type"] =["" => "none"]+Config::get('ProjectConfig.project_type');
        $data["sub_sys_selected"] =explode(",",$project->sub_systems) ;
        return view('backend.projects.edit',$data,compact('project'));
        //
    }

    /**
     * @param ProjectRequest $projectReq
     * @param $id
     * @return $this
     */
    public function update(ProjectRequest $projectReq, $id)
    {
//        try{

            $projectData = $projectReq->all();
            $projectData["sub_systems"] =  implode(",",$projectData["sub_systems"]);

            $this->model->updateData($projectData,$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Phase."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Phase."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

//    /**
//     * Show list of phases as per projects.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function phases()
//    {dd(23423423423);
//        $data["project_id"] = Input::get("project_id");
//        $data["routeName"] = $this->redirectUrl;
//        $data["redirectBackURL"] = $this->redirectUrl;
//        $data["pageTitle"] = "Phases";
//        return view('backend.projects.project-phases',$data);
//    }
}
