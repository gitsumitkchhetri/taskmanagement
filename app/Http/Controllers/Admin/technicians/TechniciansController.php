<?php

namespace App\Http\Controllers\Admin\technicians;

use App\Address;
use App\TechLevel;
use App\Technician;
use App\ProjectTechnicians;
use App\Http\Requests\TechnicianRequest;
use App\Permission;
use App\Proposal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class TechniciansController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,Technician $technician,Proposal $proposal,TechLevel $techLevel,ProjectTechnicians $projectTechnicians)
    {
        $this->pageTitle = "Technicians";
        $this->model = $technician;
        $this->users = $users;
        $this->projectTechnicians = $projectTechnicians;
        $this->techLevel = $techLevel;
        $this->proposal = $proposal;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/technicians";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        $datas["data"] = $this->model->getAllData(Input::get('keywords'),Input::get('filter'));
        return view('backend.technicians.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        return view('backend.technicians.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TechnicianRequest $technicianReq)
    {
        //dd($technicianReq->except('proposal_url','portal_number','loss_remarks','cost','expected_close_date','status','file'));
        //try{
            $this->model->addData($technicianReq->all());
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Project Technician."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dump($techLvl);
        $technician = Technician::findOrFail($id);
        //dd($data);
        //dd($technician);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "technicians.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        return view('backend.technicians.edit',$data,compact('technician'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TechnicianRequest $technicianReq, $id)
    {
        try{
            $this->model->updateData($technicianReq->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $ifAssociationExists = $this->projectTechnicians->where("technician_id","=",$id)->get();

            if(!$ifAssociationExists->isEmpty()){
                return redirect()->back()->withErrors(["alert-danger" => "There are projects that has association with this Technician.Please,remove those projects before removing this technician."]);
            }

            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
