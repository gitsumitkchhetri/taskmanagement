<?php

namespace App\Http\Controllers\Admin\timesheet;

use App\Clients;
use App\Deal;
use App\ProjectEquipment;
use App\ProjectEstimationCost;
use App\ProjectPhases;
use App\ProjectPerPhase;
use App\Http\Requests\ProjectRequest;
use App\Project;
use App\Permission;
use App\ProjectTask;
use App\ProjectTaskCost;
use App\ProjectTechnicians;
use App\TaskPerProject;
use App\TechLevel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use DB;
use Config;
use Auth;
use App\CustomHelpers\CustomDateHeplers;
use App\Technician;
use App\Timesheet;
use App\CustomHelpers\CostHelper;

class TimesheetController extends Controller
{

    public function __construct(
        User $users,
        Timesheet $timesheet,
        TechLevel $techLevel,
        Permission $permisionModel,
        Project $projects,
        ProjectPhases $projectPhases,
        Deal $deal,
        Clients $clients,
        ProjectPerPhase $projectPerPhase,
        ProjectTask $projectTask,
        ProjectEquipment $projectEquipment,
        ProjectEstimationCost $projectEstimationCost,
        ProjectTaskCost $projectTaskCost,
        TaskPerProject $taskPerProject,
        ProjectTechnicians $projectTechnicians
    )
    {
        $this->pageTitle = "Timesheet";
        $this->model = $timesheet;
        $this->projects = $projects;
        $this->techLevel = $techLevel;
        $this->users = $users;
        $this->deal = $deal;
        $this->taskPerProject = $taskPerProject;
        $this->projectTask = $projectTask;
        $this->projectPerPhase = $projectPerPhase;
        $this->projectEquipment = $projectEquipment;
        $this->projectEstimationCost = $projectEstimationCost;
        $this->projectTaskCost = $projectTaskCost;
        $this->projectTechnicians = $projectTechnicians;
        $this->clients = $clients;
        $this->projectPhases = $projectPhases;
        $this->permisionModel = $permisionModel;
        $this->projectEquipmentTable = "cs_project_equipments";
        $this->projectTechniciansTable = "cs_project_technicians";
        $this->redirectUrl = PREFIX."/timesheet";


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["pageTitle"] = $this->pageTitle;
        $data["formURL"] = $this->redirectUrl;
        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        $data["projectList"] = [""=>"none"]+$this->projects->pluck("title","id")->toArray();


        if(Input::has("project_id")){
            $selectedDate = date("Y-m-d");
            if(Input::has("prevWeekFrom") || Input::has("nextWeekFrom")){
                $from = Input::has("prevWeekFrom") ? Input::get("prevWeekFrom") : Input::get("nextWeekFrom");

            }else{
                $todayDay = date("N",strtotime($selectedDate));
                $from = $todayDay == 7 ? $selectedDate : date("Y-m-d",strtotime("-$todayDay day"));
            }
            //dump($selectedDate);

            //$todayDay = date("N");
//            $from = $todayDay == 7 ? date("Y-m-d") : date("Y-m-d",strtotime("-$todayDay day"));
//
//            $to = date("Y-m-d",strtotime('+6 day',strtotime($from)));
//
//            $checkLastData = $this->model->orderBy("date","DESC")->first();






            $to = date("Y-m-d",strtotime('+7 day',strtotime($from)));



            //$nextWeekFrom = date("Y-m-d",strtotime('+1 day',strtotime($to)));
            $nextWeekFrom = $to;
            //dump($nextWeekFrom);
            //dump('$to:'.$to);
            //dump('$from:'.$from);
            //$nextWeekTo = date("Y-m-d",strtotime('+7 day',strtotime($to)));

            $prevWeekFrom = date("Y-m-d",strtotime('-7 day',strtotime($from)));
            //$prevWeekTo = date("Y-m-d",strtotime('-7 day',strtotime($from)));

            //$checkLastData = $this->model->orderBy("date","DESC")->first();
            $checkPrevData = $this->model->where("date","<",$from)->get();
            $checkNextData = $this->model->where("date",">",$to)->get();
            //dump($checkLastData);
            //dump('$checkPrevData:'.$checkPrevData);
            //dump('$checkNextData:'.$checkNextData);

            if(!$checkPrevData->isEmpty()){
                    $data["prevWeekFrom"] = $prevWeekFrom;
            }

            if(!$checkNextData->isEmpty()){

                    $data["nextWeekFrom"] = $nextWeekFrom;

            }



            $technicians  =Technician::all();
            $data["technicians"] =$technicians;

            //$timestamp = strtotime(date('Y-m-d'));
            //dd($timestamp);
//            $day = date('N', $timestamp);
//            $test = date("Y-m-d",strtotime("-$day day"));
//            $testt = date("D",strtotime("+3 day"));
//            $testt1 = date("Y-m-d",strtotime("+7 day"));
//            $test1 = date("D",strtotime("-$day day"));


            $range = CustomDateHeplers::dayGenerator($from,$to);
            $data["range"] = $range;
            //dump($range);



            $id = Input::get("project_id");
            $projectObj = $this->projects->find($id);

            if($projectObj == null){
                return redirect()->back()->withErrors(["alert-danger" => "The Project you're trying to access, no longer exists on our record."]);

            }
            $data["projectObj"] =  $projectObj;


            $phases = $this->projectPhases->all();
            $phasesPerProject = $this->projectPerPhase->where("project_id","=",$id)->get();
            $phasePerProjectData = [];




            foreach ($phasesPerProject as $phase){
                $hasAtLeastOneLaborTask = false;

                $phasePerProjectData[$phase->id]["attributes"] = $phase->toArray();
                //dump($phase->id);
                $tasksPerProject = $this->taskPerProject->where("phase_id","=",$phase->id)->get();
//            if(!$tasksPerProject->isEmpty()){
                $tasks = $tasksPerProject;

//            }else{
//                $tasks = $this->projectTask->where("phase_id","=",$phase->id)->get();
//            }


                if(!$tasks->isEmpty()){
                    $phasePerProjectData[$phase->id]["task"] = [];



                    foreach ($tasks as $task){

                        if($task->type == "Labor"){
                            $phasePerProjectData[$phase->id]["task"][$task->id]["title"] = $task->title;
                            $phasePerProjectData[$phase->id]["task"][$task->id]["type"] = $task->type;
                            $phasePerProjectData[$phase->id]["task"][$task->id]["code"] = $task->code;

                            $phasePerProjectData[$phase->id]["task"][$task->id]["labors"] = [];
                            $taskHasLabor = false;


                            foreach ($technicians as $technician){
                                foreach ($range as $rg){
                                    $laborData = Timesheet::where("project_id","=",$id)->where("phase_id","=",$phase->id)->where("task_id","=",$task->id)->where("technician_id","=",$technician->id)->whereDate("date","=",$rg)->first();
                                    $ifAssocitedWithProjectTask = $this->projectTechnicians->where("project_id","=",$id)->where("task_id","=",$task->id)->where("technician_id","=",$technician->id)->first();
                                    if($ifAssocitedWithProjectTask != null){
                                        $phasePerProjectData[$phase->id]["task"][$task->id]["labors"][$technician->id][$rg] = $laborData == null ? 0 : $laborData->work_hr;

                                        $hasAtLeastOneLaborTask = true;
                                        $taskHasLabor = true;
                                    }
                                }

                                if(!$taskHasLabor){
                                    //dump("uh huh");
                                    //dump($phasePerProjectData[$phase->id]["task"][$task->id]);
                                    $remPhaseTask = $phasePerProjectData[$phase->id]["task"];
                                    unset($remPhaseTask[$task->id]);
                                    //dump($phasePerProjectData[$phase->id]["task"][$task->id]);
                                }

                            }
                        }




                    }
                }

//                if(!$taskHasLabor){
//                    dump("uh huh");
//                    dump($phasePerProjectData[$phase->id]["task"][$task->id]);
//                    unset($phasePerProjectData[$phase->id]["task"][$task->id]);
//                    //dump($phasePerProjectData[$phase->id]["task"][$task->id]);
//                }

                if(!$hasAtLeastOneLaborTask){
                    //dump("sd3453");
                    unset($phasePerProjectData[$phase->id]);
                }



            }




            $data["phasePerProjectData"] =$phasePerProjectData ;

        }else{
            $data["phasePerProjectData"] =[] ;
        }
        return view('backend.timesheet.table',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        return view('backend.timesheet.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TechnicianRequest $technicianReq)
    {
        //dd($technicianReq->except('proposal_url','portal_number','loss_remarks','cost','expected_close_date','status','file'));
        //try{
            $this->model->addData($technicianReq->all());
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Project Technician."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dump($techLvl);
        $technician = Technician::findOrFail($id);
        //dd($data);
        //dd($technician);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "technicians.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        return view('backend.timesheet.edit',$data,compact('technician'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TechnicianRequest $technicianReq, $id)
    {   //dd($technicianReq);
        try{
            $this->model->updateData($technicianReq->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateTimeSheet()
    {
        $projectdDatas = Input::get("timesheet");
        //dump($projectdDatas);
        $recentRow = null;

        foreach ($projectdDatas as $projectKey => $projectdPhases){
//            dd($projectdPhases);
            foreach ($projectdPhases as $phaseKey => $tasks){
                foreach ($tasks as $taskKey => $technicians){
                    foreach ($technicians as $techKey => $dates){
                        foreach ($dates as $date => $work_hr){

                            if( !(ctype_digit($work_hr) && $work_hr >= 0 && $work_hr <= 24) ){
                                return redirect()->back()->withInput()->withErrors(["alert-danger" => "Only positive integers are allowed and work hour cannot be greater than 24 hrs."]);
                            }
                        }
                    }
                }
            }
        }

        try{
            //dd(Input::all());
            DB::beginTransaction();
            foreach ($projectdDatas as $projectKey => $projectdPhases){
//            dd($projectdPhases);
                foreach ($projectdPhases as $phaseKey => $tasks){
                    foreach ($tasks as $taskKey => $technicians){
                        foreach ($technicians as $techKey => $dates){
                            foreach ($dates as $date => $work_hr){

                                $timesheetObj = $this->model->where("project_id","=",$projectKey)->where("phase_id","=",$phaseKey)->where("task_id","=",$taskKey)->where("technician_id","=",$techKey)->whereDate("date","=",$date)->first();
                                if($timesheetObj == null){
                                    $tmsData = [];
                                    $tmsData["project_id"] = $projectKey;
                                    $tmsData["phase_id"] = $phaseKey;
                                    $tmsData["task_id"] = $taskKey;
                                    $tmsData["technician_id"] = $techKey;
                                    $tmsData["date"] = $date;
                                    $tmsData["work_hr"] = $work_hr;
                                    $recentRow = $this->model->create($tmsData);
                                }else{
                                    $timesheetObj->update(["work_hr" => $work_hr]);
                                    $recentRow = $timesheetObj;
                                }
                            }
                        }
                    }
                }
            }

            if(Input::get("submit") == "authenticate"){
                $recentRow->verified = 1;
                $recentRow->save();

                CostHelper::recordLaborCost();

            }


            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return redirect()->back()->withErrors(["alert-success" => "Error updating Timesheet.Please,try again later"]);
        }

//dd();
        return redirect()->back()->withErrors(["alert-success" => "Successfully updated Timesheet."]);

        //dd("sdf");

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $ifAssociationExists = $this->projectTechnicians->where("technician_id","=",$id)->get();

            if(!$ifAssociationExists->isEmpty()){
                return redirect()->back()->withErrors(["alert-danger" => "There are projects that has association with this Technician.Please,remove those projects before removing this technician."]);
            }

            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
