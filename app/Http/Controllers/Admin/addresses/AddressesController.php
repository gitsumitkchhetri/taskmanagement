<?php

namespace App\Http\Controllers\Admin\addresses;

use App\Clients;
use App\Http\Requests\AddressesRequest;
use App\Address;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class AddressesController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,Address $address,Clients $clients)
    {
        $this->pageTitle = "Addresses";
        $this->model = $address;
        $this->users = $users;
        $this->clients = $clients;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/addresses";
        $this->clienttUrl = PREFIX."/clients";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Input::has('client_id')){
            return redirect($this->clienttUrl)->withErrors(["alert-danger" => "Invalid URL access."]);
        }

        $datas["pageTitle"] = $this->pageTitle;
        $clientObj = $this->clients->find(Input::get('client_id'));

        if($clientObj == null){
            return redirect($this->clienttUrl)->withErrors(["alert-danger" => "The Client You are looking for no longer exist on our record."]);
        }

        $datas["clientObj"] = $clientObj;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('keywords'),Input::get('client_id'));
        return view('backend.client_addresses.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        return view('backend.client_addresses.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressesRequest $addressesRequest)
    {
        //try{
            $this->model->addData($addressesRequest->all());
            return redirect($this->redirectUrl."?client_id=".$addressesRequest->get("client_id"))->withErrors(["alert-success" => "Successfully added Adresses."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id )
    {
        //dump($techLvl);
        $address = Address::findOrFail($id);
        //dd($data);
        //dd($address);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "addresses.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.client_addresses.edit',$data,compact('address'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddressesRequest $addressesRequest, $id)
    {
        //try{
            $this->model->updateData($addressesRequest->all(),$id);
            return redirect($this->redirectUrl."?client_id=".$addressesRequest->get("client_id"))->withErrors(["alert-success" => "Successfully edited Adresses."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Adresses."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
