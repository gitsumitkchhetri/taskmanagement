<?php

namespace App\Http\Controllers\Admin\user;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use Html;
use Image;
use File;
use App\User;
use App\Role;
use App\Permission;
use App\RoleUser;
use App\Http\Requests\UsersRequest;
use Carbon\Carbon;
use Auth;
use App\Deal;

class usersController extends Controller
{

    public function __construct(User $users,Permission $permisionModel)
    {
        $this->pageTitle = "Users";
        $this->model = $users;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/user";
    }

    public function index(UsersRequest $request)
    {
        $pageTitle = $this->pageTitle;
        $data = $this->model->getAllData(Input::all());
        return view('backend.users.index',compact('data','pageTitle','roles'));
    }

    public function create(UsersRequest $request)
    {

        $roles = [""=>'Please Select'] + Role::pluck('name','id')->all();

        return view('backend.users.create',compact('roles','permission','timezoneData'));
    }

    public function store(UsersRequest $request)
    {
        //$checkPoint = count(User::where('username',strtolower($request->username))->get());
//        $checkPoint = count(User::whereRaw(" LOWER(CONCAT_WS('', username)) LIKE LOWER('%". pg_escape_string($request->username) ."%')")->get());
//        if($checkPoint > 0)
//        {
//            return redirect()->back()->withErrors(['alert-danger'=>'Sorry this username is already taken!'])->withInput($request->all());
//        }
//        $checkPointEmail = count(User::whereRaw(" LOWER(CONCAT_WS('', email)) LIKE LOWER('%". pg_escape_string($request->email) ."%')")->get());
//        if($checkPointEmail > 0)
//        {
//            return redirect()->back()->withErrors(['alert-danger'=>'Sorry this email is already taken!'])->withInput($request->all());
//        }

        try {
            $attributes = $request->all();
            $rolesData = Role::where('id',$request->roles)->first();
            if(!$rolesData){
                return redirect()->back()->withErrors(['Internal Sever Error']);
            }
            $insertedData['first_name'] = $request->first_name;
            $insertedData['last_name'] = $request->last_name;
            $insertedData['username'] = $request->username;
            $insertedData['email'] = $request->email;
            $insertedData['status'] = $request->status;
            $insertedData['password'] = $request->password;
            $insertedData['created_at'] = Carbon::now();
//            $insertedData['created_by'] = Auth::user()->id;
//            $insertedData['role_id'] = $rolesData->id;
            try{
                $usersData = $this->model->create($insertedData);
                $roleData['user_id'] = $usersData->id;
                $roleData['role_id'] = $rolesData->id;
                RoleUser::create($roleData);
                return redirect($this->redirectUrl)->withErrors(['alert-success'=>'Successfully added!']);
            }
            catch (Exception $e) {
                return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not saved!']);
            }

        } catch (Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not saved!']);
        }
    }

    public function edit(UsersRequest $request,$id)
    {
        if(!is_numeric($id)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
        }
        $pageTitle = $this->pageTitle;
        $data = $this->model->find($id);
        $superUserCount = count(RoleUser::where('role_id',1)->get());
        if(empty($data)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        $roles = [""=>'Please Select'] + Role::pluck('name','id')->all();

        return view('backend.users.edit', compact('data','pageTitle','roles','permission','superUserCount','timezoneData'));
    }

    public function update(UsersRequest $request,$id)
    {
        if(!is_numeric($id)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
        }

//        $checkPoint = count(User::where('username',strtolower($request->username))->get());
//        if($checkPoint > 0)
//        {
//            return redirect()->back()->withErrors(['alert-danger'=>'Sorry this username is already taken!'])->withInput($request->all());
//        }
//        $checkPointEmail = count(User::where('email',strtolower($request->email))->get());
//        if($checkPointEmail > 0)
//        {
//            return redirect()->back()->withErrors(['alert-danger'=>'Sorry this email is already taken!'])->withInput($request->all());
//        }

        $usersData = $this->model->find($request->id);
        $attributes            = $request->all();
        $rolesData = Role::where('id',$request->roles)->first();
        if(empty($rolesData) || empty($usersData)){
            return redirect()->back()->withErrors(['alert-danger'=>'Data was not found!']);
        }
        $insertedData['first_name'] = $request->first_name;
        $insertedData['last_name'] = $request->last_name;
        $insertedData['username'] = $request->username;
        $insertedData['email'] = $request->email;
        $insertedData['status'] = $request->status;
        $insertedData['updated_at'] = Carbon::now();
        $insertedData['updated_by'] = Auth::user()->id;
        try {
            $attributes = Input::except('_token');
            $usersData->update($insertedData);


            if($request->roles == 1){
                $superUserCount = count(RoleUser::where('role_id',1)->get());
                if($superUserCount >= 1){
                    RoleUser::where('user_id',$usersData->id)->delete();
                    $roleData['user_id'] = $usersData->id;
                    $roleData['role_id'] = $rolesData->id;
                    RoleUser::create($roleData);
                }
            }else{
                RoleUser::where('user_id',$usersData->id)->delete();
                $roleData['user_id'] = $usersData->id;
                $roleData['role_id'] = $rolesData->id;
                RoleUser::create($roleData);
            }




            return redirect($this->redirectUrl)->withErrors(['alert-success'=>'Successfully updated!']);
        } catch (Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not saved!']);
        }

    }

    public function password(UsersRequest $request,$id)
    {
        if(!is_numeric($id)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
        }
        //$id                   = $request->id;
        $data = $this->model->where('id',$id)->first();
        if(empty($data)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        return view('backend.users.changepass', compact('data'));
    }


//    public function change_password(Request $request,$id)
//    {
//        if(!is_numeric($id)){
//            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
//        }
//        $data = $this->model->where('id',$id)->first();
//        if(empty($data)){
//            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
//        }
//        return view('backend.users.changepass', compact('data'));
//    }

    public function updatepassword(UsersRequest $request){
        if(!is_numeric(Input::get('id'))){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
        }
        $id                   = $request->id;
        $data = $this->model->where('id',$id);
        if(empty($data)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        $insertedData['password'] = bcrypt($request->password);
        $data->update($insertedData);
        return redirect($this->redirectUrl)->withErrors(['alert-success'=>'Successfully updated!']);
    }

    public function destroy(UsersRequest $request,$id)
    {
        $dealObj = Deal::where("sales_person","=",$id)->get();
        if(!$dealObj->isEmpty()){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'The user is involved in deal.Please remove association then try to remove.']);
        }

        if(!is_numeric($id)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
        }
        $users = $this->model->find($id);
        if(empty($users)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        if($this->model->count()==1){
            return redirect()->back()->withErrors(['Deletion unsuccessful because there must be at least one admin!']);
        }
        try {
            $users = $this->model->find($id);
            $t = $users->delete();
            RoleUser::where('user_id',$users->id)->delete();
            return redirect()->back()->withErrors(['alert-success'=>'Successfully deleted!']);
        } catch (Exception $e) {
            return redirect()->back()->withErrors(['alert-danger'=>$e->getMessage()]);
        }

    }

}
