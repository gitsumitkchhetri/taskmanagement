<?php

namespace App\Http\Controllers\Admin\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use Validator;
use Input;
use App\Http\Requests\RolesRequest;
use App\Http\Requests\RolesUpdateRequest;
use Session;
use DB;
use App\RoleUser;
use Auth;

class rolesController extends Controller
{
    public function __construct(Role $roles,Permission $permission)
    {
        $this->pageTitle = "User Roles";
        $this->model = $roles;
        $this->permissionModel = $permission;
        $this->redirectUrl = PREFIX."/role";
    }

    public function index()
    {
        if(null !== Auth::user()->rolesUser && Auth::user()->rolesUser->role_id == 1){
            //dd('hello');
            $permission = new Permission;
            $permission->refreshPermission();
        }

        $pageTitle = $this->pageTitle;
        $data = $this->model->getAllData(Input::all());
        return view('backend.roles.index',compact('data','pageTitle'));
    }

    public function create(RolesRequest $request)
    {
        $pageTitle = "Add Role";
        $permission = $this->permissionModel->groupedPermissions();
        return view('backend.roles.create',compact('permission','pageTitle'));

    }

    public function edit(RolesRequest $request,$id){
        $pageTitle = "Edit Role";
        $data = $this->model->find($id);
        if(empty($data)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        $userPermission = $this->model->getUserRoles($id);
        $permission = $this->permissionModel->groupedPermissions();
        return view('backend.roles.edit',compact('permission','userPermission','data','pageTitle'));
    }

    public function store(RolesRequest $request)
    {
        try {
            $attributes            = $request->all();
            $module = '{"home.view":"1",';
            foreach($request->permissions as $p){
                $module .= '"'.$p.'"'.':'.'"1"'.',';
            }
            $moduleData = rtrim($module, ',')."}";
            $attributes['permissions'] = $moduleData;
            $roles            = $this->model->create($attributes);
            return redirect($this->redirectUrl)->withErrors(['alert-success'=>'Successfully added!']);
        } catch (Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not saved!']);
        }
    }

    public function update(RolesRequest $request,$id)
    {
        $rolesData = $this->model->find($id);
        try {
            $attributes            = $request->all();
            $module = '{"home.home.view":"1",';
            if(!empty($request->permissions))
            {
                foreach($request->permissions as $p){
                    $module .= '"'.$p.'"'.':'.'"1"'.',';
                }
            }

            $moduleData = rtrim($module, ',')."}";
            $attributes['permissions'] = $moduleData;
            $rolesData->update($attributes);
            return redirect($this->redirectUrl)->withErrors(['alert-success'=>'Successfully updated!']);
        } catch (Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not saved!']);
        }
    }

    public function destroy(RolesRequest $request,$id)
    {
        $check = RoleUser::where('role_id',$id)->get();
        if(count($check) > 0)
        {
            return redirect()->back()->withErrors(['alert-error'=>'This role cannot be deleted because it is associated with users!']);
        }
        $roles = $this->model->find($id);
        try {
            if($roles->name=='superuser'){
                return redirect()->back()->withErrors(['You cannot delete superuser!']);
            }
            $name = $roles->name;
            $t = $roles->delete();
            return redirect()->back()->withErrors(['alert-success'=>'Successfully deleted!']);
        } catch (Exception $e) {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

    }

}
