<?php

namespace App\Http\Controllers\Admin\ajax_handler;

use App\Clients;
use App\Deal;
use App\Http\Requests\ProposalRequest;
use App\Proposal;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class AjaxHandlerController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,Proposal $proposal,Clients $clients,Deal $deal)
    {
        $this->pageTitle = "Proposals";
        $this->model = $proposal;
        $this->users = $users;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/proposals";
        $this->dealUrl = PREFIX."/deals";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Input::has('client_id')){
            $deals = Deal::where("client_id","=",Input::get('client_id'))->get();
            //dd($deal);
            $dealArr = [""=>"None"];
            foreach ($deals as $d){
                //dd($d->proposals);
                $isAnyPropsaSuccess = false;
                if($d->proposals != null){
                    foreach ($d->proposals as $proposal){
                        //dump($proposal->status);
                        if($proposal->status == "Success"){
                            $isAnyPropsaSuccess = true;
                            break;
                        }
                    }
                }


                if($isAnyPropsaSuccess == true){
                    $dealArr[$d->id] = $d->title;

                }

            }

            $deal = $dealArr;

            //dd($deal);


            return view('backend.ajax_renders.project_ajax_components.deals-dropdown',compact('deal'))->render();


        }

        if(Input::has('deal_id')){
            $proposal = Deal::where("deal_id","=",Input::get('deal_id'))->pluck("title","deal_id")->toJson();
            return view('backend.ajax_renders.project_ajax_components.deals-dropdown',compact('proposal'))->render();

//            return response()
//                ->json($returnData);
        }


        //return view('backend.proposals.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        return view('backend.proposals.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProposalRequest $proposalReq)
    {
        //try{
            $this->model->addData($proposalReq->all());
            return redirect($this->redirectUrl."?deal_id=".$proposalReq->get("deal_id"))->withErrors(["alert-success" => "Successfully added Proposal."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id )
    {
        //dump($techLvl);
        $proposal = Proposal::findOrFail($id);
        //dd($data);
        //dd($proposal);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "proposals.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.proposals.edit',$data,compact('proposal'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProposalRequest $proposalReq, $id)
    {
        try{
            $this->model->updateData($proposalReq->all(),$id);
            return redirect($this->redirectUrl."?deal_id=".$proposalReq->get("deal_id"))->withErrors(["alert-success" => "Successfully edited Proposal."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Proposal."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
