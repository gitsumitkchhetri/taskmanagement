<?php

namespace App\Http\Controllers\Admin\dispatch;

use App\Address;
use App\TechLevel;
use App\Technician;
use App\Project;
use App\ProjectTechnicians;
use App\ProjectPerPhase;
use App\TaskPerProject;
use App\Http\Requests\DispatchReq;
use App\Permission;
use App\Proposal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class DispatchController extends Controller
{

    public function __construct(TaskPerProject $taskPerProject,ProjectPerPhase $projectPerPhase,Project $project,User $users,Permission $permisionModel,Technician $technician,Proposal $proposal,TechLevel $techLevel,ProjectTechnicians $projectTechnicians)
    {
        $this->pageTitle = "Dispatch";
        $this->model = $technician;
        $this->taskPerProject = $taskPerProject;
        $this->technician = $technician;
        $this->users = $users;
        $this->project = $project;
        $this->projectTechnicians = $projectTechnicians;
        $this->projectPerPhase = $projectPerPhase;
        $this->techLevel = $techLevel;
        $this->proposal = $proposal;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/dispatch";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["tech_level"] = [""=>"none"]+$this->techLevel->pluck("title","id")->toArray();
        $datas["data"] = $this->model->getAllData(Input::get('keywords'),Input::get('filter'));

        return redirect()->route('dispatch.create');
        //return view('backend.dispatch.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["tech"] = [];
        if(Input::has("project_id")){
            $techList = $this->projectTechnicians->where("project_id","=",Input::get("project_id"))->pluck("technician_id")->toArray();
            $data["tech"] = $this->technician->whereIn("id",$techList)->pluck("id")->toArray();
            $phasePerProject = $this->projectPerPhase->where("project_id","=",Input::get("project_id"))->pluck("id")->toArray();

            $data["labor_tasks"] = [];
            foreach ($phasePerProject as $pid){
                $taskPerProject = $this->taskPerProject->where("phase_id","=",$pid)->get();
                foreach ($taskPerProject as $tp){
                    if($tp->type == "Labor"){
                        $data["labor_tasks"] = [$tp->id => $tp->title];
                    }
                }
            }

            //dd($data["labor_tasks"]);
            //$data["labor_tasks"] = $this->
        }
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["technicians"] = [""=>"none"]+$this->technician->pluck("name","id")->toArray();
        $data["projectsLists"] = [""=>"none"]+$this->project->pluck("title","id")->toArray();
        return view('backend.dispatch.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DispatchReq $dispatchReq)
    {
        //dd(Input::all());




        //dd($dispatchReq->except('proposal_url','portal_number','loss_remarks','cost','expected_close_date','status','file'));
        try{
            if(!Input::has('project_id')){
                return redirect()->back()->withErrors(["alert-danger" => "Invalid Request."]);
            }

            foreach (Input::get("technician_id") as $tech){
                $projectTechObj = $this->projectTechnicians->where("project_id","=",Input::get('project_id'))->where("task_id","=",Input::get('task_id'))->where("technician_id","=",$tech);

                $techData = [];
                $techData["project_id"] = Input::get('project_id');
                $techData["task_id"] = Input::get('task_id');
                $techData["technician_id"] = $tech;
                $techData["from_date"] = Input::get('from_date');
                $techData["to_date"] = Input::get('to_date');

                if($projectTechObj == null){

                    $projectTechObj->create($techData);
                }else{
                    $projectTechObj->update($techData);
                }
            }

            return redirect()->back()->withErrors(["alert-success" => "Successfully added Dispatch information."]);

        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dump($techLvl);
        $technician = Technician::findOrFail($id);
        //dd($data);
        //dd($technician);
        $data["routeName"] = "technicians.update";
        $data["redirectBackURL"] = $this->redirectUrl;


        $data["pageTitle"] = $this->pageTitle;
        $data["technicians"] = [""=>"none"]+$this->technician->pluck("name","id")->toArray();
        $techList = $this->projectTechnicians->where("project_id","=",$id)->pluck("technician_id")->toArray();
        $data["tech"] = [""=>"none"]+$this->technician->whereIn("id",$techList)->pluck("name","id")->toArray();
        $data["projectsLists"] = [""=>"none"]+$this->project->pluck("title","id")->toArray();
        return view('backend.dispatch.edit',$data,compact('technician'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DispatchReq $dispatchReq, $id)
    {
        try{
            $this->model->updateData($dispatchReq->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $ifAssociationExists = $this->projectTechnicians->where("technician_id","=",$id)->get();

            if(!$ifAssociationExists->isEmpty()){
                return redirect()->back()->withErrors(["alert-danger" => "There are projects that has association with this Technician.Please,remove those projects before removing this technician."]);
            }

            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
