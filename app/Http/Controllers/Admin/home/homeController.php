<?php
namespace App\Http\Controllers\Admin\home;

use App\Catalog;
use App\RetailOutlet;
use App\Skuinventory;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use Auth;
use App\User;
use DB;

class homeController extends Controller
{
    public function __construct()
    {

    }

    public $thisPageId = 'Dashboard';

    public $thisModuleId = "home";

    public function index() {
        $pageTitle="Dashboard";


        return view("backend.home.index",compact('pageTitle'));
    }


    public function aYearBeforeMonthsGen(){
        $monthArr = [];
        $date = strtotime(date("Y-m-d", strtotime("-1 year")));
        for($i = 1;$i <= 12;$i++){
            if($i == 1){
                $cust_date = $date;
            }
            array_push($monthArr,date("M", strtotime("+1 month", $cust_date)));
            $cust_date = strtotime(date("Y-m-d", strtotime("+1 month", $cust_date)));
        }
        return $monthArr;
    }


}
