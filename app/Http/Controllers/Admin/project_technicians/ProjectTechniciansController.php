<?php

namespace App\Http\Controllers\Admin\project_technicians;

use App\Http\Requests\ProjectTechniciansRequest;
use App\ProjectTechnicians;
use App\Technician;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class ProjectTechniciansController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,ProjectTechnicians $projectTech,Technician $technician)
    {
        $this->pageTitle = "Project Technicians";
        $this->model = $projectTech;
        $this->users = $users;
        $this->technician = $technician;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/project_technicians";
        $this->respectiveProjectUrl = PREFIX."/projects";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Input::has('project_id') || !Input::has('task_id')){
            return redirect($this->respectiveProjectUrl)->withErrors(["alert-danger" => "Invalid URL access."]);
        }

        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('project_id'),Input::get('task_id'),Input::get('keywords'));
        return view('backend.project_technicians.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $project_id = Input::get("project_id");
        $task_id = Input::get("task_id");
        $techOnTask = $this->model->where("project_id","=",$project_id)->where("task_id","=",$task_id)->pluck("technician_id")->toArray();

        $technicians =$this->technician->whereNotIn("id",$techOnTask)->get();
        $techArr = ["" => "none"];
        foreach ($technicians as $key =>$technician){
            $techArr[$technician->id] = $technician->name." (level:".$technician->TechLevel->title.", cost:".$technician->TechLevel->cost.")";

        }
        $data["technician"] = $techArr;

        return view('backend.project_technicians.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectTechniciansRequest $projectTechReq)
    {
        //dd($projectTechReq->all());
        //try{
            $this->model->addData($projectTechReq->all());
            return redirect($this->redirectUrl."?project_id=".$projectTechReq->get("project_id")."&task_id=".Input::get('task_id'))->withErrors(["alert-success" => "Successfully added Technician."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id )
    {
        //dump($techLvl);
        $project_id = Input::get("project_id");
        $task_id = Input::get("task_id");
        //$project_equipment = ProjectTechnicians::findOrFail($id);
        //$project_technician = ProjectTechnicians::where("project_id","=",$project_id)->where("task_id","=",$task_id)->first();
        $project_technician = ProjectTechnicians::find($id);
        if($project_technician == null){
            return redirect()->back()->withErrors(["alert-danger" => "The Data doesn't exists on our record."]);
        }

        $project_id = Input::get("project_id");
        $task_id = Input::get("task_id");
        $techOnTask = $this->model->where("project_id","!=",$project_id)->where("task_id","!=",$task_id)->pluck("technician_id")->toArray();

        $technicians =$this->technician->whereIn("id",$techOnTask)->orWhereIn("id",[$project_technician->technician_id])->get();

        $techArr = ["" => "none"];
        foreach ($technicians as $key =>$technician){
            $techArr[$technician->id] = $technician->name." (level:".$technician->TechLevel->title.", cost:".$technician->TechLevel->cost.")";

        }
        $data["technician"] = $techArr;



        //dd($data);
        //dd($project_technician);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "project_technicians.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.project_technicians.edit',$data,compact('project_technician'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectTechniciansRequest $projectTechReq, $id)
    {
        try{
            $this->model->updateData($projectTechReq->all(),$id);
            return redirect($this->redirectUrl."?project_id=".$projectTechReq->get("project_id")."&task_id=".Input::get('task_id'))->withErrors(["alert-success" => "Successfully edited Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully removed Technician."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
