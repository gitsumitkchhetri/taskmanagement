<?php

namespace App\Http\Controllers\Admin\password;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use Html;
use Image;
use File;
use App\User;
use App\Role;
use App\Permission;
use App\RoleUser;
use App\Http\Requests\UsersRequest;
use Carbon\Carbon;
use Auth;

class passwordController extends Controller
{

    public function __construct(User $users,Permission $permisionModel)
    {
        $this->pageTitle = "Users";
        $this->model = $users;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/home";
    }

    public function change_password(Request $request,$id)
    {
        if(!is_numeric($id)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
        }
        $data = $this->model->where('id',$id)->first();
        if(empty($data)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        return view('backend.password.changepass', compact('data'));
    }

    public function updatepassword(UsersRequest $request,$id){
        if(!is_numeric($id)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Invalid character used in URL!']);
        }
        $data = $this->model->where('id',$id);
        if(empty($data)){
            return redirect($this->redirectUrl)->withErrors(['alert-danger'=>'Data was not found!']);
        }
        $insertedData['password'] = bcrypt($request->password);
        $data->update($insertedData);
        return redirect($this->redirectUrl)->withErrors(['alert-success'=>'Successfully updated!']);
    }

}
