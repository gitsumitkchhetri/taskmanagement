<?php

namespace App\Http\Controllers\Admin\clients;

use App\Address;
use App\Deal;
use App\Http\Requests\ClientsRequest;
use App\Clients;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class ClientsController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,Clients $clients,Address $address,Deal $deal)
    {
        $this->pageTitle = "Clients Management";
        $this->model = $clients;
        $this->users = $users;
        $this->deal = $deal;
        $this->address = $address;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/clients";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('keywords'));
        return view('backend.clients.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        return view('backend.clients.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientsRequest $clientsRequest)
    {
        //try{
            $clientObj = $this->model->addData($clientsRequest->except('address','lat','lng','contact_no','google_map_link'));
            $addressData = $clientsRequest->only('address','lat','lng','contact_no','google_map_link');
            $addressData["client_id"] = $clientObj->id;
            $this->address->addData($addressData);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Project Phase."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dump($techLvl);
        $clients = Clients::findOrFail($id);
        //dd($data);
        //dd($clients);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "clients.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.clients.edit',$data,compact('clients'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientsRequest $clientRequest, $id)
    {
        try{
            $this->model->updateData($clientRequest->all(),$id);
            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Project Phase."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $checkDeals = $this->deal->where("client_id","=",$id)->get();

            if(!$checkDeals->isEmpty()){
                return redirect()->back()->withErrors(["alert-danger" => "There are Deals associated with this Client.Please,remove those Deals before removing this Client."]);
            }

            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Phase."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
