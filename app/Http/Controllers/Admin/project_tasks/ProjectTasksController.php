<?php

namespace App\Http\Controllers\Admin\project_tasks;

use App\Http\Requests\ProjectTaskRequest;
use App\ProjectTask;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Config;


class ProjectTasksController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,ProjectTask $projectTask)
    {
        $this->pageTitle = "Project Tasks";
        $this->model = $projectTask;
        $this->users = $users;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/project_tasks";
        $this->phaseUrl = PREFIX."/project_phases";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Input::has('phase_id')){
            return redirect($this->phaseUrl)->withErrors(["alert-danger" => "Invalid URL access."]);
        }

        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["phaseUrl"] = $this->phaseUrl;
        $datas["data"] = $this->model->getAllData(Input::get('keywords'),Input::get('phase_id'));
        return view('backend.project_tasks.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["tasksType"] = ["" => "None"]+Config::get('taskTypeConfig.tasksType');
        return view('backend.project_tasks.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectTaskRequest $projectTasksReq)
    {
        //try{
            $this->model->addData($projectTasksReq->all());
            return redirect($this->redirectUrl."?phase_id=".$projectTasksReq->get("phase_id"))->withErrors(["alert-success" => "Successfully added Project Task."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id )
    {
        //dump($techLvl);
        $project_tasks = ProjectTask::findOrFail($id);
        //dd($data);
        //dd($project_tasks);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "project_tasks.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["tasksType"] = ["" => "None"]+Config::get('taskTypeConfig.tasksType');
        return view('backend.project_tasks.edit',$data,compact('project_tasks'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectTaskRequest $projectTasksReq, $id)
    {
        try{
            $this->model->updateData($projectTasksReq->all(),$id);
            return redirect($this->redirectUrl."?phase_id=".$projectTasksReq->get("phase_id"))->withErrors(["alert-success" => "Successfully edited Project Task."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Project Task."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
