<?php

namespace App\Http\Controllers\Admin\leads;

use App\Clients;
use App\Http\Requests\LeadRequest;
use App\Lead;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;
use Config;


class LeadsController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,Lead $lead,Clients $clients)
    {
        $this->pageTitle = "Leads Management";
        $this->model = $lead;
        $this->users = $users;
        $this->clients = $clients;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/leads";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = PREFIX.'/leads';
        $datas["data"] = $this->model->getAllData(Input::get('keywords'));
        return view('backend.leads.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        $data["sources"] = ["" => "None"]+Config::get('LeadConfig.sources');
        return view('backend.leads.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadRequest $leadRequest)
    {
//        try{


            $leadObj = $this->model->addData($leadRequest->all());

            if($leadRequest->get("status") == "Verified"){
                $checkLead = $this->clients->where("lead_id","=",$leadObj->id)->first();
                if($checkLead == null){
                    $clientData = [];

                    $nameAttr = explode(" ",$leadRequest->get("name"));

                    if(array_key_exists(0,$nameAttr)){
                        $clientData["first_name"] = $nameAttr[0];
                    }

                    if(array_key_exists(1,$nameAttr)){
                        $clientData["last_name"] = $nameAttr[1];
                    }
                    $clientData["email"] = $leadRequest->get("email");
                    $clientData["phone"] = $leadRequest->get("phone");
                    $clientData["lead_id"] = $leadObj->id;
                    $this->clients->addData($clientData);
                }

            }

            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully added Lead."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "leads.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["sources"] = ["" => "None"]+Config::get('LeadConfig.sources');

        return view('backend.leads.edit',$data,compact('lead'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeadRequest $leadRequest, $id)
    {
        try{
            $this->model->updateData($leadRequest->all(),$id);
            $leadObj = $this->model->find($id);

            if($leadRequest->get("status") == "Verified"){
                $checkLead = $this->clients->where("lead_id","=",$leadObj->id)->first();
                if($checkLead == null){
                    $clientData = [];

                    $nameAttr = explode(" ",$leadRequest->get("name"));

                    if(array_key_exists(0,$nameAttr)){
                        $clientData["first_name"] = $nameAttr[0];
                    }

                    if(array_key_exists(1,$nameAttr)){
                        $clientData["last_name"] = $nameAttr[1];
                    }
                    $clientData["email"] = $leadRequest->get("email");
                    $clientData["phone"] = $leadRequest->get("phone");
                    $clientData["lead_id"] = $leadObj->id;
                    $this->clients->addData($clientData);
                }

            }

            return redirect($this->redirectUrl)->withErrors(["alert-success" => "Successfully edited Lead."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Lead."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
