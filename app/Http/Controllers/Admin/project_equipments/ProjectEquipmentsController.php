<?php

namespace App\Http\Controllers\Admin\project_equipments;

use App\Http\Requests\ProjectEquipmentRequest;
use App\ProjectEquipment;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;


class ProjectEquipmentsController extends Controller
{

    public function __construct(User $users,Permission $permisionModel,ProjectEquipment $projectEquipment)
    {
        $this->pageTitle = "Project Equipments";
        $this->model = $projectEquipment;
        $this->users = $users;
        $this->permisionModel = $permisionModel;
        $this->redirectUrl = PREFIX."/project_equipments";
        $this->respectiveProjectUrl = PREFIX."/projects";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Input::has('project_id') || !Input::has('task_id')){
            return redirect($this->respectiveProjectUrl)->withErrors(["alert-danger" => "Invalid URL access."]);
        }

        $datas["pageTitle"] = $this->pageTitle;
        $datas["formURL"] = $this->redirectUrl;
        $datas["data"] = $this->model->getAllData(Input::get('project_id'),Input::get('task_id'),Input::get('keywords'));
        return view('backend.project_equipments.index',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["routeName"] = $this->redirectUrl;
        $data["redirectBackURL"] = $this->redirectUrl;
        $data["pageTitle"] = $this->pageTitle;
        return view('backend.project_equipments.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectEquipmentRequest $projectEquipmentReq)
    {
        //dd($projectEquipmentReq->all());
        //try{
            $this->model->addData($projectEquipmentReq->all());
            return redirect($this->redirectUrl."?project_id=".$projectEquipmentReq->get("project_id")."&task_id=".Input::get('task_id'))->withErrors(["alert-success" => "Successfully added Equipment."]);
//        }catch (\Exception $e){
//            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id )
    {
        //dump($techLvl);
        $project_id = Input::get("project_id");
        $task_id = Input::get("task_id");
        //$project_equipment = ProjectEquipment::findOrFail($id);
        $project_equipment = ProjectEquipment::where("project_id","=",$project_id)->where("task_id","=",$task_id)->first();

        if($project_equipment == null){
            return redirect()->back()->withErrors(["alert-danger" => "The Data doesn't exists on our record."]);
        }
        //dd($data);
        //dd($project_equipment);
        $data["pageTitle"] = $this->pageTitle;
        $data["routeName"] = "project_equipments.update";
        $data["redirectBackURL"] = $this->redirectUrl;
        return view('backend.project_equipments.edit',$data,compact('project_equipment'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectEquipmentRequest $projectEquipmentReq, $id)
    {
        try{
            $this->model->updateData($projectEquipmentReq->all(),$id);
            return redirect($this->redirectUrl."?project_id=".$projectEquipmentReq->get("project_id")."&task_id=".Input::get('task_id'))->withErrors(["alert-success" => "Successfully edited Equipment."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->model->deleteData($id);
            return redirect()->back()->withErrors(["alert-success" => "Successfully deleted Equipment."]);
        }catch (\Exception $e){
            return redirect()->back()->withErrors(["alert-danger" => "An error Ocurred."]);
        }
    }
}
