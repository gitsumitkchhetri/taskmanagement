<?php
namespace App\Http\Controllers\Api\v1;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\Catalog;
use App\Model\Api\Categories;
use App\Model\Api\District;
use App\Model\Api\Region;
use App\Model\Api\RetailOutlet;
use App\Model\Api\Skuinventory;
use App\Model\Api\Town;
use App\Model\Api\Zones;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Transformers\NepalZoneSimplifiedTransformer;
use App\Transformers\RetailOutletTransformer;
use App\Transformers\ShopTransformer;
use App\Transformers\ZoneTransformer;
use League\Fractal\Resource\Collection;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;
use DB;
use Config;


class RetailOutletController extends Controller
{
    public function __construct()
    {
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->zonesTransformer = new ZoneTransformer();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
        $this->town = new Town();
        $this->category = new Categories();
        $this->retailOutlet = new RetailOutlet();
        $this->district = new District();
        $this->zone = new Zones();
        $this->brand = new Catalog();
    }




    /**
     * @SWG\Get(
     *   path="/shops/{shopId}",
     *   summary="Get Shops by id",
     *   operationId="get-shops",
     * tags={"Zones"},
     *  @SWG\Parameter(
     *         description="Get  Shops By Id",
     *         in="path",
     *         name="shopId",
     *         required=true,
     *         type="integer"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="Get Shop and associative Shop detail by filters"
     *   )
     * )
     */

    public function getAllShopsById($id)
    {
        $obj = new \ArrayObject();
        $shop = $this->retailOutlet->getShopsById($id);

        if (empty($shop) == true) {
            $code = ConstantCodeService::NOTFOUNDFIELDCODE;
            $key = 'id';
            $message = ConstantApiMessageService::NOTFOUNDSHOPID;
            $title = ConstantApiMessageService::INVALIDSHOPID;
            $status = ConstantStatusService::NOTFOUNDSTATUS;
            $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
            return response()->json($error, $status);
        }

        $retailOutlet = Config::get('nepalzone.retailoutlet');
        $commaSeparatedFields = implode(",", $retailOutlet);
        $shopQuery = "Select $commaSeparatedFields from retail_outlet where id=$id";
        $allShops = DB::Select($shopQuery);
        $baseUrl = env('ABS_URL');
        $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
        $resource = new Item($allShops, new ShopTransformer(), 'shop');
        $obj->append($this->manager->createData($resource)->toArray());
        unset($obj[0]['data']['links']);
        return $obj[0];

    }

    public function brandOutput($filterParams,$intersectKey,$dataPerPage,$page)
    {
        $filterValue = substr($filterParams, 1, -1);
        $filterString = app('App\Http\Controllers\Api\v1\ShopController')->paramParser($filterValue);
        $delimiters = preg_split('/\s*\(.*?\)\s*/', $filterParams);
        $catalogArray = array();
        $shopId =array();
        $shopOutput =array();
        if (count($filterString) > 1) {
            for ($i = 0; $i < count($filterString); $i++) {
                if (isset($filterString[$i]['positiveArray'])) {
                    $columnValue = $filterString[$i]['positiveArray'];
                    $value = "'" . implode("','", $columnValue) . "'";
                    $catalogQuery = "Select id from catalog where name In ($value)";
                    $catalogs = DB::select($catalogQuery);
                    $id = [];
                    foreach ($catalogs as $catalog) $id[] = $catalog->id;
                    array_push($catalogArray, $id);
                }

            }
            $retailId = array();
            foreach ($catalogArray as $catalogId) {
                $countData = count($catalogId);
                $allShops = RetailOutlet::join('retailshop_brand', 'retail_outlet.id', '=', 'retailshop_brand.retail_outlet_id')
                    ->whereIn('retailshop_brand.catalog_id',$catalogId)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.id')->get();
                foreach ($allShops as $allShop)  array_push($shopOutput,$allShop);

            }


            $newDelimiter = array_slice($delimiters, 1, -1);
            $lastIds = array();


            foreach ($newDelimiter as $key => $delimeter) {

                if ($delimeter == '+') {
                    if (!empty($retailId)) {
                        $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                    } else {
                        $lastIds =array();
                    }

                } elseif ($delimeter == '-') {
                  if (!empty($retailId)) {
                      $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                  } else {
                      $lastIds =array();
                  };

                }
                if (empty($lastIds) == false) {
                 $shopOutput =$lastIds;

                } else {
                    $shopOutput=array();
                    return $shopOutput;
                }

            }
            return $shopOutput;

        }


        $semiId = array();
        foreach ($filterString as $finalArray) {
            $table = 'catalog';
            $filterColumn = 'name';

            if (empty($finalArray['negativeArray']) == true) {
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $allShops = RetailOutlet::join('retailshop_brand', 'retail_outlet.id', '=', 'retailshop_brand.retail_outlet_id')
                        ->whereIn('retailshop_brand.catalog_id',$columnValue)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.id')->get();
                    foreach ($allShops as $allShop)  array_push($shopOutput,$allShop->id);

                }

            } else {
                $table = 'catalog';
                $filterColumn = 'name';
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }



                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $allShops = DB::select($shopQuery);
                    $allId = [];
                    foreach ($allShops as $allShop) $allId[] = $allShop->retail_outlet_id;

                }


                $value = $finalArray['negativeArray'];
                $columnName['catalog_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $negativeShops = DB::select($shopQuery);
                    $excludeId = [];
                    foreach ($negativeShops as $negativeShop) $excludeId[] = $negativeShop->retail_outlet_id;

                }



                $lastIds = array();
                $newDelimiter = array_slice($delimiters, 1, -1);


                if (empty($newDelimiter) == true) {

                    $lastIds = array_diff($allId, $excludeId);

                } else {

                    foreach ($newDelimiter as $key => $delimeter) {

                        if ($delimeter == '+') {
                            $lastIds = array_merge($semiId[$key], $semiId[$key+1]);

                        } elseif ($delimeter == '-') {
                            $lastIds = array_diff($semiId[$key], $semiId[$key+1]);
                        }
                    }
                }
                $shopOutput =$lastIds;

            }
            return $shopOutput;

        }


    }

    public function catalogOutput($filterParams,$intersectKey,$dataPerPage,$page)
    {
        $filterValue = substr($filterParams, 1, -1);
        $filterString = app('App\Http\Controllers\Api\v1\ShopController')->paramParser($filterValue);
        $delimiters = preg_split('/\s*\(.*?\)\s*/', $filterParams);
        $catalogArray = array();
        $shopOutput =array();
        if (count($filterString) > 1) {
            for ($i = 0; $i < count($filterString); $i++) {
                if (isset($filterString[$i]['positiveArray'])) {
                    $columnValue = $filterString[$i]['positiveArray'];
                    $value = "'" . implode("','", $columnValue) . "'";
                    $catalogQuery = "Select id from catalog where name In ($value)";
                    $catalogs = DB::select($catalogQuery);

                    $id = [];
                    foreach ($catalogs as $catalog) $id[] = $catalog->id;
                    array_push($catalogArray, $id);
                }

            }
            $retailId = array();
            foreach ($catalogArray as $catalogId) {
                $countData = count($catalogId);
                $allShops = RetailOutlet::join('retailshop_brand', 'retail_outlet.id', '=', 'retailshop_brand.retail_outlet_id')
                    ->whereIn('retailshop_brand.catalog_id',$catalogId)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.id')->get();
                foreach ($allShops as $allShop)  array_push($shopOutput,$allShop);

            }


            $newDelimiter = array_slice($delimiters, 1, -1);
            $lastIds = array();


            foreach ($newDelimiter as $key => $delimeter) {

                if ($delimeter == '+') {
                    if (!empty($retailId)){
                        $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                    }

                } elseif ($delimeter == '-') {
                if (!empty($retailId)){
                    $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                }

                }
                if (empty($lastIds) == false) {
                    $shopOutput =$lastIds;

                } else {
                    $shopOutput['data']=[];
                    return $shopOutput;
                }

            }
            return $shopOutput;

        }


        $semiId = array();
        foreach ($filterString as $finalArray) {
            $table = 'catalog';
            $filterColumn = 'name';

            if (empty($finalArray['negativeArray']) == true) {
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $allShops = RetailOutlet::join('retailshop_brand', 'retail_outlet.id', '=', 'retailshop_brand.retail_outlet_id')
                        ->whereIn('retailshop_brand.catalog_id',$columnValue)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.id')->get();
                    foreach ($allShops as $allShop)  array_push($shopOutput,$allShop->id);


                }

            } else {
                $table = 'catalog';
                $filterColumn = 'name';
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }



                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $allShops = DB::select($shopQuery);
                    $allId = [];
                    foreach ($allShops as $allShop) $allId[] = $allShop->retail_outlet_id;

                }


                $value = $finalArray['negativeArray'];
                $columnName['catalog_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $negativeShops = DB::select($shopQuery);
                    $excludeId = [];
                    foreach ($negativeShops as $negativeShop) $excludeId[] = $negativeShop->retail_outlet_id;

                }
                if (empty($finalArray['negativeArray']) == false && empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_diff($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery); foreach ($lastShops as $lastShop) array_push($shopOutput, $lastShop->id);
                } elseif (empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_merge($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);
                    foreach ($lastShops as $lastShop) array_push($shopOutput, $lastShop->id);
                }



//                $lastIds = array();
//                $newDelimiter = array_slice($delimiters, 1, -1);
//
//
//                if (empty($newDelimiter) == true) {
//
//                    $lastIds = array_diff($allId, $excludeId);
//
//                } else {
//
//                    foreach ($newDelimiter as $key => $delimeter) {
//
//                        if ($delimeter == '+') {
//                            $lastIds = array_merge($semiId[$key], $semiId[$key+1]);
//
//                        } elseif ($delimeter == '_') {
//                            $lastIds = array_diff($semiId[$key], $semiId[$key+1]);
//                        }
//                    }
//                }

            }
            return $shopOutput;

        }


    }

    public function skuInventoryOutput($filterParams,$intersectKey)
    {
        $filterValue = substr($filterParams, 1, -1);
        $filterString = app('App\Http\Controllers\Api\v1\ShopController')->paramParser($filterValue);
        $delimiters = preg_split('/\s*\(.*?\)\s*/', $filterParams);
        $skuArray = array();
        $shopOutput =array();
        if (count($filterString) > 1) {
            for ($i = 0; $i < count($filterString); $i++) {
                if (isset($filterString[$i]['positiveArray'])) {
                    $columnValue = $filterString[$i]['positiveArray'];
                    $value = "'" . implode("','", $columnValue) . "'";
                    $skuQuery = "Select id from skuinventory where name In ($value)";
                    $skuDatas = DB::select($skuQuery);
                    $id = [];
                    foreach ($skuDatas as $skuData) $id[] = $skuData->id;
                    array_push($skuArray, $id);
                }

            }
            $retailId = array();
            foreach ($skuArray as $skuId) {
                $countData = count($skuId);
                $allShops = RetailOutlet::join('retailshop_skus', 'retail_outlet.id', '=', 'retailshop_skus.retailoutlet_id')
                    ->whereIn('retailshop_skus.sku_id',$skuId)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.id')->get();
           foreach ($allShops as $allShop) array_push($shopOutput,$allShop->id);
            }


            $newDelimiter = array_slice($delimiters, 1, -1);
            $lastIds = array();


            foreach ($newDelimiter as $key => $delimeter) {

                if ($delimeter == '+') {
                    $lastIds = array_merge($retailId[$key], $retailId[$key + 1]);

                } elseif ($delimeter == '-') {
                    $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                }
                if (empty($lastIds) == false) {
                    array_push($shopOutput,$allShop->id);

                } else {
                    $shopOutput['data']=[];
                    return $shopOutput;
                }


            }
            return $shopOutput;

        }


        $semiId = array();
        foreach ($filterString as $finalArray) {
            $table = 'skuinventory';
            $filterColumn = 'name';

            if (empty($finalArray['negativeArray']) == true) {
                $value = $finalArray['positiveArray'];
                $columnName['sku_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $allShops = RetailOutlet::join('retailshop_skus', 'retail_outlet.id', '=', 'retailshop_skus.retailoutlet_id')
                        ->whereIn('retailshop_skus.sku_id',$columnValue)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.id')
                        ->get();
                    foreach ($allShops as $allShop) array_push($shopOutput,$allShop->id);

                }



            } else {

                $table = 'skuinventory';
                $filterColumn = 'name';
                $value = $finalArray['positiveArray'];
                $columnName['sku_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }



                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retailoutlet_id from retailshop_skus where sku_id IN ($value)";
                    $allShops = DB::select($shopQuery);
                    $allId = [];
                    foreach ($allShops as $allShop) $allId[] = $allShop->retailoutlet_id;

                }


                $value = $finalArray['negativeArray'];
                $columnName['sku_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retailoutlet_id from retailshop_skus where sku_id IN ($value)";
                    $negativeShops = DB::select($shopQuery);
                    $excludeId = [];
                    foreach ($negativeShops as $negativeShop) $excludeId[] = $negativeShop->retailoutlet_id;

                }
                if (empty($finalArray['negativeArray']) == false && empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_diff($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                } elseif (empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_merge($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                }


//
//                $lastIds = array();
//                $newDelimiter = array_slice($delimiters, 1, -1);
//
//
//                if (empty($newDelimiter) == true) {
//
//                    $lastIds = array_diff($allId, $excludeId);
//
//                    array_push($shopOutput,$lastIds);
//                } else {
//
//                    foreach ($newDelimiter as $key => $delimeter) {
//
//                        if ($delimeter == '+') {
//                            $lastIds = array_merge($semiId[$key], $semiId[$key+1]);
//
//                        } elseif ($delimeter == '_') {
//                            $lastIds = array_diff($semiId[$key], $semiId[$key+1]);
//                        }
//                        array_push($shopOutput,$lastIds);
//
//
//                    }
//
//                }


            }
            return $shopOutput;
        }


    }




    /**
     * @SWG\Get(
     *   path="/limitedshops",
     *   summary="Get Shop ",
     *   operationId="get-shops",
     * tags={"Zones"},
     * @SWG\Response(
     *     response=200,
     *     description="Get Shop"
     *   )
     * )
     */

    public function getLimitedShops()
    {
        $geomfield = 'geom';
        $srid = '4326';
        $query = "SELECT *, st_asgeojson(st_transform(" . $geomfield . ",$srid)) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),$srid),$srid)) AS centroid  FROM retail_outlet LIMIT 5";
        $allShops = DB::select($query);
        $shops = $this->simpleJson($allShops);
        return $shops;
    }

    public function simpleJson($allShops)
    {
        $json = [];
        foreach ($allShops as $shop) {
            $thisData['gid'] = $shop->id;
            $thisData['lat'] = $shop->latitude;
            $thisData['long'] = $shop->longitude;
            $json [] = $thisData;
        }
        return $json;
    }

    public function geoJsonFormat($allShops)
    {
        $json = [];
        foreach ($allShops as $shop) {
            $thisData['gid'] = $shop->gid;

            $shops ['type'] = 'Feature';
            $shops ['geometry'] = json_decode($shop->geojson);
            $shops['properties'] = $thisData;
            array_push($json, $shops);
        }
        $geoJson['type'] = "FeatureCollection";
        $geoJson['features'] = $json;
        return $geoJson;
    }



}