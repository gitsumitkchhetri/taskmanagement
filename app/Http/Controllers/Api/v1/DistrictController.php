<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\JsonParser\DefaultSerializer;
use App\Model\Api\District;
use App\Model\Api\Zones;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Transformers\DistrictTransformer;
use App\Transformers\NepalZoneSimplifiedTransformer;
use App\Transformers\ZoneTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use DB;
use Config;

class DistrictController extends Controller
{
    public function __construct()
    {
        $this->district = new District();
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->zonesTransformer = new ZoneTransformer();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
        $this->zones = new Zones();
    }

    /**
     * @SWG\Get(
     *   path="/districts",
     *   summary="Get district and associative zone detail by filters",
     *   operationId="get-districts",
     *  tags={"Zones"},
     *       @SWG\Parameter(
     *         name="fields",
     *         in="query",
     *         description="Get data by fields",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * @SWG\Parameter(
     *         name="filter",
     *         in="query",
     *         description="Get data by filter by zone id",
     *         required=false,
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="Get District and associative district detail by filters"
     *   )
     * )
     */

    public function getFilterResults(Request $request)
    {
        $geomfield = 'geom';
        $srid = '4326';
        $allZones = $this->zones->getZoneDetails();
        $allZonesId = [];
        foreach ($allZones as $allZone) $allZonesId[] = $allZone->gid;

        if ($request->exists('fields') == true && $request->exists('filter') == true) {
            $filter = $request->get('filter');
            $zone = $filter['zone'];
            $zoneIds = explode(',', $zone);
            $fieldQuery = $request->get('fields');
            $fields = explode(',', $fieldQuery);
            $nepalZoneField = str_replace("-", "_", $fields);
            $nepalZoneField = array_intersect(Config::get('nepalzone.districtfields'), $nepalZoneField);
            $commaSeparatedFields = implode(",", $nepalZoneField);
            $nepalZoneFieldId = array_intersect($allZonesId, $zoneIds);

            if (empty($zone) == true) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = $filter;
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            } else {
                if (app('App\Http\Controllers\Api\v1\ZoneController')->in_array_any($nepalZoneField, Config::get('nepalzone.districtfields')) == false) {
                    $code = ConstantCodeService::NOTFOUNDFIELDCODE;
                    $key = $fieldQuery;
                    $message = ConstantApiMessageService::NOTFOUNDFIELD;
                    $title = ConstantApiMessageService::INVALIDFIELD;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);

                } else {

                    if (empty($nepalZoneFieldId) == true) {
                        $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                        $key = $filter['zone'];
                        $message = ConstantApiMessageService::NOTFOUNDFILTER;
                        $title = ConstantApiMessageService::INVALIDFILTER;
                        $status = ConstantStatusService::NOTFOUNDSTATUS;
                        $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                        return response()->json($error, $status);
                    }

                    $zoneId= implode(',', $nepalZoneFieldId);

                    $query = "SELECT gid," . $commaSeparatedFields . ", st_asgeojson(geom) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),$srid),$srid)) AS centroid  FROM districts where zone_id IN ($zoneId)";

                    $coordinates = DB::select($query);
                    $baseUrl = env('ABS_URL');
                    $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                    $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'district');
                    return $this->manager->createData($resource)->toArray();
                }

            }


        } elseif ($request->exists('filter') == true) {
            $filter = $request->get('filter');
            $zone = $filter['zone'];
            $zoneIds = explode(',', $zone);
            $field =Config::get('nepalzone.districtfields');
            $commaSeparatedFields = implode(",", $field);
            $nepalZoneFieldId = array_intersect($allZonesId, $zoneIds);
            $zoneId = implode(",", $nepalZoneFieldId);
            if (empty($zone) == true) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = $filter;
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            } else {
                if (empty($nepalZoneFieldId) == true) {
                   $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                   $key = $filter['zone'];
                   $message = ConstantApiMessageService::NOTFOUNDFILTER;
                   $title = ConstantApiMessageService::INVALIDFILTER;
                   $status = ConstantStatusService::NOTFOUNDSTATUS;
                   $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                   return response()->json($error, $status);
               }

                $query = "SELECT gid," . $commaSeparatedFields . ", st_asgeojson(geom) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),$srid),$srid)) AS centroid  FROM districts where zone_id IN ($zoneId)";

                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'district');
                return $this->manager->createData($resource)->toArray();
            }

        } elseif ($request->exists('fields') == true) {
            $fieldQuery = $request->get('fields');
            $fields = explode(',', $fieldQuery);
            $field = str_replace("-", "_", $fields);

            if (app('App\Http\Controllers\Api\v1\ZoneController')->in_array_any($field, Config::get('nepalzone.districtfields')) == false) {
                $code = ConstantCodeService::NOTFOUNDFIELDCODE;
                $key = $fieldQuery;
                $message = ConstantApiMessageService::NOTFOUNDFIELD;
                $title = ConstantApiMessageService::INVALIDFIELD;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);

            } else {
                $districtField = array_intersect(Config::get('nepalzone.districtfields'), $field);
                $commaSeparatedFields = implode(",", $districtField);
                $query = "SELECT gid," . $commaSeparatedFields . ", st_asgeojson(geom) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),$srid),$srid)) AS centroid  FROM districts";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer($districtField), 'district');
                return $this->manager->createData($resource)->toArray();
            }
        }
        $field = Config::get('nepalzone.districtfields');
        $commaSeparatedFields = implode(",", $field);

        $query = "SELECT " . $commaSeparatedFields . ",st_asgeojson(geom) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),$srid),$srid)) AS centroid  FROM districts";
        $coordinates = DB::select($query);
        $baseUrl = env('ABS_URL');
        $this->manager->setSerializer(new DefaultSerializer($baseUrl));
        $resource = new Collection($coordinates, new DistrictTransformer(null), 'district');

        return $this->manager->createData($resource)->toArray();
    }

}