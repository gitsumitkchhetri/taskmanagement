<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\Channel;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Transformers\NepalZoneSimplifiedTransformer;
use App\Transformers\ZoneTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use DB;
use Config;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->zonesTransformer = new ZoneTransformer();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
        $this->channel = new Channel();
    }

    /**
     * @SWG\Get(
     *   path="/categories",
     *   summary="Get category and associative category detail by fields",
     *   operationId="get-categories",
     *  tags={"Zones"},
     *       @SWG\Parameter(
     *         name="fields",
     *         in="query",
     *         description="Get data by fields",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="Get category and associative category detail by fields"
     *   )
     * )
     */

    public function getFilterResults(Request $request)
    {
        $allChannels = $this->channel->getAllChannel();
        $allChannelsIds = [];
        foreach ($allChannels as $allChannel) $allChannelsIds[] = $allChannel->id;


        if ($request->exists('fields') == true && $request->exists('filter') == true) {
            $filter = $request->get('filter');
            $channel = $filter['channel'];
            $channelIds = explode(',', $channel);
            $fieldQuery = $request->get('fields');
            $fields = explode(',', $fieldQuery);
            $fieldQueryParam = str_replace("-", "_", $fields);
            $categoriesField = array_intersect(Config::get('nepalzone.categoryfields'), $fieldQueryParam);
            $commaSeparatedFields = implode(",", $categoriesField);
            $channelFieldId = array_intersect($allChannelsIds, $channelIds);

            if (empty($filter) == true || empty($channel) == true ) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                if (empty($filter)) $key = $channel; else $key = $filter;
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            } else {
                if (app('App\Http\Controllers\Api\v1\ZoneController')->in_array_any($fieldQueryParam, Config::get('nepalzone.categoryfields')) == false) {
                    $code = ConstantCodeService::NOTFOUNDFIELDCODE;
                    $key = $fieldQuery;
                    $message = ConstantApiMessageService::NOTFOUNDFIELD;
                    $title = ConstantApiMessageService::INVALIDFIELD;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);

                } else {
                    if (empty($channelFieldId) == true && $this->channel->countChannelById($channel) == 0 ) {
                        $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                        $key = $filter['channel'];
                        $message = ConstantApiMessageService::NOTFOUNDFILTER;
                        $title = ConstantApiMessageService::INVALIDFILTER;
                        $status = ConstantStatusService::NOTFOUNDSTATUS;
                        $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                        return response()->json($error, $status);
                    }

                    $channelId = "'".implode ("','",array_unique($channelFieldId))."'";
                    $query = "SELECT id," . $commaSeparatedFields . " FROM category where channel_id IN ($channelId)";
                    $coordinates = DB::select($query);
                    $baseUrl = env('ABS_URL');
                    $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                    $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'category');
                    return $this->manager->createData($resource)->toArray();
                }

            }


        } elseif ($request->exists('filter') == true) {
            $filter = $request->get('filter');
            $channel = $filter['channel'];
            $channelIds = explode(',', $channel);
            $field =Config::get('nepalzone.categoryfields');
            $commaSeparatedFields = implode(",", $field);
            $channelFieldId = array_intersect($allChannelsIds, $channelIds);

            if (empty($filter) == true || empty($channel) == true ) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                if (empty($filter)) $key = $channel; else $key = $filter;
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            } else {
                if (empty($channelFieldId) == true && $this->channel->countChannelById($channelIds) == 0 ) {
                    $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                    $key = $filter['channel'];
                    $message = ConstantApiMessageService::NOTFOUNDFILTER;
                    $title = ConstantApiMessageService::INVALIDFILTER;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
                $channelId = "'".implode ("','", array_unique($channelFieldId))."'";
                $query = "SELECT id," . $commaSeparatedFields . " FROM category where channel_id IN ($channelId)";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'category');
                return $this->manager->createData($resource)->toArray();


            }

        }elseif ($request->exists('fields') == true) {
            $fieldQuery = $request->get('fields');
            $fields = explode(',', $fieldQuery);
            $field = str_replace("-", "_", $fields);

            if (app('App\Http\Controllers\Api\v1\ZoneController')->in_array_any($field, Config::get('nepalzone.townfields')) == false) {
                $code = ConstantCodeService::NOTFOUNDFIELDCODE;
                $key = $fieldQuery;
                $message = ConstantApiMessageService::NOTFOUNDFIELD;
                $title = ConstantApiMessageService::INVALIDFIELD;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);

            } else {
                $categoryField = array_intersect(Config::get('nepalzone.categoryfields'), $field);
                $commaSeparatedFields = implode(",", $categoryField);
                $query = "SELECT id," .$commaSeparatedFields. " FROM category";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'category');
                return $this->manager->createData($resource)->toArray();
            }

        }
        $field =Config::get('nepalzone.categoryfields');
        $commaSeparatedFields = implode(",", $field);

        $query = "SELECT " .$commaSeparatedFields. " FROM category";
        $coordinates = DB::select($query);
        $baseUrl = env('ABS_URL');
        $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
        $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'category');
        return $this->manager->createData($resource)->toArray();
    }
}