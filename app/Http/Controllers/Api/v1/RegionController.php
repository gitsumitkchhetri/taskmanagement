<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\Region;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Transformers\NepalZoneSimplifiedTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use DB;
use Config;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   schemes={"http"},
 *   host="",
 *   @SWG\Info(
 *     title="Rosia",
 *     version="v1"
 *   )
 * )
 */
class RegionController extends Controller
{

    public function __construct()
    {
        $this->region = new Region();
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
    }

    /**
     * @SWG\Get(
     *   path="/zones",
     *   summary="Get Zone and associative zone detail by filters",
     *   operationId="get-zones",
     *  tags={"Zones"},
     *       @SWG\Parameter(
     *         name="fields",
     *         in="query",
     *         description="Get data by fields",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * @SWG\Parameter(
     *         name="filter",
     *         in="query",
     *         description="Get data by filter array",
     *         required=false,
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="Get Zone and associative zone detail by filters"
     *   )
     * )
     */

    public function getFilterResults(Request $request)
    {
        $geomfield = 'geom';
        $srid = '4326';
        if ($request->exists('fields') == true) {
            $fieldQuery = $request->get('fields');
            $fields = explode(',', $fieldQuery);
            $fieldParameter = str_replace("-", "_", $fields);

            if ($this->in_array_any($fieldParameter, Config::get('nepalzone.developmentregionfields')) == false) {
                $code = ConstantCodeService::NOTFOUNDFIELDCODE;
                $key = $fieldQuery;
                $message = ConstantApiMessageService::NOTFOUNDFIELD;
                $title = ConstantApiMessageService::INVALIDFIELD;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);

            } else {
                $developmentRegionFields = array_intersect(Config::get('nepalzone.developmentregionfields'), $fieldParameter);
                $commaSeparatedFields = implode(",", $developmentRegionFields);
                $query = "SELECT gid," . $commaSeparatedFields . ", st_asgeojson(geom) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),$srid),$srid)) AS centroid  FROM development_regions";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer($fieldParameter), 'region');
                return $this->manager->createData($resource)->toArray();
            }

        }
        $field =Config::get('nepalzone.developmentregionfields');
        $commaSeparatedFields = implode(",", $field);
        $query = "SELECT gid," . $commaSeparatedFields . ", st_asgeojson(geom) AS geojson, st_asgeojson(st_transform(ST_SetSRID(ST_AsText(ST_Centroid(geom)),$srid),$srid)) AS centroid  FROM development_regions";
        $region = DB::select($query);
        $baseUrl = env('ABS_URL');
        $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
        $resource = new Collection($region, new NepalZoneSimplifiedTransformer(null), 'region');
        return $this->manager->createData($resource)->toArray();
    }

    public function in_array_any($fields, $configData)
    {
        return !!array_intersect($fields, $configData);
    }


}