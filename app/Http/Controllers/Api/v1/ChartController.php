<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 4/24/17
 * Time: 4:12 PM
 */

namespace App\Http\Controllers\Api\v1;


use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\Catalog;
use App\Model\Api\Channel;
use App\Model\Api\District;
use App\Model\Api\RetailOutlet;
use App\Model\Api\Zones;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use Illuminate\Http\Request;
ini_set('max_execution_time', 30000);
ini_set('memory_limit', '100480M');

class ChartController extends  Controller
{
    public function __construct()
    {
        $this->jsonValidationRequest = new JsonValidationRequest();
    }

    public function countAllChartData(Request $request)
    {
        $filter = $request->get('filter');

        if (empty($filter) == true) {
            $zones = Zones::orderBy('gid','ASC')->select('gid','name')->get();
            $districts = District::orderBy('gid','ASC')->select('gid','name')->get();
            $categories = Category::orderBy('id','ASC')->select('id','name')->get();
            $channels = Channel::orderBy('id','ASC')->select('id','name')->get();
            $zoneData['zone'] = [];
            $zoneData['category'] =[];
            $zoneData['district'] =[];
            $zoneData['channel'] =[];
            foreach ($zones as $zone) {
                $countZoneShop = RetailOutlet::where('zone_id',$zone->gid)->count();
                $zoneName['name'] = $zone->name;
                $zoneName['shops']  = $countZoneShop;
                array_push($zoneData['zone'],$zoneName);
            }
            foreach ($districts as $district) {
                $countDistrictShop = RetailOutlet::where('district_id',$district->gid)->count();
                $districtName['name'] = $district->name;
                $districtName['shops']  = $countDistrictShop;
                array_push( $zoneData['district'],$districtName);
            }
            foreach ($categories as $category) {
                $countCategoryShop = RetailOutlet::where('category_id',$category->id)->count();
                $categoryName['name'] = $category->name;
                $categoryName['shops']  = $countCategoryShop;
                array_push( $zoneData['category'],$categoryName);
            }
            foreach ($channels as $channel) {
                $countChannelShop = RetailOutlet::where('channel_id',$channel->id)->count();
                $channelName['name'] = $channel->name;
                $channelName['shops']  = $countChannelShop;
                array_push( $zoneData['channel'],$channelName);
            }
            return $zoneData;
        } elseif ($filter) {
            $filterKey = array('town', 'category', 'catalog', 'district', 'region', 'zone', 'skuinventory');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);
            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
        if (in_array('catalog', $intersectKeys)&&
            in_array('skuinventory', $intersectKeys)&&
            in_array('town', $intersectKeys)&&
            in_array('category', $intersectKeys)) {
            $townIds =array();
            $categoryIds = array();
            foreach ($intersectKeys as $intersectKey) {
                if ($intersectKey == 'catalog') {
                    $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                }elseif ($intersectKey == 'skuinventory'){
                    $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                } elseif ($intersectKey == 'town') {
                    $table = 'town';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$intersectKey] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($townIds,$val);

                    }
                }elseif ($intersectKey == 'category') {
                    $table = 'retail_outlet_categories';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$table] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($categoryIds,$val);

                    }
                }

            }
            $outletId = array_intersect($catalogShopsId,$skuShopsId);
            $data =array();

            foreach ($outletId as $id) {
                $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                foreach ($allShops as $shop) array_push($data,$shop->id);
            }

            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.id',$data)
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        }elseif (in_array('skuinventory', $intersectKeys)&&
            in_array('town', $intersectKeys)&&
            in_array('category', $intersectKeys)) {
            $townIds =array();
            $categoryIds = array();
            foreach ($intersectKeys as $intersectKey) {
                if ($intersectKey == 'skuinventory'){
                    $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                } elseif ($intersectKey == 'town') {
                    $table = 'town';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$intersectKey] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($townIds,$val);

                    }
                }elseif ($intersectKey == 'category') {
                    $table = 'retail_outlet_categories';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$table] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($categoryIds,$val);

                    }
                }

            }
            $data =array();

            foreach ($skuShopsId as $id) {
                $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                foreach ($allShops as $shop) array_push($data,$shop->id);
            }

            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.id',$data)
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        }elseif (in_array('catalog', $intersectKeys)&&
            in_array('town', $intersectKeys)&&
            in_array('category', $intersectKeys)) {
            $townIds =array();
            $categoryIds = array();
            foreach ($intersectKeys as $intersectKey) {
                if ($intersectKey == 'catalog') {
                    $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                } elseif ($intersectKey == 'town') {
                    $table = 'town';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$intersectKey] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($townIds,$val);

                    }
                }elseif ($intersectKey == 'category') {
                    $table = 'retail_outlet_categories';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$table] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($categoryIds,$val);

                    }
                }

            }
            $data =array();

            foreach ($catalogShopsId as $id) {
                $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                foreach ($allShops as $shop) array_push($data,$shop->id);
            }

            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.id',$data)
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        }elseif (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
                $allData = $this->countChart($retailOutlets);
                return $allData;

            } elseif (in_array('catalog', $intersectKeys)&&
            in_array('skuinventory', $intersectKeys)&&
            in_array('category', $intersectKeys)) {
            $townIds =array();
            $categoryIds =array();

            foreach ($intersectKeys as $intersectKey) {
                if ($intersectKey == 'catalog') {
                    $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                }elseif ($intersectKey == 'skuinventory'){
                    $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                } elseif ($intersectKey == 'category') {
                    $table = 'retail_outlet_categories';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$table] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($categoryIds,$val);

                    }
                }

            }
            $outletId = array_intersect($catalogShopsId,$skuShopsId);
            $data =array();

            foreach ($outletId as $id) {
                $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('catgeory_id',$categoryIds[0])->select('id')->get();
                foreach ($allShops as $shop) array_push($data,$shop->id);
            }

            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.id',$data)
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        }elseif (in_array('catalog', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);


                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $data =array();
                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }
                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
                $allData = $this->countChart($retailOutlets);
                return $allData;

            } elseif (in_array('category', $intersectKeys)&&
            in_array('town', $intersectKeys)) {
            $townIds =array();
            $categoryIds=array();

            foreach ($intersectKeys as $intersectKey) {
                if ($intersectKey == 'category') {
                    $table = 'retail_outlet_categories';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$table] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($categoryIds,$val);

                    }
                } elseif ($intersectKey == 'town') {
                    $table = 'town';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$intersectKey] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($townIds,$val);

                    }
                }

            }

            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.town_id',$townIds[0])
                ->whereIn('retail_outlet.category_id',$categoryIds[0])
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        }elseif  (in_array('catalog', $intersectKeys)&&
            in_array('skuinventory', $intersectKeys)) {
            $allShops =[];
            foreach ($intersectKeys as $intersectKey) {

                if ($intersectKey == 'catalog') {
                    $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->catalogOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    array_push($allShops,$shops);

                } elseif ($intersectKey == 'skuinventory') {
                    $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    array_push($allShops,$shops);

                }
            }
            $outletId = array_intersect($allShops[0],$allShops[1]);
            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.id',$outletId)
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        } elseif(in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }

                }


            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.town_id',$townIds[0])
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        } elseif(in_array('category', $intersectKeys)) {

            foreach ($intersectKeys as $intersectKey) {
                $categoryIds =array();
                $table = 'retail_outlet_categories';
                $filterColumn = 'id';
                $value= $filter[$intersectKey];
                $tableFilter[$table] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }
                foreach ($intersectValues as $value){
                    $val = explode(',',$value);
                    array_push($categoryIds,$val);

                }

            }


            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.category_id',$categoryIds[0])
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        } elseif(in_array('district', $intersectKeys)) {

            foreach ($intersectKeys as $intersectKey) {
                $districtsId =array();
                $table = 'districts';
                $filterColumn = 'gid';
                $value= $filter[$intersectKey];
                $tableFilter[$table] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }
                foreach ($intersectValues as $value){
                    $val = explode(',',$value);
                    array_push($districtsId,$val);

                }

            }


            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->whereIn('retail_outlet.district_id',$districtsId[0])
                ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
            $allData = $this->countChart($retailOutlets);
            return $allData;

        }elseif(in_array('catalog', $intersectKeys)) {

                foreach ($intersectKeys as $intersectKey) {
                    $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->catalogOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                }
                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$shops)
                    ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
                $allData = $this->countChart($retailOutlets);
                return $allData;

            }elseif(in_array('skuinventory', $intersectKeys)){
                foreach ($intersectKeys as $intersectKey) {
                    $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey,null,null);

                }

                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$shops)
                    ->get(array('zones.gid as zoneid','districts.gid as districtid','retail_outlet_categories.id as categoryid','retail_outlet_channel.id as channelid','retail_outlet.id as outletid'));
                $allData = $this->countChart($retailOutlets);
                return $allData;


            }

        }


    }
    public function countChart($retailOutlets)
    {
        $zoneIds = array();
        $categoryIds =array();
        $channelIds =array();
        $districtIds =array();
        $outletIds = array();

        foreach ($retailOutlets as $retailOutlet) {
            array_push($zoneIds,$retailOutlet->zoneid);
            array_push($districtIds,$retailOutlet->districtid);
            array_push($channelIds,$retailOutlet->channelid);
            array_push($categoryIds,$retailOutlet->categoryid);
            array_push($categoryIds,$retailOutlet->categoryid);
            array_push($outletIds,$retailOutlet->outletid);
        }

        $zoneId =array_unique($zoneIds);
        $categoryId = array_unique($categoryIds);
        $channelId = array_unique($channelIds);
        $districtId = array_unique($districtIds);

        $allData['zone'] = array();
        $allData['category'] =array();
        $allData['district'] =array();
        $allData['channel'] =array();
        $allZones = Zones::select('name')->get();
        $allDistricts = District::select('name')->get();
        $allCategories = Category::select('name')->get();
        $allChannels = Channel::select('name')->get();
        $zoneNames=[];
        $districtNames = [];
        $categoryNames = [];
        $channelNames =[];
        $filterZoneName =[];
        $filterDistrictName =[];
        $filterCategoryName =[];
        $filterChannelName =[];
        $selectedZones = RetailOutlet::join('zones','retail_outlet.zone_id','zones.gid')->whereIn('retail_outlet.id',$outletIds)->groupBy('zones.name')->select('zones.name')->get();
        $selectedDistricts = RetailOutlet::join('districts','retail_outlet.district_id','districts.gid')->whereIn('retail_outlet.id',$outletIds)->groupBy('districts.name')->select('districts.name')->get();
        $selectedCategory = RetailOutlet::join('retail_outlet_categories','retail_outlet.category_id','retail_outlet_categories.id')->whereIn('retail_outlet.id',$outletIds)->groupBy('retail_outlet_categories.name')->select('retail_outlet_categories.name')->get();
        $selectedChannels = RetailOutlet::join('retail_outlet_channel','retail_outlet.channel_id','retail_outlet_channel.id')->whereIn('retail_outlet.id',$outletIds)->groupBy('retail_outlet_channel.name')->select('retail_outlet_channel.name')->get();

        foreach ($allZones as $zone) $zoneNames[]=$zone->name;
        foreach ($allChannels as $channel) $channelNames[]=$channel->name;
        foreach ($allCategories as $allCategory) $categoryNames[]=$allCategory->name;
        foreach ($allDistricts as $allDistrict) $districtNames[]=$allDistrict->name;

        foreach ($selectedZones as $selectedZone) $filterZoneName[]=$selectedZone->name;
        foreach ($selectedDistricts as $selectedDistrict) $filterDistrictName[]=$selectedDistrict->name;
        foreach ($selectedCategory as $selectedCat) $filterCategoryName[]=$selectedCat->name;
        foreach ($selectedChannels as $selectedChannel) $filterChannelName[]=$selectedChannel->name;
        $allZonesName =array_diff($zoneNames,$filterZoneName);
        foreach ($allZonesName as $allZone) {
            $zoneDetail['name']=$allZone;
            $zoneDetail['shops'] = 0;
            array_push($allData['zone'],$zoneDetail);
        }
        $allDistrictsName =array_diff($districtNames,$filterDistrictName);
        foreach ($allDistrictsName as $district) {
            $zoneDetail['name']=$district;
            $zoneDetail['shops'] = 0;
            array_push($allData['district'],$zoneDetail);
        }
        $allCategoriesName =array_diff($categoryNames,$filterCategoryName);
        foreach ($allCategoriesName as $allCategory) {
            $zoneDetail['name']=$allCategory;
            $zoneDetail['shops'] = 0;
            array_push($allData['category'],$zoneDetail);
        }
        $allChannelsName =array_diff($channelNames,$filterChannelName);
        foreach ($allChannelsName as $allChannel) {
            $zoneDetail['name']=$allChannel;
            $zoneDetail['shops'] = 0;
            array_push($allData['channel'],$zoneDetail);
        }

        foreach ($zoneId as $id) {
            $zoneName = Zones::where('gid',$id)->first();
            $zoneDetail['name']=$zoneName->name;
            $zoneDetail['shops'] = count(array_keys($zoneIds, $id));
            array_push($allData['zone'],$zoneDetail);
        }
        foreach ($districtId as $id) {
            $districtName = District::where('gid',$id)->first();
            $districtDetail['name']=$districtName->name;
            $districtDetail['shops'] = count(array_keys($districtIds, $id));
            array_push($allData['district'],$districtDetail);
        }
        foreach ($channelId as $id) {
            $channelName = Channel::where('id',$id)->first();
            $channelDetail['name']=$channelName->name;
            $channelDetail['shops'] = count(array_keys($channelIds, $id));
            array_push($allData['channel'],$channelDetail);
        }
        foreach ($categoryId as $id) {
            $categoryName = Category::where('id',$id)->first();
            $categoryDetail['name']=$categoryName->name;
            $categoryDetail['shops'] = count(array_keys($categoryIds, $id));
            array_push($allData['category'],$categoryDetail);
        }

        return $allData;


    }
}