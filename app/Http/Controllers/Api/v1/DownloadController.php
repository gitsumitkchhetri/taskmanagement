<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 4/24/17
 * Time: 5:00 PM
 */

namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;
use App\Model\Api\Categories;
use App\Model\Api\Channel;
use App\Model\Api\District;
use App\Model\Api\RetailOutlet;
use App\Model\Api\Town;
use App\Model\Api\Zones;
use App\RetailOutletUser;
use App\Services\ConstantStatusService;
use App\Street;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function downloadShops(Request $request)
    {
        $filter = $request->get('filter');

        if (empty($filter) == true) {
            ini_set('max_execution_time', 30000);
            ini_set('memory_limit', '100480M');
            $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
            $outlet = $this->exportResponse($retailOutlets);
            return $outlet;

        } else {
            $filterKey = array('town', 'category', 'catalog', 'district', 'region', 'zone', 'skuinventory');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);
            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
            if (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $townIds =array();
                $categoryIds = array();
                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;

            }elseif (in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $townIds =array();
                $categoryIds = array();
                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $data =array();

                foreach ($skuShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;

            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $townIds =array();
                $categoryIds = array();
                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $data =array();

                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;

            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;

            } elseif (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $townIds =array();
                $categoryIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('catgeory_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;


            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);


                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $data =array();
                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }
                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;

            }elseif (in_array('category', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();
                $categoryIds = array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $data =array();

                    $allShops = RetailOutlet::whereIn('category_id',$categoryIds[0])->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$data)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;

            } elseif  (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)) {
                $allShops =[];
                foreach ($intersectKeys as $intersectKey) {

                    if ($intersectKey == 'catalog') {
                        $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->catalogOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                        array_push($allShops,$shops);

                    } elseif ($intersectKey == 'skuinventory') {
                        $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                        array_push($allShops,$shops);

                    }
                }
                $outletId = array_intersect($allShops[0],$allShops[1]);
                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$outletId)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;


            } elseif(in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    $table = 'town';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$intersectKey] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($townIds,$val);

                    }

                }


                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.town_id',$townIds[0])
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;


            } elseif(in_array('category', $intersectKeys)) {
                foreach ($intersectKeys as $intersectKey) {
                    $categoryIds =array();
                    $table = 'retail_outlet_categories';
                    $filterColumn = 'id';
                    $value= $filter[$intersectKey];
                    $tableFilter[$table] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($categoryIds,$val);

                    }

                }


                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.category_id',$categoryIds[0])
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;


            } elseif(in_array('district', $intersectKeys)) {

                foreach ($intersectKeys as $intersectKey) {
                    $districtsId =array();
                    $table = 'districts';
                    $filterColumn = 'gid';
                    $value= $filter[$intersectKey];
                    $tableFilter[$table] = $value;
                    $columnId[$table] = $filterColumn;
                    $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                    if (isset($intersectValues['errors']) == true) {
                        return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                    }
                    foreach ($intersectValues as $value){
                        $val = explode(',',$value);
                        array_push($districtsId,$val);

                    }

                }


                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.district_id',$districtsId[0])
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;


            }elseif(in_array('catalog', $intersectKeys)) {
                foreach ($intersectKeys as $intersectKey) {
                    $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->catalogOutput($filter[$intersectKey]['name'],$intersectKey,null,null);

                }
                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$shops)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;


            }elseif(in_array('skuinventory', $intersectKeys)){
                foreach ($intersectKeys as $intersectKey) {
                    $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey,null,null);

                }
                $retailOutlets = RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id',$shops)
                    ->get(array('zones.name as z','street.name as s','town.name as t','phone as p','districts.name as d','retail_outlet_categories.name as c','retail_outlet_channel.name as ch','retail_outlet.id as outletid','retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'));
                $outlet = $this->exportResponse($retailOutlets);
                return $outlet;


            }

        }
    }

    public function exportResponse($retailOutlets)
    {
        $outlets['shops'] = $retailOutlets;
        $outlets['helper'] = array('n' => 'name',
            'on' => 'owner name',
            'op' => 'owner_phone',
            'ln' => 'longitude',
            'lt' => 'latitude',
            'ss' => 'store size',
            'mab' => 'monthly avg business',
            'vo' => 'verified_on',
            'hr' => 'hotspot',
            'vb' => 'verified_by',
            'rb' => 'registered_by',
            's' => 'street',
            'c' => 'category',
            't' => 'town',
            'd' => 'district',
            'ch' => 'channel',
            'z' => 'zone',
            'p'=>'phone'

        );

        return $outlets;
    }

}