<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 4/17/17
 * Time: 1:17 PM
 */

namespace App\Http\Controllers\Api\v1;


use League\Fractal\TransformerAbstract;

class ZoneDistrictTransformer extends TransformerAbstract
{

    public function transform($district)
    {
        return [
            'id' => $district->gid,
            'name' => $district->name,
            'geojson' => $district->geojson,
            'centroid' => $district->centroid,
        ];


    }
}