<?php
namespace App\Http\Controllers\Api\v1;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\Catalog;
use App\Model\Api\Categories;
use App\Model\Api\District;
use App\Model\Api\Region;
use App\Model\Api\RetailOutlet;
use App\Model\Api\Skuinventory;
use App\Model\Api\Town;
use App\Model\Api\Zones;
use App\MysqlCatalog;
use App\MysqlRetailOutlet;
use App\MysqlRetailOutletBrand;
use App\MysqlRetailOutletSku;
use App\MysqlRetailStreet;
use App\MysqlStreet;
use App\RetailOutletBrand;
use App\RetailOutletSku;
use App\RetailOutletStreet;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Street;
use App\Transformers\NepalZoneSimplifiedTransformer;
use App\Transformers\RetailOutletTransformer;
use App\Transformers\ShopTransformer;
use App\Transformers\ZoneTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use DB;
use Config;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;


class ShopController extends Controller
{
    public function __construct()
    {
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->zonesTransformer = new ZoneTransformer();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
        $this->town = new Town();
        $this->category = new Categories();
        $this->retailOutlet = new RetailOutlet();
        $this->district = new District();
        $this->zone = new Zones();
        $this->brand = new Catalog();
    }

    public function getSearchResults(Request $request)
    {

        ini_set('max_execution_time', 30000);
        ini_set('memory_limit', '100480M');

        $jsonDatas = $request->json()->all();
        $type = [];
        $filter = [];
        $data = [];
        foreach ($jsonDatas as $json) {
            $type[] = $json['type'];
            $filter[$json['type']] = $json['filter'];
            $data[$json['type']] = $json['data'];
        }

        if (empty($json['type']) == true
            && empty($json['filter']) == true
            && empty($json['data']) == true
        ) {
            $code = ConstantCodeService::NOTFOUNDFILTERCODE;
            $key = 'search-parameter';
            $message = ConstantApiMessageService::NOTFOUNDFILTER;
            $title = ConstantApiMessageService::INVALIDFILTER;
            $status = ConstantStatusService::NOTFOUNDSTATUS;
            $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
            return response()->json($error, $status);
        } elseif (empty($json['type']) == true) {
            $code = ConstantCodeService::NOTFOUNDTYPECODE;
            $key = 'type';
            $message = ConstantApiMessageService::NOTFOUNDTYPE;
            $title = ConstantApiMessageService::INVALIDTYPE;
            $status = ConstantStatusService::NOTFOUNDSTATUS;
            $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
            return response()->json($error, $status);

        } elseif (empty($json['filter']) == true) {
            $code = ConstantCodeService::NOTFOUNDFILTERCODE;
            $key = 'filter';
            $message = ConstantApiMessageService::NOTFOUNDFILTER;
            $title = ConstantApiMessageService::INVALIDFILTER;
            $status = ConstantStatusService::NOTFOUNDSTATUS;
            $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
            return response()->json($error, $status);

        } elseif (empty($json['data']) == true) {
            $code = ConstantCodeService::NOTFOUNDDATACODE;
            $key = 'data';
            $message = ConstantApiMessageService::NOTFOUNDDATA;
            $title = ConstantApiMessageService::INVALIDDATA;
            $status = ConstantStatusService::NOTFOUNDSTATUS;
            $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
            return response()->json($error, $status);
        }


        if (in_array("category", $type) == true &&
            in_array("town", $type) == true &&
            in_array("brand", $type) == true &&
            in_array("product", $type) == true
        ) {

            $category = $data['category'];
            $categoryName = "'" . implode("','", $category) . "'";
            $categoryId = [];

            $brand = $data['brand'];
            $brandName = "'" . implode("','", $brand) . "'";
            $brandId = [];

            $town = $data['town'];
            $townName = "'" . implode("','", $town) . "'";
            $townId = [];

            $product = $data['product'];
            $productName = "'" . implode("','", $product) . "'";
            $productId = [];


            if ($filter['product'] == 'And') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name)  IN (' . strtolower($productName) . ')')->get();

                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['product'] == 'Not') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name) NOT IN (' . strtolower($productName) . ')')->get();


                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['town'] == 'And') {
                $allTownDatas = Town::whereRaw('LOWER(name)  IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['town'] == 'Not') {
                $allTownDatas = Town::whereRaw('LOWER(name) NOT IN (' . strtolower($townName) . ')')->get();


                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['category'] == 'And') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name)  IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['category'] == 'Not') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['brand'] == 'And') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name)  IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['brand'] == 'Not') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name) NOT IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            }

            foreach ($allProductDatas as $allProductData) $productId [] = $allProductData->id;
            foreach ($allTownDatas as $allTownData) $townId [] = $allTownData->id;
            foreach ($allCatalogDatas as $allCatalogData) $brandId [] = $allCatalogData->id;
            foreach ($allCategoryDatas as $allCategoryData) $categoryId [] = $allCategoryData->id;


            $allShops = RetailOutlet::whereIn('town_id', $townId)
                ->whereIn('catalog_id', $brandId)
                ->whereIn('category_id', $categoryId)
                ->whereIn('sku_id', $productId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;


        } elseif (in_array("category", $type) == true && in_array("product", $type) == true) {
            $category = $data['category'];
            $categoryName = "'" . implode("','", $category) . "'";
            $categoryId = [];

            $product = $data['product'];
            $productName = "'" . implode("','", $product) . "'";
            $productId = [];


            if ($filter['product'] == 'And') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name)  IN (' . strtolower($productName) . ')')->get();

                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['product'] == 'Not') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name) NOT IN (' . strtolower($productName) . ')')->get();


                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['category'] == 'And') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name)  IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['category'] == 'Not') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }


            foreach ($allProductDatas as $allProductData) $productId [] = $allProductData->id;
            foreach ($allCategoryDatas as $allCategoryData) $categoryId [] = $allCategoryData->id;

            $allShops = RetailOutlet::whereIn('sku_id', $productId)
                ->whereIn('category_id', $categoryId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;


        } elseif (in_array("brand", $type) == true && in_array("product", $type) == true) {
            $brand = $data['brand'];
            $brandName = "'" . implode("','", $brand) . "'";
            $brandId = [];

            $product = $data['product'];
            $productName = "'" . implode("','", $product) . "'";
            $productId = [];


            if ($filter['product'] == 'And') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name)  IN (' . strtolower($productName) . ')')->get();

                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['product'] == 'Not') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name) NOT IN (' . strtolower($productName) . ')')->get();


                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['brand'] == 'And') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name)  IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['brand'] == 'Not') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name) NOT IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            }


            foreach ($allProductDatas as $allProductData) $productId [] = $allProductData->id;
            foreach ($allCatalogDatas as $allCatalogData) $brandId [] = $allCatalogData->id;

            $allShops = RetailOutlet::whereIn('sku_id', $productId)
                ->whereIn('catalog_id', $brandId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;


        } elseif (in_array("product", $type) == true && in_array("town", $type) == true) {
            $product = $data['product'];
            $productName = "'" . implode("','", $product) . "'";
            $productId = [];

            $town = $data['town'];
            $townName = "'" . implode("','", $town) . "'";
            $townId = [];

            if ($filter['town'] == 'And') {
                $allTownDatas = Town::whereRaw('LOWER(name)  IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['town'] == 'Not') {
                $allTownDatas = Town::whereRaw('LOWER(name) NOT IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['product'] == 'And') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name)  IN (' . strtolower($productName) . ')')->get();

                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['product'] == 'Not') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name) NOT IN (' . strtolower($productName) . ')')->get();


                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }


            foreach ($allTownDatas as $allTownData) $townId [] = $allTownData->id;
            foreach ($allProductDatas as $allProductData) $productId [] = $allProductData->id;

            $allShops = RetailOutlet::whereIn('town_id', $townId)
                ->whereIn('sku_id', $productId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;


        } elseif (in_array("category", $type) == true && in_array("town", $type) == true) {
            $category = $data['category'];
            $categoryName = "'" . implode("','", $category) . "'";
            $categoryId = [];

            $town = $data['town'];
            $townName = "'" . implode("','", $town) . "'";
            $townId = [];

            if ($filter['town'] == 'And') {
                $allTownDatas = Town::whereRaw('LOWER(name)  IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['town'] == 'Not') {
                $allTownDatas = Town::whereRaw('LOWER(name) NOT IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['category'] == 'And') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name)  IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['category'] == 'Not') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }


            foreach ($allTownDatas as $allTownData) $townId [] = $allTownData->id;
            foreach ($allCategoryDatas as $allCategoryData) $categoryId [] = $allCategoryData->id;

            $allShops = RetailOutlet::whereIn('town_id', $townId)
                ->whereIn('category_id', $categoryId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;


        } elseif (in_array("town", $type) == true && in_array("brand", $type) == true) {

            $town = $data['town'];
            $townName = "'" . implode("','", $town) . "'";
            $townId = [];

            $brand = $data['brand'];
            $brandName = "'" . implode("','", $brand) . "'";
            $brandId = [];


            if ($filter['town'] == 'And') {
                $allTownDatas = Town::whereRaw('LOWER(name)  IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['town'] == 'Not') {
                $allTownDatas = Town::whereRaw('LOWER(name) NOT IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['brand'] == 'And') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name)  IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['brand'] == 'Not') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name) NOT IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            }
            foreach ($allTownDatas as $allTownData) $townId [] = $allTownData->id;
            foreach ($allCatalogDatas as $allCatalogData) $brandId [] = $allCatalogData->id;

            $allShops = RetailOutlet::whereIn('town_id', $townId)
                ->whereIn('catalog_id', $brandId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;


        } elseif (in_array("zone", $type) == true && in_array("brand", $type) == true) {
            $zone = $data['zone'];
            $zoneName = "'" . implode("','", $zone) . "'";
            $zoneId = [];

            $brand = $data['brand'];
            $brandName = "'" . implode("','", $brand) . "'";
            $brandId = [];

            if ($filter['zone'] == 'And') {
                $allZoneDatas = Zones::whereRaw('LOWER(name)  IN (' . strtolower($zoneName) . ')')->get();

                if ($allZoneDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zoneName;
                    $message = ConstantApiMessageService::INVALIDZONEREQUEST;
                    $title = ConstantApiMessageService::NOTFOUNDZONENAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['zone'] == 'Not') {
                $allZoneDatas = Zones::whereRaw('LOWER(name) NOT IN (' . strtolower($zoneName) . ')')->get();

                if ($allZoneDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zoneName;
                    $message = ConstantApiMessageService::INVALIDZONEREQUEST;
                    $title = ConstantApiMessageService::NOTFOUNDZONENAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            }

            if ($filter['brand'] == 'And') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name)  IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['brand'] == 'Not') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name) NOT IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            }
            foreach ($allZoneDatas as $allZoneData) $zoneId [] = $allZoneData->id;
            foreach ($allCatalogDatas as $allCatalogData) $brandId [] = $allCatalogData->id;

            $allShops = RetailOutlet::whereIn('zone_id', $zoneId)
                ->whereIn('catalog_id', $brandId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;

        } elseif (in_array("zone", $type) == true && in_array("category", $type) == true) {
            $zone = $data['zone'];
            $zoneName = "'" . implode("','", $zone) . "'";
            $zoneId = [];

            $category = $data['category'];
            $categoryName = "'" . implode("','", $category) . "'";
            $categoryId = [];


            if ($filter['zone'] == 'And') {
                $allZoneDatas = Zones::whereRaw('LOWER(name)  IN (' . strtolower($zoneName) . ')')->get();

                if ($allZoneDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zoneName;
                    $message = ConstantApiMessageService::INVALIDZONEREQUEST;
                    $title = ConstantApiMessageService::NOTFOUNDZONENAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['zone'] == 'Not') {
                $allZoneDatas = Zones::whereRaw('LOWER(name) NOT IN (' . strtolower($zoneName) . ')')->get();

                if ($allZoneDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zoneName;
                    $message = ConstantApiMessageService::INVALIDZONEREQUEST;
                    $title = ConstantApiMessageService::NOTFOUNDZONENAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            }

            if ($filter['category'] == 'And') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name)  IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['category'] == 'Not') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            foreach ($allCategoryDatas as $allCategoryData) $categoryId [] = $allCategoryData->id;
            foreach ($allZoneDatas as $allZoneData) $zoneId [] = $allZoneData->gid;

            $allShops = RetailOutlet::whereIn('zone_id', $zoneId)
                ->whereIn('category_id', $categoryId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;

        } elseif (in_array("category", $type) == true && in_array("brand", $type) == true) {
            $category = $data['category'];
            $categoryName = "'" . implode("','", $category) . "'";
            $categoryId = [];

            $brand = $data['brand'];
            $brandName = "'" . implode("','", $brand) . "'";
            $brandId = [];

            if ($filter['brand'] == 'And') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name)  IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['brand'] == 'Not') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name) NOT IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            if ($filter['category'] == 'And') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name)  IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            } elseif ($filter['category'] == 'Not') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }


            foreach ($allCategoryDatas as $allCategoryData) $categoryId [] = $allCategoryData->id;
            foreach ($allCatalogDatas as $allCatalogData) $brandId [] = $allCatalogData->id;

            $allShops = RetailOutlet::whereIn('catalog_id', $brandId)
                ->whereIn('category_id', $categoryId)
                ->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;
        } elseif (in_array("brand", $type) == true) {
            $brand = $data['brand'];
            $brandName = "'" . implode("','", $brand) . "'";
            $brandId = [];


            if ($filter['brand'] == 'And') {
                $allCatalogDatas = Catalog::whereRaw('LOWER(name) IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDBRAND;
                    $title = ConstantApiMessageService::NOTFOUNDBRAND;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['brand'] == 'Not') {

                $allCatalogDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($brandName) . ')')->get();

                if ($allCatalogDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $brandName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }


            foreach ($allCatalogDatas as $allCatalogData) $brandId [] = $allCatalogData->id;
            $allShops = RetailOutlet::whereIn('catalog_id', $brandId)->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;
        } elseif (in_array("region", $type) == true) {
            $region = $data['region'];
            $regionName = "'" . implode("','", $region) . "'";
            $regionId = [];


            if ($filter['region'] == 'And') {
                $allRegionDatas = Region::whereRaw('LOWER(region) IN (' . strtolower($regionName) . ')')->get();

                if ($allRegionDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $regionName;
                    $message = ConstantApiMessageService::INVALIDREGIONNAME;
                    $title = ConstantApiMessageService::NOTFOUNDREGIONNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['region'] == 'Not') {

                $allRegionDatas = Region::whereRaw('LOWER(name) NOT IN (' . strtolower($regionName) . ')')->get();

                if ($allRegionDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDBRAND;
                    $key = $regionName;
                    $message = ConstantApiMessageService::INVALIDREGIONNAME;
                    $title = ConstantApiMessageService::NOTFOUNDREGIONNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }


            foreach ($allRegionDatas as $allRegionData) $regionId [] = $allRegionData->gid;
            $allShops = RetailOutlet::whereIn('development_region_id', $regionId)->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;
        } elseif (in_array("town", $type) == true) {
            $town = $data['town'];
            $townName = "'" . implode("','", $town) . "'";
            $townId = [];


            if ($filter['town'] == 'And') {
                $allTownDatas = Town::whereRaw('LOWER(name) IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['town'] == 'Not') {

                $allTownDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($townName) . ')')->get();

                if ($allTownDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDTOWNID;
                    $key = $townName;
                    $message = ConstantApiMessageService::INVALIDTOWN;
                    $title = ConstantApiMessageService::NOTFOUNDTOWN;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            foreach ($allTownDatas as $allTownData) $townId [] = $allTownData->id;
            $allShops = RetailOutlet::whereIn('town_id', $townId)->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;
        } elseif (in_array("category", $type) == true) {
            $category = $data['category'];
            $categoryName = "'" . implode("','", $category) . "'";
            $categoryId = [];

            if ($filter['category'] == 'And') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name) IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['category'] == 'Not') {
                $allCategoryDatas = Categories::whereRaw('LOWER(name) NOT IN (' . strtolower($categoryName) . ')')->get();

                if ($allCategoryDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDCATEGORY;
                    $key = $categoryName;
                    $message = ConstantApiMessageService::INVALIDCATEGORY;
                    $title = ConstantApiMessageService::NOTFOUNDCATEGORY;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            foreach ($allCategoryDatas as $allCategoryData) $categoryId [] = $allCategoryData->id;
            $allShops = RetailOutlet::whereIn('category_id', $categoryId)->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;
        } elseif (in_array("district", $type) == true) {
            $district = $data['district'];
            $districtName = "'" . implode("','", $district) . "'";
            $districtId = [];

            if ($filter['district'] == 'And') {
                $allDistrictDatas = District::whereRaw('LOWER(name) IN (' . strtolower($districtName) . ')')->get();

                if ($allDistrictDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDDISTRICT;
                    $key = $districtName;
                    $message = ConstantApiMessageService::INVALIDDISTRICT;
                    $title = ConstantApiMessageService::NOTFOUNDDISTRICT;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['district'] == 'Not') {
                $allDistrictDatas = District::whereRaw('LOWER(name) NOT IN (' . strtolower($districtName) . ')')->get();

                if ($allDistrictDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDDISTRICT;
                    $key = $districtName;
                    $message = ConstantApiMessageService::INVALIDDISTRICT;
                    $title = ConstantApiMessageService::NOTFOUNDDISTRICT;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            foreach ($allDistrictDatas as $allDistrictData) $districtId [] = $allDistrictData->gid;
            $allShops = RetailOutlet::whereIn('district_id', $districtId)->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;

        } elseif (in_array("zone", $type) == true) {

            $zone = $data['zone'];
            $zoneName = "'" . implode("','", $zone) . "'";
            $zoneId = [];

            if ($filter['zone'] == 'And') {
                $allZoneDatas = Zones::whereRaw('LOWER(name) IN (' . strtolower($zoneName) . ')')->get();

                if ($allZoneDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zoneName;
                    $message = ConstantApiMessageService::INVALIDZONEREQUEST;
                    $title = ConstantApiMessageService::NOTFOUNDZONENAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['zone'] == 'Not') {
                $allZoneDatas = Zones::whereRaw('LOWER(name) NOT IN (' . strtolower($zoneName) . ')')->get();

                if ($allZoneDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zoneName;
                    $message = ConstantApiMessageService::INVALIDZONEREQUEST;
                    $title = ConstantApiMessageService::NOTFOUNDZONENAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            foreach ($allZoneDatas as $allZoneData) $zoneId [] = $allZoneData->gid;
            $allShops = RetailOutlet::whereIn('zone_id', $zoneId)->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;

        } elseif (in_array("product", $type) == true) {
            $product = $data['product'];
            $productName = "'" . implode("','", $product) . "'";
            $productId = [];

            if ($filter['product'] == 'And') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name) IN (' . strtolower($productName) . ')')->get();

                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

            } elseif ($filter['product'] == 'Not') {
                $allProductDatas = Skuinventory::whereRaw('LOWER(name) NOT IN (' . strtolower($productName) . ')')->get();

                if ($allProductDatas->isEmpty() == true) {
                    $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                    $key = $productName;
                    $message = ConstantApiMessageService::INVALIDPRODUCTNAME;
                    $title = ConstantApiMessageService::NOTFOUNDPRODUCTNAME;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }
            }

            foreach ($allProductDatas as $allProductData) $productId [] = $allProductData->id;
            $allShops = RetailOutlet::whereIn('sku_id', $productId)->select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;

        }
    }

    public function getFilterResults(Request $request)
    {

        ini_set('max_execution_time', 30000);
        ini_set('memory_limit', '100480M');
        $filter = $request->get('filter');

        if (empty($filter) == true) {
            $allShops = RetailOutlet::select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;
        } elseif ($filter) {
            $filterKey = array('town','catalog','skuinventory','category','brand','product','district','zone');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);
            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
            $columnName = [];
            $table = [];
            $value = [];
            $filterColumn = [];
            if (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $townIds =array();
                $categoryIds = array();
                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {

                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;
            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;

            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds =array();
                $townIds=array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $data =array();

                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;

            }elseif (in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds =array();
                $townIds=array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $data =array();

                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;

            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;

            }elseif  (in_array('catalog', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds  =[];
                foreach ($intersectKeys as $intersectKey) {

                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);

                    } elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = $this->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }
                }
                $data=array();
                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;
            } elseif  (in_array('skuinventory', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds  =[];
                foreach ($intersectKeys as $intersectKey) {

                    if ($intersectKey == 'catalog') {
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }
                }
                foreach ($skuShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;

            }elseif  (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)) {
                foreach ($intersectKeys as $intersectKey) {

                    if ($intersectKey == 'catalog') {
                        $catalogShops = app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    } elseif ($intersectKey == 'skuinventory') {
                        $productShops =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);

                    }

                }
                $outletId = array_intersect($catalogShops,$productShops);
                $shopOutput =   RetailOutlet::whereIn('id',$outletId)->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopOutput);
                return $shops;

            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = $this->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $data =array();
                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }
                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;
            } elseif (in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);


                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = $this->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $data =array();
                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }
                $shopsData = RetailOutlet::whereIn('id',$data)->select('id','latitude','longitude')->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopsData);
                return $shops;
            }

            foreach ($intersectKeys as $intersectKey) {
                if (isset($filter[$intersectKey]['name'])) {
                    if($intersectKey == 'catalog') {
                        $shops =  $this->catalogOutput($filter[$intersectKey]['name'],$intersectKey);
                        return $shops;
                    } elseif ($intersectKey == 'skuinventory') {
            
                        $shops =  $this->productOutput($filter[$intersectKey]['name'],$intersectKey);
                        return $shops;
                    }
                }
                if ($intersectKey == 'brand') {
                    $table[] = 'catalog';

                    if (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['catalog_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['catalog_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey];
                        $columnName['catalog_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'town') {
                    $table[] = 'town';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['town_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['town_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['town_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['town_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey];
                        $columnName['town_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'zone') {
                    $table[] = 'zones';
                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['zone_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['zone_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey];
                        $columnName['zone_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }
                } elseif ($intersectKey == 'region') {
                    $table[] = 'development_regions';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'region';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'region';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['development_region_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'gid';
                        $value [] = $filter[$intersectKey]['NOT'];
                        $columnName['development_region_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey];
                        $columnName['development_region_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'category') {
                    $table[] = 'retail_outlet_categories';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['category_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['category_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['category_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);
                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['category_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey];
                        $columnName['category_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'district') {
                    $table[] = 'districts';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['district_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['district_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['district_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'gid';
                        $value [] = $filter[$intersectKey]['NOT'];
                        $columnName['district_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey];
                        $columnName['district_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                }
            }
            $columnId = array_combine($table, $filterColumn);
            $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

            if (isset($intersectValues['errors']) == true) {
                return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
            }


            if (isset($filter[$intersectKey]['AND']) || isset($filter[$intersectKey]['AND']['name'])) {
                $query = "";
                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    if ($query !== "")
                        $query = $query . " AND $columnKey In ($value)";
                    else
                        $query = "where $columnKey In ($value)";

                }

            } elseif (isset($filter[$intersectKey]['NOT']) || isset($filter[$intersectKey]['NOT']['name'])) {
                $query = "";
                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    if ($query !== "")
                        $query = $query . " AND $columnKey NOT IN ($value)";
                    else
                        $query = "where $columnKey NOT IN ($value)";

                }
            } else {
                $query = "";
                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    if ($query !== "")
                        $query = $query . " AND $columnKey In ($value)";
                    else
                        $query = "where $columnKey In ($value)";

                }
            }
            $retailQuery = "select id,latitude,longitude from retail_outlet $query";
            $allShops = DB::select($retailQuery);
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;

        }
    }

    public function getAutoSuggestion(Request $request)
    {
        ini_set('max_execution_time', 30000);
        ini_set('memory_limit', '100480M');

        if ($request->exists('filter') == true) {
            $filter = $request->get('filter');
            $filterKey = array('town', 'category', 'brand', 'district', 'region', 'zone', 'product');
            $keyName = array(key($filter));
            $keyexist = count(array_intersect($keyName, $filterKey));
            $columnName = [];
            $table = [];

            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            } elseif (empty($filter) == true) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = 'filter';
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            } else {
                if (array_key_exists('brand', $filter) == true) {
                    $table = 'catalog';
                    $columnName['name'] = $filter[key($filter)];
                } elseif (array_key_exists('category', $filter) == true) {
                    $table = 'retail_outlet_categories';
                    $columnName = 'name';
                } elseif (array_key_exists('district', $filter) == true) {
                    $table = 'districts';
                    $columnName['name'] = $filter[key($filter)];
                } elseif (array_key_exists('region', $filter) == true) {
                    $table = 'development_regions';
                    $columnName['region'] = $filter[key($filter)];
                } elseif (array_key_exists('product', $filter) == true && key($filter) == 'product') {
                    $table = 'skuinventory';
                    $columnName['name'] = $filter[key($filter)];
                } elseif (array_key_exists('town', $filter) == true) {
                    $table = 'town';
                    $columnName['name'] = $filter[key($filter)];
                } elseif (array_key_exists('zone', $filter) == true) {
                    $table = 'zones';
                    $columnName['name'] = $filter[key($filter)];
                }

                foreach ($columnName as $columnKey => $columnValue) {
                    $value = strtolower($columnValue);
                    if (!empty($columnValue)) $searchQuery = "where LOWER($columnKey) Like '$value%'";

                }
//                if ($table == 'skuinventory') {
//                    if (empty($searchQuery)) $query = "Select skuinventory.name as name,catalog.name as brand from $table inner join catalog ON skuinventory.catalog_id=catalog.id where LOWER($columnKey) Like '$value%' order by name asc";
//                    else $query = "Select skuinventory.name as name,catalog.name as brand from $table inner join catalog ON skuinventory.catalog_id=catalog.id where LOWER($columnKey) Like '$value%' order by name asc";
//                } else{
                    if (empty($searchQuery)) $query = "Select name from $table order by name asc";
                    else $query = "Select $columnKey from $table $searchQuery order by $columnKey asc";

                $datas = DB::select($query);
                $name = [];
                foreach ($datas as $data) $name[] = $data;
                return $name;

            }


        }
    }

    public function filterZone($columnNames, $tables, $columnId = null)
    {
        foreach ($tables as $table => $tableValue) {
            $column = [];
            if (!is_array($tableValue)) {
                $colValue = str_replace("_", "-", $tableValue);
                $value = explode(',', $colValue);
            } else {
                $colValue = $tableValue;
                $value = str_replace("_", "-", $colValue);

            }

            if (array_key_exists($table, $columnId)) {
                $query = "Select $columnId[$table] from $table";
                $datas = DB::select($query);
                $columnName = $columnId[$table];
                foreach ($datas as $data) $column[] = $data->$columnName;
                $filterData = array_intersect($value, $column);

                if (in_array(false, array_map(function ($v) {
                    return is_numeric($v);
                }, $filterData))) {
                    $nameResult = "'" . implode("','", $filterData) . "'";
                    $result = strtolower($nameResult);

                    $query = "Select id from $table where lower(name) IN ($result)";
                    $ids = DB::select($query);
                    $resultId = [];
                    foreach ($ids as $id) $resultId[] = $id->id;

                    $intersectData[$table] = implode(',', $resultId);
                } else {
                    $intersectData[$table] = implode(',', $filterData);
                }

            }
        }


        foreach ($intersectData as $filterKey => $filterData) {

            if (empty($filterData) == true && $filterKey == 'zones') {
                $code = ConstantCodeService::NOTFOUNDZONENAME;
                $title = ConstantApiMessageService::INVALIDZONEIDREQUEST;
                $message = ConstantApiMessageService::NOTFOUNDZONEID;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'catalog') {
                $code = ConstantCodeService::NOTFOUNDBRAND;
                $title = ConstantApiMessageService::INVALIDBRAND;
                $message = ConstantApiMessageService::NOTFOUNDBRAND;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'retail_oultet_categories') {
                $code = ConstantCodeService::NOTFOUNDCATEGORY;
                $title = ConstantApiMessageService::INVALIDCATEGORY;
                $message = ConstantApiMessageService::NOTFOUNDCATEGORY;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'development_regions') {
                $code = ConstantCodeService::NOTFOUNDREGION;
                $title = ConstantApiMessageService::INVALIDREGION;
                $message = ConstantApiMessageService::NOTFOUNDREGION;
                $key = implode(',', $filterData);
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'skuinventory') {
                $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                $title = ConstantApiMessageService::INVALIDPRODUCT;
                $message = ConstantApiMessageService::NOTFOUNDPRODUCT;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'town') {
                $code = ConstantCodeService::NOTFOUNDTOWNID;
                $title = ConstantApiMessageService::INVALIDTOWN;
                $message = ConstantApiMessageService::NOTFOUNDTOWN;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'districts') {
                $code = ConstantCodeService::NOTFOUNDDISTRICT;
                $title = ConstantApiMessageService::INVALIDDISTRICT;
                $message = ConstantApiMessageService::NOTFOUNDDISTRICT;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } else {
                break;
            }

        }


        if (array_key_exists('zones', $intersectData)) $newData['zone_id'] = $intersectData['zones'];
        if (array_key_exists('districts', $intersectData)) $newData['district_id'] = $intersectData['districts'];
        if (array_key_exists('catalog', $intersectData)) $newData['catalog_id'] = $intersectData['catalog'];
        if (array_key_exists('retail_outlet_categories', $intersectData)) $newData['category_id'] = $intersectData['retail_outlet_categories'];
        if (array_key_exists('development_regions', $intersectData)) $newData['development_region_id'] = $intersectData['development_regions'];
        if (array_key_exists('skuinventory', $intersectData)) $newData['sku_id'] = $intersectData['skuinventory'];
        if (array_key_exists('town', $intersectData)) $newData['town_id'] = $intersectData['town'];

        return $newData;


    }

    public function getAllRetailOutlets(Request $request)
    {
        ini_set('max_execution_time', 30000);
        $filter = $request->get('filter');
        $dataPerPage = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        if (empty($filter) == true) {

            $shops = RetailOutlet::orderBy('verified_on', 'DESC')
                ->paginate($dataPerPage);
            $shops->appends($page);
            $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
            $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
            $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
            return $this->manager->createData($resource)->toArray();

        } elseif ($filter) {
            $filterKey = array('town', 'category', 'catalog', 'district', 'region', 'zone', 'skuinventory');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);
            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
            if (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $townIds =array();
                $categoryIds = array();
                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();
            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)) {
                $townIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds =array();
                $townIds=array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $data =array();

                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            }elseif (in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds =array();
                $townIds=array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $data =array();

                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            }elseif (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds =array();

                foreach ($intersectKeys as $intersectKey) {
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);
                    }elseif ($intersectKey == 'skuinventory'){
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$table] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }

                }
                $outletId = array_intersect($catalogShopsId,$skuShopsId);
                $data =array();

                foreach ($outletId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            }elseif  (in_array('catalog', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds  =[];
                foreach ($intersectKeys as $intersectKey) {

                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,null,null);

                    } elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }
                }
                foreach ($catalogShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            } elseif  (in_array('skuinventory', $intersectKeys)&&
                in_array('category', $intersectKeys)) {
                $categoryIds  =[];
                foreach ($intersectKeys as $intersectKey) {

                    if ($intersectKey == 'catalog') {
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);
                    } elseif ($intersectKey == 'category') {
                        $table = 'retail_outlet_categories';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = app('App\Http\Controllers\Api\v1\ShopController')->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($categoryIds,$val);

                        }
                    }
                }
                foreach ($skuShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('category_id',$categoryIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }

                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            }elseif  (in_array('catalog', $intersectKeys)&&
                in_array('skuinventory', $intersectKeys)) {
                $allShops =[];
                foreach ($intersectKeys as $intersectKey) {

                    if ($intersectKey == 'catalog') {
                        $shops = $this->brandOutput($filter[$intersectKey]['name'],$intersectKey,$dataPerPage,$page);
                        $id = [];
                        foreach ($shops as $shop) {
                            $id[] = $shop['gid'];
                        }
                    } elseif ($intersectKey == 'skuinventory') {
                        $shops =  $this->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey,$dataPerPage,$page);
                        $id = [];
                        foreach ($shops as $shop) {
                            $id [] = $shop['gid'];
                        }

                    }

                }
                $outletId = array_intersect($allShops[0],$allShops[1]);
                $shopOutput =   RetailOutlet::whereIn('id',$outletId)->get();
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopOutput);
                return $shops;

            } elseif (in_array('catalog', $intersectKeys)&&
                in_array('town', $intersectKeys)) {

                foreach ($intersectKeys as $intersectKey) {
                    $townIds =array();
                    if ($intersectKey == 'catalog') {
                        $catalogShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->brandOutput($filter[$intersectKey]['name'],$intersectKey,$dataPerPage,$page);


                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = $this->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $data =array();
                foreach ($catalogShopsId as $id) {
                $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                foreach ($allShops as $shop) array_push($data,$shop->id);
                }
                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            }elseif (in_array('skuinventory', $intersectKeys)&&
                in_array('town', $intersectKeys)) {

                foreach ($intersectKeys as $intersectKey) {
                    $townIds =array();
                    if ($intersectKey == 'skuinventory') {
                        $skuShopsId =  app('App\Http\Controllers\Api\v1\RetailOutletController')->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey);


                    } elseif ($intersectKey == 'town') {
                        $table = 'town';
                        $filterColumn = 'id';
                        $value= $filter[$intersectKey];
                        $tableFilter[$intersectKey] = $value;
                        $columnId[$table] = $filterColumn;
                        $intersectValues = $this->filterZone($filterColumn,$tableFilter, $columnId);

                        if (isset($intersectValues['errors']) == true) {
                            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                        }
                        foreach ($intersectValues as $value){
                            $val = explode(',',$value);
                            array_push($townIds,$val);

                        }
                    }

                }
                $data =array();
                foreach ($skuShopsId as $id) {
                    $allShops = RetailOutlet::where('id',$id)->whereIn('town_id',$townIds[0])->select('id')->get();
                    foreach ($allShops as $shop) array_push($data,$shop->id);
                }
                $shops = RetailOutlet::whereIn('id',$data)->paginate($dataPerPage);
                $shops->appends($page);

                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                return $this->manager->createData($resource)->toArray();

            }


            $columnName = [];
            $table = [];
            $value = [];
            $filterColumn = [];
            foreach ($intersectKeys as $intersectKey) {
                if (isset($filter[$intersectKey]['name'])) {
                    if ($intersectKey == 'catalog') {
                       $shops = $this->brandOutput($filter[$intersectKey]['name'],$intersectKey,$dataPerPage,$page);
                    } elseif ($intersectKey == 'skuinventory') {
                        $shops =  $this->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey,$dataPerPage,$page);
                    } elseif ($intersectKey == 'town') {
                        $shops =  $this->skuInventoryOutput($filter[$intersectKey]['name'],$intersectKey,$dataPerPage,$page);
                    }
                    return $shops;

                }

                if ($intersectKey == 'brand') {
                    $table[] = 'catalog';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['catalog_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    }if (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['catalog_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['catalog_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey];
                        $columnName['catalog_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'town') {
                    $table[] = 'town';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['town_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['town_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['town_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['town_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey];
                        $columnName['town_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'zone') {
                    $table[] = 'zones';
                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['zone_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['zone_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey];
                        $columnName['zone_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }
                } elseif ($intersectKey == 'region') {
                    $table[] = 'development_regions';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'region';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'region';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['zone_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['development_region_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'gid';
                        $value [] = $filter[$intersectKey]['NOT'];
                        $columnName['development_region_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey];
                        $columnName['development_region_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'category') {
                    $table[] = 'retail_outlet_categories';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['category_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['category_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['category_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);
                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey]['NOT'];
                        $columnName['category_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'id';
                        $value[] = $filter[$intersectKey];
                        $columnName['category_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                } elseif ($intersectKey == 'district') {
                    $table[] = 'districts';

                    if (isset($filter[$intersectKey]['AND']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['AND']['name'];
                        $columnName['district_id'] = $filter[$intersectKey]['AND']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT']['name'])) {
                        $filterColumn[] = 'name';
                        $value[] = $filter[$intersectKey]['NOT']['name'];
                        $columnName['district_id'] = $filter[$intersectKey]['NOT']['name'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['AND'])) {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey]['AND'];
                        $columnName['district_id'] = $filter[$intersectKey]['AND'];
                        $tableFilter = array_combine($table, $value);

                    } elseif (isset($filter[$intersectKey]['NOT'])) {
                        $filterColumn[] = 'gid';
                        $value [] = $filter[$intersectKey]['NOT'];
                        $columnName['district_id'] = $filter[$intersectKey]['NOT'];
                        $tableFilter = array_combine($table, $value);
                    } else {
                        $filterColumn[] = 'gid';
                        $value[] = $filter[$intersectKey];
                        $columnName['district_id'] = $filter[$intersectKey];
                        $tableFilter = array_combine($table, $value);
                    }

                }
            }


            $columnId = array_combine($table, $filterColumn);
            $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

            if (isset($intersectValues['errors']) == true) {
                return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
            }

            if (isset($filter[$intersectKey]['AND']) || isset($filter[$intersectKey]['AND']['name'])) {
                $query = "";
                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    if ($query !== "")
                        $query = $query . "AND $columnKey IN ($value)";
                    else
                        $query = "where $columnKey IN ($value)";
                }
                $shopQuery = "Select id from retail_outlet $query";
                $outletId = [];
                $shopIds = DB::select($shopQuery);
                foreach ($shopIds as $shopId) $outletId[] = $shopId->id;
                $shops = RetailOutlet::whereIn('id', $outletId)->paginate($dataPerPage);
            } elseif (isset($filter[$intersectKey]['NOT']) || isset($filter[$intersectKey]['NOT']['name'])) {
                $query = "";
                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    if ($query !== "")
                        $query = $query . "AND $columnKey NOT IN ($value)";
                    else
                        $query = "where $columnKey NOT IN ($value)";
                }
                $shopQuery = "Select id from retail_outlet $query";
                $outletId = [];
                $shopIds = DB::select($shopQuery);
                foreach ($shopIds as $shopId) $outletId[] = $shopId->id;
                $shops = RetailOutlet::whereIn('id', $outletId)->paginate($dataPerPage);
            } else {
                $query = "";
                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    if ($query !== "")
                        $query = $query . "AND $columnKey IN ($value)";
                    else
                        $query = "where $columnKey IN ($value)";
                }
                $shopQuery = "Select id from retail_outlet $query";
                $outletId = [];
                $shopIds = DB::select($shopQuery);
                foreach ($shopIds as $shopId) $outletId[] = $shopId->id;
                $shops = RetailOutlet::whereIn('id', $outletId)->paginate($dataPerPage);
            }

            $shops->appends($page);
            $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
            $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
            $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
            return $this->manager->createData($resource)->toArray();

        }
    }

    public function townOutput($filterParam,$intersectKey,$dataPerPage,$perPage) {
        $table = 'town';
        $filterColumn = 'id';
        $value= $filterParam;
        $tableFilter[$intersectKey] = $value;
        $columnId[$table] = $filterColumn;
        $intersectValues = $this->filterZone($filterColumn,$tableFilter, $columnId);

        if (isset($intersectValues['errors']) == true) {
            return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
        }

        $shopId=array();
        foreach ($intersectValues as $columnKey => $columnValue) {
            $columnValue = explode(',', $columnValue);
            $shops = RetailOutlet::whereIn('town_id',$columnValue)->select('id')->get();
            foreach ($shops as $shop) array_push($shopId,$shop->id);
         array_push($shopId,$shops);
        };
    return $shopId;
    }

    public function getComplexSearchResult(Request $request)
    {
        $filter = $request->get('filter');
        if (empty($filter) == true) {
            $allShops = RetailOutlet::select('id', 'latitude', 'longitude')->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($allShops);
            return $shops;
        } elseif ($filter) {
            $filterKey = array('town', 'category', 'catalog', 'district', 'region', 'zone', 'skuinventory');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);
            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
        }


        if  (in_array('catalog', $intersectKeys)&&
            in_array('skuinventory', $intersectKeys)) {
            $allShops =[];
            foreach ($intersectKeys as $intersectKey) {

                if ($intersectKey == 'catalog') {
                    $filterParams = $filter['catalog']['name'];
                    $shops = $this->catalogOutput($filterParams, $intersectKey);
                    $id = [];
                    foreach ($shops as $shop) {
                        $id[] = $shop['gid'];
                    }
                } elseif ($intersectKey == 'skuinventory') {
                    $filterParams = $filter['skuinventory']['name'];
                    $shops = $this->productOutput($filterParams, $intersectKey);
                    $id = [];
                    foreach ($shops as $shop) {
                        $id [] = $shop['gid'];
                    }

                }
                array_push($allShops, $id);

            }
            $outletId = array_intersect($allShops[0],$allShops[1]);
            $shopOutput =   RetailOutlet::whereIn('id',$outletId)->get();
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopOutput);
            return $shops;

        } else{
            foreach ($intersectKeys as $intersectKey) {
                if (array_key_exists('catalog', $filter)) {
                    $filterParams = $filter[$intersectKey]['name'];
                    $shops =$this->catalogOutput($filterParams,$intersectKey);
                    return $shops;


                } elseif (array_key_exists('skuinventory', $filter)) {
                    $filterParams = $filter[$intersectKey]['name'];
                    $shops = $this->productOutput($filterParams,$intersectKey);
                    return $shops;

                }


            }
        }

    }

    public function catalogOutput($filterParams,$intersectKey)
    {
        $filterValue = substr($filterParams, 1, -1);
        $filterString = $this->paramParser($filterValue);
        $delimiters = preg_split('/\s*\(.*?\)\s*/', $filterParams);
        $catalogArray = array();
        $shopOutput =array();
        if (count($filterString) > 1) {
            for ($i = 0; $i < count($filterString); $i++) {
                if (isset($filterString[$i]['positiveArray'])) {
                    $columnValue = $filterString[$i]['positiveArray'];
                    $value = "'" . implode("','", $columnValue) . "'";
                    $catalogQuery = "Select id from catalog where name In ($value)";
                    $catalogs = DB::select($catalogQuery);
                    $id = [];
                    foreach ($catalogs as $catalog) $id[] = $catalog->id;
                    array_push($catalogArray, $id);
                }

            }
            $retailId = array();
            foreach ($catalogArray as $catalogId) {
                $allId = "'" . implode("','", $catalogId) . "'";
                $countData = count($catalogId);
                $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_brand rb on ro.id = rb.retail_outlet_id WHERE rb.catalog_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";
                $retailShopId = DB::select($retailShopQuery);
                $shopId = [];
                foreach ($retailShopId as $retailallId) $shopId[] = $retailallId->id;
                array_push($retailId, $shopId);
            }


            $newDelimiter = array_slice($delimiters, 1, -1);
            $lastIds = array();


                foreach ($newDelimiter as $key => $delimeter) {

                    if ($delimeter == '+') {
                        $lastIds = array_merge($retailId[$key], $retailId[$key + 1]);

                    } elseif ($delimeter == '-') {
                        $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                    }
                    if (empty($lastIds) == false) {
                        $finalId = "'" . implode("','", $lastIds) . "'";


                        $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                        $lastShops = DB::select($shopLastQuery);

                        array_push($shopOutput, $lastShops);
                    } else {
                        $shopOutput=[];
                        return $shopOutput;
                    }


                }


            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopOutput[0]);
            return $shops;
        }


        $semiId = array();
        foreach ($filterString as $finalArray) {
            $table = 'catalog';
            $filterColumn = 'name';
            $shopOutput = array();


            if (empty($finalArray['positiveArray']) == true) {
                $value = $finalArray['negativeArray'];
                $columnName['brand_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);
                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                $query = "";
                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    if ($query !== "")
                        $query = $query . " AND $columnKey In ($value)";
                    else
                        $query = "where $columnKey In ($value)";

                }
                $shopQuery = "Select retail_outlet_id from retailshop_brand $query";
                $allShops = DB::select($shopQuery);
                $allId = [];
                foreach ($allShops as $allShop) $allId[] = $allShop->retail_outlet_id;
                array_push($semiId, $allId);
                $finalId = "'" . implode("','", $semiId) . "'";

                $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                $lastShops = DB::select($shopLastQuery);

                array_push($shopOutput, $lastShops);
            } elseif (empty($finalArray['negativeArray']) == true) {
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT ro.* FROM retail_outlet ro INNER JOIN retailshop_brand rb on ro.id = rb.retail_outlet_id WHERE rb.catalog_id in ($value) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";
                     $allShops = DB::select($shopQuery);

                }

                array_push($shopOutput, $allShops);
            } else {
                $table = 'catalog';
                $filterColumn = 'name';
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }



                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $allShops = DB::select($shopQuery);
                    $allId = [];
                    foreach ($allShops as $allShop) $allId[] = $allShop->retail_outlet_id;

                }


                $value = $finalArray['negativeArray'];
                $columnName['catalog_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $negativeShops = DB::select($shopQuery);
                    $excludeId = [];
                    foreach ($negativeShops as $negativeShop) $excludeId[] = $negativeShop->retail_outlet_id;

                }

                if (empty($finalArray['negativeArray']) == false && empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_diff($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                } elseif (empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_merge($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                }



//                $lastIds = array();
//                $newDelimiter = array_slice($delimiters, 1, -1);
//                dd($newDelimiter);
//
//
//                if (empty($newDelimiter) == true) {
//
//                    $lastIds = array_diff($allId, $excludeId);
//
//                    $finalId = "'" . implode("','", $lastIds) . "'";
//
//                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
//                    $lastShops = DB::select($shopLastQuery);
//
//                    array_push($shopOutput, $lastShops);
//                } else {
//                    foreach ($newDelimiter as $key => $delimeter) {
//
//                        if ($delimeter == '+') {
//                            $lastIds = array_merge($semiId[$key], $semiId[$key+1]);
//
//                        } elseif ($delimeter == '-') {
//                            $lastIds = array_diff($semiId[$key], $semiId[$key+1]);
//                        }
//                        $finalId = "'" . implode("','", $lastIds) . "'";
//
//                        $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
//                        $lastShops = DB::select($shopLastQuery);
//
//                        array_push($shopOutput, $lastShops);
//
//
//                    }
//                }


            }
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopOutput[0]);
            return $shops;
        }


    }

    public function brandOutput($filterParams,$intersectKey,$dataPerPage,$page)
    {
        $filterValue = substr($filterParams, 1, -1);
        $filterString = $this->paramParser($filterValue);
        $delimiters = preg_split('/\s*\(.*?\)\s*/', $filterParams);
        $catalogArray = array();
        $shopOutput =array();
        if (count($filterString) > 1) {
            for ($i = 0; $i < count($filterString); $i++) {
                if (isset($filterString[$i]['positiveArray'])) {
                    $columnValue = $filterString[$i]['positiveArray'];
                    $value = "'" . implode("','", $columnValue) . "'";
                    $catalogQuery = "Select id from catalog where name In ($value)";
                    $catalogs = DB::select($catalogQuery);
                    $id = [];
                    foreach ($catalogs as $catalog) $id[] = $catalog->id;
                    array_push($catalogArray, $id);
                }

            }
            $retailId = array();
            foreach ($catalogArray as $catalogId) {
                $countData = count($catalogId);
                $allShops = RetailOutlet::join('retailshop_brand', 'retail_outlet.id', '=', 'retailshop_brand.retail_outlet_id')
                    ->whereIn('retailshop_brand.catalog_id',$catalogId)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.*')->paginate($dataPerPage);
                $allShops->appends($page);
                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($allShops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($allShops));
                return $this->manager->createData($resource)->toArray();

            }


            $newDelimiter = array_slice($delimiters, 1, -1);
            $lastIds = array();


            foreach ($newDelimiter as $key => $delimeter) {

                if ($delimeter == '+') {
                    $lastIds = array_merge($retailId[$key], $retailId[$key + 1]);

                } elseif ($delimeter == '-') {
                    $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                }
                if (empty($lastIds) == false) {
                    $shops =  RetailOutlet::whereIn('id',$lastIds)->paginate($dataPerPage);

                    $shops->appends($page);
                    $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                    $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                    $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                    return $this->manager->createData($resource)->toArray();

                } else {
                    $shopOutput['data']=[];
                    return $shopOutput;
                }


            }


            $shops =  RetailOutlet::whereIn('id',$shopOutput[0])->paginate($dataPerPage);
            $shops->appends($page);
            $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
            $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
            $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
            return $this->manager->createData($resource)->toArray();
        }


        $semiId = array();
        foreach ($filterString as $finalArray) {
            $table = 'catalog';
            $filterColumn = 'name';

            if (empty($finalArray['negativeArray']) == true) {
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $allShops = RetailOutlet::join('retailshop_brand', 'retail_outlet.id', '=', 'retailshop_brand.retail_outlet_id')
                    ->whereIn('retailshop_brand.catalog_id',$columnValue)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.*')->paginate($dataPerPage);
                    $allShops->appends($page);
                    $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                    $resource = new Collection($allShops, new RetailOutletTransformer(null), 'retail');
                    $resource->setPaginator(new IlluminatePaginatorAdapter($allShops));
                    return $this->manager->createData($resource)->toArray();

                }

            } else {
                $table = 'catalog';
                $filterColumn = 'name';
                $value = $finalArray['positiveArray'];
                $columnName['catalog_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }



                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $allShops = DB::select($shopQuery);
                    $allId = [];
                    foreach ($allShops as $allShop) $allId[] = $allShop->retail_outlet_id;

                }


                $value = $finalArray['negativeArray'];
                $columnName['catalog_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retail_outlet_id from retailshop_brand where catalog_id IN ($value)";
                    $negativeShops = DB::select($shopQuery);
                    $excludeId = [];
                    foreach ($negativeShops as $negativeShop) $excludeId[] = $negativeShop->retail_outlet_id;

                }
                if (empty($finalArray['negativeArray']) == false && empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_diff($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                } elseif (empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_merge($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                }



//                $lastIds = array();
//                $newDelimiter = array_slice($delimiters, 1, -1);
//
//
//                if (empty($newDelimiter) == true) {
//
//                    $lastIds = array_diff($allId, $excludeId);
//
//                    $shops =  RetailOutlet::whereIn('id',$lastIds)->paginate($dataPerPage);
//
//                    $shops->appends($page);
//                    $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
//                    $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
//                    $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
//                    return $this->manager->createData($resource)->toArray();
//                } else {
//
//                    foreach ($newDelimiter as $key => $delimeter) {
//
//                        if ($delimeter == '+') {
//                            $lastIds = array_merge($semiId[$key], $semiId[$key+1]);
//
//                        } elseif ($delimeter == '-') {
//                            $lastIds = array_diff($semiId[$key], $semiId[$key+1]);
//                        }
//
//                        $shops =  RetailOutlet::whereIn('id',$lastIds)->paginate($dataPerPage);
//
//                        $shops->appends($page);
//                        $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
//                        $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
//                        $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
//                        return $this->manager->createData($resource)->toArray();
//
//
//                    }
//                }


            }

        }


    }
    public function skuInventoryOutput($filterParams,$intersectKey,$dataPerPage,$page)
    {
        $filterValue = substr($filterParams, 1, -1);
        $filterString = $this->paramParser($filterValue);
        $delimiters = preg_split('/\s*\(.*?\)\s*/', $filterParams);
        $skuArray = array();
        $shopOutput =array();
        if (count($filterString) > 1) {
            for ($i = 0; $i < count($filterString); $i++) {
                if (isset($filterString[$i]['positiveArray'])) {
                    $columnValue = $filterString[$i]['positiveArray'];
                    $value = "'" . implode("','", $columnValue) . "'";
                    $skuQuery = "Select id from skuinventory where name In ($value)";
                    $skuDatas = DB::select($skuQuery);
                    $id = [];
                    foreach ($skuDatas as $skuData) $id[] = $skuData->id;
                    array_push($skuArray, $id);
                }

            }
            $retailId = array();
            foreach ($skuArray as $skuId) {
                $countData = count($skuId);
                $allShops = RetailOutlet::join('retailshop_skus', 'retail_outlet.id', '=', 'retailshop_skus.retailoutlet_id')
                    ->whereIn('retailshop_skus.sku_id',$skuId)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.*')->paginate($dataPerPage);
                $allShops->appends($page);
                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($allShops, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($allShops));
                return $this->manager->createData($resource)->toArray();
            }


            $newDelimiter = array_slice($delimiters, 1, -1);
            $lastIds = array();


            foreach ($newDelimiter as $key => $delimeter) {

                if ($delimeter == '+') {
                    $lastIds = array_merge($retailId[$key], $retailId[$key + 1]);

                } elseif ($delimeter == '-') {
                    $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                }
                if (empty($lastIds) == false) {
                    $shops =  RetailOutlet::whereIn('id',$lastIds)->paginate($dataPerPage);

                    $shops->appends($page);
                    $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                    $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
                    $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
                    return $this->manager->createData($resource)->toArray();

                } else {
                    $shopOutput['data']=[];
                    return $shopOutput;
                }


            }

        }



        $semiId = array();
        foreach ($filterString as $finalArray) {
            $table = 'skuinventory';
            $filterColumn = 'name';

            if (empty($finalArray['negativeArray']) == true) {
                $value = $finalArray['positiveArray'];
                $columnName['sku_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $allShops = RetailOutlet::join('retailshop_skus', 'retail_outlet.id', '=', 'retailshop_skus.retailoutlet_id')
                        ->whereIn('retailshop_skus.sku_id',$columnValue)->groupBy('retail_outlet.id')->having(DB::raw('COUNT(retail_outlet.id)'),'=',$countData)->select('retail_outlet.*')->paginate($dataPerPage);
                    $allShops->appends($page);
                    $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                    $resource = new Collection($allShops, new RetailOutletTransformer(null), 'retail');
                    $resource->setPaginator(new IlluminatePaginatorAdapter($allShops));
                    return $this->manager->createData($resource)->toArray();

                }



            } else {

                $table = 'skuinventory';
                $filterColumn = 'name';
                $value = $finalArray['positiveArray'];
                $columnName['sku_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }



                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retailoutlet_id from retailshop_skus where sku_id IN ($value)";
                    $allShops = DB::select($shopQuery);
                    $allId = [];
                    foreach ($allShops as $allShop) $allId[] = $allShop->retailoutlet_id;

                }


                $value = $finalArray['negativeArray'];
                $columnName['sku_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retailoutlet_id from retailshop_skus where sku_id IN ($value)";
                    $negativeShops = DB::select($shopQuery);
                    $excludeId = [];
                    foreach ($negativeShops as $negativeShop) $excludeId[] = $negativeShop->retailoutlet_id;

                }
                if (empty($finalArray['negativeArray']) == false && empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_diff($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                } elseif (empty($finalArray['positiveArray']) == false) {
                    $lastIds = array_merge($allId, $excludeId);
                    $finalId = "'" . implode("','", $lastIds) . "'";

                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                }



//                $lastIds = array();
//                $newDelimiter = array_slice($delimiters, 1, -1);
//
//
//                if (empty($newDelimiter) == true) {
//
//                    $lastIds = array_diff($allId, $excludeId);
//
//                    $shops =  RetailOutlet::whereIn('id',$lastIds)->paginate($dataPerPage);
//
//                    $shops->appends($page);
//                    $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
//                    $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
//                    $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
//                    return $this->manager->createData($resource)->toArray();
//                } else {
//
//                    foreach ($newDelimiter as $key => $delimeter) {
//
//                        if ($delimeter == '+') {
//                            $lastIds = array_merge($semiId[$key], $semiId[$key+1]);
//
//                        } elseif ($delimeter == '-') {
//                            $lastIds = array_diff($semiId[$key], $semiId[$key+1]);
//                        }
//                        $shops =  RetailOutlet::whereIn('id',$lastIds)->paginate($dataPerPage);
//
//                        $shops->appends($page);
//                        $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
//                        $resource = new Collection($shops, new RetailOutletTransformer(null), 'retail');
//                        $resource->setPaginator(new IlluminatePaginatorAdapter($shops));
//                        return $this->manager->createData($resource)->toArray();
//
//
//                    }
//                }


            }

        }


    }


    public function productOutput($filterParams,$intersectKey)
    {

        //preg_match_all('#\((.*?)\)#', $filterParams, $filterValue);
        $filterValue = substr($filterParams, 1, -1);
        $filterString = $this->paramParser($filterValue);
        $delimiters = preg_split('/\s*\(.*?\)\s*/', $filterParams);
        $skuArray = array();
        $shopOutput =array();
        if (count($filterString) > 1) {
            for ($i = 0; $i < count($filterString); $i++) {
                if (isset($filterString[$i]['positiveArray'])) {
                    $columnValue = $filterString[$i]['positiveArray'];
                    $value = "'" . implode("','", $columnValue) . "'";
                    $skuQuery = "Select id from skuinventory where name In ($value)";
                    $skuDatas = DB::select($skuQuery);
                    $id = [];
                    foreach ($skuDatas as $skuData) $id[] = $skuData->id;
                    array_push($skuArray, $id);
                }

            }
            $retailId = array();
            foreach ($skuArray as $skuId) {
                $allId = "'" . implode("','", $skuId) . "'";
                $countData = count($skuId);
                $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_skus rs on ro.id = rs.retailoutlet_id WHERE rs.sku_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";
                $retailShopId = DB::select($retailShopQuery);
                $shopId = [];
                foreach ($retailShopId as $retailallId) $shopId[] = $retailallId->id;
                array_push($retailId, $shopId);
            }


            $newDelimiter = array_slice($delimiters, 1, -1);
            $lastIds = array();


            foreach ($newDelimiter as $key => $delimeter) {

                if ($delimeter == '+') {
                    $lastIds = array_merge($retailId[$key], $retailId[$key + 1]);

                } elseif ($delimeter == '-') {
                    $lastIds = array_diff($retailId[$key], $retailId[$key + 1]);
                }
                if (empty($lastIds) == false) {
                    $finalId = "'" . implode("','", $lastIds) . "'";


                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                    $lastShops = DB::select($shopLastQuery);

                    array_push($shopOutput, $lastShops);
                } else {
                    $shopOutput=[];
                    return $shopOutput;
                }


            }


            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopOutput[0]);
            return $shops;
        }

        $semiId = array();
        foreach ($filterString as $finalArray) {
            $table = 'skuinventory';
            $filterColumn = 'name';
            $shopOutput = array();

       if (empty($finalArray['negativeArray']) == true) {
                $value = $finalArray['positiveArray'];
                $columnName['sku_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;

                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);


           if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $countData =count($columnValue);
                    $shopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_skus rs on ro.id = rs.retailoutlet_id WHERE rs.sku_id in ($value) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";
                    $allShops = DB::select($shopQuery);
                   if (!empty($allShops)) {
                       $lastShopIds = [];
                       foreach ($allShops as $lastId)  $lastShopIds [] =$lastId->id;
                       $finalId = "'" . implode("','", $lastShopIds) . "'";

                       $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
                       $lastShops = DB::select($shopLastQuery);

                       array_push($shopOutput, $lastShops);
                   } else {
                       $shopOutput=[];
                       return $shopOutput;
                   }

                }



       } else {

           $table = 'skuinventory';
                $filterColumn = 'name';
                $value = $finalArray['positiveArray'];
                $columnName['sku_id'] = $finalArray['positiveArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);


                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }



                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retailoutlet_id from retailshop_skus where sku_id IN ($value)";
                    $allShops = DB::select($shopQuery);
                    $allId = [];
                    foreach ($allShops as $allShop) $allId[] = $allShop->retailoutlet_id;

                }


                $value = $finalArray['negativeArray'];
                $columnName['sku_id'] = $finalArray['negativeArray'];
                $tableFilter[$intersectKey] = $value;
                $columnId[$table] = $filterColumn;
                $intersectValues = $this->filterZone($columnName, $tableFilter, $columnId);

                if (isset($intersectValues['errors']) == true) {
                    return response()->json($intersectValues, ConstantStatusService::NOTFOUNDSTATUS);
                }


                foreach ($intersectValues as $columnKey => $columnValue) {
                    $columnValue = explode(',', $columnValue);
                    $countData =count($columnValue);
                    $value = "'" . implode("','", $columnValue) . "'";
                    $shopQuery = "SELECT retailoutlet_id from retailshop_skus where sku_id IN ($value)";
                    $negativeShops = DB::select($shopQuery);
                    $excludeId = [];
                    foreach ($negativeShops as $negativeShop) $excludeId[] = $negativeShop->retailoutlet_id;

                }

           if (empty($finalArray['negativeArray']) == false && empty($finalArray['positiveArray']) == false) {
               $lastIds = array_diff($allId, $excludeId);
               $finalId = "'" . implode("','", $lastIds) . "'";

               $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
               $lastShops = DB::select($shopLastQuery);

               array_push($shopOutput, $lastShops);
           } elseif (empty($finalArray['positiveArray']) == false) {
               $lastIds = array_merge($allId, $excludeId);
               $finalId = "'" . implode("','", $lastIds) . "'";

               $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
               $lastShops = DB::select($shopLastQuery);

               array_push($shopOutput, $lastShops);
           }


//
//                $lastIds = array();
//                $newDelimiter = array_slice($delimiters, 1, -1);
//
//
//                if (empty($newDelimiter) == true) {
//
//                    $lastIds = array_diff($allId, $excludeId);
//
//                    $finalId = "'" . implode("','", $lastIds) . "'";
//
//                    $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
//                    $lastShops = DB::select($shopLastQuery);
//
//                    array_push($shopOutput, $lastShops);
//                } else {
//
//                    foreach ($newDelimiter as $key => $delimeter) {
//
//                        if ($delimeter == '+') {
//                            $lastIds = array_merge($semiId[$key], $semiId[$key+1]);
//
//                        } elseif ($delimeter == '-') {
//                            $lastIds = array_diff($semiId[$key], $semiId[$key+1]);
//                        }
//                        $finalId = "'" . implode("','", $lastIds) . "'";
//
//                        $shopLastQuery = "Select id,latitude,longitude from retail_outlet where id IN ($finalId)";
//                        $lastShops = DB::select($shopLastQuery);
//
//                        array_push($shopOutput, $lastShops);
//
//
//                    }
//                }


            }
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($shopOutput[0]);
            return $shops;
        }


    }




    public function paramParser($filterParams)
    {
        $value=array();
        $array = explode("+", $filterParams);
            $negativeArray = [];
            $positiveArray= [];

            foreach ($array as $data) {
                $newkey=str_replace(")","",$data);
                $newArray = str_replace("(","",$newkey);

                if (preg_match('/-/', $newArray)) {
                    $positiveArray[] = substr($newArray, 0, strpos($newArray, '-'));
                    $array2 = explode("-", $newArray);
                    for ($i = 0; $i < count($array2); $i++) {
                        if ($i != 0) $negativeArray[] = $array2[$i];
                    }
                }else{
                    $positiveArray[] = $newArray;
                }
            }
            $result =['positiveArray'=>$positiveArray, 'negativeArray' =>$negativeArray];
            array_push($value,$result);


        return $value;


    }


}