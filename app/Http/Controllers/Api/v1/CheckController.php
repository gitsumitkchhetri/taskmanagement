<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 5/5/17
 * Time: 5:13 PM
 */

namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\Catalog;
use App\Model\Api\Categories;
use App\Model\Api\Channel;
use App\Model\Api\District;
use App\Model\Api\RetailOutlet;
use App\Model\Api\Town;
use App\Model\Api\Zones;
use App\MysqlRetailOutletSku;
use App\RetailOutletSku;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Transformers\NepalZoneSimplifiedTransformer;
use App\Transformers\RetailOutletTransformer;
use App\Transformers\ZoneTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use Illuminate\Http\Request;
use DB;
use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;

ini_set('max_execution_time', 30000);
ini_set('memory_limit', '100480M');

class CheckController extends Controller
{

    public function __construct()
    {
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->zonesTransformer = new ZoneTransformer();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
        $this->town = new Town();
        $this->category = new Categories();
        $this->retailOutlet = new RetailOutlet();
        $this->district = new District();
        $this->zone = new Zones();
        $this->brand = new Catalog();
    }


    public function getFilterResult(Request $request)
    {
        if ($request->exists('filter')) {
            $filter = $request->get('filter');
            $filterKey = array('town', 'catalog', 'skuinventory', 'retail_outlet_categories', 'districts', 'zones', 'development_regions');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);

            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
            $shopsId = array();


            foreach ($intersectKeys as $intersectKey) {
                if (array_key_exists('id', $filter[$intersectKey]) ||
                    array_key_exists('gid', $filter[$intersectKey])
                ) {
                    $validateDatasId = array();
                    $columnName = key($filter[$intersectKey]);
                    $table = $intersectKey;
                    $value = $filter[$intersectKey][$columnName];
                    $columnId[$table] = $columnName;
                    $intersectValues = $this->validateData($table, $columnName, $value, $columnId);
                    array_push($validateDatasId, $intersectValues);
                    if ($intersectKey == 'town') {
                        $shopsId['town_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'retail_outlet_categories') {
                        $shopsId['category_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    } elseif ($intersectKey == 'districts') {
                        $shopsId['district_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    }


                } elseif (array_key_exists('name', $filter[$intersectKey])) {
                    $columnName = key($filter[$intersectKey]);
                    $value = $filter[$intersectKey][$columnName];

                    $filterValue = substr($value, 1, -1);

                    $delimiters = preg_split('/\s*\(.*?\)\s*/', $value);
                    $newDelimiter = array_slice($delimiters, 1, -1);

                    $table = $intersectKey;
                    $columnId[$table] = $columnName;

                    $tempArr = $this->paramParser($filterValue);
                    if ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    } elseif ($intersectKey == 'skuinventory') {
                        $shopsId['sku_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    }

                }
            }

            if (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";

                $districtShopIds = $shopsId['district_id'];
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";

                $allShopsId = array_intersect($skuIds, $catalogIds);

                if (empty($allShopsId)||
                    empty($districtShopIds)||
                    empty($categoryShopIds)) {
                    return $shops =[];

                }

                $allId = "'" . implode("','", array_unique($allShopsId)) . "'";


                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND category_id IN ($allCatId) AND district_id IN ($alldistrictId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['town_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $townShopIds = $shopsId['town_id'];
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                $townId = array_pluck($townIds, 'town_id');
                $allTownId = "'" . implode("','", $townId) . "'";

                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";

                $allShopsId = array_intersect($skuIds, $catalogIds);

                if (empty($allShopsId) ||
                    empty($categoryShopIds)  ||empty($townShopIds)) {
                    return $shops =[];

                }

                $allId = "'" . implode("','", array_unique($allShopsId)) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND category_id IN ($allCatId) AND town_id IN ($allTownId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];

                $districtShopIds = $shopsId['district_id'];
                $allShopsId = array_intersect($skuIds, $catalogIds);

                if (empty($allShopsId) ||empty($districtShopIds) ) {
                    return $shops =[];

                }
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";
                $allId = "'" . implode("','", array_unique($allShopsId)) . "'";


                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND district_id IN ($alldistrictId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $districtShopIds = $shopsId['district_id'];
                $categoryShopIds = $shopsId['category_id'];


                if (empty($skuIds) ||
                    empty($districtShopIds)||
                    empty($categoryShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $skuIds) . "'";

                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";


                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";


                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND district_id IN ($alldistrictId) AND category_id IN ($allCatId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['town_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $townShopIds = $shopsId['town_id'];
                $categoryShopIds = $shopsId['category_id'];



                if (empty($skuIds) ||
                    empty($townShopIds)||
                    empty($categoryShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $skuIds) . "'";

                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                $ids = array_pluck($townIds, 'town_id');
                $allTownId = "'" . implode("','", $ids) . "'";

                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";


                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND town_id IN ($allTownId) AND category_id IN ($allCatId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            }elseif (isset($shopsId['catalog_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['district_id'])
            ) {
                $catalogIds = $shopsId['catalog_id'];
                $categoryShopIds = $shopsId['category_id'];
                $districtShopIds = $shopsId['district_id'];



                if (empty($catalogIds) ||
                    empty($districtShopIds)||
                    empty($categoryShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $catalogIds) . "'";

                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";

                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND district_id IN ($alldistrictId) AND category_id IN ($allCatId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            }  elseif (isset($shopsId['catalog_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['town_id'])
            ) {
                $catalogIds = $shopsId['catalog_id'];
                $townShopIds = $shopsId['town_id'];
                $categoryShopIds = $shopsId['category_id'];

                if (empty($catalogIds) ||
                    empty($townShopIds)||
                    empty($categoryShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $catalogIds) . "'";


                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                $ids = array_pluck($townIds, 'town_id');
                $allTownId = "'" . implode("','", $ids) . "'";


                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND town_id IN ($allTownId) AND category_id IN ($allCatId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            }elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id'])
            ) {
                $skuIds = $shopsId['sku_id'];



                $categoryShopIds = $shopsId['category_id'];
                $catalogIds = $shopsId['catalog_id'];
                $allIds = array_intersect($skuIds, $catalogIds);

                if (empty($allIds) ||
                    empty($categoryShopIds)) {
                    return $shops =[];

                }

                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";
                $allId = "'" . implode("','",array_unique($allIds)) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND category_id IN ($allCatId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $categoryShopIds = $shopsId['category_id'];

                if (empty($catalogIds)||
                    empty($districtShopIds) ||
                    empty($categoryShopIds)) {
                    return $shops = [];
                }


                $allId = "'" . implode("','", $catalogIds) . "'";


                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";


                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND category_id IN ($allCatId) AND district_id IN ($alldistrictId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['catalog_id'])) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];

                $allIds = array_intersect($skuIds, $catalogIds);
                if (empty($allIds)) {
                    return $shops =[];

                }
                $allId = "'" . implode("','", array_unique($allIds)) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['town_id'])) {
                $skuIds = $shopsId['sku_id'];
                $townShopIds = $shopsId['town_id'];

                if (empty($skuIds)||
                    empty($townShopIds)) {
                    return $shops = [];
                }


                $allId = "'" . implode("','", $skuIds) . "'";


                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                $ids = array_pluck($townIds, 'town_id');
                $allTownId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND town_id in ($allTownId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['category_id']) && isset($shopsId['town_id'])) {
                $categoryShopIds = $shopsId['category_id'];
                $townShopIds = $shopsId['town_id'];

                if (empty($categoryShopIds)||
                    empty($townShopIds)) {
                    return $shops = [];
                }


                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";


                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                $ids = array_pluck($townIds, 'town_id');
                $allTownId = "'" . implode("','", $ids) . "'";


                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where category_id IN ($allCatId) AND town_id in ($allTownId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['district_id'])) {
                $skuShopIds = $shopsId['sku_id'];
                $districtShopIds = $shopsId['district_id'];


                if (empty($skuShopIds) ||
                    empty($districtShopIds)) {
                    return $shops = [];
                }



                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";
                $allId = "'" . implode("','", $skuShopIds) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND district_id in ($alldistrictId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            }  elseif (isset($shopsId['sku_id']) && isset($shopsId['town_id'])) {
                $skuShopIds = $shopsId['sku_id'];
                $allId = "'" . implode("','", $skuShopIds) . "'";

                $townShopIds = $shopsId['town_id'];


                if (empty($skuShopIds) ||
                    empty($townShopIds)) {
                    return $shops = [];
                }




                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                $ids = array_pluck($townIds, 'town_id');
                $allTownId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND town_id in ($allTownId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['district_id'])) {
                $catalogShopIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];

                if (empty($catalogIds)||
                    empty($districtShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $catalogShopIds) . "'";

                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND district_id in ($alldistrictId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['category_id']) && isset($shopsId['district_id'])) {
                $categoryShopIds = $shopsId['category_id'];
                $districtShopIds = $shopsId['district_id'];

                if (empty($categoryShopIds)||
                    empty($districtShopIds)) {
                    return $shops = [];
                }

                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";


                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                $districtId = array_pluck($districtIds, 'district_id');
                $alldistrictId = "'" . implode("','", $districtId) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where category_id IN ($allCatId) AND district_id in ($alldistrictId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['town_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $townShopIds = $shopsId['town_id'];

                if (empty($catalogIds)||
                    empty($townShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $catalogIds) . "'";


                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                $ids = array_pluck($townIds, 'town_id');
                $allTownId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND town_id in ($allTownId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['category_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $categoryShopIds = $shopsId['category_id'];

                if (empty($catalogIds)||
                    empty($categoryShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $catalogIds) . "'";


                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND category_id in ($allCatId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['category_id'])) {
                $skuIds = $shopsId['sku_id'];
                $categoryShopIds = $shopsId['category_id'];

                if (empty($skuIds)||
                    empty($categoryShopIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $skuIds) . "'";


                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                $ids = array_pluck($categoryIds, 'category_id');
                $allCatId = "'" . implode("','", $ids) . "'";

                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId) AND category_id in ($allCatId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;


            } elseif (isset($shopsId['sku_id'])) {
                $skuIds = $shopsId['sku_id'];

                if (empty($skuIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $skuIds) . "'";
                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['category_id'])) {
                $categoryIds = $shopsId['category_id'];

                if (empty($categoryIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $categoryIds) . "'";
                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;

            } elseif (isset($shopsId['town_id'])) {
                $townIds = $shopsId['town_id'];
                $allId = "'" . implode("','", $townIds) . "'";
                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;
            } elseif (isset($shopsId['catalog_id'])) {
                $catalogIds = $shopsId['catalog_id'];

                if (empty($catalogIds)) {
                    return $shops = [];
                }
                $allId = "'" . implode("','", $catalogIds) . "'";


                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId)";
                $result = DB::Select($retailShopQuery);

                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;
            } elseif (isset($shopsId['district_id'])) {

                $districtIds = $shopsId['district_id'];

                if (empty($districtIds)) {
                    return $shops = [];
                }

                $allId = "'" . implode("','", $districtIds) . "'";
                $retailShopQuery = "Select id,latitude,longitude from retail_outlet where id IN ($allId)";
                $result = DB::Select($retailShopQuery);
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
                return $shops;
            }

        } else {
            $retailShopQuery = "Select id,latitude,longitude from retail_outlet";
            $result = DB::Select($retailShopQuery);
            $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($result);
            return $shops;
        }


    }

    public function getChartOutletResult(Request $request)
    {
        if ($request->exists('filter')) {
            $filter = $request->get('filter');
            $filterKey = array('town', 'catalog', 'skuinventory', 'retail_outlet_categories', 'districts', 'zones', 'development_regions');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);

            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
            $shopsId = array();


            foreach ($intersectKeys as $intersectKey) {
                if (array_key_exists('id', $filter[$intersectKey]) ||
                    array_key_exists('gid', $filter[$intersectKey])
                ) {
                    $validateDatasId = array();
                    $columnName = key($filter[$intersectKey]);
                    $table = $intersectKey;
                    $value = $filter[$intersectKey][$columnName];
                    $columnId[$table] = $columnName;
                    $intersectValues = $this->validateData($table, $columnName, $value, $columnId);
                    array_push($validateDatasId, $intersectValues);
                    if ($intersectKey == 'town') {
                        $shopsId['town_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'retail_outlet_categories') {
                        $shopsId['category_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    } elseif ($intersectKey == 'districts') {
                        $shopsId['district_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    }


                } elseif (array_key_exists('name', $filter[$intersectKey])) {
                    $columnName = key($filter[$intersectKey]);
                    $value = $filter[$intersectKey][$columnName];

                    $filterValue = substr($value, 1, -1);
                    $delimiters = preg_split('/\s*\(.*?\)\s*/', $value);
                    $newDelimiter = array_slice($delimiters, 1, -1);

                    $table = $intersectKey;
                    $columnId[$table] = $columnName;

                    $tempArr = $this->paramParser($filterValue);
                    if ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    } elseif ($intersectKey == 'skuinventory') {
                        $shopsId['sku_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    } elseif ($intersectKey == 'districts') {
                        $shopsId['district_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    }

                }
            }


            if (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $allShopId = array_intersect($catalogIds, $skuIds);
                $shopId = array_unique($allShopId);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $ids)
                    ->whereIn('retail_outlet.id', $shopId)->whereIn('category_id', $catIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            }
            if (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['town_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];

                $townShopIds = $shopsId['town_id'];
                $town = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($town, $townId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $allIds = array_intersect($skuIds, $catalogIds);

                if (empty($allIds)) {
                    return $shops =[];

                }

                $ids = array_unique($allIds);
                $outlet = collect([]);

                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $ids)
                    ->whereIn('retail_outlet.town_id', $town)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $allShopsId = array_intersect($skuIds, $catalogIds);

                if (empty($allShopsId)) {
                    return $shops =[];

                }

                $shopIds = array_unique($allShopsId);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $shopIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);


                $allIds = array_intersect($skuIds, $catalogIds);

                if (empty($allIds)) {
                    return $shops =[];

                }

                $ids = array_unique($allIds);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $ids)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['district_id'])) {
                $catalogShopIds = $shopsId['catalog_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->district_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogShopIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;
            } elseif (isset($shopsId['sku_id']) && isset($shopsId['district_id'])) {
                $skuIds = $shopsId['sku_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->district_id);
                $outlet = collect([]);

                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['catalog_id'])) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];
                $allIds = array_intersect($skuIds, $catalogIds);

                if (empty($allShopsId)) {
                    return $shops =[];

                }

                $ids = array_unique($allIds);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['town_id'])) {
                $skuIds = $shopsId['sku_id'];
                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.town_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['town_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->whereIn('retail_outlet.town_id', $ids)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['town_id']) && isset($shopsId['category_id'])) {

                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.town_id', $ids)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['category_id']) && isset($shopsId['district_id'])) {
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->district_id);
                $outlet = collect([]);

                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['category_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['category_id'])) {
                $skuIds = $shopsId['sku_id'];
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['district_id']) && isset($shopsId['category_id'])) {
                $districtShopIds = $shopsId['district_id'];
                $allDistrictId = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($allDistrictId, $districtId->district_id);
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.district_id', $allDistrictId)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['sku_id'])) {
                $skuIds = $shopsId['sku_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;

            } elseif (isset($shopsId['catalog_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;
            } elseif (isset($shopsId['category_id'])) {
                $categoryIds = $shopsId['category_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $categoryIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;
            } elseif (isset($shopsId['town_id'])) {
                $townIds = $shopsId['town_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $townIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;
            } elseif (isset($shopsId['district_id'])) {
                $districtIds = $shopsId['district_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $districtIds)
                    ->select(array('zones.gid as zoneid', 'districts.gid as districtid', 'retail_outlet_categories.id as categoryid', 'retail_outlet_channel.id as channelid', 'retail_outlet.id as outletid'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $allData = app('App\Http\Controllers\Api\v1\ChartController')->countChart($outlet);
                return $allData;
            }

        } else {
            $zones = Zones::orderBy('gid', 'ASC')->select('gid', 'name')->get();
            $districts = District::orderBy('gid', 'ASC')->select('gid', 'name')->get();
            $categories = Categories::orderBy('id', 'ASC')->select('id', 'name')->get();
            $channels = Channel::orderBy('id', 'ASC')->select('id', 'name')->get();
            $zoneData['zone'] = [];
            $zoneData['category'] = [];
            $zoneData['district'] = [];
            $zoneData['channel'] = [];
            foreach ($zones as $zone) {
                $countZoneShop = RetailOutlet::where('zone_id', $zone->gid)->count();
                $zoneName['name'] = $zone->name;
                $zoneName['shops'] = $countZoneShop;
                array_push($zoneData['zone'], $zoneName);
            }
            foreach ($districts as $district) {
                $countDistrictShop = RetailOutlet::where('district_id', $district->gid)->count();
                $districtName['name'] = $district->name;
                $districtName['shops'] = $countDistrictShop;
                array_push($zoneData['district'], $districtName);
            }
            foreach ($categories as $category) {
                $countCategoryShop = RetailOutlet::where('category_id', $category->id)->count();
                $categoryName['name'] = $category->name;
                $categoryName['shops'] = $countCategoryShop;
                array_push($zoneData['category'], $categoryName);
            }
            foreach ($channels as $channel) {
                $countChannelShop = RetailOutlet::where('channel_id', $channel->id)->count();
                $channelName['name'] = $channel->name;
                $channelName['shops'] = $countChannelShop;
                array_push($zoneData['channel'], $channelName);
            }
            return $zoneData;
        }


    }

    public function getRetailOutletResult(Request $request)
    {
        $dataPerPage = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        if ($request->exists('filter')) {
            $filter = $request->get('filter');
            $filterKey = array('town', 'catalog', 'skuinventory', 'retail_outlet_categories', 'districts', 'zones', 'development_regions');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);

            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
            $shopsId = array();


            foreach ($intersectKeys as $intersectKey) {
                if (array_key_exists('id', $filter[$intersectKey]) ||
                    array_key_exists('gid', $filter[$intersectKey])
                ) {
                    $validateDatasId = array();
                    $columnName = key($filter[$intersectKey]);
                    $table = $intersectKey;
                    $value = $filter[$intersectKey][$columnName];
                    $columnId[$table] = $columnName;
                    $intersectValues = $this->validateData($table, $columnName, $value, $columnId);
                    array_push($validateDatasId, $intersectValues);
                    if ($intersectKey == 'town') {
                        $shopsId['town_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'retail_outlet_categories') {
                        $shopsId['category_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    } elseif ($intersectKey == 'districts') {
                        $shopsId['district_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    }


                } elseif (array_key_exists('name', $filter[$intersectKey])) {
                    $columnName = key($filter[$intersectKey]);
                    $value = $filter[$intersectKey][$columnName];

                    $filterValue = substr($value, 1, -1);
                    $delimiters = preg_split('/\s*\(.*?\)\s*/', $value);
                    $newDelimiter = array_slice($delimiters, 1, -1);

                    $table = $intersectKey;
                    $columnId[$table] = $columnName;

                    $tempArr = $this->paramParser($filterValue);
                    if ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    } elseif ($intersectKey == 'skuinventory') {
                        $shopsId['sku_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    }

                }
            }


            if (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $allShopId = array_intersect($catalogIds, $skuIds);

                if (empty($allShopsId)) {
                    return $shops =[];

                }
                $shopId = array_unique($allShopId);

                $outlet = RetailOutlet::whereIn('id', $shopId)->whereIn('category_id', $catIds)
                    ->whereIn('district_id', $ids)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['town_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];

                $townShopIds = $shopsId['town_id'];
                $town = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($town, $townId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $allIds = array_intersect($catalogIds, $skuIds);
                if (empty($allIds)) {
                    return $shops =[];

                }
                $ids = array_unique($allIds);
                $outlet = RetailOutlet::whereIn('id', $ids)
                    ->whereIn('town_id', $town)
                    ->whereIn('category_id', $catIds)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $allShopsId = array_intersect($skuIds, $catalogIds);

                if (empty($allShopsId)) {
                    return $shops =[];

                }

                $shopIds = array_unique($allShopsId);

                $outlet = RetailOutlet::select('id', 'latitude', 'longitude')
                    ->whereIn('id', $shopIds)
                    ->whereIn('district_id', $ids)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = RetailOutlet::select('id', 'latitude', 'longitude')
                    ->whereIn('id', $skuIds)->whereIn('category_id', $catIds)
                    ->whereIn('district_id', $ids)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['catalog_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['district_id'])
            ) {
                $catalogIds = $shopsId['catalog_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = RetailOutlet::select('id', 'latitude', 'longitude')
                    ->whereIn('id', $catalogIds)->whereIn('category_id', $catIds)
                    ->whereIn('district_id', $ids)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $allIds = array_intersect($skuIds, $catalogIds);

                if (empty($allIds)) {
                    return $shops =[];

                }

                $ids = array_unique($allIds);
                $outlet = RetailOutlet::whereIn('id', $ids)
                    ->whereIn('category_id', $catIds)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['district_id'])) {
                $skuShopIds = $shopsId['sku_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->district_id);
                $outlet = collect([]);
                RetailOutlet::select('id', 'latitude', 'longitude')
                    ->whereIn('id', $skuShopIds)
                    ->whereIn('district_id', $ids)
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $shops = app('App\Http\Controllers\Api\v1\RetailOutletController')->simpleJson($outlet);
                return $shops;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['district_id'])) {
                $catalogShopIds = $shopsId['catalog_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->district_id);
                $outlet = RetailOutlet::whereIn('id', $catalogShopIds)
                    ->whereIn('district_id', $ids)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['catalog_id'])) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];
                $allIds = array_intersect($skuIds, $catalogIds);
                if (empty($allIds)) {
                    return $shops =[];

                }
                $ids = array_unique($allIds);
                $outlet = RetailOutlet::whereIn('id', $ids)->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['town_id'])) {
                $skuIds = $shopsId['sku_id'];

                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);

                $outlet = RetailOutlet::whereIn('id', $skuIds)
                    ->whereIn('town_id', $ids)->paginate($dataPerPage);

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['town_id'])) {
                $catalogIds = $shopsId['catalog_id'];

                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);

                $outlet = RetailOutlet::whereIn('id', $catalogIds)
                    ->whereIn('town_id', $ids)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['district_id']) && isset($shopsId['category_id'])) {
                $districtShopIds = $shopsId['district_id'];
                $allDistrictId = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($allDistrictId, $districtId->district_id);
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = RetailOutlet::whereIn('district_id', $allDistrictId)
                    ->whereIn('category_id', $catIds)
                    ->paginate($dataPerPage);
            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['category_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = RetailOutlet::whereIn('id', $catalogIds)
                    ->whereIn('category_id', $catIds)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['town_id']) && isset($shopsId['category_id'])) {
                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = RetailOutlet::whereIn('town_id', $ids)
                    ->whereIn('category_id', $catIds)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['category_id'])) {
                $skuIds = $shopsId['sku_id'];

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = RetailOutlet::whereIn('id', $skuIds)
                    ->whereIn('category_id', $catIds)
                    ->paginate($dataPerPage);

            } elseif (isset($shopsId['sku_id'])) {
                $skuIds = $shopsId['sku_id'];
                $outlet = RetailOutlet::whereIn('id', $skuIds)->paginate($dataPerPage);

            } elseif (isset($shopsId['catalog_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $outlet = RetailOutlet::whereIn('id', $catalogIds)->paginate($dataPerPage);
            } elseif (isset($shopsId['category_id'])) {
                $categoryIds = $shopsId['category_id'];
                $outlet = RetailOutlet::whereIn('id', $categoryIds)->paginate($dataPerPage);
            } elseif (isset($shopsId['catalog_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $outlet = RetailOutlet::whereIn('id', $catalogIds)->paginate($dataPerPage);
            } elseif (isset($shopsId['town_id'])) {
                $townIds = $shopsId['town_id'];
                $outlet = RetailOutlet::whereIn('id', $townIds)->paginate($dataPerPage);
            } elseif (isset($shopsId['district_id'])) {
                $districtIds = $shopsId['district_id'];
                $outlet = RetailOutlet::whereIn('id', $districtIds)->paginate($dataPerPage);
            }
            if ($outlet->isEmpty() == false) {
                $outlet->appends($page);
                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($outlet, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($outlet));
                return $this->manager->createData($resource)->toArray();
            } else {
                $shops['data'] = [];
                $data = $shops;
                return $data;
            }

        } else {
            $outlet = RetailOutlet::paginate($dataPerPage);
            if ($outlet->isEmpty() == false) {
                $outlet->appends($page);
                $this->manager->setSerializer(new JsonApiSerializer(env('ABS_URL')));
                $resource = new Collection($outlet, new RetailOutletTransformer(null), 'retail');
                $resource->setPaginator(new IlluminatePaginatorAdapter($outlet));
                return $this->manager->createData($resource)->toArray();
            } else {
                $shops['data'] = [];
                $data = $shops;
                return $data;
            }
        }


    }

    public function getDownloadOutletResult(Request $request)
    {
        if ($request->exists('filter')) {
            $filter = $request->get('filter');
            $filterKey = array('town', 'catalog', 'skuinventory', 'retail_outlet_categories', 'districts', 'zones', 'development_regions');
            $key = [];

            foreach ($filter as $keyName => $filterData) {
                $key[] = $keyName;
            }

            $intersectKeys = array_intersect($key, $filterKey);

            $keyexist = count($intersectKeys);
            if ($keyexist == 0) {
                $code = ConstantCodeService::NOTFOUNDFILTERCODE;
                $key = key($filter);
                $message = ConstantApiMessageService::NOTFOUNDFILTER;
                $title = ConstantApiMessageService::INVALIDFILTER;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);
            }
            $shopsId = array();


            foreach ($intersectKeys as $intersectKey) {
                if (array_key_exists('id', $filter[$intersectKey]) ||
                    array_key_exists('gid', $filter[$intersectKey])
                ) {
                    $validateDatasId = array();
                    $columnName = key($filter[$intersectKey]);
                    $table = $intersectKey;
                    $value = $filter[$intersectKey][$columnName];
                    $columnId[$table] = $columnName;
                    $intersectValues = $this->validateData($table, $columnName, $value, $columnId);
                    array_push($validateDatasId, $intersectValues);
                    if ($intersectKey == 'town') {
                        $shopsId['town_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'retail_outlet_categories') {
                        $shopsId['category_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);
                    } elseif ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    } elseif ($intersectKey == 'districts') {
                        $shopsId['district_id'] = app('App\Services\ShopService')->getIdResult($validateDatasId);

                    }


                } elseif (array_key_exists('name', $filter[$intersectKey])) {
                    $columnName = key($filter[$intersectKey]);
                    $value = $filter[$intersectKey][$columnName];

                    $filterValue = substr($value, 1, -1);
                    $delimiters = preg_split('/\s*\(.*?\)\s*/', $value);
                    $newDelimiter = array_slice($delimiters, 1, -1);

                    $table = $intersectKey;
                    $columnId[$table] = $columnName;

                    $tempArr = $this->paramParser($filterValue);
                    if ($intersectKey == 'catalog') {
                        $shopsId['catalog_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    } elseif ($intersectKey == 'skuinventory') {
                        $shopsId['sku_id'] = app('App\Services\ShopService')->getNameResult($tempArr, $newDelimiter, $table, $columnName, $columnId, $intersectKey);
                    }

                }
            }


            if (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $allShopId = array_intersect($catalogIds, $skuIds);

                if (empty($allShopsId)) {
                    return $shops =[];

                }

                $shopId = array_unique($allShopId);

                $outlet = collect([]);

                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $shopId)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['town_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];

                $townShopIds = $shopsId['town_id'];
                $town = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($town, $townId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);


                $allIds = array_intersect($skuIds, $catalogIds);
                $ids = array_unique($allIds);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $ids)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->whereIn('retail_outlet.town_id', $town)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $allShopsId = array_intersect($skuIds, $catalogIds);
                $shopIds = array_unique($allShopsId);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $shopIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['district_id'])
            ) {
                $skuIds = $shopsId['sku_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id']) &&
                isset($shopsId['district_id'])
            ) {

                $catalogIds = $shopsId['catalog_id'];
                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['sku_id']) &&
                isset($shopsId['category_id']) &&
                isset($shopsId['catalog_id'])
            ) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $allIds = array_intersect($skuIds, $catalogIds);
                $ids = array_unique($allIds);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $ids)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['district_id'])) {
                $catalogShopIds = $shopsId['catalog_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->district_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogShopIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;
            } elseif (isset($shopsId['sku_id']) && isset($shopsId['district_id'])) {
                $skuIds = $shopsId['sku_id'];

                $districtShopIds = $shopsId['district_id'];
                $ids = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($ids, $districtId->district_id);
                $outlet = collect([]);

                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.district_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['catalog_id'])) {
                $skuIds = $shopsId['sku_id'];
                $catalogIds = $shopsId['catalog_id'];
                $allIds = array_intersect($skuIds, $catalogIds);
                $ids = array_unique($allIds);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['district_id']) && isset($shopsId['category_id'])) {
                $districtShopIds = $shopsId['district_id'];
                $allDistrictId = array();
                $districtIds = RetailOutlet::whereIn('id', $districtShopIds)->groupBy('district_id')->select('district_id')->get();
                foreach ($districtIds as $districtId) array_push($allDistrictId, $districtId->district_id);
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet_channel.district_id', $allDistrictId)
                    ->whereIn('retail_outlet_channel.category_id', $catIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;


            } elseif (isset($shopsId['sku_id']) && isset($shopsId['town_id'])) {
                $skuIds = $shopsId['sku_id'];

                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.town_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['town_id'])) {
                $catalogIds = $shopsId['catalog_id'];

                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->whereIn('retail_outlet.town_id', $ids)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['catalog_id']) && isset($shopsId['category_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->whereIn('retail_outlet.category_id', $categoryIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['sku_id']) && isset($shopsId['category_id'])) {
                $skuIds = $shopsId['sku_id'];

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);

                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->whereIn('retail_outlet.category_id', $categoryIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['town_id']) && isset($shopsId['category_id'])) {
                $townShopIds = $shopsId['town_id'];
                $ids = array();
                $townIds = RetailOutlet::whereIn('id', $townShopIds)->groupBy('town_id')->select('town_id')->get();
                foreach ($townIds as $townId) array_push($ids, $townId->town_id);

                $catIds = array();
                $categoryShopIds = $shopsId['category_id'];
                $categoryIds = RetailOutlet::whereIn('id', $categoryShopIds)->groupBy('category_id')->select('category_id')->get();
                foreach ($categoryIds as $categoryId) array_push($catIds, $categoryId->category_id);
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.town_id', $ids)
                    ->whereIn('retail_outlet.category_id', $catIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['sku_id'])) {
                $skuIds = $shopsId['sku_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $skuIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;

            } elseif (isset($shopsId['catalog_id'])) {
                $catalogIds = $shopsId['catalog_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $catalogIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;
            } elseif (isset($shopsId['district_id'])) {
                $districtIds = $shopsId['district_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $districtIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;
            } elseif (isset($shopsId['town_id'])) {
                $townIds = $shopsId['town_id'];
                $outlet = collect([]);
                RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                    ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                    ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                    ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                    ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                    ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                    ->whereIn('retail_outlet.id', $townIds)
                    ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                    ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                        $outlet = $outlet->merge($retailOutlets);

                    });
                $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
                return $outletData;
            }

        } else {
            $outlet = collect([]);
            RetailOutlet::join('zones', 'retail_outlet.zone_id', '=', 'zones.gid')
                ->join('districts', 'retail_outlet.district_id', '=', 'districts.gid')
                ->join('town', 'retail_outlet.town_id', '=', 'town.id')
                ->join('street', 'retail_outlet.street_id', '=', 'street.id')
                ->join('retail_outlet_categories', 'retail_outlet.category_id', '=', 'retail_outlet_categories.id')
                ->join('retail_outlet_channel', 'retail_outlet.channel_id', '=', 'retail_outlet_channel.id')
                ->select(array('zones.name as z', 'street.name as s', 'town.name as t', 'phone as p', 'districts.name as d', 'retail_outlet_categories.name as c', 'retail_outlet_channel.name as ch', 'retail_outlet.id as outletid', 'retail_outlet.name as n', 'retail_outlet.owner_name as on', 'retail_outlet.owner_phone as op', 'retail_outlet.longitude as ln', 'retail_outlet.latitude as lt', 'retail_outlet.store_size as ss', 'retail_outlet.monthly_avg_business as mab', 'retail_outlet.verified_on as vo', 'retail_outlet.hotspot as ht'))
                ->chunk(300000, function ($retailOutlets) use (&$outlet) {
                    $outlet = $outlet->merge($retailOutlets);

                });
            $outletData = app('App\Http\Controllers\Api\v1\DownloadController')->exportResponse($outlet);
            return $outletData;
        }


    }


    public function validateData($table, $columnNames, $value, $columnId = null)
    {

        $tables[$table] = $value;
        foreach ($tables as $table => $tableValue) {
            $column = [];
            if (!is_array($tableValue)) {
                $value = explode(',', strtolower($tableValue));
            } else {
                $data = implode("','", $tableValue);
                $colValue = str_replace("{", "(", $data);
                $allData = str_replace("}", ")", $colValue);
                $newValue = str_replace("_", "-", $allData);
                $newStripValue = explode("','", strtolower($newValue));
                $value = $newStripValue;

            }


            if (array_key_exists($table, $columnId)) {
                if ($table == 'districts') {
                    $columnName = 'gid';
                    $query = "Select gid from $table";
                } else {
                    $columnName = $columnId[$table];
                    $query = "Select $columnId[$table] from $table";
                }

                $datas = DB::select($query);

                foreach ($datas as $data) $column[] = strtolower($data->$columnName);
                $filterData = array_intersect($value, $column);
//                dd($column);


                if (in_array(false, array_map(function ($v) {
                    return is_numeric($v);
                }, $filterData))) {
                    if (preg_match("/'/u", implode("','", $filterData)) == 1) {
                        $resultData = str_replace("'", "''", $filterData);
                    } else {
                        $resultData = $filterData;
                    }

                    $nameResult = "'" . implode("','", $resultData) . "'";
                    $result = strtolower($nameResult);


                    $query = "Select id from $table where lower(name) IN ($result)";
                    $ids = DB::select($query);
                    $resultId = [];
                    foreach ($ids as $id) $resultId[] = $id->id;

                    $intersectData[$table] = implode(',', $resultId);
                } else {
                    $intersectData[$table] = implode(',', $filterData);
                }

            }
        }


        foreach ($intersectData as $filterKey => $filterData) {

            if (empty($filterData) == true && $filterKey == 'zones') {
                $code = ConstantCodeService::NOTFOUNDZONENAME;
                $title = ConstantApiMessageService::INVALIDZONEIDREQUEST;
                $message = ConstantApiMessageService::NOTFOUNDZONEID;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'catalog') {
                $code = ConstantCodeService::NOTFOUNDBRAND;
                $title = ConstantApiMessageService::INVALIDBRAND;
                $message = ConstantApiMessageService::NOTFOUNDBRAND;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'retail_oultet_categories') {
                $code = ConstantCodeService::NOTFOUNDCATEGORY;
                $title = ConstantApiMessageService::INVALIDCATEGORY;
                $message = ConstantApiMessageService::NOTFOUNDCATEGORY;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'development_regions') {
                $code = ConstantCodeService::NOTFOUNDREGION;
                $title = ConstantApiMessageService::INVALIDREGION;
                $message = ConstantApiMessageService::NOTFOUNDREGION;
                $key = implode(',', $filterData);
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'skuinventory') {
                $code = ConstantCodeService::NOTFOUNDPRODUCTNAME;
                $title = ConstantApiMessageService::INVALIDPRODUCT;
                $message = ConstantApiMessageService::NOTFOUNDPRODUCT;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'town') {
                $code = ConstantCodeService::NOTFOUNDTOWNID;
                $title = ConstantApiMessageService::INVALIDTOWN;
                $message = ConstantApiMessageService::NOTFOUNDTOWN;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } elseif (empty($filterData) == true && $filterKey == 'districts') {
                $code = ConstantCodeService::NOTFOUNDDISTRICT;
                $title = ConstantApiMessageService::INVALIDDISTRICT;
                $message = ConstantApiMessageService::NOTFOUNDDISTRICT;
                $key = $filterData;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return $error;
            } else {
                break;
            }

        }


        if (array_key_exists('zones', $intersectData)) $newData['zone_id'] = $intersectData['zones'];
        if (array_key_exists('districts', $intersectData)) $newData['district_id'] = $intersectData['districts'];
        if (array_key_exists('catalog', $intersectData)) $newData['catalog_id'] = $intersectData['catalog'];
        if (array_key_exists('retail_outlet_categories', $intersectData)) $newData['category_id'] = $intersectData['retail_outlet_categories'];
        if (array_key_exists('development_regions', $intersectData)) $newData['development_region_id'] = $intersectData['development_regions'];
        if (array_key_exists('skuinventory', $intersectData)) $newData['sku_id'] = $intersectData['skuinventory'];
        if (array_key_exists('town', $intersectData)) $newData['town_id'] = $intersectData['town'];
        return $newData;


    }


    public function paramParser($filterParams)
    {
        $value = array();
        $array = explode("+", $filterParams);
        $negativeArray = [];
        $positiveArray = [];

        foreach ($array as $data) {
            $newkey = str_replace(")", "", $data);
            $newArray = str_replace("(", "", $newkey);


            if (preg_match('/-/', $newArray)) {
                $positiveArray[] = substr($newArray, 0, strpos($newArray, '-'));
                $array2 = explode("-", $newArray);
                for ($i = 0; $i < count($array2); $i++) {
                    if ($i != 0) $negativeArray[] = $array2[$i];
                }
            } else {
                $positiveArray[] = $newArray;
            }
        }
        $result = ['positiveArray' => $positiveArray, 'negativeArray' => $negativeArray];
        array_push($value, $result);


        return $value;


    }


}