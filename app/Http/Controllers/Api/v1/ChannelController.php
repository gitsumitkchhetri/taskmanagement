<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\Zones;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Transformers\ChannelTransformer;
use App\Transformers\NepalZoneSimplifiedTransformer;
use App\Transformers\ZoneTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use DB;
use Config;

class ChannelController extends Controller
{
    public function __construct()
    {
        $this->zones = new Zones();
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->zonesTransformer = new ZoneTransformer();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
    }

    /**
     * @SWG\Get(
     *   path="/channels",
     *   summary="Get Channels and associative channel field by filters",
     *   operationId="get-channels",
     *  tags={"Zones"},
     *       @SWG\Parameter(
     *         name="fields",
     *         in="query",
     *         description="Get channel field by fields",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="Get Zone and associative zone detail by filters"
     *   )
     * )
     */

    public function getFilterResults(Request $request)
    {

        if ($request->exists('fields') == true) {
            $fieldQuery = $request->get('fields');
            $fields = explode(',', $fieldQuery);
            $channelField = str_replace("-", "_", $fields);

            if (app('App\Http\Controllers\Api\v1\ChannelController')->in_array_any($channelField, Config::get('nepalzone.channelfields')) == false) {
                $code = ConstantCodeService::NOTFOUNDFIELDCODE;
                $key = $fieldQuery;
                $message = ConstantApiMessageService::NOTFOUNDFIELD;
                $title = ConstantApiMessageService::INVALIDFIELD;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);

            } else {
                $channelFieldFilter = array_intersect(Config::get('nepalzone.channelfields'), $channelField);
                $commaSeparatedFields = implode(",", $channelFieldFilter);
                $query = "SELECT id," . $commaSeparatedFields . " FROM channel";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new ChannelTransformer($channelFieldFilter), 'channel');
                return $this->manager->createData($resource)->toArray();
            }

        }
        $field =Config::get('nepalzone.channelfields');
        $commaSeparatedFields = implode(",", $field);

        $query = "SELECT " .$commaSeparatedFields. " FROM retail_outlet_channel";

        $coordinates = DB::select($query);
        $baseUrl = env('ABS_URL');
        $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
        $resource = new Collection($coordinates, new ChannelTransformer(null), 'channel');
        return $this->manager->createData($resource)->toArray();
    }
}