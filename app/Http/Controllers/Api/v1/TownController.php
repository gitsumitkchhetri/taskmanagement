<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\JsonValidationRequest;
use App\Model\Api\District;
use App\Model\Api\Town;
use App\Model\Api\Zones;
use App\Services\ConstantApiMessageService;
use App\Services\ConstantCodeService;
use App\Services\ConstantStatusService;
use App\Transformers\NepalZoneSimplifiedTransformer;
use App\Transformers\ZoneTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use DB;
use Config;

class TownController extends Controller
{
    public function __construct()
    {
        $this->manager = new Manager();
        $this->jsonValidationRequest = new JsonValidationRequest();
        $this->zonesTransformer = new ZoneTransformer();
        $this->nepalZoneSimplifiedTransformer = new NepalZoneSimplifiedTransformer();
        $this->town = new Town();
        $this->district = new District();
        $this->zones = new Zones();
    }

    /**
     * @SWG\Get(
     *   path="/towns",
     *   summary="Get town and associative town detail by fields",
     *   operationId="get-districts",
     *  tags={"Zones"},
     *       @SWG\Parameter(
     *         name="fields",
     *         in="query",
     *         description="Get data by fields",
     *         required=false,
     *         type="string",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * @SWG\Response(
     *     response=200,
     *     description="Get town and associative town detail by fields"
     *   )
     * )
     */

    public function getFilterResults(Request $request)
    {
        if ($request->exists('filter') == true) {
            $filter = $request->get('filter');

        if (array_key_exists('zone', $filter) == true
                &&$request->exists('fields') == true) {
                $zone = $filter['zone'];
                $zoneId = explode(',', $zone);
                $zones = $this->zones->getZoneDetails();
                $zoneIds = [];
                foreach ($zones as $zone) $zoneIds[] = $zone->gid;
                $intersectZoneId = array_intersect($zoneId, $zoneIds);
                $fieldQuery = $request->get('fields');
                $fields = explode(',', $fieldQuery);
                $field = str_replace("-", "_", $fields);

                if (empty($intersectZoneId) == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zone;
                    $message = ConstantApiMessageService::NOTFOUNDZONEID;
                    $title = ConstantApiMessageService::INVALIDZONEID;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

                $districtField = array_intersect(Config::get('nepalzone.townfields'), $field);
                $commaSeparatedFields = implode(",", $districtField);
                $implodeTownIds = "'" . implode("','", array_unique($intersectZoneId)) . "'";
                $query = "SELECT id," . $commaSeparatedFields . " FROM town where zone_id IN ($implodeTownIds)";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'town');
                return $this->manager->createData($resource)->toArray();

            } if (array_key_exists('district', $filter) == true
                &&$request->exists('fields') == true) {
                $district = $filter['district'];
                $districtId = explode(',', $district);
                $districts = $this->district->getDistricts();
                $districtIds = [];
                foreach ($districts as $district) $districtIds[] = $district->gid;
                $intersectDistrictId = array_intersect($districtId, $districtIds);
                $fieldQuery = $request->get('fields');
                $fields = explode(',', $fieldQuery);
                $field = str_replace("-", "_", $fields);

                if (empty($intersectDistrictId) == true) {
                    $code = ConstantCodeService::NOTFOUNDDISTRICT;
                    $key = $district;
                    $message = ConstantApiMessageService::NOTFOUNDDISTRICT;
                    $title = ConstantApiMessageService::INVALIDDISTRICT;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

                $districtField = array_intersect(Config::get('nepalzone.townfields'), $field);
                $commaSeparatedFields = implode(",", $districtField);
                $implodeTownIds = "'" . implode("','", array_unique($intersectDistrictId)) . "'";
                $query = "SELECT id," . $commaSeparatedFields . " FROM town where district_id IN ($implodeTownIds)";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'town');
                return $this->manager->createData($resource)->toArray();

            }elseif (array_key_exists('zone', $filter)) {
                $zone = $filter['zone'];
                $zoneId = explode(',', $zone);
                $zones = $this->zones->getZoneDetails();
                $zoneIds = [];
                foreach ($zones as $zone) $zoneIds[] = $zone->gid;
                $intersectZoneId = array_intersect($zoneId, $zoneIds);

                if (empty($intersectZoneId) == true) {
                    $code = ConstantCodeService::NOTFOUNDZONENAME;
                    $key = $zone;
                    $message = ConstantApiMessageService::NOTFOUNDZONEID;
                    $title = ConstantApiMessageService::INVALIDZONEID;
                    $status = ConstantStatusService::NOTFOUNDSTATUS;
                    $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                    return response()->json($error, $status);
                }

                $field = Config::get('nepalzone.townfields');
                $commaSeparatedFields = implode(",", $field);
                $implodeTownIds = "'" . implode("','", array_unique($intersectZoneId)) . "'";
                $query = "SELECT id," . $commaSeparatedFields . " FROM town where zone_id IN ($implodeTownIds)";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'town');
                return $this->manager->createData($resource)->toArray();
            }


        } elseif ($request->exists('fields') == true) {
            $fieldQuery = $request->get('fields');
            $fields = explode(',', $fieldQuery);
            $field = str_replace("-", "_", $fields);

            if (app('App\Http\Controllers\Api\v1\ZoneController')->in_array_any($field, Config::get('nepalzone.townfields')) == false) {
                $code = ConstantCodeService::NOTFOUNDFIELDCODE;
                $key = $fieldQuery;
                $message = ConstantApiMessageService::NOTFOUNDFIELD;
                $title = ConstantApiMessageService::INVALIDFIELD;
                $status = ConstantStatusService::NOTFOUNDSTATUS;
                $error = $this->jsonValidationRequest->missingDataAttribute(null, $code, $key, $message, $title);
                return response()->json($error, $status);

            } else {
                $districtField = array_intersect(Config::get('nepalzone.townfields'), $field);
                $commaSeparatedFields = implode(",", $districtField);
                $query = "SELECT id," . $commaSeparatedFields . " FROM town";
                $coordinates = DB::select($query);
                $baseUrl = env('ABS_URL');
                $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
                $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer($districtField), 'town');
                return $this->manager->createData($resource)->toArray();
            }

        }
        $field = Config::get('nepalzone.townfields');
        $commaSeparatedFields = implode(",", $field);

        $query = "SELECT " . $commaSeparatedFields . " FROM town";
        $coordinates = DB::select($query);
        $baseUrl = env('ABS_URL');
        $this->manager->setSerializer(new JsonApiSerializer($baseUrl));
        $resource = new Collection($coordinates, new NepalZoneSimplifiedTransformer(null), 'town');
        return $this->manager->createData($resource)->toArray();
    }


}