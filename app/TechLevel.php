<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechLevel extends Model
{
    protected $table = "cs_tech_levels";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['title','cost'];
    protected $perPage = 30;

    public function getAllData($keyword = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("title","LIKE","%$keyword%")
                    ->orWhere("cost","LIKE","%$keyword%");
            });
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }
}
