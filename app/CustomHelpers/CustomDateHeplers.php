<?php

/**
 * Created by PhpStorm.
 * User: sumitkc
 * Date: 8/4/17
 * Time: 11:34 AM
 */

namespace App\CustomHelpers;

class CustomDateHeplers
{
    public static function aYearBeforeMonthsGen(){
        $monthArr = [];
        $date = strtotime(date("Y-m-d", strtotime("-1 year")));
        for($i = 1;$i <= 12;$i++){
            if($i == 1){
                $cust_date = $date;
            }
            array_push($monthArr,date("M", strtotime("+1 month", $cust_date)));
            $cust_date = strtotime(date("Y-m-d", strtotime("+1 month", $cust_date)));
        }
        return $monthArr;
    }

    public  static function dayGenerator($from,$to){
        $tempFrom = $from;
        $dateArr = [];
        $dateArr[] = $from;
        while($tempFrom < $to){

            $dateArr[] = date("Y-m-d", strtotime("+1 day", strtotime($tempFrom)));
            $tempFrom = date("Y-m-d", strtotime("+1 day", strtotime($tempFrom)));
        }

        return $dateArr;

    }
}


