<?php
/**
 * Created by PhpStorm.
 * User: sumitkc
 * Date: 8/8/17
 * Time: 4:04 PM
 */

namespace App\CustomHelpers;

use App\ProjectTechnicians;
use App\ProjectPerPhase;
use App\Timesheet;
use App\TechLevel;
use App\Technician;
use App\TaskPerProject;
use App\Project;


class CostHelper
{
    public static function recordLaborCost(){

        $projects = Project::all();

        foreach ($projects as $project){
            $id = $project->id;

            $phasesPerProject = ProjectPerPhase::where("project_id","=",$id)->get();
            $phasePerProjectData = [];




            foreach ($phasesPerProject as $phase) {
                $phasePerProjectData[$phase->id]["attributes"] = $phase->toArray();
                $tasksPerProject = TaskPerProject::where("phase_id", "=", $phase->id)->get();
                $tasks = $tasksPerProject;



                if (!$tasks->isEmpty()) {
                    $phasePerProjectData[$phase->id]["task"] = [];


                        foreach ($tasks as $task) {

                            if($task->type == "Labor"){
                                $technical = ProjectTechnicians::where("project_id","=",$id)->where("task_id","=",$task->id)->get();
                                if(!$technical->isEmpty()){

                                    $phasePerProjectData[$phase->id]["task"][$task->id]["cost"] = ProjectTechnicians::selectRaw('sum(cost) as total_cost')
                                        ->where("project_id","=",$id)
                                        ->where("task_id","=",$task->id)
                                        ->first()->total_cost;
                                }
                                $latest_verified_work = Timesheet::where("project_id","=",$id)
                                    ->where("phase_id","=",$phase->id)
                                    //->where("verified","=",1)
                                    ->where("task_id","=",$task->id)
                                    ->orderBy("date","DESC")
                                    ->first();


                                //get latest cummulative work hrs of technicians assigned for specific task of project.
                                if($latest_verified_work != null){
                                    $timesheet_work = Timesheet::selectRaw("SUM(work_hr) AS total_hr,technician_id,project_id,task_id,phase_id")
                                        ->where("project_id","=",$id)
                                        ->where("phase_id","=",$phase->id)
                                        ->whereDate("date","<=",$latest_verified_work->date)
                                        ->where("task_id","=",$task->id)
                                        ->groupBy("technician_id","project_id","task_id","phase_id")
                                        ->get();

                                    //calculate cummulative costs of technicians assigned for specific task of project.
                                    $cost_to_date = 0;
                                    foreach ($timesheet_work as $tw){
                                        $techObj = Technician::find($tw->technician_id);
                                        $techLvlObj = TechLevel::find($techObj->level_id);

                                        $cost_to_date += ($tw->total_hr * $techLvlObj->cost);

                                    }

                                    $phasePerProjectData[$phase->id]["task"][$task->id]["total_hr_cost"] = $cost_to_date;
                                    $task->labor_cost_to_date = $cost_to_date;
                                    $task->save();
                                }
                            }


                        }

                }

            }




        }


    }
}