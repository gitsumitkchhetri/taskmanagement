<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = "crm_addresses";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['client_id','address','lat','lng','contact_no','google_map_link'];
    protected $perPage = 30;

    public function getAllData($keyword = null,$client_id = null){

        $data = $this->query();
        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("address","LIKE","%$keyword%")
                    ->orWhere("lat","LIKE","%$keyword%")
                    ->orWhere("contact_no","LIKE","%$keyword%")
                    ->orWhere("lng","LIKE","%$keyword%");
            });
        }

        if($client_id != null){
            $data->where("client_id","=",$client_id);
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }

    public  function client(){
        return $this->belongsTo("App/Clients","client_id","id");
    }
}
