<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Deal;
use App\User;

class Project extends Model
{
    protected $table = "cs_projects";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['title','total_budget','deal_id','description','job_types','sub_systems','project_type'];
    protected $perPage = 30;

    public function getAllData($keyword = null,$filter = null,$user){

        $data = $this->query();

//        $userObj = User::find($user);
//
//        if( !($user == 1 || $userObj->roles->first()->name == "superuser" || $userObj->roles->first()->name == "superadmin") ){
//            $dealsArr =Deal::where("sales_person","=",$user)->pluck("id")->toArray();
//
//            $data->whereIn("deal_id",$dealsArr);
//        }

        if($keyword != null){
            $keyword = trim($keyword);
            $data->where(function($query) use ($keyword){
                $query
                    ->orWhere("title","LIKE","%$keyword%");
            });
        }

        if($filter != null){
            $filter = trim($filter);
            $data->where("deal_id","=",$filter);
        }

        return $data->orderBy("created_at","DESC")->paginate($this->perPage);
    }

    public function getDataById($id){

    }

    public function addData($dataArray){
        return $this->create($dataArray);
    }

    public function updateData($dataArray,$id){

        return $this->find($id)->update($dataArray);
    }

    public function deleteData($id){
        return $this->find($id)->delete();
    }

    public  function deal(){
        return $this->belongsTo("App\Deal","deal_id","id");
    }
}
