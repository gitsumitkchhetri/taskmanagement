<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 5/16/17
 * Time: 2:31 PM
 */

namespace App\Services;


use App\Brand;
use App\Model\Api\BrandResponse;
use App\Model\Api\BrandSkuResponse;
use App\Model\Api\Skuinventory;
use App\Model\Api\SkuResponse;

class ApiLoadTestingService
{
    public function exceuteApiLoadTesting($skuNumber=null,$brandNumber=null,$bothNumber=null)
    {
        $this->brandResponse = new BrandResponse();
        $this->skuResponse = new SkuResponse();
        $this->brandSkuResponse = new BrandSkuResponse();

        $delimeters = array('%2B', '-');
        $newDelimeters = $delimeters[array_rand($delimeters)];

        /* skuknventory */
        if ($skuNumber != null){
        $j=1;
        while($j<=$skuNumber) {
            $skus = array();
            $randomSkus = Skuinventory::all()->random(5);
            foreach ($randomSkus as $randomSku) {
                $brandAmperSand =str_replace("&", "%26",  $randomSku->name);
                $brandOpeningBracket = str_replace("{", "(", $brandAmperSand);
                $brandClosingBracket = str_replace("}", ")", $brandOpeningBracket);
                $newValue = str_replace("-", "_", $brandClosingBracket);
                array_push($skus, str_replace("&", "%26",  $newValue));
            }
            $skuParser = $this->generateParser($randomSkus, $newDelimeters, $skus);
            $url = "http://103.198.8.236:8000/api/v1/shops?filter[skuinventory][name]=" . $skuParser;
            $curlResponse = $this->curlApi($url);
            $skuResponse['parser'] ='SKU:'.$skuParser;
            if ($curlResponse['httpstatuscode'] == 500) $skuResponse['count'] =0;
            else $skuResponse['count'] = count(json_decode($curlResponse['content'], true));
            $skuResponse['executiontime'] = null;
            $skuResponse['statuscode'] = $curlResponse['httpstatuscode'];;
            $this->brandResponse->addData($skuResponse);
            $j++;
        }
        }

        /* brand */

        if ($brandNumber != null) {
            $i = 1;
            while ($i <= $brandNumber) {
                $catalogs = array();
                $randomBrands = Brand::all()->random(5);

                foreach ($randomBrands as $randomBrand) {
                    $brandAmperSand =str_replace("&", "%26",  $randomBrand->name);
                    $brandOpeningBracket = str_replace("{", "(", $brandAmperSand);
                    $brandClosingBracket = str_replace("}", ")", $brandOpeningBracket);
                    $newValue = str_replace("-", "_", $brandClosingBracket);
                    array_push($catalogs, str_replace("&", "%26", $newValue));
                }

                $parser = $this->generateParser($randomBrands, $newDelimeters, $catalogs);
                $url = "http://103.198.8.236:8000/api/v1/shops?filter[catalog][name]=" . $parser;
                $curlResponse = $this->curlApi($url);
                $brandResponse['parser'] = 'Brand:'.$parser;
                if ($curlResponse['httpstatuscode'] == 500) $brandResponse['count'] = 0;
                else $brandResponse['count'] = count(json_decode($curlResponse['content'], true));
                $brandResponse['executiontime'] = null;
                $brandResponse['statuscode'] = $curlResponse['httpstatuscode'];
                $this->brandResponse->addData($brandResponse);

                $i++;
            }
        }


        /* brand sku response*/
        if ($bothNumber != null) {
            $k = 1;
            while ($k <= $bothNumber) {
                $catalogs = array();
                $randomBrands = Brand::all()->random(5);
                foreach ($randomBrands as $randomBrand) {
                    $brandAmperSand =str_replace("&", "%26",  $randomBrand->name);
                    $brandOpeningBracket = str_replace("{", "(", $brandAmperSand);
                    $brandClosingBracket = str_replace("}", ")", $brandOpeningBracket);
                    $newValue = str_replace("-", "_", $brandClosingBracket);
                    array_push($catalogs, str_replace("&", "%26", $newValue));
                }

                $skus = array();
                $randomSkus = Skuinventory::all()->random(5);

                foreach ($randomSkus as $randomSku) {
                    $brandAmperSand =str_replace("&", "%26",  $randomSku->name);
                    $brandOpeningBracket = str_replace("{", "(", $brandAmperSand);
                    $brandClosingBracket = str_replace("}", ")", $brandOpeningBracket);
                    $newValue = str_replace("-", "_", $brandClosingBracket);
                    array_push($skus, str_replace("&", "%26",  $newValue));
                }
                $parser = $this->generateParser($randomBrands, $newDelimeters, $catalogs);
                $skuParser = $this->generateParser($randomSkus, $newDelimeters, $skus);

                $url = "http://103.198.8.236:8000/api/v1/shops?filter[skuinventory][name]=" . $skuParser . '&filter[catalog][name]=' . $parser;

                $curlResponse = $this->curlApi($url);
                $brandSkuResponse['parser'] = 'brand:' . $parser . 'SKU:' . $skuParser;
                if ($curlResponse['httpstatuscode'] == 500) $brandSkuResponse['count'] = 0;
                else $brandSkuResponse['count'] = count(json_decode($curlResponse['content'], true));
                $brandSkuResponse['executiontime'] = null;
                $brandSkuResponse['statuscode'] = $curlResponse['httpstatuscode'];
                $this->brandResponse->addData($brandSkuResponse);
                $k++;
            }
        }
    }

    public function generateParser($randData, $delimeters, $randModelData)
    {
        $parser = '';
        for ($i = 0; $i < count($randData); $i++) {
            $modelData = str_replace(" ", "%20", $randModelData[$i]);
            if ($i == 0) $parser = '(' . $modelData;
            else {
                if ($delimeters == '%2B') {
                    $skuParser = $parser . $delimeters . $modelData;
                    if ($i == count($randData) - 1) $parser = $skuParser . ')';
                } else {
                    if ($i != count($randData) - 1) $parser .= ')' . $delimeters . '(' . $modelData;
                    else $parser .= ')';
                }
            }

        }
        return $parser;
    }

    public function guzzleCurlApi($filterFrom,$filterField,$parser)
    {
        $client = new \GuzzleHttp\Client();
        $client->request('GET', 'http://103.198.8.236:8000/api/v1/shops', [
            'query' => ['filter' => [
                $filterFrom =>[
                    $filterField => $parser
                ]
            ]]
        ])->getBody();
        $stringBody = (string) $client;

    }

    public function curlApi($url)
    {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );
        curl_setopt_array($ch, $options);

        $curlData['content'] = curl_exec($ch);
        $curlData['httpstatuscode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        curl_close($ch);
        return $curlData;


    }
}