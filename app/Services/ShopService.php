<?php
/**
 * Created by PhpStorm.
 * User: samina-mac-mini
 * Date: 5/9/17
 * Time: 2:15 PM
 */

namespace App\Services;
use DB;


class ShopService
{
    public function getNameResult($filterResults,$newDelimiter,$table,$columnName,$columnId,$intersectKey)
    {
        $temp=array();
        $i =0;

        foreach ($filterResults as $filterResult) {
            if (isset($filterResult['positiveArray']) && isset($filterResult['negativeArray']) && !empty($filterResult['negativeArray'])&& !empty($filterResult['positiveArray'])) {
                $negativeData=array();
                $positiveData=array();

                $intersectpositiveValues =  app('App\Http\Controllers\Api\v1\CheckController')->validateData($table,$columnName, $filterResult['positiveArray'], $columnId);

                $allIds = explode (',',array_values($intersectpositiveValues)[0]);
                $allId = "'" . implode("','", $allIds) . "'";
                $countData = count($allIds);
                if ($intersectKey == 'skuinventory') {
                $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_skus rs on ro.id = rs.retailoutlet_id WHERE rs.sku_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";

                } elseif ($intersectKey == 'catalog') {
                $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_brand rb on ro.id = rb.retail_outlet_id WHERE rb.catalog_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";

                }
                $positiveIds = DB::select($retailShopQuery);
                foreach ($positiveIds as $positiveId) array_push($positiveData,$positiveId->id);


                $intersectNegativeValues =  app('App\Http\Controllers\Api\v1\CheckController')->validateData($table,$columnName, $filterResult['negativeArray'], $columnId);
                $allIds = explode (',',array_values($intersectNegativeValues)[0]);
                $allId = "'" . implode("','", $allIds) . "'";
                $countData = count($allIds);
                if ($intersectKey == 'skuinventory') {
                    $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_skus rs on ro.id = rs.retailoutlet_id WHERE rs.sku_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";
                } elseif ($intersectKey == 'catalog') {
                    $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_brand rb on ro.id = rb.retail_outlet_id WHERE rb.catalog_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";
                }

                $negativeIds = DB::select($retailShopQuery);
                foreach ($negativeIds as $negativeId) array_push($negativeData,$negativeId->id);
                $intersectIds = array_diff($positiveData,$negativeData);
                if (count($intersectIds) > 0) {
                    $allId = "'" . implode("','", $intersectIds) . "'";
                    array_push($temp,$intersectIds);
                }


            }elseif (empty($filterResult['negativeArray'])== true && empty($filterResult['positiveArray']) == false) {
                $intersectValues =  app('App\Http\Controllers\Api\v1\CheckController')->validateData($table,$columnName, $filterResult['positiveArray'], $columnId);
                $allIds = explode (',',array_values($intersectValues)[0]);
                $allId = "'" . implode("','", $allIds) . "'";
                $countData = count($allIds);
                if ($intersectKey == 'skuinventory') {
                    $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_skus rs on ro.id = rs.retailoutlet_id WHERE rs.sku_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";

                } elseif ($intersectKey == 'catalog') {
                    $retailShopQuery = "SELECT ro.id FROM retail_outlet ro INNER JOIN retailshop_brand rb on ro.id = rb.retail_outlet_id WHERE rb.catalog_id in ($allId) GROUP BY ro.id HAVING COUNT(ro.id) = $countData";

                }
                $result =DB::Select($retailShopQuery);
                $ids = array_pluck($result, 'id');
                array_push($temp,$ids);

            }
            $i++;
        }

        if (count($temp) == 1) {
            return $temp[0];
        } elseif (count($temp) == 0) {
            return $temp;
        }

        $finalArray = array();
        for($i=1;$i<count($temp);$i++) {
            if($i == 1){
                if ($newDelimiter[$i - 1] == '+') {

                   // $finalArray = array_merge($temp[$i -1], $temp[$i]);
                    $final = array_intersect($temp[$i -1], $temp[$i]);
                }else{
                    $finalArray = array_diff($temp[$i -1], $temp[$i]);
                }
            }
            else{
                if ($newDelimiter[$i - 1] == '-') {

                   // $final = array_merge($finalArray, $temp[$i]);
                    $final = array_intersect($finalArray, $temp[$i]);
                    $finalArray = array();
                    $finalArray = $final;
                }else{
                    $final = array_diff($finalArray, $temp[$i]);
                    $finalArray = array();
                    $finalArray = $final;
                }
            }
            $final = array();
        }
        return $finalArray;

    }

    public function getIdResult($validateDatasId)
    {
        $query = "";
        foreach ($validateDatasId as $validateData) {
            $columnKey = key($validateData);
            if ($columnKey == 'errors') {
                return response()->json($validateData, ConstantStatusService::NOTFOUNDSTATUS);
            }
            $columnValue = array_values($validateData);
            if ($columnKey == 'catalog_id') {
                $colKey = 'rb.'.$columnKey;
            } elseif ($columnKey == 'sku_id') {
                $colKey = 'rs.'.$columnKey;
            } else {
                $colKey = $columnKey;
            }
            $columnValue = explode(',', $columnValue[0]);
            $value = "'" . implode("','", $columnValue) . "'";
            if ($query !== "")
                $query= $query . " AND $colKey In ($value)";
            else
                $query= "where $colKey In ($value)";

        }


        if ($columnKey == 'catalog_id') {

            $retailQuery = "SELECT ro.id,ro.latitude,ro.longitude FROM retail_outlet ro INNER JOIN retailshop_brand rb on ro.id = rb.retail_outlet_id $query";
        }elseif ($columnKey == 'sku_id'){
            $retailQuery = "SELECT ro.id,ro.latitude,ro.longitude FROM retail_outlet ro INNER JOIN retailshop_skus rs on ro.id = rs.retailoutlet_id $query";
        }else{
            $retailQuery = "select id,latitude,longitude from retail_outlet $query";
        }

        $outlet=array();
        $result = DB::connection('pgsql')->select($retailQuery);
        foreach(array_chunk($result, 1000) as $retailOutlets){
            foreach($retailOutlets as $retailOutlet){
                array_push($outlet,$retailOutlet->id);
            }
        }
        return $outlet;


    }

}