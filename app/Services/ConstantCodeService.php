<?php
/**
 * Created by PhpStorm.
 * User: samina
 * Date: 12/15/16
 * Time: 4:47 PM
 */

namespace App\Services;


class ConstantCodeService
{

    const NOTFOUNDZONENAME = 120;
    const NOTFOUNDFIELDCODE = 121;
    const NOTFOUNDFILTERCODE = 122;
    const NOTFOUNDTYPECODE = 123;
    const NOTFOUNDDATACODE = 124;
    const NOTFOUNDDISTRICT = 125;
    const NOTFOUNDBRAND = 126;
    const NOTFOUNDCATEGORY = 127;
    const NOTFOUNDTOWNID = 128;
    const NOTFOUNDTOWNNAME = 129;
    const NOTFOUNDREGION = 130;
    const NOTFOUNDPRODUCTNAME = 131;
}