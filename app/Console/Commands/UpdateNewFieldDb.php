<?php

namespace App\Console\Commands;

use App\Bridge\User;
use App\Model\Api\Catalog;
use App\Model\Api\Categories;
use App\Model\Api\Channel;
use App\Model\Api\Skuinventory;
use App\MysqlCatalog;
use App\MysqlStreet;
use App\Street;
use Carbon\Carbon;
use Illuminate\Console\Command;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateNewFieldDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:field';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update New Field';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $directory = base_path() . '/csv';
//
//        if (File::isDirectory($directory) == false) {
//            File::makeDirectory($directory, $mode = 0777, true, true);
//        }
//        $csvbrandCategoryPath = $directory . '/brandcategory.csv';
//        $csvCatalogLevelPath = $directory . '/catalogLevel.csv';
//        $csvGroupRolePath = $directory . '/grouprole.csv';
//        $csvRetailOutletImagesPath = $directory . '/retailoutletimages.csv';
//        $csvRolePath = $directory . '/role.csv';
//        $mysqlData = array();
//
//        if (File::exists($csvbrandCategoryPath) == false &&
//            File::exists($csvCatalogLevelPath) == false &&
//            File::exists($csvGroupRolePath) == false &&
//            File::exists($csvRetailOutletImagesPath) == false &&
//            File::exists($csvRolePath) == false
//        ) {
//            $mysqlData[$csvbrandCategoryPath] = sprintf('SELECT * FROM brand_category');
//            $mysqlData[$csvCatalogLevelPath] = sprintf('SELECT * FROM catalog_level ');
//            $mysqlData[$csvGroupRolePath] = sprintf('SELECT * FROM group_role ');
//            $mysqlData[$csvRetailOutletImagesPath] = sprintf('SELECT * FROM retailoutlet_images ');
//            $mysqlData[$csvRolePath] = sprintf('SELECT * FROM role');
//
//
//        }
//
//        $dbHost = env('DB_HOST');
//
//        foreach ($mysqlData as $key => $mysql) {
//            // $command = "mysql -h ".env('DB_HOST')." -u ".env('DB_USERNAME')." -p'".env('DB_PASSWORD')."' ".env('DB_DATABASE')." -e '".$mysql."' | ".env('TR')." '\t' ',' > ".$key;
//            $command = "mysql -h " . $dbHost . " -u " . env('DB2_USERNAME') . " -p'" . env('DB2_PASSWORD') . "' " . env('DB2_DATABASE') . " -e '" . $mysql . "' | " . env('TR') . " '\t' ',' > " . $key;
//              exec($command);
//        }
//
//        $loadPath = base_path() . '/load';
//        if (File::isDirectory($loadPath) == false) {
//            File::makeDirectory($loadPath, $mode = 0777, true, true);
//        }
//
//        $brandCategoryLoadPath = $loadPath . '/brandCategory.load';
//
//        $brandCategoryContent = sprintf(' LOAD CSV  FROM \'' . $csvbrandCategoryPath . '\' (brand_id,category_id) INTO postgresql://' .env('DB_USERNAME').':'.env('DB_PASSWORD').'@'.env('DB_HOST').':5432/'.env('DB_DATABASE') . '?brand_category (brand_id,category_id)  WITH truncate,skip header = 1,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists brand_category; $$,$$ create table brand_category (brand_id int,category_id int);$$; ');
//        File::put($brandCategoryLoadPath, $brandCategoryContent);
//        $brandCategoryCommand = sprintf('pgloader ' . $brandCategoryLoadPath);
//        exec($brandCategoryCommand);
//
//        $catalogLevelLoadPath = $loadPath . '/cataloglevel.load';
//
//        $catalogLevelContent = sprintf('LOAD CSV  FROM \'' . $csvCatalogLevelPath . '\' (id,created_by,updated_by,level,name,created_on,updated_on) INTO postgresql://'.env('DB_USERNAME').':'.env('DB_PASSWORD').'@'.env('DB_HOST').':5432/'. '?catalog_level (id,created_by,updated_by,level,name,created_on,updated_on) WITH truncate,skip header = 1,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists catalog_level; $$,$$ create table catalog_level (id BIGSERIAL PRIMARY KEY,created_by int,updated_by int,level varchar(100),name varchar(100),created_on timestamp,updated_on timestamp);$$;');
//        File::put($catalogLevelLoadPath, $catalogLevelContent);
//        exec('pgloader ' . $catalogLevelLoadPath);
//
//
//        $groupRoleLoadPath = $loadPath . '/grouprole.load';
//
//        $groupRoleContent = sprintf('LOAD CSV  FROM \'' . $csvGroupRolePath . '\' (group_id,role_id) INTO postgresql://' .env('DB_USERNAME').':'.env('DB_PASSWORD').'@'.env('DB_HOST').':5432/'. '?group_role (group_id,role_id) WITH truncate,skip header = 1,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists group_role; $$,$$ create table group_role (group_id int,role_id int);$$;');
//        File::put($groupRoleLoadPath, $groupRoleContent);
//        exec('pgloader ' . $groupRoleLoadPath);
//
//        $retailoutletImagesPath = $loadPath . '/retailoutletimages.load';
//
//        $retailoutletImagesContent = sprintf('LOAD CSV  FROM \'' . $csvRetailOutletImagesPath . '\' (id,retailoutlet_id,created_by,updated_by,name,image_category,created_on,updated_on) INTO postgresql://'.env('DB_USERNAME').':'.env('DB_PASSWORD').'@'.env('DB_HOST').':5432/'. '?retailoutlet_images (id,retailoutlet_id,created_by,updated_by,name,image_category,created_on,updated_on) WITH truncate,skip header = 1,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retailoutlet_images; $$,$$ create table retailoutlet_images (id BIGSERIAL PRIMARY KEY,retailoutlet_id int,created_by int,updated_by int,name varchar(255),image_category varchar(255),created_on timestamp,updated_on timestamp);$$; ');
//        File::put($retailoutletImagesPath, $retailoutletImagesContent);
//        exec('pgloader ' . $retailoutletImagesPath);
//
//        $roleLoadPath = $loadPath . '/role.load';
//
//        $roleContent = sprintf('LOAD CSV  FROM \'' . $csvRolePath . '\' (id,parent,created_by,updated_by,name,created_on,updated_on) INTO postgresql://' .env('DB_USERNAME').':'.env('DB_PASSWORD').'@'.env('DB_HOST').':5432/'.'?role (id,parent,created_by,updated_by,name,created_on,updated_on) WITH truncate,skip header = 1,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists role; $$,$$ create table role (id BIGSERIAL PRIMARY KEY,parent int,created_by int,updated_by int,name varchar(100),created_on timestamp,updated_on timestamp);$$;');
//        File::put($roleLoadPath, $roleContent);
//        exec('pgloader ' . $roleLoadPath);


//        DB::statement("DROP TABLE users");
//        DB::statement("CREATE TABLE logs (id BIGSERIAL PRIMARY KEY,user_id INTEGER ,data text NOT NULL,created_at TIMESTAMP ,updated_at TIMESTAMP,module VARCHAR (255))");
//        DB::statement("CREATE TABLE permissions (id BIGSERIAL PRIMARY KEY,slug text NOT NULL,name VARCHAR (255),created_at TIMESTAMP ,updated_at TIMESTAMP,module VARCHAR (255))");
//        DB::statement("CREATE TABLE roles (id BIGSERIAL PRIMARY KEY, name varchar(100) NOT NULL)");
        DB::statement("ALTER TABLE retail_user RENAME TO users;");
        DB::statement("ALTER TABLE users RENAME group_id TO role_id");
        DB::statement("ALTER TABLE users ALTER COLUMN password  TYPE VARCHAR(255)");
        DB::statement("ALTER TABLE users ADD COLUMN updated_at TIMESTAMP ;");
        DB::statement("CREATE TABLE role_user (user_id integer, role_id integer)");
        DB::statement("ALTER TABLE skuinventory ADD COLUMN created_by integer,ADD COLUMN updated_by integer, ADD COLUMN created_on TIMESTAMP, ADD COLUMN updated_on TIMESTAMP");
        DB::statement("ALTER TABLE catalog ADD COLUMN catalog_level_id integer,ADD COLUMN created_by integer,ADD COLUMN updated_by integer, ADD COLUMN created_on TIMESTAMP, ADD COLUMN updated_on TIMESTAMP");
        DB::statement("ALTER TABLE retail_outlet_categories ADD COLUMN created_by integer,ADD COLUMN updated_by integer, ADD COLUMN created_on TIMESTAMP, ADD COLUMN updated_on TIMESTAMP, ADD COLUMN map_color varchar(255)");
        DB::statement("ALTER TABLE retail_outlet_channel ADD COLUMN created_by integer,ADD COLUMN updated_by integer, ADD COLUMN created_on TIMESTAMP, ADD COLUMN updated_on TIMESTAMP");
        DB::statement("ALTER TABLE street ADD COLUMN created_by integer,ADD COLUMN updated_by integer, ADD COLUMN created_on TIMESTAMP, ADD COLUMN updated_on TIMESTAMP");


        $psqlStreets = Street::get();
        $streetName = array_pluck($psqlStreets, 'name');
        $datas = MysqlStreet::whereIn('name', $streetName)->get();

        foreach ($datas as $streetData) {
            Street::where('name', $streetData->name)->update(
                array('created_by' => $streetData->created_by,
                    'updated_by' => $streetData->updated_by,
                    'created_on' => $streetData->created_on,
                    'updated_on' => $streetData->updated_on));
        }

        $psqlCatalogs = Catalog::get();
        $streetName = array_pluck($psqlCatalogs, 'name');
        $datas = MysqlCatalog::whereIn('title', $streetName)->get();

        foreach ($datas as $catalogData) {
            Catalog::where('name', $catalogData->title)->update(
                array('created_by' => $catalogData->created_by,
                    'updated_by' => $catalogData->updated_by,
                    'created_on' => $catalogData->created_on,
                    'updated_on' => $catalogData->updated_on,
                    'catalog_level_id' => $catalogData->catalog_level_id));
        }

        $psqlSkuinventory = Skuinventory::get();
        $skuName = array_pluck($psqlSkuinventory, 'name');
        $resultData = str_replace("'", "''", $skuName);
        $implodeSkuDatas = "'" . implode("','", $resultData) . "'";
        $datas = DB::connection('mysql')->select("Select * from SkuInventory where title IN ($implodeSkuDatas)");


        foreach ($datas as $skuData) {
            Skuinventory::where('name', $skuData->title)->update(
                array('created_by' => $skuData->created_by,
                    'updated_by' => $skuData->updated_by,
                    'created_on' => $skuData->created_on,
                    'updated_on' => $skuData->updated_on));
        }

        $psqlChannels = Channel::get();
        $channelName = array_pluck($psqlChannels, 'name');
        $resultData = str_replace("'", "''", $channelName);
        $implodeChannelDatas = "'" . implode("','", $resultData) . "'";
        $datas = DB::connection('mysql')->select("Select * from channel where name IN ($implodeChannelDatas)");

        foreach ($datas as $channelData) {
            Channel::where('name', $channelData->name)->update(
                array('created_by' => $channelData->created_by,
                    'updated_by' => $channelData->updated_by,
                    'created_on' => $channelData->created_on,
                    'updated_on' => $channelData->updated_on));
        }

        $psqlCategories = Categories::get();
        $categoriesName = array_pluck($psqlCategories, 'name');
        $resultData = str_replace("'", "''", $categoriesName);
        $implodeCategoryDatas = "'" . implode("','", $resultData) . "'";
        $datas = DB::connection('mysql')->select("Select * from category where name IN ($implodeCategoryDatas)");

        foreach ($datas as $categoryData) {
            Categories::where('name', $categoryData->name)->update(
                array('created_by' => $categoryData->created_by,
                    'updated_by' => $categoryData->updated_by,
                    'created_on' => $categoryData->created_on,
                    'updated_on' => $categoryData->updated_on,
                    'map_color' => $categoryData->map_color));
        }


    }

}
