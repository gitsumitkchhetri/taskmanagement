<?php

namespace App\Console\Commands;

use App\Model\Api\Town;
use App\Street;
use Illuminate\Console\Command;
use File;
use DB;
use Illuminate\Support\Facades\Schema;

class ImportPostgresDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:postgresdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Postgres database from Mysql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = base_path() . '/sql/nepal.sql';

        if (File::exists($path) == false) {
            $this->info("File Does Not Exist!! ");
        } else {
            $db = env('DB_DATABASE2');

            if (Schema::hasTable('development_regions') == false
                && Schema::hasTable('distrticts') == false
                && Schema::hasTable('town') == false
                && Schema::hasTable('zones') == false
            ) {
                $dbHost = env('DB_HOST2');
                $username = env('DB_USERNAME2');
               // $command = sprintf('psql -h'.$dbHost.'-d ' . $db . '-U '.$username.'-f\'< ' . $path);
                $command = sprintf('psql ' . $db . '< ' . $path);
                exec($command);
            }

            $directory = base_path() . '/csv';

            if (File::isDirectory($directory) == false) {
                File::makeDirectory($directory, $mode = 0777, true, true);
            }
            $csvStreetPath = $directory . '/street.csv';
            $csvChannelPath = $directory . '/channel.csv';
            $csvCategoryPath = $directory . '/category.csv';
            $csvRetailOutletPath = $directory . '/retailoutlet.csv';
            $csvCatalogPath = $directory . '/catalog.csv';
            $csvSkuInventory = $directory . '/skuinventory.csv';
            $csvRetailShopSkus = $directory . '/retailshopsku.csv';
            $csvRetailOutletStreet = $directory . '/retailoutletstreet.csv';
            $csvRetailShopBrand = $directory . '/retailoutletshop.csv';
            $csvUserGroup = $directory . '/retailusergroup.csv';
            $csvUser = $directory . '/retailuser.csv';


            if (File::exists($csvStreetPath) == false &&
                File::exists($csvChannelPath) == false &&
                File::exists($csvCategoryPath) == false &&
                File::exists($csvRetailOutletPath) == false &&
                File::exists($csvCatalogPath) == false &&
                File::exists($csvSkuInventory) == false &&
                File::exists($csvRetailShopSkus) == false &&
                File::exists($csvRetailShopBrand) == false &&
                File::exists($csvRetailOutletStreet) == false &&
                File::exists($csvUserGroup) == false &&
               File::exists($csvUser) == false
            ) {
                DB::connection('mysql')->select('SELECT id,town_id,created_by,updated_by,name,api_way_id,created_on,updated_on FROM street where town_id IS NOT NULL INTO OUTFILE \'' . $csvStreetPath . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT id,name FROM channel INTO OUTFILE \'' . $csvChannelPath . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT id,channel_id,name FROM category INTO OUTFILE \'' . $csvCategoryPath . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT id,category_id,created_by,updated_by,created_on,updated_on,name,address,owner_name,owner_phone,pan_no,phone,longitude,latitude,store_size,monthly_avg_business,verfied_by,v_longitude,v_latitude,is_verified,hotspot,verified_on FROM retail_outlet INTO OUTFILE\'' . $csvRetailOutletPath . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT id,IF(parent IS NULL or parent = \'\', 0, parent) as parent ,title FROM catalog INTO OUTFILE \'' . $csvCatalogPath . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT id,catalog_id ,title FROM Skuinventory INTO OUTFILE \'' . $csvSkuInventory . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT retailoutlet_id,sku_id FROM retailshop_sku INTO OUTFILE \'' . $csvRetailShopSkus . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT street_id,retailoutlet_id FROM retailoutlet_street INTO OUTFILE \'' . $csvRetailOutletStreet . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT retail_outlet_id,catalog_id FROM retailshop_brand INTO OUTFILE \'' . $csvRetailShopBrand . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT id,name FROM user_group INTO OUTFILE \'' . $csvUserGroup . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
                DB::connection('mysql')->select('SELECT id,group_id,created_by,updated_by,username,first_name,last_name,email,password,api_token,remember_token,phone_no,pin_no,created_on,updated_on,is_deleted,imei_no,image FROM user INTO OUTFILE \'' . $csvUser . '\' FIELDS TERMINATED BY \',\'ENCLOSED BY \'"\'LINES TERMINATED BY \'\n\'');
            }

            if (Schema::hasTable('street') == false
                && Schema::hasTable('retail_outlet') == false
                && Schema::hasTable('retail_outlet_channel') == false
                && Schema::hasTable('retail_outlet_categories') == false
                && Schema::hasTable('catalog') == false
                && Schema::hasTable('skuinventory') == false
                && Schema::hasTable('retailoutlet_skus') == false
                && Schema::hasTable('retailshop_skus') == false
                && Schema::hasTable('retailoutlet_street') == false
                && Schema::hasTable('retailshop_brand') == false
                && Schema::hasTable('user_group') == false
                && Schema::hasTable('user') == false


            ) {
                $loadPath = base_path() . '/load';
                if (File::isDirectory($loadPath) == false) {
                    File::makeDirectory($loadPath, $mode = 0777, true, true);
                }

                $streetLoadPath = $loadPath . '/street.load';

                $streetContent = sprintf(' LOAD CSV  FROM \'' . $csvStreetPath . '\' (id,town_id,created_by,updated_by,name,api_way_id,created_on,updated_on) INTO postgresql:///'.env('DB_DATABASE2').'?street (id,town_id,name)  WITH truncate,skip header = 1,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists street; $$,$$ create table street (id BIGSERIAL PRIMARY KEY,town_id int,name varchar(255) NOT NULL);$$; ');
                File::put($streetLoadPath, $streetContent);
                $streetCommand = sprintf('pgloader ' . $streetLoadPath);
                exec($streetCommand);

                $channelLoadPath = $loadPath . '/channel.load';

                $channelContent = sprintf('LOAD CSV  FROM \'' . $csvChannelPath .'\' (id,name) INTO postgresql:///'.env('DB_DATABASE2').'?retail_outlet_channel (id,name)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retail_outlet_channel; $$,$$ create table retail_outlet_channel (id BIGSERIAL PRIMARY KEY,name varchar(100));$$;');
                File::put($channelLoadPath, $channelContent);
                exec('pgloader ' . $channelLoadPath);

                $retailOutletStreetLoadPath = $loadPath . '/retailOutletStreet.load';

                $retailOutletStreetContent = sprintf('LOAD CSV  FROM \'' . $csvRetailOutletStreet .'\' (street_id,retailoutlet_id) INTO postgresql:///'.env('DB_DATABASE2').'?retailoutlet_street (street_id,retailoutlet_id)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retailoutlet_street; $$,$$ create table retailoutlet_street (street_id int,retailoutlet_id int);$$;');
                File::put($retailOutletStreetLoadPath, $retailOutletStreetContent);
                exec('pgloader ' . $retailOutletStreetLoadPath);

                $categoryLoadPath = $loadPath . '/category.load';

                $categoryContent = sprintf('LOAD CSV  FROM \'' . $csvCategoryPath .'\' (id,channel_id,name) INTO postgresql:///'.env('DB_DATABASE2').'?retail_outlet_categories (id,channel_id,name)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retail_outlet_categories; $$,$$ create table retail_outlet_categories (id BIGSERIAL PRIMARY KEY,channel_id int,name varchar(100));$$; ');
                File::put($categoryLoadPath, $categoryContent);
                exec('pgloader ' . $categoryLoadPath);

                $catalogLoadPath = $loadPath . '/catalog.load';

                $catalogContent = sprintf('LOAD CSV  FROM \'' . $csvCatalogPath .'\' (id,parent,name) INTO postgresql:///'.env('DB_DATABASE2').'?catalog (id,parent,name)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists catalog; $$,$$ create table catalog (id BIGSERIAL PRIMARY KEY,parent int,name varchar(100));$$;');
                File::put($catalogLoadPath, $catalogContent);
                exec('pgloader ' . $catalogLoadPath);

                $skuInventoryLoadPath = $loadPath . '/skuinventory.load';

                $skuInventoryContent = sprintf('LOAD CSV  FROM \'' . $csvSkuInventory .'\' (id,catalog_id,name) INTO postgresql:///'.env('DB_DATABASE2').'?skuinventory (id,catalog_id,name)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists skuinventory; $$,$$ create table skuinventory (id BIGSERIAL PRIMARY KEY,catalog_id int,name varchar(100));$$; ');
                File::put($skuInventoryLoadPath, $skuInventoryContent);
                exec('pgloader ' . $skuInventoryLoadPath);

                $userGroupLoadPath = $loadPath . '/usergroup.load';

                $userGroupContent = sprintf('LOAD CSV  FROM \'' . $csvUserGroup .'\' (id,name) INTO postgresql:///'.env('DB_DATABASE2').'?user_group (id,name)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists user_group; $$,$$ create table user_group (id BIGSERIAL PRIMARY KEY,name varchar(100));$$; ');
                File::put($userGroupLoadPath, $userGroupContent);
                exec('pgloader ' . $userGroupLoadPath);

                $userLoadPath = $loadPath . '/user.load';

                $userContent = sprintf('LOAD CSV  FROM \'' . $csvUser .'\' (id, group_id,created_by, updated_by,username,first_name,last_name,email,password,api_token,remember_token,phone_no,pin_no,created_on,updated_on,is_deleted,imei_no,image) INTO postgresql:///'.env('DB_DATABASE2').'?retail_user (id,group_id,created_by,updated_by,username,first_name,last_name,email,password,api_token,remember_token,phone_no,pin_no,created_on,updated_on,is_deleted,imei_no,image)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retail_user; $$,$$ create table retail_user (id integer,group_id integer,created_by integer,updated_by integer,username varchar(64),first_name varchar(64),last_name varchar(64),email varchar(100),password varchar(42),api_token varchar(100),remember_token varchar(100),phone_no varchar(100),pin_no varchar(42),created_on timestamp,updated_on timestamp,is_deleted boolean,imei_no varchar(42),image text);$$; ');
                File::put($userLoadPath, $userContent);
                exec('pgloader ' . $userLoadPath);

                $retailOutletLoadPath = $loadPath . '/retailoutlet.load';

                $retailOutletContent = sprintf('LOAD CSV  FROM  \'' . $csvRetailOutletPath .'\' (id,category_id,created_by,updated_by,created_on,updated_on,name,address,owner_name,owner_phone,pan_no,phone,longitude,latitude,store_size,monthly_avg_business,verfied_by,v_longitude,v_latitude,is_verified,hotspot,verified_on) INTO postgresql:///'.env('DB_DATABASE2').'?retail_outlet (id,category_id,created_by,updated_by,created_on,updated_on,name,address,owner_name,owner_phone,pan_no,phone,longitude,latitude,store_size,monthly_avg_business,verfied_by,v_longitude,v_latitude,is_verified,hotspot,verified_on)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retail_outlet; $$,$$ create table retail_outlet (id BIGSERIAL PRIMARY KEY,category_id int,name varchar(100),created_by integer,updated_by integer,created_on timestamp,updated_on timestamp,address varchar(100),owner_name varchar(100),owner_phone varchar(100),pan_no varchar(100),phone varchar(100),longitude float,latitude float,store_size varchar(100),monthly_avg_business varchar(255),verfied_by integer,v_longitude varchar(100),v_latitude varchar(100),is_verified boolean,hotspot boolean,verified_on timestamp);$$;');
                File::put($retailOutletLoadPath, $retailOutletContent);
                exec('pgloader ' . $retailOutletLoadPath);

                $retailShopBrandLoadPath = $loadPath . '/retailshopbrand.load';

                $retailShopBrandContent = sprintf('LOAD CSV  FROM  \'' . $csvRetailShopBrand .'\' (retail_outlet_id,catalog_id) INTO postgresql:///'.env('DB_DATABASE2').'?retailshop_brand (retail_outlet_id,catalog_id)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retailshop_brand; $$,$$ create table retailshop_brand (retail_outlet_id int,catalog_id int);$$;');
                File::put($retailShopBrandLoadPath, $retailShopBrandContent);
                exec('pgloader ' . $retailShopBrandLoadPath);


                $retailShopSkusLoadPath = $loadPath . '/retailshopskus.load';

                $retailShopSkusContent = sprintf(' LOAD CSV  FROM \'' . $csvRetailShopSkus .'\' (retailoutlet_id,sku_id) INTO postgresql:///'.env('DB_DATABASE2').'?retailshop_skus (retailoutlet_id,sku_id)  WITH truncate,fields optionally enclosed by \'"\',fields escaped by double-quote,fields terminated by \',\'SET client_encoding to \'utf8\',work_mem to \'12MB\',standard_conforming_strings to \'on\'BEFORE LOAD DO$$ drop table if exists retailshop_skus; $$,$$ create table retailshop_skus (retailoutlet_id int,sku_id int);$$; ');
                File::put($retailShopSkusLoadPath, $retailShopSkusContent);
                exec('pgloader ' . $retailShopSkusLoadPath);

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN channel_id integer;");
                DB::statement("UPDATE retail_outlet SET channel_id = retail_outlet_categories.channel_id FROM retail_outlet_categories WHERE retail_outlet_categories.id = retail_outlet.category_id");

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN street_id integer;");
                DB::statement("UPDATE retail_outlet SET street_id = retailoutlet_street.street_id FROM retailoutlet_street WHERE retailoutlet_street.retailoutlet_id = retail_outlet.id");

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN town_id integer;");
                DB::statement("UPDATE retail_outlet SET town_id = street.town_id FROM street WHERE street.id = retail_outlet.street_id");

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN district_id integer;");
                DB::statement("UPDATE retail_outlet SET district_id = town.district_id FROM town WHERE town.id::integer = retail_outlet.town_id::integer");

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN zone_id integer;");
                DB::statement("UPDATE retail_outlet SET zone_id = districts.zone_id FROM districts WHERE districts.gid::integer = retail_outlet.district_id::integer");

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN development_region_id integer;");
                DB::statement("UPDATE retail_outlet SET development_region_id = zones.development_region_id FROM zones WHERE zones.gid::integer = retail_outlet.zone_id::integer");

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN catalog_id integer;");
                DB::statement("UPDATE retail_outlet SET catalog_id = retailshop_brand.catalog_id FROM retailshop_brand WHERE retailshop_brand.retail_outlet_id = retail_outlet.id");


                DB::statement("ALTER TABLE retail_outlet ADD COLUMN geom geometry;");
                DB::statement("UPDATE retail_outlet SET geom = ST_SetSRID(ST_MakePoint(longitude,latitude),4326) ");


                DB::statement("ALTER TABLE skuinventory ADD COLUMN retail_outlet_id integer;");
                DB::statement("UPDATE skuinventory SET retail_outlet_id = retailshop_brand.retail_outlet_id FROM retailshop_brand WHERE retailshop_brand.catalog_id = skuinventory.catalog_id");

                DB::statement("ALTER TABLE retail_outlet ADD COLUMN sku_id integer;");
                DB::statement("UPDATE retail_outlet SET sku_id = retailshop_skus.sku_id FROM retailshop_skus WHERE retailshop_skus.retailoutlet_id = retail_outlet.id");


            }


        }
        $townName = [];
        $psqlTownName = [];
        $towns = DB::connection('mysql')->select("Select name from town");
        foreach ($towns as $town)$townName [] = $town->name;
        $psqlTowns = Town::select('name')->get();
        foreach ($psqlTowns as $psqlTown)$psqlTownName [] = $psqlTown->name;
        $townDatas = array_diff($townName, $psqlTownName);
        $implodeTownDatas = "'" . implode("','", $townDatas) . "'";
        $datas = DB::connection('mysql')->select("Select * from town where name IN ($implodeTownDatas)");

       if (empty($townDatas) == false) {

           foreach ($datas as $townData) {
               Town::insert(
                   [
                       'id'=>$townData->id,
                       'zone_id'=>$townData->zone_id,
                       'created_by'=>$townData->created_by,
                       'updated_by'=>$townData->updated_by,
                       'name'=>$townData->name,
                       'created_on'=>$townData->created_on,
                       'updated_on'=>$townData->updated_on,
               ]
               );
           }
           $this->info("New Town has been Inserted sucessfully!! ");
       }


    }
}
