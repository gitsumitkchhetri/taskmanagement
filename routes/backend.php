<?php
//Backend



Route::group(['prefix' => 'system','middleware'=>['bindings','auth','role','log','permission:'.basename(request()->path())]], function () {
    Route::get('log/pages/Exception-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
  Route::group(['namespace' => 'Admin' ], function () {

      Route::get('/home','home\homeController@index');
      Route::resource('log', 'log\logController');
      Route::resource('user', 'user\usersController');
      Route::resource('role', 'user\rolesController');
      Route::get('LogViewer','log\LogViewerController@index');
      Route::get('password/updatepassword','password\passwordController@index');
      Route::get('/user/password/{id}','user\usersController@password');
      Route::post('/user/update_password/{id}','user\usersController@updatepassword');
      Route::get('/user/change_password/{id}','password\passwordController@change_password');
      Route::post('/user/change_password/{id}','password\passwordController@updatepassword');
      Route::post('/change_language','config\settingsController@store');
      Route::resource('leads', 'leads\LeadsController');
      Route::resource('clients', 'clients\ClientsController');
      Route::resource('addresses', 'addresses\AddressesController');
      Route::resource('tech_levels', 'configs\TechLevelsController');
      Route::resource('project_phases', 'configs\ProjectPhasesController');
      Route::resource('project_tasks', 'project_tasks\ProjectTasksController');
//      Route::group(['middleware' => 'checkAuthorizedDealsBroker' ], function () {
          Route::resource('deals', 'deals\DealsController');
          Route::resource('proposals', 'proposals\ProposalsController');
          Route::resource('projects', 'projects\ProjectsController');
          Route::resource('project_equipments', 'project_equipments\ProjectEquipmentsController');
          Route::resource('project_technicians', 'project_technicians\ProjectTechniciansController');
//      });
      Route::resource('technicians', 'technicians\TechniciansController');
      Route::resource('dispatch', 'dispatch\DispatchController');
      Route::resource('timesheet', 'timesheet\TimesheetController');
      Route::post('/timesheet/save', 'timesheet\TimesheetController@UpdateTimeSheet');

      Route::resource('phases', 'phases\PhasesController');
      Route::resource('ajax_handler', 'ajax_handler\AjaxHandlerController');
      //Route::get('ajax_handler', 'ajax_handler\AjaxHandlerController');
//      Route::get('projects/phases', function (){
//          dd("sdfwrwe");
//      });


//      Route::get('{module}/pages/{page}', Request::segment(4) . 'Controller@index')->middleware('permission:'.Request::segment(4));
//
//      Route::get('{module}', Request::segment(2) . 'Controller@index')->middleware('permission:'.Request::segment(2));
//
//      Route::get('{module}/{page}', Request::segment(2) . 'Controller@' . Request::segment(3))->middleware('permission:'.Request::segment(2));
//
//      Route::post('{module}/{page}', Request::segment(2) . 'Controller@' . Request::segment(3))->middleware('permission:'.Request::segment(2));
//
//      Route::get('{module}/{page}/{id}', Request::segment(2) . 'Controller@' . Request::segment(3))->middleware('permission:'.Request::segment(2));
//
//      Route::get('{module}/pages/{page}/{id}', Request::segment(4) . 'Controller@' . Request::segment(5))->middleware('permission:'.Request::segment(4));
//
//      Route::post('{module}/pages/{page}/{id}', Request::segment(4) . 'Controller@' . Request::segment(5))->middleware('permission:'.Request::segment(4));
//
//      Route::get('{module}/pages/{page}/{abc}/{id}', Request::segment(4) . 'Controller@' . Request::segment(5))->middleware('permission:'.Request::segment(4));
//
//      Route::post('{module}/pages/{page}/{abc}/{id}', Request::segment(4) . 'Controller@' . Request::segment(5))->middleware('permission:'.Request::segment(4));


  });

});
