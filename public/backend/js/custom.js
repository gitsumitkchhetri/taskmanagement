// $('#maincheckbox').click(function() {
//     // if ($(this).is(':checked')) {
//     //     $('div input').attr('checked', true);
//     // } else {
//     //     $('div input').attr('checked', false);
//     // }
//     $(this.form.elements).filter(':checkbox').prop('checked', this.checked);
// });

(function($){
    var container = null;
    var toast = function(message, options){
        // default properties
        options = options || {};
        options.width = options.width || 0;
        options.duration = options.duration || 2000;
        options.type = options.type || '';
        options.align = options.align || 'bottom';
        if(!options.hasOwnProperty('singleton')) {
            options.singleton = true;
        }

        // init toast container
        if(!container){
            container = $('<ul></ul>').addClass('toast').appendTo(document.body).hide();
        }

        switch(options.align) {
            case 'top':
                container.css({'top': '0', 'bottom': '', 'margin': '40px 0 0 0'});
                break;
            case 'bottom':
                container.css({'top': '', 'bottom': '0', 'margin': '0 0 40px 0'});
                break;
        }

        if(options.singleton) {
            container.html('');
        }

        // append message to container
        var span = "<span>" + message + "</span>";
        var item = $('<li></li>').hide();
        if(options.width > 0) {
            span = "<span style='width: " + options.width + "px'>" + message + "</span>";
        }
        if(options.align == 'top') {
            item.html(span).prependTo(container);
        } else {
            item.html(span).appendTo(container)
        }

        // set custom class
        if(options.type !== '') item.addClass(options.type);
        // setup timeout
        var timeout = null;
        timeout = setTimeout(function(){
            clearTimeout(timeout);
            item.animate({ height: 0, opacity: 0}, 'fast', function(){
                item.remove();
                container.children().length || container.hide();
            });
        }, options.duration);

        // show toast
        container.show();
        item.fadeIn('normal');
    };
    $.extend({ toast: toast });
})(jQuery);






jQuery.fn.sortElements = (function(){

    var sort = [].sort;

    return function(comparator, getSortable) {

        getSortable = getSortable || function(){return this;};

        var placements = this.map(function(){

            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,

                // Since the element itself will change position, we have
                // to have some way of storing it's original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );

            return function() {

                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }

                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);

            };

        });

        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });

    };

})();



var table1 = $('#skutable');

$('#skuheader, #brandheader,#createdheader')
    .wrapInner('<span title="sort this column"/>')
    .each(function(){

        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){

            table1.find('td').filter(function(){

                return $(this).index() === thIndex;

            }).sortElements(function(a, b){

                return $.text([a]) > $.text([b]) ?
                    inverse ? -1 : 1
                    : inverse ? 1 : -1;

            }, function(){

                // parentNode is the element we want to move
                return this.parentNode;

            });

            inverse = !inverse;

        });

    });

var tt1 = 0;
$("#tt1").on('click',function(){
    tt1 = tt1+1;
    if(tt1%2 == 0)
    {
        $("#tt1").html("<i class='fa fa-sort-desc' aria-hidden='true'></i>");
    }else{
        $("#tt1").html("<i class='fa fa-sort-asc' aria-hidden='true'></i>");
    }

});

var tt2 = 0;
$("#tt2").on('click',function(){
    tt2 = tt2+1;
    if(tt2%2 == 0)
    {
        $("#tt2").html("<i class='fa fa-sort-desc' aria-hidden='true'></i>");
    }else{
        $("#tt2").html("<i class='fa fa-sort-asc' aria-hidden='true'></i>");
    }

});
//
// $('#submitDeleteForm').click(function(){
//     //console.log('hehaha');
//     $('.deleteConfirmation')[0].submit();
// });
//
// function confirmationBox()
// {
//
//     $('#confirm-submit').modal('show');
//     return false;
// }

// $(document).ready(function () {
//     // Select the submit buttons of forms with data-confirm attribute
//     var submit_buttons = $("form[data-confirm] input[type='submit']");
//
//     // On click of one of these submit buttons
//     submit_buttons.on('click', function (e) {
//
//         // Prevent the form to be submitted
//         e.preventDefault();
//
//         var button = $(this); // Get the button
//         var form = button.closest('form'); // Get the related form
//         var msg = form.data('confirm'); // Get the confirm message
//
//         if(confirm(msg)) form.submit(); // If the user confirm, submit the form
//
//     });
//
// });

// $(document).ready(function() {
//     $("form[data-confirm]").click(function() {
//         //var href = $(this).attr('href');
//         if (!$('#dataConfirmModal').length) {
//             $('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
//         }
//         $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
//         $('#dataConfirmOK').on('click', function (e) {
//             //console.log('hello');
//
//
//             var button = $(this); // Get the button
//             console.log(button);
//             var form = button.closest('form'); // Get the related form
//             console.log(form);
//             // var msg = form.data('confirm'); // Get the confirm message
//
//             form.submit(); // If the ,user confirm submit the form
//         })
//
//
//         $('#dataConfirmModal').modal({show:true});
//         return false;
//     });
// });


// $('.confirm').on('click', function (e) {
//     if (confirm($(this).data('confirm'))) {
//         return true;
//     }
//     else {
//         return false;
//     }
// });

// $(document).ready(function () {
//     // Select the submit buttons of forms with data-confirm attribute
//     var submit_buttons = $("form[data-confirm] input[type='submit']");
//
//     // On click of one of these submit buttons
//     submit_buttons.on('click', function (e) {
//
//         // Prevent the form to be submitted
//         e.preventDefault();
//
//         var button = $(this); // Get the button
//         var form = button.closest('form'); // Get the related form
//         var msg = form.data('confirm'); // Get the confirm message
//
//         if(confirm(msg)) form.submit(); // If the user confirm, submit the form
//
//     });
//
// });

$('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
    e.preventDefault();
    var $form=$(this);
    $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .on('click', '#delete-btn', function(){
            $form.submit();
        });
});

jQuery('#bchange').change(function() {
    this.form.submit();
});