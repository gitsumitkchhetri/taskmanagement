# API DOCUMENTATION

Composer Install
Run php artisan migrate
Run php artisan passport:install

## Swagger Implementation

Run 
 - php artisan l5-swagger:publish (to publish everything)
 - php artisan l5-swagger:publish-config (to publish configs) (config/l5-swagger.php)
 - php artisan l5-swagger:publish-assets (to publish swagger-ui to your public folder (public/vendor/l5-swagger))
 - php artisan l5-swagger:publish-views (to publish views (resources/views/vendor/l5-swagger))
 - php artisan l5-swagger:generate (to generate docs or set L5_SWAGGER_GENERATE_ALWAYS param to true in your config or .env file given to .env.example)

#Import Postdata
Run 
 - pg_dump rmap < /Users/samina-mac-mini/ekbana/rosia/sql/rmap.sql

#Pgloader
 - brew install pgloader (For Mac)
 - apt-get install pgloader (For Linux)
Mysql
Create Database named rmap to mysql or anything you want to name. 
- mysql -u root -p rmap < /Users/samina-mac-mini/ekbana/rosia/sql/retailmapping.sql
OR Import sql from phpmyadmin
PostGres
Create Database named rmap or anything you want to name.
Run
 - php artisan import:postgresdb

## Dingo Implementation

copy from .env.example to .env to specify version of API


#CMS DOCUMENTATION

- Clone CMS from git@git.ekbana.info:ekcms/ekcms-ver4.git
- Run Composer Install
- Set Permission 777 to storage and bootstrap folder
- Run php artisan key:generate from terminal
- Setup .env with database and PREFIX
- Run php artisan migrate
- Run php artisan db:seed --class=TownTableSeeder
